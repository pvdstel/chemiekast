<div align="center">
    <img src="static/logo.png" width="128">
    <h1>Chemiekast</h1>
</div>

A chemical management solution.

## Running Chemiekast

Chemiekast is designed to run on Apache, with PHP. The `mod_rewrite` module is required for correct redirection. Other modules are optional, and if they are disabled, they will not be configured. Backend configuration options can be stored in two locations:

- A file named `chemiekast.ini`, which must exist in the directory above the directory in which the back-end files are stored.
- Environment variables, which can be set in `.htaccess`. This file cannot be accessed from outside, and can be created on shared hosts.

A MySQL or MariaDB database server is also required. The database account for the application should have the roles `ALTER`, `CREATE`, `DELETE`, `INSERT`, `SELECT`, `UPDATE`. More roles are not required and should not be granted to improve security. The database can be created by running the `/db/initialize-schema.sql` file. It will automatically create the schema and set up the tables, indexes and other constraints.

The front-end, which will be entirely rendered by the browser, is automatically served by the back-end, if the back-end and front-end exist on the same web service. They can also served from separate origins, in which case the back-end must be configured to allow connections from the front-end using CORS, and the front-end must be recompiled with the `CHEMIEKAST_HOST` webpack variable, so that it connects to the correct origin. Alternatively, the `index.html` file can be edited so that it points to the correct location.

Chemiekast is capable of sending out e-mails. These are sent when new user accounts are created or when users request a password reset. To do so, a valid e-mail configuration must be provided. In the Chemiekast system settings, correct values need to be provided in the _SMPT server_, _SMTP port_, _Email username_ and _Email password_ fields.

## Configuration options

Chemiekast can be configured in either an ini file, or with environment variables.

```
DB_CONNECTION_STRING="mysql connection string"
DB_USER=database user name
DB_PASSWORD=database user password
ALLOWED_ORIGINS=cors allowed origins
```

## Building

Required tooling to build Chemiekast:
- Node.js
  - Yarn
- PHP
  - Composer

1. Run `yarn` to install the required dependencies. This command will also execute a Composer command to install PHP dependencies.
2. Run `yarn build` to build the application. Run `yarn build:prod` for production builds. These commands will produce a `dist` folder in the root of the repository, whose contents can be placed on an Apache web server directly.
