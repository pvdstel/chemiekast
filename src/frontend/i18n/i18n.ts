import i18next from 'i18next';
import i18nLanguageDetector from 'i18next-browser-languagedetector';

const commonNamespace = 'common';
const languages = ['nl', 'en'];
const defaultLanguage = 'nl';

const resources: any = {};
languages.forEach(lng => {
    resources[lng] = {
        [commonNamespace]: require(`@/resources/i18n/${lng}.json`),
    };
});

const i18n = i18next;
i18n.use(i18nLanguageDetector)
    .init({
        defaultNS: commonNamespace,
        fallbackLng: defaultLanguage,
        ns: [commonNamespace],
        resources: resources,
    });

export default i18n;

const listeners: (() => void)[] = [];
export function listen(callback: () => void) {
    if (listeners.indexOf(callback) < 0) {
        listeners.push(callback);
    }

    return () => {
        const removeIndex = listeners.indexOf(callback);
        if (removeIndex >= 0) {
            listeners.splice(removeIndex, 1);
        }
    };
}

export async function changeLanguage(language: string) {
    i18n.changeLanguage(language, () => {
        listeners.forEach(l => l());
    });
}

export function getLanguages() {
    return languages;
}

/** Obtains the current i18n language, without the locale. */
export function geti18nLanguage() {
    return i18n.language.substr(0, 2);
}
