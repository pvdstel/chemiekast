import React from 'react';
import { ISession } from '@/api/session';

export interface ISessionContext {
    current: ISession | null;
    refresh: () => void;
    update: (next: ISession | null) => void;
}

const defaultSessionContext: ISessionContext = {
    current: null,
    refresh: () => { /* noop */ },
    update: (_next) => { /* noop */ },
};

const SessionContext = React.createContext<ISessionContext>(defaultSessionContext);
export default SessionContext;
