import { switchDomain } from '@/api/domain';
import { animationDuration } from '@/components/FullscreenLoader/FullscreenLoader';
import i18n from '@/i18n/i18n';
import { handleUnsuccessfulApiCall } from './api';
import { hideForegroundLoader, showForegroundLoader } from './loading';
import { broadcastNotification, createNotification, NotificationType } from './notifications';

export async function handleSwitchDomain(domainId: number, refreshSession: () => void) {
    showForegroundLoader();
    await new Promise(resolve => setTimeout(resolve, animationDuration));
    switchDomain(domainId, r => {
        if (r.isSuccessful()) {
            refreshSession();
        } else if (r.isUnsuccessful()) {
            handleUnsuccessfulApiCall(r);
        }
        setTimeout(() => {
            if (r.isSuccessful()) {
                broadcastNotification(createNotification(i18n.t('admin.domains.switched'), NotificationType.info, 5000));
            }
            hideForegroundLoader();
        }, 500);
    });
}
