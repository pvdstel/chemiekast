const foregroundLoaderListeners: ((count: number) => void)[] = [];
let foregroundLoaders = 0;

function notifyForegroundLoaderListeners() {
    foregroundLoaderListeners.forEach(l => l(foregroundLoaders));
}

/**
 * Notifies via a callback when the fullscreen loader state changes.
 * @param callback The callback to be invoked when the value changes.
 */
export function registerForegroundLoaderListener(callback: (count: number) => void): { unregister: () => void, value: number } {
    if (foregroundLoaderListeners.indexOf(callback) < 0) {
        foregroundLoaderListeners.push(callback);
    }

    return {
        unregister: () => {
            const removeIndex = foregroundLoaderListeners.indexOf(callback);
            if (removeIndex >= 0) {
                foregroundLoaderListeners.splice(removeIndex, 1);
            }
        },
        value: 0,
    };
}

/** Shows a foreground loader. */
export function showForegroundLoader() {
    ++foregroundLoaders;
    notifyForegroundLoaderListeners();
}

/** Hides a foreground loader. */
export function hideForegroundLoader() {
    --foregroundLoaders;
    notifyForegroundLoaderListeners();
}

const backgroundLoaderListeners: ((count: number) => void)[] = [];
let backgroundLoaders = 0;

function notifyBackgroundLoaderListeners() {
    backgroundLoaderListeners.forEach(l => l(backgroundLoaders));
}

/**
 * Notifies via a callback when the background loader state changes.
 * @param callback The callback to be invoked when the value changes.
 */
export function registerBackgroundLoaderListener(callback: (count: number) => void): { unregister: () => void, value: number } {
    if (backgroundLoaderListeners.indexOf(callback) < 0) {
        backgroundLoaderListeners.push(callback);
    }

    return {
        unregister: () => {
            const removeIndex = backgroundLoaderListeners.indexOf(callback);
            if (removeIndex >= 0) {
                backgroundLoaderListeners.splice(removeIndex, 1);
            }
        },
        value: 0,
    };
}

/** Shows a background loader. */
export function showBackgroundLoader() {
    ++backgroundLoaders;
    notifyBackgroundLoaderListeners();
}

/** Hides a background loader. */
export function hideBackgroundLoader() {
    --backgroundLoaders;
    notifyBackgroundLoaderListeners();
}
