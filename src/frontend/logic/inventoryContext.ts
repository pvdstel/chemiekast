import React from 'react';
import { IChemical } from '@/api/chemical';

export interface IInventory {
    sidebarOpen: boolean;
    sidebarPinned: boolean;
    spaceTable: boolean;
    hiddenColumns: string[];
    refreshHandler: () => void;
    activeChemical: number | undefined;
}

export interface IInventoryContext {
    current: IInventory;
    update: <K extends keyof IInventory>(next: Pick<IInventory, K>) => void;
    editChemical: (chemical: IChemical) => void;
    setWorksheetCode: (worksheet: string) => void;
}

const defaultInventoryContext: IInventoryContext = {
    current: {
        sidebarOpen: false,
        sidebarPinned: true,
        spaceTable: false,
        hiddenColumns: [],
        refreshHandler: () => { /* noop */ },
        activeChemical: undefined,
    },
    update: (_next) => { /* noop */ },
    editChemical: (_chemical) => { /* noop */ },
    setWorksheetCode: (_worksheet) => { /* noop */ },
};

const InventoryContext = React.createContext<IInventoryContext>(defaultInventoryContext);
export default InventoryContext;
