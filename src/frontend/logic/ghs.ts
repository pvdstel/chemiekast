import i18n from '@/i18n/i18n';

/**
 * All GHS symbols.
 */
export enum GhsSymbols {
    NonStandard = -1,
    None = 0, // is also not standard
    Explosive = 1,
    Flammable = 2,
    Oxidizing = 4,
    CompressedGas = 8,
    Corrosive = 16,
    Toxic = 32,
    Harmful = 64,
    HealthHazard = 128,
    EnvironmentalHazard = 256,
}

export type GhsSymbolClasses = 'ghs01' | 'ghs02' | 'ghs03' | 'ghs04' | 'ghs05'
    | 'ghs06' | 'ghs07' | 'ghs08' | 'ghs09' | 'empty' | 'none' | 'unknown';

/**
 * Gets GHS data.
 */
export function ghsData(): IGhsSymbol[] {
    return [
        new GhsSymbolImpl(GhsSymbols.Explosive, 'GHS01', i18n.t('inventory.ghs.ghs01'), undefined, 'ghs01'),
        new GhsSymbolImpl(GhsSymbols.Flammable, 'GHS02', i18n.t('inventory.ghs.ghs02'), undefined, 'ghs02'),
        new GhsSymbolImpl(GhsSymbols.Oxidizing, 'GHS03', i18n.t('inventory.ghs.ghs03'), undefined, 'ghs03'),
        new GhsSymbolImpl(GhsSymbols.CompressedGas, 'GHS04', i18n.t('inventory.ghs.ghs04'), undefined, 'ghs04'),
        new GhsSymbolImpl(GhsSymbols.Corrosive, 'GHS05', i18n.t('inventory.ghs.ghs05'), undefined, 'ghs05'),
        new GhsSymbolImpl(GhsSymbols.Toxic, 'GHS06', i18n.t('inventory.ghs.ghs06'), undefined, 'ghs06'),
        new GhsSymbolImpl(GhsSymbols.Harmful, 'GHS07', i18n.t('inventory.ghs.ghs07'), undefined, 'ghs07'),
        new GhsSymbolImpl(GhsSymbols.HealthHazard, 'GHS08', i18n.t('inventory.ghs.ghs08'), undefined, 'ghs08'),
        new GhsSymbolImpl(GhsSymbols.EnvironmentalHazard, 'GHS09', i18n.t('inventory.ghs.ghs09'), undefined, 'ghs09'),
    ];
}

/**
 * Gets the empty symbol.
 */
export function emptySymbol(): IGhsSymbol {
    return new GhsSymbolImpl(GhsSymbols.NonStandard, '[empty]', i18n.t('inventory.ghs.empty_name'), i18n.t('inventory.ghs.empty_description'), 'empty');
}

/**
 * Gets the safe icon.
 */
export function noneSymbol(): IGhsSymbol {
    return new GhsSymbolImpl(GhsSymbols.None, '[none]', i18n.t('inventory.ghs.none_name'), i18n.t('inventory.ghs.none_description'), 'none');
}

/**
 * Gets the unknown icon.
 */
export function unknownSymbol(): IGhsSymbol {
    return new GhsSymbolImpl(GhsSymbols.NonStandard, '[unknown]', i18n.t('inventory.ghs.unknown_name'), i18n.t('inventory.ghs.unknown_description'), 'unknown');
}

export function determineGhsStatus(ghsSymbols: number | undefined): IGhsSymbol {
    if (ghsSymbols === undefined) {
        return unknownSymbol();
    }
    else if (ghsSymbols === GhsSymbols.None) {
        return noneSymbol();
    }
    else {
        return emptySymbol();
    }
}

/**
 * Checks whether a given symbol is in a symbols value.
 * @param symbol The symbol to check for.
 * @param symbolsValue The symbols value detailing symbols.
 */
export function hasGhsSymbol(symbol: IGhsSymbol, symbolsValue: number | undefined) {
    return symbolsValue !== undefined && (symbolsValue & symbol.Symbol) === symbol.Symbol;
}

/**
 * Determines which GHS symbols apply to the chemical.
 * @param chemical The chemical to determine symbols of.
 */
export function determineGhsSymbols(symbolsValue: number | undefined): IGhsSymbol[] {
    if (symbolsValue === undefined || symbolsValue === null || isNaN(symbolsValue)) {
        return [unknownSymbol()];
    }
    else if (symbolsValue === GhsSymbols.None) {
        return [noneSymbol()];
    }
    else {
        const data = ghsData();
        return data.filter(ghs => hasGhsSymbol(ghs, symbolsValue));
    }
}

export function determineStandardGhsSymbols(symbolsValue: number | undefined): IGhsSymbol[] {
    if (symbolsValue === undefined || symbolsValue === null || isNaN(symbolsValue)) {
        return [];
    }

    const data = ghsData();
    return data.filter(ghs => hasGhsSymbol(ghs, symbolsValue));
}

export function getGhsSymbolsNumber(symbols: IGhsSymbol[] | undefined | null): number | undefined {
    if (symbols === undefined || symbols === null) {
        return undefined;
    } else {
        let ghsNumber = 0;
        symbols.forEach(s => ghsNumber = ghsNumber | s.Symbol);
        return ghsNumber;
    }
}

export function ghsLinkStringToArray(ghsLinks: string | undefined): string[] {
    if (ghsLinks === undefined || ghsLinks === null) {
        return [];
    }
    
    return ghsLinks
        .split('|')
        .map(s => s.trim())
        .filter(s => s.length > 0);
}

export function ghsLinkArrayToString(ghsLinks: string[]): string {
    return ghsLinks.join('|');
}

/**
 * Represents a GHS symbol.
 */
export interface IGhsSymbol {
    /**
     * The symbol of the GHS symbol.
     */
    Symbol: GhsSymbols;
    /**
     * The code of the GHS symbol.
     */
    Code: string;
    /**
     * The name of the GHS symbol.
     */
    Name: string;
    /**
     * The description of the GHS symbol.
     */
    Description: string;
    /**
     * The class name of the GHS symbol. Used to get a visual representation.
     */
    ClassName: GhsSymbolClasses;
}

class GhsSymbolImpl implements IGhsSymbol {

    public Symbol: GhsSymbols;
    public Code: string;
    public Name: string;
    public Description: string;
    public ClassName: GhsSymbolClasses;

    public constructor(symbol: GhsSymbols, code: string, name: string, description: string | undefined, className: GhsSymbolClasses) {
        this.Symbol = symbol;
        this.Code = code;
        this.Name = name;
        this.Description = description || (code + ': ' + name);
        this.ClassName = className;
    }
}
