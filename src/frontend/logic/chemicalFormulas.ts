export type ChemicalFormulaTokenTypes = 'number' | 'atomNumber' | 'abstractAtomNumber' | 'atom' | 'groupStart' | 'groupEnd' | 'hydrate' | 'text' | 'unknown';

export interface IChemicalFormulaToken {
    type: ChemicalFormulaTokenTypes;
    data?: string;
}

export function hasData(token: IChemicalFormulaToken): token is IChemicalFormulaToken & { data: string } {
    return token.type === 'number'
        || token.type === 'atomNumber'
        || token.type === 'abstractAtomNumber'
        || token.type === 'atom'
        || token.type === 'unknown'
        || token.type === 'text';
}

const rgxWhitespace = /^\s$/;
const rgxAlphabetUC = /^[A-Z]$/;
const rgxAlphabetLC = /^[a-z]$/;
const rgxNumber = /^\d$/;

export function tokenizeFormula(formula: string): IChemicalFormulaToken[] {
    const tokens: IChemicalFormulaToken[] = [];

    if (formula.charAt(0) === '\'') {
        tokens.push({ type: 'text', data: formula.substr(1) });
        return tokens;
    }

    let currentType: ChemicalFormulaTokenTypes | null = null;
    let currentData: string | undefined = undefined;

    function next() {
        if (currentType) {
            tokens.push({ type: currentType, data: currentData });
        }
        currentType = null;
        currentData = undefined;
    }

    for (let i = 0; i < formula.length; ++i) {
        const char = formula.charAt(i);
        if (rgxWhitespace.test(char)) {
            if (currentType === 'unknown') {
                // append space for unknown
                currentData += ' ';
            }
            if (currentType !== 'unknown') {
                next();
            }
            continue;
        }

        if (rgxAlphabetUC.test(char)) {
            // uppercase letter: start an atom
            next();
            currentType = 'atom';
            currentData = char;
            continue;
        }

        if (rgxAlphabetLC.test(char)) {
            if (currentType === 'atom' && currentData!.length < 2) {
                // lowercase letter: atom continued
                currentData += char;
                continue;
            } else if (currentType === 'atomNumber' || currentType === 'abstractAtomNumber') {
                // continue the (abstract) atom number
                currentType = 'abstractAtomNumber';
                currentData += char;
                continue;
            } else if (currentType === 'groupEnd' || currentType === 'atom') {
                // start an abstract atom number
                next();
                currentType = 'abstractAtomNumber';
                currentData = char;
                continue;
            }
        }

        if (rgxNumber.test(char)) { // we found a number
            if (currentType === 'groupEnd' || currentType === 'atom') {
                // the number indicates the count of an entity
                next();
                currentType = 'atomNumber';
                currentData = char;
                continue;
            } else if (currentType === 'atomNumber' || currentType === 'number') {
                // the number belongs to a previous number
                currentData += char;
                continue;
            } else {
                // the number is a new standalone number
                next();
                currentType = 'number';
                currentData = char;
                continue;
            }
        }

        if (char === '_' && currentType === 'atom') { // force an abstract atom number
            next();
            currentType = 'abstractAtomNumber';
            currentData = '';
            continue;
        }

        if (char === '(') { // we found the start of a group
            next();
            currentType = 'groupStart';
            continue;
        }

        if (char === ')') { // we found the end of a group
            next();
            currentType = 'groupEnd';
            continue;
        }

        if (char === '*') { // we found a hydrate
            next();
            currentType = 'hydrate';
            continue;
        }

        if (currentType !== 'unknown') { // unknown character(s)
            next();
            currentType = 'unknown';
            currentData = char;
        } else {
            currentData += char;
        }
    }

    next(); // process the final token as well

    return tokens;
}
