import { createTypedRoute, ITypedRoute, TypedRouteBuilder } from 'typed-route-builder';

export const forgotPasswordRoute = createTypedRoute('/forgot-password');
export const resetPasswordRoute = createTypedRoute('/reset-password');

export const homeRoute = createTypedRoute('/');
export const loginRoute = homeRoute;
export const accountRoute = createTypedRoute('/account');

export const inventoryRoute = new TypedRouteBuilder().segment('inventory').build();
export const inventoryWorksheetRoute = new TypedRouteBuilder(inventoryRoute)
    .segment('worksheet')
    .param('ws')
    .optionalParam('chemical')
    .build();
export const inventoryChemicalRoute = new TypedRouteBuilder(inventoryRoute)
    .segment('chemical')
    .param('id')
    .build();
export const inventorySearchRoute = new TypedRouteBuilder(inventoryRoute)
    .segment('search')
    .param('query')
    .build();
export const inventoryAllRoute = new TypedRouteBuilder(inventoryRoute)
    .segment('all')
    .build();
export const inventoryRestrictedRoute = new TypedRouteBuilder(inventoryRoute)
    .segment('me')
    .build();

export type AdminPage = 'users' | 'roles' | 'notices' | 'worksheets' | 'columns' | 'logs' | 'domains' | 'system' | 'about';
const adminRoutes = new Map<AdminPage, ITypedRoute<{}, [], string>>();
export const adminRoute = createTypedRoute('/configuration');
export const getAdminPageRoute = (page: AdminPage) =>
    adminRoutes.has(page)
        ? adminRoutes.get(page)!
        : adminRoutes.set(page, new TypedRouteBuilder(adminRoute).segment(page).build()).get(page)!;
