import { ApiResultStatus, UnsuccessfulApiResult } from '@/api/apiBase';
import i18n from '@/i18n/i18n';
import { broadcastNotification, createNotification, NotificationType } from './notifications';

export function handleUnsuccessfulApiCall(r: UnsuccessfulApiResult) {
    if (r.status === ApiResultStatus.failed) {
        broadcastNotification(createNotification(i18n.t(`api.${r.error}`), NotificationType.warning, 10000));
    } else if (r.status === ApiResultStatus.error) {
        broadcastNotification(createNotification(i18n.t(`api.${r.error}`), NotificationType.danger, 10000));
    }
}
