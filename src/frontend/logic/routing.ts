import { History } from 'history';
import React from 'react';

/**
 * Navigates to a path.
 * @param path The path to navigate to.
 * @param state The state to store with this action.
 */
export function navigate(history: History, path: string, state?: any) {
    history.push(path, state);
}

/**
 * Sets the document title.
 * @param title The title.
 */
export function setTitle(title: string | undefined) {
    if (title) {
        document.title = `${title} - ${CHEMIEKAST_GLOBAL_APP_NAME}`;
    } else {
        document.title = CHEMIEKAST_GLOBAL_APP_NAME;
    }
}

export function useTitle(title: string) {
    React.useEffect(() => {
        setTitle(title);
        return () => setTitle(undefined);
    });
}

export function linkHandler(history: History) {
    return function (e: Event | React.SyntheticEvent | React.MouseEvent) {
        if (!(e.currentTarget instanceof HTMLAnchorElement) || (e as React.MouseEvent).button !== 0) {
            return;
        }
        e.preventDefault();
        const href = e.currentTarget.getAttribute('href');
        if (href !== null) {
            navigate(history, href);
        }
    }
}
