type PersistentStorageKeys = {
    'space-inventory-tables': boolean;
    'inventory-sidebar-pinned': boolean;
};
type EphemeralStorageKeys = {
    'admin-system-config-shown': boolean;
};

function getStorageItem(storage: Storage, key: string, defaultValue?: any) {
    const jsonValue = storage.getItem(key);
    if (jsonValue !== null) {
        return JSON.parse(jsonValue);
    } else {
        return defaultValue;
    }
}

function setStorageItem(storage: Storage, key: string, value: any) {
    storage.setItem(key, JSON.stringify(value));
}

/**
 * Gets a record from persistent storage.
 * @param key The key of the value to retrieve.
 * @param defaultValue The default value to return, if the value does not exist.
 */
export function getPersistentStorageItem<K extends keyof PersistentStorageKeys, D extends PersistentStorageKeys[K] | undefined>(key: K, defaultValue?: D): D extends PersistentStorageKeys[K] ? PersistentStorageKeys[K] : (PersistentStorageKeys[K] | undefined) {
    return getStorageItem(localStorage, key, defaultValue);
}

/**
 * Stores a record to persistent storage.
 * @param key The key of the value to store.
 * @param value The value to store.
 */
export function setPersistentStorageItem<K extends keyof PersistentStorageKeys>(key: K, value: PersistentStorageKeys[K]): void {
    setStorageItem(localStorage, key, value);
}

/**
 * Gets a record from ephemeral storage.
 * @param key The key of the value to retrieve.
 * @param defaultValue The default value to return, if the value does not exist.
 */
export function getEphemeralStorageItem<K extends keyof EphemeralStorageKeys, D extends EphemeralStorageKeys[K] | undefined>(key: K, defaultValue?: D): D extends EphemeralStorageKeys[K] ? EphemeralStorageKeys[K] : (EphemeralStorageKeys[K] | undefined) {
    return getStorageItem(sessionStorage, key, defaultValue);
}

/**
 * Stores a record to ephemeral storage.
 * @param key The key of the value to store.
 * @param value The value to store.
 */
export function setEphemeralStorageItem<K extends keyof EphemeralStorageKeys>(key: K, value: EphemeralStorageKeys[K]): void {
    setStorageItem(sessionStorage, key, value);
}

/**
 * Clear ephemeral storage.
 */
export function clearEphemeralStorageItem(): void {
    sessionStorage.clear();
}
