let matchesFn: (selectors: string) => boolean = Element.prototype.matches;

/* Check various vendor-prefixed versions of Element.matches */
if (!matchesFn) {
    ['webkit', 'ms', 'moz'].some(function checkPrefixedMatches(prefix) {
        const prefixedFn = prefix + 'MatchesSelector';
        if (Element.prototype.hasOwnProperty(prefixedFn)) {
            matchesFn = (Element.prototype as any)[prefixedFn];
            return true;
        }
        return false;
    });
}

export const matches = matchesFn;

function passedThrough(event: Event | React.SyntheticEvent, selector: string, stopAt: EventTarget | null): Node | undefined {
    let currentNode: Node | null = event.target as Node;

    while (currentNode) {
        if (matches.call(currentNode, selector)) {
            return currentNode;
        }
        else if (currentNode !== stopAt && currentNode !== document.body) {
            currentNode = currentNode.parentNode;
        }
        else {
            return undefined;
        }
    }
    return undefined;
}

export default function delegateEventListener<T extends Event | React.SyntheticEvent>(fn: (event: T) => void, selector: string) {
    const eventHandler = (e: T) => {
        const found = passedThrough(e, selector, e.currentTarget);

        if (found) {
            fn.call(found, e);
        }
    };

    return eventHandler;
}
