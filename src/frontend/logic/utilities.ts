/**
 * Converts an object to a parameter string.
 * @param obj The object to convert.
 */
export function objectToParams(obj: any): string {
    return Object.keys(obj).map(function createParam(key) {
        return `${encodeURIComponent(key)}=${encodeURIComponent(obj[key])}`;
    }).join('&');
}

/**
 * Converts an arbitrary string into a color value.
 * @param str The string to convert.
 */
export function stringToColor(str: string) {
    let hash = 5381;
    for (let i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) + hash);
    }
    let colour = '#';
    for (let i = 0; i < 3; i++) {
        const value = (hash >> (i * 8)) & 0xFF;
        colour += ('00' + value.toString(16)).substr(-2);
    }
    return colour;
}

/**
 * Generates a random string with the desired length and characters.
 * This function should not be used for security purposes.
 * @param length The length of the string.
 * @param chars The type of characters in the string.
 */
export function PseudorandomString(length: number, chars: string = 'aA#'): string {
    let mask = '';
    if (chars.indexOf('a') > -1) {
        mask += 'abcdefghijklmnopqrstuvwxyz';
    }
    if (chars.indexOf('A') > -1) {
        mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    }
    if (chars.indexOf('#') > -1) {
        mask += '0123456789';
    }
    if (chars.indexOf('!') > -1) {
        mask += '~`!@#$%^&*()_+-={}[]:";\'<>?,./|\\';
    }
    const result: String[] = [];
    for (let i = length; i > 0; --i) {
        result.push(mask[Math.round(Math.random() * (mask.length - 1))]);
    }
    return result.join('');
}

/**
 * Extracts the origin of a URL.
 * @param url The URL to extract the origin from.
 */
export function extractOrigin(url: string) {
    let hostname;
    
    //find & remove protocol (http, ftp, etc.) and get origin
    if (url.indexOf('://') > -1) {
        hostname = url.split('/')[2];
    }
    else {
        hostname = url.split('/')[0];
    }

    return hostname;
}

export function nameof<T>(key: keyof T, _instance?: T): keyof T {
    return key;
}
