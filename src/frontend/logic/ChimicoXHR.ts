type RequestMethod = 'GET' | 'POST';

export default class ChimicoXHR {

    /** The URL to send the XHR request to. */
    public readonly url: string;
    /** The request timeout. */
    public readonly timeout: number = 60000;
    /** The HTTP method to use. */
    public readonly method: RequestMethod = 'GET';
    /** The XmlHttpRequest object used internally. */
    public readonly request: XMLHttpRequest;

    /** The callback function called on success. */
    public success?: (response: any) => any;
    /** The callback function called on failure. */
    public failure?: (reason: any) => any;
    /** The callback function called on completion. This callback is always called, regardless of success or failure. */
    public completed?: Function;

    private setFormContentType: boolean = false;

    /**
     * Initializes a new instance of the ChimicoXHR object.
     * @param url The URL to send the XHR request to.
     * @param method The HTTP method to use.
     * @param timeout The request timeout.
     */
    public constructor(url: string, method?: RequestMethod, timeout?: number) {

        this.url = url;
        if (method) {
            this.method = method;
        }
        if (timeout) {
            this.timeout = timeout;
        }

        const request = new XMLHttpRequest();
        request.onreadystatechange = () => {
            if (request.readyState === XMLHttpRequest.DONE) {
                if (request.status === 200) {
                    if (this.success) {
                        this.success(request.response);
                    }
                }
                else {
                    if (this.failure) {
                        this.failure(request.statusText);
                    }
                }
                if (this.completed) {
                    this.completed();
                }
            }
        };
        request.withCredentials = CHEMIEKAST_GLOBAL_HOST.length > 0;
        request.open(this.method, url, true);
        request.timeout = this.timeout;
        this.request = request;
    }

    /**
     * Sends the XHR request.
     * @param data The data to send with the request.
     */
    public send(data?: any): void {
        this.request.send(data);
    }

    public setFormContentHeader(): void {
        if (!this.setFormContentType) {
            this.request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            this.setFormContentType = true;
        }
    }
}
