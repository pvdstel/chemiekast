export enum NotificationType {
    none,
    primary,
    link,
    info,
    success,
    warning,
    danger,
}

/** An interface for notification objects. */
export interface INotification {
    /** The notification message. */
    message: React.ReactElement | string;
    /** The notification type. */
    type: NotificationType;
    /** The notification timeout. */
    timeout: number | undefined;
}

/**
 * Constructs a new notification object.
 * @param message The notification message.
 * @param type The notification type.
 * @param timeout The notification timeout.
 */
export function createNotification(
    message: React.ReactElement | string,
    type: NotificationType = NotificationType.none,
    timeout: INotification['timeout'] = undefined,
): INotification {
    return {
        message,
        type,
        timeout,
    };
}

type NotificationListener = (notification: INotification) => void;
const notificationListeners = new Set<NotificationListener>();

/**
 * Subscribes a listener to notifications.
 * @param listener The listener to subscribe.
 * @returns A callback to unsubscribe.
 */
export function subscribeNotifications(listener: NotificationListener) {
    notificationListeners.add(listener);
    return () => notificationListeners.delete(listener);
}

/**
 * Broadcasts a notification to all notification listeners.
 * @param notification The notification to broadcast.
 */
export function broadcastNotification(notification: INotification) {
    notificationListeners.forEach(l => l(notification));
}
