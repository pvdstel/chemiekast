declare const CHEMIEKAST_GLOBAL_APP_NAME: string;
declare const CHEMIEKAST_GLOBAL_APP_VERSION: string;
declare const CHEMIEKAST_GLOBAL_APP_BUILD_DATE: string;
declare const CHEMIEKAST_GLOBAL_HOST: string;
declare const CHEMIEKAST_GLOBAL_PRODUCTION_BUILD: boolean;
declare const CHEMIEKAST_GLOBAL_SOURCE_REPO: string;

declare type NullableMembers<T> = { [P in keyof T]: T[P] | null };

declare module 'core-js/es/set';
declare module 'core-js/es/map';

declare module '*.scss' {
    const classes: Record<string, string>;
    export default classes;
}
