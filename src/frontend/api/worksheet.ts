import ChimicoXHR from '@/logic/ChimicoXHR';
import { ApiResult, endRequest, getApiUrl, processApiResponse, requestFailureApiResponse, startRequest } from './apiBase';

/** Represents a worksheet column. */
export interface IWorksheetColumn {
    /** The name of the column. */
    Name: string;
    /** The name of the column as displayed in the UI. */
    UIName: string;
    /** Whether the column is visible. */
    IsVisible: boolean;
    /** Whether the column is searchable. */
    IsSearchable: boolean;
    /** Whether the column is a collection of URLs. */
    IsUrl: boolean;
    /** Whether the column is in markdown format. */
    IsMarkdown: boolean;
    /** Whether the column is formatted as a formula. */
    IsFormula: boolean;
}

/** Represents a worksheet. */
export interface IWorksheet {
    /** The ID of the worksheet. */
    ID: number;
    /** The code of the worksheet. */
    Code: string;
    /** The name of the worksheet. */
    Name: string;
}

/**
 * Instantiates a new user with default values.
 */
export function emptyWorksheet(): IWorksheet {
    return {
        ID: 0,
        Code: '',
        Name: '',
    };
}

/**
 * Gets the worksheet information.
 * @param callback The callback function to call when the data has been received.
 */
export function getWorksheets(callback: (result: ApiResult<IWorksheet[]>) => void): void {
    const busy = startRequest();
    const worksheetsRequest = new ChimicoXHR(getApiUrl('worksheet/get'));
    worksheetsRequest.success = r => callback(processApiResponse(r));
    worksheetsRequest.failure = () => callback(requestFailureApiResponse);
    worksheetsRequest.completed = () => endRequest(busy);
    worksheetsRequest.send();
}

/**
 * Adds a worksheet.
 * @param worksheet The worksheet to store.
 * @param callback The callback function to call when the action has completed.
 */
export function addWorksheet(worksheet: IWorksheet, callback: (result: ApiResult<never>) => void): void {
    const busy = startRequest();
    const addWorksheetRequest = new ChimicoXHR(getApiUrl('worksheet/add'), 'POST');
    addWorksheetRequest.success = r => callback(processApiResponse(r));
    addWorksheetRequest.failure = () => callback(requestFailureApiResponse);
    addWorksheetRequest.completed = () => endRequest(busy);
    addWorksheetRequest.send(JSON.stringify(worksheet));
}

/**
 * Edits a stored worksheet.
 * @param worksheet The worksheet to edit.
 * @param callback The callback function to call when the action has completed.
 */
export function editWorksheet(worksheet: IWorksheet, callback: (result: ApiResult<never>) => void): void {
    const busy = startRequest();
    const addWorksheetRequest = new ChimicoXHR(getApiUrl('worksheet/edit'), 'POST');
    addWorksheetRequest.success = r => callback(processApiResponse(r));
    addWorksheetRequest.failure = () => callback(requestFailureApiResponse);
    addWorksheetRequest.completed = () => endRequest(busy);
    addWorksheetRequest.send(JSON.stringify(worksheet));
}

/**
 * Deletes a worksheet.
 * @param worksheetId The ID of the worksheet to delete.
 * @param callback The callback function to call when the action has completed.
 */
export function deleteWorksheet(worksheetId: number, callback: (result: ApiResult<never>) => void): void {
    const busy = startRequest();
    const deleteWorksheetRequest = new ChimicoXHR(getApiUrl(`worksheet/delete/${worksheetId}`), 'POST');
    deleteWorksheetRequest.success = r => callback(processApiResponse(r));
    deleteWorksheetRequest.failure = () => callback(requestFailureApiResponse);
    deleteWorksheetRequest.completed = () => endRequest(busy);
    deleteWorksheetRequest.send();
}
