import ChimicoXHR from '@/logic/ChimicoXHR';
import { ApiResult, endRequest, getApiUrl, processApiResponse, requestFailureApiResponse, startRequest } from './apiBase';

export interface IConfiguration {
    'system-logging-enabled': boolean;
    'system-custom-roles-enabled': boolean;
    'security-user-session-timeout': number;
    'security-user-inactivity-timeout': number;
    'security-password-reset-enabled': boolean;
    'security-password-hash-cost': number;
    'ui-split-restricted-worksheet': boolean;
    'mail-smtp-host': string;
    'mail-port': number;
    'mail-username': string;
    'mail-password': string;
}

/**
 * Gets the available settings from the Chemiekast configuration.
 * @param The callback function to call when the data has been received.
 */
export function getConfiguration(callback: (result: ApiResult<IConfiguration>) => void): void {
    const busy = startRequest();
    const configRequest = new ChimicoXHR(getApiUrl('config/get/*'));
    configRequest.success = r => callback(processApiResponse(r));
    configRequest.failure = () => callback(requestFailureApiResponse);
    configRequest.completed = () => endRequest(busy);
    configRequest.send();
}

/**
 * Sets the Chemiekast configuration from the given data.
 * @param configuration The settings to set.
 * @param callback The callback function to call when the action has completed.
 */
export function setConfiguration(configuration: Partial<IConfiguration>, callback: (result: ApiResult<never>) => void): void {
    const busy = startRequest();
    const setConfigRequest = new ChimicoXHR(getApiUrl('config/set/*'), 'POST');
    setConfigRequest.success = r => callback(processApiResponse(r));
    setConfigRequest.failure = () => callback(requestFailureApiResponse);
    setConfigRequest.completed = () => endRequest(busy);
    setConfigRequest.send(JSON.stringify(configuration));
}

/**
 * Resets all configuration data to the defaults.
 * @param callback The callback function to call when the action has completed.
 */
export function resetConfiguration(callback: (result: ApiResult<never>) => void): void {
    const busy = startRequest();
    const resetConfigRequest = new ChimicoXHR(getApiUrl('config/reset'), 'POST');
    resetConfigRequest.success = r => callback(processApiResponse(r));
    resetConfigRequest.failure = () => callback(requestFailureApiResponse);
    resetConfigRequest.completed = () => endRequest(busy);
    resetConfigRequest.send();
}
