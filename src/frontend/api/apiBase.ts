import { hideBackgroundLoader, showBackgroundLoader } from '@/logic/loading';

/** The different statuses an API response might assert. */
export enum ApiResultStatus {
    completed,
    failed,
    error,
}

export type SuccessfulApiResult<T> = {
    status: ApiResultStatus,
    result: T,
};

export type UnsuccessfulApiResult = {
    status: ApiResultStatus,
    error: string,
};

/** The result of an API request. */
export class ApiResult<T> {

    /** The request status. */
    public readonly status: ApiResultStatus;

    /** The result of the API request. */
    public readonly result: T | undefined;

    /** The error message of the API request. */
    public readonly error: string | undefined;

    /** Type guard to determine if the result has an actual result. */
    public isSuccessful(): this is SuccessfulApiResult<T> {
        return this.status === ApiResultStatus.completed;
    }

    /** Type guard to determine if the result has an error message. */
    public isUnsuccessful(): this is UnsuccessfulApiResult {
        return this.status === ApiResultStatus.failed
            || this.status === ApiResultStatus.error;
    }

    /**
     * Initializes a new instance of the ApiResult class.
     * @param status The request status.
     * @param result The result of the API request.
     * @param error Errors that occurred during the API request.
     */
    constructor(status: ApiResultStatus, result: T | undefined, error: string | undefined) {
        this.status = status;
        this.result = result || undefined;
        this.error = error || undefined;
    }
}

/** The structure of a server's API response. */
type ApiResponse = {
    ok: boolean;
    data: any;
    error: string;
};

/** The default error API response. */
export const requestFailureApiResponse = new ApiResult<never>(ApiResultStatus.error, undefined, 'connectionFailure');

/**
 * Processes a server's API response.
 * @param response The response from the server.
 */
export function processApiResponse<T>(response: string): ApiResult<T> {
    try {
        if (response) {
            const parsed = JSON.parse(response) as ApiResponse;
            if (parsed.ok) {
                return new ApiResult<T>(ApiResultStatus.completed, parsed.data as T | undefined, undefined);
            }
            else {
                return new ApiResult<T>(ApiResultStatus.failed, undefined, parsed.error);
            }
        }
    } catch (error) {
        console.error(error);
    }
    return new ApiResult<T>(ApiResultStatus.error, undefined, 'invalidResponse');
}

/**
 * Gets the URL to an API endpoint.
 * @param apiPath The path of the API endpoint.
 */
export function getApiUrl(apiPath: string) {
    return `${CHEMIEKAST_GLOBAL_HOST}/api/${apiPath}`;
}

const busyObjects = new Set<{}>();

/** Gets a busy lock. */
export function startRequest(): {} {
    const o = {};
    busyObjects.add(o);
    showBackgroundLoader();
    return o;
}

/**
 * Releases a busy lock.
 * @param o The busy lock.
 */
export function endRequest(o: {}) {
    if (busyObjects.has(o)) {
        busyObjects.delete(o);
        hideBackgroundLoader();
    }
}
