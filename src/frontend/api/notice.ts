import ChimicoXHR from '@/logic/ChimicoXHR';
import { ApiResult, endRequest, getApiUrl, processApiResponse, requestFailureApiResponse, startRequest } from './apiBase';

export interface INotice {
    /** The ID of the notice. */
    ID: number;
    /** The name of the user who posted the notice. */
    Poster: string;
    /** The post date of the notice. */
    Date: string;
    /** The message of the notice. */
    Message: string;
    /** Whether the notice is a system notice. */
    System: boolean;
}

export interface INoticeCollection {
    /** The notices in this notice collection. */
    Notices: INotice[];
    /** Whether the user who requested the collection can edit system notices. */
    CanEditSystem: boolean;
}

/**
 * Gets the notice information.
 * @param callback The callback function to call when the data has been received.
 */
export function getNotices(callback: (result: ApiResult<INoticeCollection>) => void): void {
    const busy = startRequest();
    const noticesRequest = new ChimicoXHR(getApiUrl('notice/get/*'));
    noticesRequest.success = r => callback(processApiResponse(r));
    noticesRequest.failure = () => requestFailureApiResponse;
    noticesRequest.completed = () => endRequest(busy);
    noticesRequest.send();
}

/**
 * Adds a notice.
 * @param notice The notice to store.
 * @param callback The callback function to call when the action has completed.
 */
export function addNotice(notice: Pick<INotice, 'Message' | 'System'>, callback: (result: ApiResult<never>) => void): void {
    const busy = startRequest();
    const addNoticeRequest = new ChimicoXHR(getApiUrl('notice/add'), 'POST');
    addNoticeRequest.success = r => callback(processApiResponse(r));
    addNoticeRequest.failure = () => callback(requestFailureApiResponse);
    addNoticeRequest.completed = () => endRequest(busy);
    addNoticeRequest.send(JSON.stringify(notice));
}

/**
 * Edits a stored notice.
 * @param notice The notice to edit.
 * @param callback The callback function to call when the action has completed.
 */
export function editNotice(notice: INotice, callback: (result: ApiResult<never>) => any): void {
    const busy = startRequest();
    const editNoticeRequest = new ChimicoXHR(getApiUrl('notice/edit'), 'POST');
    editNoticeRequest.success = r => callback(processApiResponse(r));
    editNoticeRequest.failure = () => callback(requestFailureApiResponse);
    editNoticeRequest.completed = () => endRequest(busy);
    editNoticeRequest.send(JSON.stringify(notice));
}

/**
 * Deletes a notice.
 * @param noticeId The ID of the notice to delete.
 * @param callback The callback function to call when the action has completed.
 */
export function deleteNotice(noticeId: number, callback: (result: ApiResult<never>) => void): void {
    const busy = startRequest();
    const deleteNoticeRequest = new ChimicoXHR(getApiUrl(`notice/delete/${noticeId}`), 'POST');
    deleteNoticeRequest.success = r => callback(processApiResponse(r));
    deleteNoticeRequest.failure = () => callback(requestFailureApiResponse);
    deleteNoticeRequest.completed = () => endRequest(busy);
    deleteNoticeRequest.send();
}
