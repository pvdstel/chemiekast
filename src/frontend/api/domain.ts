import ChimicoXHR from '@/logic/ChimicoXHR';
import { ApiResult, endRequest, getApiUrl, processApiResponse, requestFailureApiResponse, startRequest } from './apiBase';

/** Represents a domain. */
export interface IDomain {
    /** The ID of the domain. */
    ID: number;
    /** The name of the domain. */
    Name: string;
}

export function emptyDomain(): IDomain {
    return {
        ID: 0,
        Name: '',
    };
}

/**
 * Gets the domain information.
 * @param callback The callback function to call when the data has been received.
 */
export function getDomains(callback: (result: ApiResult<IDomain[]>) => void): void {
    const busy = startRequest();
    const domainsRequest = new ChimicoXHR(getApiUrl('domain/get'));
    domainsRequest.success = r => callback(processApiResponse(r));
    domainsRequest.failure = () => callback(requestFailureApiResponse);
    domainsRequest.completed = () => endRequest(busy);
    domainsRequest.send();
}

/**
 * Adds a domain.
 * @param domain The domain to store.
 * @param callback The callback function to call when the action has completed.
 */
export function addDomain(domain: IDomain, callback: (result: ApiResult<never>) => void): void {
    const busy = startRequest();
    const addDomainRequest = new ChimicoXHR(getApiUrl('domain/add'), 'POST');
    addDomainRequest.success = r => callback(processApiResponse(r));
    addDomainRequest.failure = () => callback(requestFailureApiResponse);
    addDomainRequest.completed = () => endRequest(busy);
    addDomainRequest.send(JSON.stringify(domain));
}

/**
 * Edits a stored domain.
 * @param domain The domain to edit.
 * @param callback The callback function to call when the action has completed.
 */
export function editDomain(domain: IDomain, callback: (result: ApiResult<never>) => void): void {
    const busy = startRequest();
    const addDomainRequest = new ChimicoXHR(getApiUrl('domain/edit'), 'POST');
    addDomainRequest.success = r => callback(processApiResponse(r));
    addDomainRequest.failure = () => callback(requestFailureApiResponse);
    addDomainRequest.completed = () => endRequest(busy);
    addDomainRequest.send(JSON.stringify(domain));
}

/**
 * Deletes a domain.
 * @param domainId The ID of the domain to delete.
 * @param callback The callback function to call when the action has completed.
 */
export function deleteDomain(domainId: number, callback: (result: ApiResult<never>) => void): void {
    const busy = startRequest();
    const deleteDomainRequest = new ChimicoXHR(getApiUrl(`domain/delete/${domainId}`), 'POST');
    deleteDomainRequest.success = r => callback(processApiResponse(r));
    deleteDomainRequest.failure = () => callback(requestFailureApiResponse);
    deleteDomainRequest.completed = () => endRequest(busy);
    deleteDomainRequest.send();
}

/**
 * Switches to another domain.
 * @param domainId The ID of the domain to switch to.
 * @param callback The callback function to call when the action has completed.
 */
export function switchDomain(domainId: number, callback: (result: ApiResult<number>) => void): void {
    const busy = startRequest();
    const switchDomainRequest = new ChimicoXHR(getApiUrl(`domain/switch/${domainId}`), 'POST');
    switchDomainRequest.success = r => callback(processApiResponse(r));
    switchDomainRequest.failure = () => callback(requestFailureApiResponse);
    switchDomainRequest.completed = () => endRequest(busy);
    switchDomainRequest.send();
}
