import ChimicoXHR from '@/logic/ChimicoXHR';
import { ApiResult, endRequest, getApiUrl, processApiResponse, requestFailureApiResponse, startRequest } from './apiBase';

export const CHEMIEKAST_HIGHEST_DEFAULT_ROLE = 6;

/**
 * Represents a user.
 */
export interface IUser {
    /**
     * The ID of the user.
     */
    ID: number;
    /**
     * The user's username.
     */
    Username: string;
    /**
     * The user's password. This field may not always have a value.
     */
    Password?: string;
    /**
     * The domain of the user.
     */
    Domain: number;
    /**
     * The role of the user.
     */
    Role: number;
    /**
     * The email address of the user.
     */
    Email: string;
    /**
     * The user's first name.
     */
    FirstName: string;
    /**
     * The user's last name.
     */
    LastName: string;
    /**
     * Whether the user can log in.
     */
    State: boolean;
    /**
     * Detailed access to worksheets.
     */
    UserAccess?: IUserAccess[];
}

export interface IUserAccess {
    /**
     * The worksheet the user is authorized to view.
     */
    Worksheet: string;
    /**
     * Whether the user is also authorized to edit the worksheet.
     */
    Editing: boolean;
}

export interface IUserRole {
    /**
     * The role ID.
     */
    ID: number;
    /**
     * The role name.
     */
    Name: string;
}

export interface IPasswordChange {
    CurrentPassword: string;
    NewPassword: string;
}

export interface IPasswordReset {
    Username: string;
    Token: string;
    NewPassword: string;
}

/**
 * Instantiates a new user with default values.
 */
export function emptyUser(): IUser {
    return {
        ID: 0,
        Username: '',
        Password: '',
        Domain: 0,
        Role: CHEMIEKAST_HIGHEST_DEFAULT_ROLE,
        Email: '',
        FirstName: '',
        LastName: '',
        State: true,
        UserAccess: [],
    };
}

/**
 * Gets the user information.
 * @param callback The callback function to call when the data has been received.
 */
export function getUsers(callback: (result: ApiResult<IUser[]>) => void): void {
    const busy = startRequest();
    const getUsersRequest = new ChimicoXHR(getApiUrl('user/get/*'));
    getUsersRequest.success = r => callback(processApiResponse(r));
    getUsersRequest.failure = () => callback(requestFailureApiResponse);
    getUsersRequest.completed = () => endRequest(busy);
    getUsersRequest.send();
}

/**
 * Gets a user by ID.
 * @param userId The ID of the user to get.
 * @param callback The callback function to call when the data has been received.
 */
export function getUser(userId: number, callback: (result: ApiResult<IUser>) => void): void {
    const busy = startRequest();
    const getUserRequest = new ChimicoXHR(getApiUrl(`user/get/${userId}`));
    getUserRequest.success = r => callback(processApiResponse(r));
    getUserRequest.failure = () => callback(requestFailureApiResponse);
    getUserRequest.completed = () => endRequest(busy);
    getUserRequest.send();
}

/**
 * Adds a user.
 * @param user The user to store.
 * @param callback The callback function to call when the action has completed.
 */
export function addUser(user: IUser, callback: (result: ApiResult<never>) => void): void {
    const busy = startRequest();
    const addUserRequest = new ChimicoXHR(getApiUrl('user/add'), 'POST');
    addUserRequest.success = r => callback(processApiResponse(r));
    addUserRequest.failure = () => callback(requestFailureApiResponse);
    addUserRequest.completed = () => endRequest(busy);
    addUserRequest.send(JSON.stringify(user));
}

/**
 * Edits a stored user.
 * @param user The user to edit.
 * @param callback The callback function to call when the action has completed.
 */
export function editUser(user: IUser, callback: (result: ApiResult<never>) => void): void {
    const busy = startRequest();
    const editUserRequest = new ChimicoXHR(getApiUrl('user/edit'), 'POST');
    editUserRequest.success = r => callback(processApiResponse(r));
    editUserRequest.failure = () => callback(requestFailureApiResponse);
    editUserRequest.completed = () => endRequest(busy);
    editUserRequest.send(JSON.stringify(user));
}

/**
 * Deletes a user.
 * @param userId The ID of the user to delete.
 * @param callback The callback function to call when the action has completed.
 */
export function deleteUser(userId: number, callback: (result: ApiResult<never>) => void): void {
    const busy = startRequest();
    const deleteUserRequest = new ChimicoXHR(getApiUrl(`user/delete/${userId}`), 'POST');
    deleteUserRequest.success = r => callback(processApiResponse(r));
    deleteUserRequest.failure = () => callback(requestFailureApiResponse);
    deleteUserRequest.completed = () => endRequest(busy);
    deleteUserRequest.send();
}

/**
 * Changes the current user's password.
 * @param change The password change data.
 * @param callback The callback function to call when the action has completed.
 */
export function changeUserPassword(change: IPasswordChange, callback: (result: ApiResult<never>) => void): void {
    const busy = startRequest();
    const changeUserPasswordRequest = new ChimicoXHR(getApiUrl('me/change-password'), 'POST');
    changeUserPasswordRequest.success = r => callback(processApiResponse(r));
    changeUserPasswordRequest.failure = () => callback(requestFailureApiResponse);
    changeUserPasswordRequest.completed = () => endRequest(busy);
    changeUserPasswordRequest.send(JSON.stringify(change));
}

/**
 * Sends a test email to the current user.
 * @param callback The callback function to call when the data has been received.
 */
export function sendTestMail(callback: (result: ApiResult<IUser>) => void): void {
    const busy = startRequest();
    const mailRequest = new ChimicoXHR(getApiUrl('me/test-email'));
    mailRequest.success = r => callback(processApiResponse(r));
    mailRequest.failure = () => callback(requestFailureApiResponse);
    mailRequest.completed = () => endRequest(busy);
    mailRequest.send();
}

export function forgotPassword(username: string, callback: (result: ApiResult<never>) => void): void {
    const busy = startRequest();
    const forgotPasswordRequest = new ChimicoXHR(getApiUrl('me/forgot-password'), 'POST');
    forgotPasswordRequest.success = r => callback(processApiResponse(r));
    forgotPasswordRequest.failure = () => callback(requestFailureApiResponse);
    forgotPasswordRequest.completed = () => endRequest(busy);
    forgotPasswordRequest.send(JSON.stringify({ Username: username }));
}

export function resetPassword(reset: IPasswordReset, callback: (result: ApiResult<never>) => void): void {
    const busy = startRequest();
    const resetPasswordRequest = new ChimicoXHR(getApiUrl('me/reset-password'), 'POST');
    resetPasswordRequest.success = r => callback(processApiResponse(r));
    resetPasswordRequest.failure = () => callback(requestFailureApiResponse);
    resetPasswordRequest.completed = () => endRequest(busy);
    resetPasswordRequest.send(JSON.stringify(reset));
}
