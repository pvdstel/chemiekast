import ChimicoXHR from '@/logic/ChimicoXHR';
import { ApiResult, endRequest, getApiUrl, processApiResponse, requestFailureApiResponse, startRequest } from './apiBase';
import { IChemical } from './chemical';

/**
 * Represents an inventory worksheet.
 */
export interface IInventoryWorksheet {
    /** The worksheet code. */
    Code: string;
    /** The worksheet name. */
    Name: string;
    /** The chemicals in the inventory worksheet. */
    Chemicals: IChemical[];
    /** Whether the worksheet is editable by the current user. */
    Editable: boolean;
    /** Whether chemicals in the worksheet are selectable by the current user. */
    Selectable: boolean;
}

type InventoryCallbackType = (result: ApiResult<IInventoryWorksheet[]>) => any;

/**
 * Gets a worksheet from an API.
 * @param worksheetCode The code of the worksheet to get.
 * @param callback The callback to call when the data has been received.
 */
export function getWorksheetInventory(worksheetCode: string, callback: InventoryCallbackType): void {
    getInventory(`/inventory/worksheet/${encodeURIComponent(worksheetCode)}`, callback);
}

/**
 * Gets search results from an API.
 * @param query The search query.
 * @param callback The callback to call when the data has been received.
 */
export function getSearchResults(query: string, callback: InventoryCallbackType): void {
    getInventory(`/inventory/search/${encodeURIComponent(query)}`, callback);
}

/**
 * Gets the restricted inventory from an API.
 * @param callback The callback to call when the data has been received.
 */
export function getRestrictedInventory(callback: InventoryCallbackType): void {
    getInventory('/inventory/restricted', callback);
}

/**
 * Gets inventory data from an API.
 * @param endpointUrl The API url to get inventory data from.
 * @param callback The callback function to call when the data has been received.
 */
function getInventory(endpointUrl: string, callback: InventoryCallbackType): void {
    const busy = startRequest();
    const inventoryRequest = new ChimicoXHR(getApiUrl(endpointUrl));
    inventoryRequest.success = r => callback(processApiResponse(r));
    inventoryRequest.failure = () => callback(requestFailureApiResponse);
    inventoryRequest.completed = () => endRequest(busy);
    inventoryRequest.send();
}
