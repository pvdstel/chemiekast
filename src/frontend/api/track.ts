import { getApiUrl } from './apiBase';

export interface ILogData {
    Activity: string;
    Data: string;
}

export function sendLogActivity(activity: string, data: string = ''): boolean {
    if (navigator.sendBeacon) {
        const logData: ILogData = {
            Activity: activity,
            Data: data,
        };
        return navigator.sendBeacon(getApiUrl('track/log-beacon'), JSON.stringify(logData));
    }
    return false;
}
