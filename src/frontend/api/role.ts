import ChimicoXHR from '@/logic/ChimicoXHR';
import { ApiResult, endRequest, getApiUrl, processApiResponse, requestFailureApiResponse, startRequest } from './apiBase';
import { IChemical } from './chemical';

/** Represents a role definition. */
export interface IRoleDefinition {
    /** The ID of the role. */
    ID: number;
    /** The name of the role. */
    Name: string;
}

/**
 * Represents a role.
 */
export interface IRole extends IRoleDefinition {
    /** The chemicals in the role. */
    Chemicals: Pick<IChemical, 'ID' | 'Name'>[];
}

/**
 * Adds chemicals to a role.
 * @param chemicals The chemicals to add to the role.
 * @param role The role to add chemicals to.
 * @param callback The callback function to call when the action has completed.
 */
export function addChemicalsToRole(chemicalIds: number[], role: string | number, callback: (result: ApiResult<never>) => void): void {
    const busy = startRequest();
    const addToRoleRequest = new ChimicoXHR(getApiUrl('role/add-chemicals'), 'POST');
    addToRoleRequest.success = r => callback(processApiResponse(r));
    addToRoleRequest.failure = () => requestFailureApiResponse;
    addToRoleRequest.completed = () => endRequest(busy);
    addToRoleRequest.send(JSON.stringify({
        Chemicals: chemicalIds,
        Role: role,
    }));
}

/**
 * Gets the role information.
 * @param callback The callback function to call when the data has been received.
 */
export function getRoles(callback: (result: ApiResult<IRole[]>) => any): void {
    const busy = startRequest();
    const rolesRequest = new ChimicoXHR(getApiUrl('role/get/*'));
    rolesRequest.success = r => callback(processApiResponse(r));
    rolesRequest.failure = () => requestFailureApiResponse;
    rolesRequest.completed = () => endRequest(busy);
    rolesRequest.send();
}

/**
 * Removes a chemical from a role.
 * @param chemicalId The ID of the chemical to remove from the role.
 * @param roleId The ID of the role to remove the chemical from.
 * @param callback The callback function to call when the action has completed.
 */
export function removeChemicalFromRole(chemicalId: number, roleId: number, callback: (result: ApiResult<never>) => void): void {
    const busy = startRequest();
    const removeFromRoleRequest = new ChimicoXHR(getApiUrl('role/remove-chemical'), 'POST');
    removeFromRoleRequest.success = r => callback(processApiResponse(r));
    removeFromRoleRequest.failure = () => requestFailureApiResponse;
    removeFromRoleRequest.completed = () => endRequest(busy);
    removeFromRoleRequest.send(JSON.stringify({
        Chemical: chemicalId,
        Role: roleId,
    }));
}

/**
 * Renames a role.
 * @param role The role data.
 * @param callback The callback function to call when the action has completed.
 */
export function renameRole(role: IRoleDefinition, callback: (result: ApiResult<never>) => void): void {
    const busy = startRequest();
    const renameRoleRequest = new ChimicoXHR(getApiUrl('role/rename'), 'POST');
    renameRoleRequest.success = r => callback(processApiResponse(r));
    renameRoleRequest.failure = () => requestFailureApiResponse;
    renameRoleRequest.completed = () => endRequest(busy);
    renameRoleRequest.send(JSON.stringify(role));
}

/**
 * Deletes a role.
 * @param roleId The ID of the role to delete.
 * @param callback The callback function to call when the action has completed.
 */
export function deleteRole(roleId: number, callback: (result: ApiResult<never>) => void): void {
    const busy = startRequest();
    const deleteRoleRequest = new ChimicoXHR(getApiUrl(`role/delete/${roleId}`), 'POST');
    deleteRoleRequest.success = r => callback(processApiResponse(r));
    deleteRoleRequest.failure = () => requestFailureApiResponse;
    deleteRoleRequest.completed = () => endRequest(busy);
    deleteRoleRequest.send();
}
