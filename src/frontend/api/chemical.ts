import ChimicoXHR from '@/logic/ChimicoXHR';
import { ApiResult, endRequest, getApiUrl, processApiResponse, requestFailureApiResponse, startRequest } from './apiBase';

/**
 * Represents a chemical.
 */
export interface IChemical {
    /** The chemical ID. */
    ID: number;
    /** The chemical name. */
    Name: string;
    /** The code of the chemical worksheet. */
    Worksheet: string;
    /** The GHS symbols. */
    GhsSymbols: number;
    /** The GHS links. */
    GhsLinks: string;
    /** Other custom fields. */
    [property: string]: string | number;
}

export function emtpyChemical(): Partial<IChemical> {
    return {
        ID: undefined,
        Name: undefined,
        GhsLinks: undefined,
        GhsSymbols: undefined,
    };
}

export type GetChemicalResultType = {
    Chemical: IChemical;
    Editable: boolean;
};

/**
 * Gets data for a stored chemical.
 * @param chemicalId The ID of the chemical.
 * @param callback The callback function to call when the action has completed.
 */
export function getChemical(chemicalId: number, callback: (result: ApiResult<GetChemicalResultType>) => void): void {
    const busy = startRequest();
    const getChemicalRequest = new ChimicoXHR(getApiUrl(`chemical/get/${chemicalId}`), 'GET');
    getChemicalRequest.success = r => callback(processApiResponse(r));
    getChemicalRequest.failure = () => callback(requestFailureApiResponse);
    getChemicalRequest.completed = () => endRequest(busy);
    getChemicalRequest.send();
}

/**
 * Stores a new chemical.
 * @param chemicalData The chemical data.
 * @param callback The callback function to call when the action has completed.
 */
export function addChemical(chemicalDelta: Partial<IChemical>, callback: (result: ApiResult<never>) => any): void {
    const busy = startRequest();
    const addChemicalRequest = new ChimicoXHR(getApiUrl('chemical/add'), 'POST');
    addChemicalRequest.success = r => callback(processApiResponse(r));
    addChemicalRequest.failure = () => callback(requestFailureApiResponse);
    addChemicalRequest.completed = () => endRequest(busy);
    addChemicalRequest.send(JSON.stringify(chemicalDelta));
}

/**
 * Edits stored chemical data.
 * @param chemicalData The chemical data.
 * @param callback The callback function to call when the action has completed.
 */
export function editChemical(chemicalDelta: Partial<IChemical>, callback: (result: ApiResult<never>) => any): void {
    const busy = startRequest();
    const editChemicalRequest = new ChimicoXHR(getApiUrl('chemical/edit'), 'POST');
    editChemicalRequest.success = r => callback(processApiResponse(r));
    editChemicalRequest.failure = () => callback(requestFailureApiResponse);
    editChemicalRequest.completed = () => endRequest(busy);
    editChemicalRequest.send(JSON.stringify(chemicalDelta));
}

/**
 * Edits stored chemical data.
 * @param chemicalData The chemical data.
 * @param callback The callback function to call when the action has completed.
 */
export function deleteChemical(chemicalId: number, callback: (result: ApiResult<never>) => any): void {
    const busy = startRequest();
    const deleteChemicalRequest = new ChimicoXHR(getApiUrl('chemical/delete'), 'POST');
    deleteChemicalRequest.success = r => callback(processApiResponse(r));
    deleteChemicalRequest.failure = () => callback(requestFailureApiResponse);
    deleteChemicalRequest.completed = () => endRequest(busy);
    deleteChemicalRequest.send(chemicalId);
}
