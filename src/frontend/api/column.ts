import ChimicoXHR from '@/logic/ChimicoXHR';
import { ApiResult, endRequest, getApiUrl, processApiResponse, requestFailureApiResponse, startRequest } from './apiBase';
import { IWorksheetColumn } from './worksheet';

export const CHEMICALS_ID_COLUMN = 'ID';
export const CHEMICALS_NAME_COLUMN = 'Name';
export const CHEMICALS_WORKSHEET_COLUMN = 'Worksheet';
export const CHEMICALS_GHS = 'GHS';
export const CHEMICALS_GHS_SYMBOLS_COLUMN = 'GhsSymbols';
export const CHEMICALS_GHS_LINKS_COLUMN = 'GhsLinks';

/** Represents a worksheet column for editing purposes. */
export interface IAdminWorksheetColumn extends IWorksheetColumn {
    /** A hash of the name. */
    NameHash: string;
    /** Whether this column is a system column that cannot be modified. */
    IsSystemColumn: boolean;
}

/** Represents the directions in which a column can be moved. */
export enum ColumnMoveDirection {
    /** Moves the column up. */
    Up = 'up',
    /** Moves the column down. */
    Down = 'down',
}

export function emptyAdminColumn(): IAdminWorksheetColumn {
    return {
        Name: '',
        NameHash: '',
        UIName: '',
        IsVisible: true,
        IsSystemColumn: false,
        IsSearchable: false,
        IsUrl: false,
        IsMarkdown: false,
        IsFormula: false,
    };
}

export function isSystemColumn(column: string | IWorksheetColumn): boolean {
    let columnName;
    if (typeof column === 'string') {
        columnName = column;
    } else {
        columnName = column.Name;
    }

    return columnName === CHEMICALS_ID_COLUMN
        || columnName === CHEMICALS_NAME_COLUMN
        || columnName === CHEMICALS_WORKSHEET_COLUMN
        || columnName === CHEMICALS_GHS_SYMBOLS_COLUMN
        || columnName === CHEMICALS_GHS_LINKS_COLUMN;
}

export function isNumericColumn(column: string | IWorksheetColumn): boolean {
    let columnName;
    if (typeof column === 'string') {
        columnName = column;
    } else {
        columnName = column.Name;
    }

    return columnName === CHEMICALS_ID_COLUMN
        || columnName === CHEMICALS_GHS_SYMBOLS_COLUMN;
}

/**
 * Gets the column information.
 * @param callback The callback function to call when the data has been received.
 */
export function getColumns(callback: (result: ApiResult<IAdminWorksheetColumn[]>) => void): void {
    const busy = startRequest();
    const columnsRequest = new ChimicoXHR(getApiUrl('column/get'));
    columnsRequest.success = r => callback(processApiResponse(r));
    columnsRequest.failure = () => callback(requestFailureApiResponse);
    columnsRequest.completed = () => endRequest(busy);
    columnsRequest.send();
}

/**
 * Adds a column.
 * @param column The column to store.
 * @param callback The callback function to call when the action has completed.
 */
export function addColumn(column: IAdminWorksheetColumn, callback: (result: ApiResult<never>) => void): void {
    const busy = startRequest();
    const addColumnRequest = new ChimicoXHR(getApiUrl('column/add'), 'POST');
    addColumnRequest.success = r => callback(processApiResponse(r));
    addColumnRequest.failure = () => callback(requestFailureApiResponse);
    addColumnRequest.completed = () => endRequest(busy);
    addColumnRequest.send(JSON.stringify(column));
}

/**
 * Edits a worksheet column.
 * @param column The column to edit.
 * @param callback The callback function to call when the action has completed.
 */
export function editColumn(column: IAdminWorksheetColumn, callback: (result: ApiResult<never>) => void): void {
    const busy = startRequest();
    const editColumnRequest = new ChimicoXHR(getApiUrl('column/edit'), 'POST');
    editColumnRequest.success = r => callback(processApiResponse(r));
    editColumnRequest.failure = () => callback(requestFailureApiResponse);
    editColumnRequest.completed = () => endRequest(busy);
    editColumnRequest.send(JSON.stringify(column));
}

/**
 * Moves a worksheet column.
 * @param columnName The name of the column to move.
 * @param direction The direction to move the column in.
 * @param callback The callback function to call when the action has completed.
 */
export function moveColumn(columnName: string, direction: ColumnMoveDirection, callback: (result: ApiResult<never>) => void): void {
    const data = {
        columnName,
        direction,
    };

    const busy = startRequest();
    const moveColumnRequest = new ChimicoXHR(getApiUrl('column/move'), 'POST');
    moveColumnRequest.success = r => callback(processApiResponse(r));
    moveColumnRequest.failure = () => callback(requestFailureApiResponse);
    moveColumnRequest.completed = () => endRequest(busy);
    moveColumnRequest.send(JSON.stringify(data));
}
