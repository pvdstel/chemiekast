import ChimicoXHR from '@/logic/ChimicoXHR';
import { ApiResult, endRequest, getApiUrl, processApiResponse, requestFailureApiResponse, startRequest } from './apiBase';

/** Represents a log entry. */
export interface ILogEntry {
    /** The ID of the log entry. */
    ID: number;
    /** The user ID of the user that triggered the creation of the log entry. */
    UserID: number;
    /** The username of the user that triggered the creation of the log entry at the time the log entry was created. */
    Username: string;
    /** The creation date of the log entry. */
    Date: string;
    /** The role of the user that triggered the creation of the log entry at the time the log entry was created. */
    UserRole: number;
    /** The full name of the user that triggered the creation of the log entry at the time the log entry was created. */
    UserFullName: string;
    /** The activities performed by the user that triggered the creation of the log entry. */
    Activities: ILogEntryActivy[];
}

/** Represents an activity on a log entry. */
export interface ILogEntryActivy {
    /** The date and time of the activity. */
    DateTime: string;
    /** The activity performed by the user. */
    Activity: string;
    /** Additional data associated with the activity. */
    Data: string | null;
}

/**
 * Retrieves log entries.
 * @param limit The number of logs to retrieve.
 * @param callback The callback to call when the request has finished.
 */
export function getLogEntries(limit: number | undefined, callback: (result: ApiResult<Omit<ILogEntry, 'Activities'>[]>) => void): void {
    const busy = startRequest();
    const logsRequest = new ChimicoXHR(limit !== undefined ? getApiUrl(`log/get/limit/${limit}`) : getApiUrl('log/get/*'));
    logsRequest.success = r => callback(processApiResponse(r));
    logsRequest.failure = () => callback(requestFailureApiResponse);
    logsRequest.completed = () => endRequest(busy);
    logsRequest.send();
}

/**
 * Retrieves a log entry.
 * @param limit The number of logs to retrieve.
 * @param callback The callback to call when the request has finished.
 */
export function getLogEntry(id: number, callback: (result: ApiResult<ILogEntry>) => void): void {
    const busy = startRequest();
    const logEntryRequest = new ChimicoXHR(getApiUrl(`log/get/${id}`));
    logEntryRequest.success = r => callback(processApiResponse(r));
    logEntryRequest.failure = () => callback(requestFailureApiResponse);
    logEntryRequest.completed = () => endRequest(busy);
    logEntryRequest.send();
}
