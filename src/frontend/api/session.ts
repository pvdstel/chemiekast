import ChimicoXHR from '@/logic/ChimicoXHR';
import { ApiResult, endRequest, getApiUrl, processApiResponse, requestFailureApiResponse, startRequest } from './apiBase';
import { isSystemColumn } from './column';
import { IDomain } from './domain';
import { IRoleDefinition } from './role';
import { IWorksheetColumn } from './worksheet';

/** A credential value pair. */
export interface ICredentials {
    username: string;
    password: string;
}

/** The User structure for session objects. */
export interface ISessionUser {
    Username: string;
    FirstName: string;
    LastName: string;
    Email: string;
}

/** The worksheet structure for session objects. */
export interface ISessionWorksheet {
    Code: string;
    Name: string;
    CanEdit: boolean;
}

/** An interface representing a session. */
export interface ISession {
    Authenticated: boolean;

    User?: ISessionUser;
    HasDefaultRole?: boolean;
    CanAccessGlobalSettings?: boolean;
    CanAccessDomainSettings?: boolean;
    Columns?: IWorksheetColumn[];
    DomainName?: string;

    Worksheets?: ISessionWorksheet[];

    Roles?: IRoleDefinition[];
    CustomRoles?: IRoleDefinition[];
    CustomRolesEnabled?: boolean;

    CurrentDomain?: number;
    Domains?: IDomain[];
}

type AuthenticatedSession = Required<Pick<ISession, 'Authenticated' | 'User' | 'HasDefaultRole' | 'CanAccessGlobalSettings' | 'CanAccessDomainSettings' | 'Columns' | 'DomainName'>>;

type DefaultRoleSession = AuthenticatedSession & Required<Pick<ISession, 'Worksheets'>>;

type ManagerSession = DefaultRoleSession & Required<Pick<ISession, 'Roles' | 'CustomRoles' | 'CustomRolesEnabled'>>;

type AdminSession = ManagerSession & Required<Pick<ISession, 'CurrentDomain' | 'Domains'>>;

/** Creates a default session object. */
export function defaultSession(): ISession {
    return {
        Authenticated: false,
    };
}

/** Type guard to determine if the session is authenticated. */
export function isSessionAuthorized(session: ISession): session is AuthenticatedSession {
    return session.Authenticated;
}

/** Type guard to determine if the session is a default role session. */
export function isDefaultRoleSession(session: ISession): session is DefaultRoleSession {
    return isSessionAuthorized(session) && session.HasDefaultRole;
}

/** Type guard to determine if the session is a manager session. */
export function isManagerSession(session: ISession): session is ManagerSession {
    return isSessionAuthorized(session) && session.CanAccessDomainSettings;
}

/** Type guard to determine if the session is an admin session. */
export function isAdminSession(session: ISession): session is AdminSession {
    return isSessionAuthorized(session) && session.CanAccessGlobalSettings;
}

export function getNonSystemColumns(session: ISession & AuthenticatedSession): IWorksheetColumn[] {
    const nonSystemColumns = session.Columns.filter(c => !isSystemColumn(c));
    return nonSystemColumns;
}

/**
 * Gets the current session.
 * @param The callback function to call when the data has been received.
 */
export function getSession(callback: (result: ApiResult<ISession>) => void): void {
    const busy = startRequest();
    const sessionRequest = new ChimicoXHR(getApiUrl('session/get'));
    sessionRequest.success = r => callback(processApiResponse(r));
    sessionRequest.failure = () => callback(requestFailureApiResponse);
    sessionRequest.completed = () => endRequest(busy);
    sessionRequest.send();
}

/**
 * Sign in to a new session.
 * @param The callback function to call when the data has been received.
 */
export function signIn(credentials: ICredentials, callback: (result: ApiResult<ISession>) => void): void {
    const busy = startRequest();
    const signInRequest = new ChimicoXHR(getApiUrl('session/signin'), 'POST');
    signInRequest.success = r => callback(processApiResponse(r));
    signInRequest.failure = () => callback(requestFailureApiResponse);
    signInRequest.completed = () => endRequest(busy);
    signInRequest.send(JSON.stringify(credentials));
}

export function signOut(callback: (result: ApiResult<never>) => void): void {
    const busy = startRequest();
    const signOutRequest = new ChimicoXHR(getApiUrl('session/signout'), 'POST');
    signOutRequest.success = r => callback(processApiResponse(r));
    signOutRequest.failure = () => callback(requestFailureApiResponse);
    signOutRequest.completed = () => endRequest(busy);
    signOutRequest.send();
}
