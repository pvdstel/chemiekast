import '@/style/generic.scss';
import compareVersions from 'compare-versions';
import { BrowserInfo, detect } from 'detect-browser';
import React from 'react';
import { getSession, ISession } from '@/api/session';
import Frame from '@/components/Frame';
import { listen as i18nListen } from '@/i18n/i18n';
import SessionContext, { ISessionContext } from '@/logic/sessionContext';
import ErrorPage from './components/ErrorPage/ErrorPage';
import UnsupportedMessage from './components/UnsupportedMessage/UnsupportedMessage';
import { handleUnsuccessfulApiCall } from './logic/api';

const sessionRefreshInterval = 60 * 1000;

interface IAppState {
    session: ISession | null;
    unsupported: boolean;
    error: Error | undefined;
}

export default class App extends React.Component<{}, IAppState> {
    private unregisterListener!: (() => void);
    private sessionRefreshTimeout: number | undefined;

    constructor(props: {}) {
        super(props);

        this.state = {
            session: null,
            unsupported: false,
            error: undefined,
        };
    }

    private updateSession = (next: ISession | null) => this.setState({ session: next });
    private refreshSession = () => {
        clearTimeout(this.sessionRefreshTimeout);
        getSession(d => {
            if (d.isSuccessful()) {
                this.updateSession(d.result);
            } else if (d.isUnsuccessful()) {
                handleUnsuccessfulApiCall(d);
            }
            setTimeout(this.refreshSession, sessionRefreshInterval);
        });
    }

    componentDidMount() {
        this.unregisterListener = i18nListen(() => this.forceUpdate());
        const browser = detect() as BrowserInfo | null;
        if (browser !== null) {
            if (browser.name === 'ie' || (browser.name === 'ios' && compareVersions(browser.version, '10.0.0') < 0)) {
                this.setState({ unsupported: true });
            }
        }
    }

    componentWillUnmount() {
        this.unregisterListener();
    }

    componentDidCatch(error: Error) {
        this.setState({ error: error });
    }

    render() {
        if (this.state.error) {
            return <ErrorPage error={this.state.error} />;
        }

        if (this.state.unsupported) {
            return UnsupportedMessage();
        }

        const sessionContextValue: ISessionContext = {
            current: this.state.session,
            refresh: this.refreshSession,
            update: this.updateSession,
        };

        return (
            <SessionContext.Provider value={sessionContextValue}>
                <Frame />
            </SessionContext.Provider>
        );
    }
}
