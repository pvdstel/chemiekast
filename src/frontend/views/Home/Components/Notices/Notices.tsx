import clsx from 'clsx';
import { sanitize } from 'dompurify';
import marked from 'marked';
import React from 'react';
import { Link } from 'react-router-dom';
import { getNotices, INotice } from '@/api/notice';
import Loader from '@/components/Loader/Loader';
import i18n from '@/i18n/i18n';
import { handleUnsuccessfulApiCall } from '@/logic/api';
import { getAdminPageRoute } from '@/logic/routes';
import SessionContext from '@/logic/sessionContext';
import bulmaShared from '@/style/bulmaShared';
import css from './Notices.scss';

export default function Notices() {
    const [notices, setNotices] = React.useState<INotice[]>();
    const session = React.useContext(SessionContext).current;

    React.useEffect(() => {
        getNotices(r => {
            if (r.isSuccessful()) {
                setNotices(r.result.Notices);
            } else if (r.isUnsuccessful()) {
                handleUnsuccessfulApiCall(r);
            }
        });
    }, []);

    if (!notices) {
        return <Loader />;
    }

    return (
        <div className={css.wrapper}>
            {session && session.CanAccessDomainSettings &&
                <Link className={css.editLink} to={getAdminPageRoute('notices').fill}>{i18n.t('general.edit')}</Link>
            }

            {notices.length === 0 &&
                <p>{i18n.t('admin.notices.noNotices')}</p>
            }

            {notices.map(n => {
                const message = sanitize(marked(n.Message));

                return <div className={clsx(css.notice, n.System && css.system)} key={n.ID}>
                    <div className={bulmaShared.content} dangerouslySetInnerHTML={{ __html: message }} />
                    <p className={css.info}>
                        <span>{n.Poster}</span>
                        <span>{new Date(n.Date).toLocaleString()}</span>
                        {n.System && <span>{i18n.t('admin.notices.systemNotice')}</span>}
                    </p>
                </div>;
            })}
        </div>
    );
}
