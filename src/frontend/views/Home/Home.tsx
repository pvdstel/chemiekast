import clsx from 'clsx';
import React from 'react';
import { Link } from 'react-router-dom';
import { isDefaultRoleSession, isSessionAuthorized } from '@/api/session';
import { SearchBox } from '@/components/Form/Form';
import Content from '@/components/Sidebar/Content';
import Sidebar from '@/components/Sidebar/Sidebar';
import i18n from '@/i18n/i18n';
import { inventoryAllRoute, inventoryRestrictedRoute, inventoryWorksheetRoute } from '@/logic/routes';
import SessionContext from '@/logic/sessionContext';
import bulmaS from '@/style/bulmaShared';
import Notices from './Components/Notices/Notices';

export default function Home() {
    const context = React.useContext(SessionContext);
    const [sidebarOpen, setSidebarOpen] = React.useState(false);

    return (
        <>
            <Sidebar
                title={i18n.t('home.noticesTitle')}
                open={sidebarOpen}
                requestOpen={setSidebarOpen}
                pullButtonIconProps={{ style: 'fas', icon: 'faEnvelopeOpenText' }}
            >
                <Notices />
            </Sidebar>
            {context.current && isSessionAuthorized(context.current) ?
                <Content sidebar>
                    <h1 className={clsx(bulmaS.title, bulmaS.is1)}>{i18n.t('home.mainTitle')}</h1>
                    <p className={bulmaS.subtitle}>{i18n.t('home.subTitle', { replace: { domain: context.current!.DomainName } })}</p>
                    <SearchBox formClassName={bulmaS.field} size='large' buttonColor='link' />
                    {isDefaultRoleSession(context.current)
                        ?
                        <>
                            <Link
                                className={clsx(bulmaS.button, bulmaS.isLink, bulmaS.isOutlined)}
                                to={inventoryAllRoute.fill}
                            >
                                {i18n.t('home.allChemicals')}
                            </Link>
                            <hr />
                            <h3 className={clsx(bulmaS.title, bulmaS.is3)}>{i18n.t('general.worksheet', { count: 0 })}</h3>
                            <ul>
                                {context.current.Worksheets.map(w => <li key={w.Code}>
                                    <Link
                                        key={w.Code}
                                        to={inventoryWorksheetRoute.fill(w.Code)(undefined)}
                                    >
                                        {w.Name}
                                    </Link>
                                </li>)}
                            </ul>
                        </> : <>
                            <Link
                                className={clsx(bulmaS.button, bulmaS.isLink, bulmaS.isOutlined)}
                                to={inventoryRestrictedRoute.fill}
                            >
                                {i18n.t('home.allChemicals')}
                            </Link>
                        </>
                    }
                </Content> : null
            }
        </>
    );
}
