import React from 'react';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';
import { forgotPassword } from '@/api/user';
import Button from '@/components/Button';
import { Control, Field, Input } from '@/components/Form/Form';
import i18n from '@/i18n/i18n';
import { handleUnsuccessfulApiCall } from '@/logic/api';
import { broadcastNotification, createNotification, NotificationType } from '@/logic/notifications';
import { loginRoute } from '@/logic/routes';
import { PseudorandomString } from '@/logic/utilities';
import bulmaShared from '@/style/bulmaShared';
import boxContentCss from '../BoxContent.scss';

export function ForgotPassword() {
    const [username, setUsername] = React.useState('');
    const [isMakingApiCall, setIsMakingApiCall] = React.useState(false);
    const id = React.useMemo(() => PseudorandomString(10, 'Aa'), []);
    const history = useHistory();

    function submitForgot(e: React.SyntheticEvent<HTMLElement>) {
        e.preventDefault();
        if (isMakingApiCall) { return; }

        setIsMakingApiCall(true);
        forgotPassword(username, r => {
            if (r.isSuccessful()) {
                broadcastNotification(createNotification(i18n.t('forgotPassword.sent'), NotificationType.success, 10000));
                history.push('/');
            } else if (r.isUnsuccessful()) {
                handleUnsuccessfulApiCall(r);
                setIsMakingApiCall(false);
            }
        });
    }

    return (
        <div className={boxContentCss.boxContent}>
            <h1 className={boxContentCss.title}>{i18n.t('forgotPassword.title')}</h1>
            <p className={boxContentCss.description}>{i18n.t('forgotPassword.description')}</p>
            <form onSubmit={submitForgot}>
                <Field>
                    <label htmlFor={id} className={bulmaShared.label}>{i18n.t('general.username')}</label>
                    <Control>
                        <Input
                            value={username}
                            onChange={e => setUsername(e.currentTarget.value)}
                            disabled={isMakingApiCall}
                            id={id}
                            type='text'
                            required
                            autoFocus />
                    </Control>
                </Field>
                <Field>
                    <Button color='primary' type='submit' loading={isMakingApiCall}>{i18n.t('forgotPassword.submit')}</Button>
                </Field>
                <p>
                    <Link to={loginRoute.path}>{i18n.t('forgotPassword.back')}</Link>
                </p>
            </form>
        </div>
    );
}
