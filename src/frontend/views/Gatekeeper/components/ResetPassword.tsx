import React from 'react';
import { useHistory } from 'react-router';
import { IPasswordReset, resetPassword } from '@/api/user';
import Button from '@/components/Button';
import { Control, Field, Input } from '@/components/Form/Form';
import i18n from '@/i18n/i18n';
import { handleUnsuccessfulApiCall } from '@/logic/api';
import { broadcastNotification, createNotification, NotificationType } from '@/logic/notifications';
import { PseudorandomString } from '@/logic/utilities';
import bulmaShared from '@/style/bulmaShared';
import boxContentCss from '../BoxContent.scss';

export function ResetPassword() {
    const history = useHistory();
    
    const [newPassword, setNewPassword] = React.useState('');
    const [confirmPassword, setConfirmPassword] = React.useState('');
    const [isMakingApiCall, setIsMakingApiCall] = React.useState(false);
    const ids = React.useMemo(() => Array(2).fill(undefined).map(() => PseudorandomString(10, 'Aa')), []);
    const resetSubmissionData = React.useMemo<Pick<IPasswordReset, 'Username' | 'Token'>>(() => {
        const hashData = window.location.hash.substr(1).split('&');
        return {
            Username: hashData[0],
            Token: hashData[1],
        };
    }, [window.location.hash]);

    function submitReset(e: React.SyntheticEvent<HTMLElement>) {
        e.preventDefault();
        if (isMakingApiCall) { return; }

        setIsMakingApiCall(true);
        resetPassword({ ...resetSubmissionData, NewPassword: newPassword }, r => {
            if (r.isSuccessful()) {
                broadcastNotification(createNotification(i18n.t('resetPassword.complete'), NotificationType.success, 10000));
                history.push('/');
            } else if (r.isUnsuccessful()) {
                handleUnsuccessfulApiCall(r);
                setIsMakingApiCall(false);
            }
        });
    }

    return (
        <div className={boxContentCss.boxContent}>
            <h1 className={boxContentCss.title}>{i18n.t('resetPassword.title')}</h1>
            <p className={boxContentCss.description}>{i18n.t('resetPassword.description')}</p>
            <form onSubmit={submitReset}>
                <Field>
                    <label htmlFor={ids[0]} className={bulmaShared.label}>{i18n.t('resetPassword.newPassword')}</label>
                    <Control>
                        <Input
                            value={newPassword}
                            onChange={e => setNewPassword(e.currentTarget.value)}
                            disabled={isMakingApiCall}
                            id={ids[0]}
                            type='password'
                            required
                            autoFocus
                        />
                    </Control>
                </Field>
                <Field>
                    <label htmlFor={ids[1]} className={bulmaShared.label}>{i18n.t('resetPassword.confirmPassword')}</label>
                    <Control>
                        <Input
                            value={confirmPassword}
                            onChange={e => setConfirmPassword(e.currentTarget.value)}
                            disabled={isMakingApiCall}
                            id={ids[1]}
                            type='password'
                            required
                        />
                    </Control>
                </Field>
                <p>
                    <Button color='primary' type='submit' loading={isMakingApiCall}>{i18n.t('resetPassword.setPassword')}</Button>
                </p>
            </form>
        </div>
    );
}
