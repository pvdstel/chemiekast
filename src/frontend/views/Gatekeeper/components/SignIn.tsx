import clsx from 'clsx';
import React, { useContext, useEffect, useRef, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { ICredentials, signIn } from '@/api/session';
import Button from '@/components/Button';
import { Control, Field, Input } from '@/components/Form/Form';
import { animationDuration } from '@/components/FullscreenLoader/FullscreenLoader';
import i18n from '@/i18n/i18n';
import { hideForegroundLoader, showForegroundLoader } from '@/logic/loading';
import { forgotPasswordRoute } from '@/logic/routes';
import SessionContext from '@/logic/sessionContext';
import bulmaShared from '@/style/bulmaShared';
import boxContentCss from '../BoxContent.scss';

export default function SignIn() {
    const context = useContext(SessionContext);
    const history = useHistory();

    const [showLoginFail, setShowLoginFail] = useState(false);
    const [showDenied, setShowDenied] = useState(false);
    const [showSignedOut, setShowSignedOut] = useState(false);
    const [showInactive, setShowInactive] = useState(false);
    const [showExpired, setShowExpired] = useState(false);
    const [loginFailKey, setLoginFailKey] = useState('');
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [isSigningIn, setIsSigningIn] = useState(false);
    const usernameInput = useRef<HTMLInputElement | null>(null);

    useEffect(function () {
        determineMessages();
        window.addEventListener('hashchange', determineMessages);
        return function () {
            window.removeEventListener('hashChange', determineMessages);
        }
    }, []);

    useEffect(function() {
        usernameInput.current && usernameInput.current.focus()
    }, [isSigningIn])

    function determineMessages() {
        const hash = location.hash.substr(1);
        setShowDenied(location.pathname.length > 1) // exclude leading /
        setShowExpired(hash === 'expired');
        setShowInactive(hash === 'inactive')
        setShowSignedOut(hash === 'signed-out')
        setShowLoginFail(false)
        setLoginFailKey('');
    }

    function submitLogin(e: React.FormEvent) {
        e.preventDefault();
        if (isSigningIn) { return; }

        setIsSigningIn(true);

        const credentials: ICredentials = {
            username: username,
            password: password,
        };

        signIn(credentials, d => {
            if (d.isSuccessful()) {
                window.setTimeout(() => {
                    if (location.pathname.length === 1) { history.replace('/'); }
                    context.update(d.result);
                    hideForegroundLoader();
                }, animationDuration);
                showForegroundLoader();
            } else {
                setShowLoginFail(false);
                setLoginFailKey(d.isUnsuccessful() ? d.error : 'serverError');
                setIsSigningIn(false);
                setUsername('');
                setPassword('');
                setShowDenied(false);
                setShowExpired(false);
                setShowInactive(false);
                setShowSignedOut(false)
                //() => this.usernameInput.current && this.usernameInput.current.focus());
            }
        });
    }

    const nCN = {
        [boxContentCss.notification]: true,
    };

    const success = boxContentCss.isSuccess;
    const warning = boxContentCss.isWarning;
    const danger = boxContentCss.isDanger;

    return (
        <div className={boxContentCss.boxContent}>
            <h1 className={boxContentCss.title}>{CHEMIEKAST_GLOBAL_APP_NAME}</h1>

            {showLoginFail &&
                <div className={clsx(nCN, danger)}>{i18n.t(`api.${loginFailKey}`)}</div>}
            {showDenied &&
                <div className={clsx(nCN, warning)}>{i18n.t('signIn.notSignedIn')}</div>}
            {showSignedOut &&
                <div className={clsx(nCN, success)}>{i18n.t('signIn.signedOut')}</div>}
            {showInactive &&
                <div className={clsx(nCN, warning)}>{i18n.t('signIn.inactive')}</div>}
            {showExpired &&
                <div className={clsx(nCN, warning)}>{i18n.t('signIn.expired')}</div>}

            <h3 className={boxContentCss.subtitle}>{i18n.t('signIn.please')}</h3>
            <form onSubmit={submitLogin}>
                <Field>
                    <label htmlFor='signin-username' className={bulmaShared.label}>{i18n.t('general.username')}</label>
                    <Control>
                        <Input
                            value={username}
                            onChange={e => setUsername(e.currentTarget.value)}
                            disabled={isSigningIn}
                            innerRef={usernameInput}
                            id='signin-username'
                            type='text'
                            required
                            autoFocus />
                    </Control>
                </Field>
                <Field>
                    <label htmlFor='signin-password' className={bulmaShared.label}>{i18n.t('general.password')}</label>
                    <Control>
                        <Input
                            value={password}
                            onChange={e => setPassword(e.currentTarget.value)}
                            disabled={isSigningIn}
                            id='signin-password'
                            type='password'
                            required />
                    </Control>
                </Field>
                <p>
                    <Button color='primary' type='submit' loading={isSigningIn}>{i18n.t('signIn.signIn')}</Button>
                </p>
                <p className={boxContentCss.forgotPassword}>
                    <Link to={forgotPasswordRoute.fill}>{i18n.t('signIn.forgotPassword')}</Link>
                </p>
            </form>
        </div>
    );
}

