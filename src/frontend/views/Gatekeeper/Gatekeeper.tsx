import clsx from 'clsx';
import React, { useEffect, useState } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import FontAwesome from '@/components/FA';
import * as i18n from '@/i18n/i18n';
import { forgotPasswordRoute, resetPasswordRoute } from '@/logic/routes';
import { ForgotPassword } from './components/ForgotPassword';
import { ResetPassword } from './components/ResetPassword';
import SignIn from './components/SignIn';
import css from './Gatekeeper.scss';

function LeftFooter() {
    return (
        <a href={CHEMIEKAST_GLOBAL_SOURCE_REPO} target='_blank' rel='noreferrer noopener'>
            {CHEMIEKAST_GLOBAL_APP_NAME}{' '}
            {CHEMIEKAST_GLOBAL_APP_VERSION}
            {!CHEMIEKAST_GLOBAL_PRODUCTION_BUILD && <small> dev</small>}
        </a>
    );
}

function RightFooter() {
    return (
        <span>
            © Paul van der Stel
        </span>
    );
}

export default function Gatekeeper() {
    const [hiResBackground, setHiResBackground] = useState(false);

    useEffect(() => {
        checkSize();
        window.addEventListener('resize', checkSize);
        return function() {
            window.removeEventListener('resize', checkSize);
        }
    }, []);

    const renderFooter = () => (
        <div className={css.footer}>
            <LeftFooter />
            <RightFooter />
        </div>
    )

    const renderLanguageSwitcher = () => <div className={css.languageSelect}>
        <FontAwesome style='fas' icon='faGlobe' />
        <select
            value={i18n.geti18nLanguage()}
            onChange={e => i18n.changeLanguage(e.currentTarget.value)}>
            {i18n.getLanguages().map(l =>
                <option value={l} key={l}>{i18n.default.t(`language.${l}`)}</option>,
            )}
        </select>
        <FontAwesome style='fas' icon='faChevronDown' />
    </div>

    function checkSize() {
        if (window.innerWidth > 1920 || window.innerHeight > 1080) {
            setHiResBackground(true)
        }
    }

        const backgroundClassName = clsx(
            css.background,
            { [css.hiResBackground]: hiResBackground },
        );

        return (
            <div>
                <div className={backgroundClassName} />
                <div className={css.boxWrapper}>
                    <div className={css.box}>
                        <BrowserRouter>
                            <>
                                <Switch>
                                    <Route path={forgotPasswordRoute.path} component={ForgotPassword} />
                                    <Route path={resetPasswordRoute.path} component={ResetPassword} />
                                    <SignIn />
                                </Switch>
                            </>
                        </BrowserRouter>
                        {renderLanguageSwitcher()}
                        {renderFooter()}
                    </div>
                </div>
                {renderFooter()}
            </div>
        );
}

