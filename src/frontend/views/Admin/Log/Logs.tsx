import React from 'react';
import { getLogEntries, getLogEntry, ILogEntry } from '@/api/logs';
import { isAdminSession } from '@/api/session';
import FontAwesome from '@/components/FA';
import { PageLoader } from '@/components/Loader/Loader';
import Modal, { DefaultActions } from '@/components/Modal/Modal';
import Table from '@/components/Table/Table';
import i18n from '@/i18n/i18n';
import { handleUnsuccessfulApiCall } from '@/logic/api';
import SessionContext from '@/logic/sessionContext';
import bulmaShared from '@/style/bulmaShared';
import { AdminError } from '../AdminError';
import css from './Logs.scss';

export default function Logs() {
    const [logEntries, setLogEntries] = React.useState<Omit<ILogEntry, 'Activities'>[]>([]);
    const [logEntriesLoaded, setLogEntriesLoaded] = React.useState<number | undefined>(15);
    const [displaying, setDisplaying] = React.useState<number>(25);
    const [viewingLogEntry, setViewingLogEntry] = React.useState<ILogEntry>();

    const session = React.useContext(SessionContext).current;
    if (session === null || !isAdminSession(session)) {
        return <AdminError />;
    }

    React.useEffect(() => {
        getLogEntries(logEntriesLoaded, r => {
            if (r.isSuccessful()) {
                setLogEntries(r.result);
            } else if (r.isUnsuccessful()) {
                handleUnsuccessfulApiCall(r);
            }
        });
    }, [logEntriesLoaded, session.DomainName]);

    function loadViewingLogEntry(id: number) {
        getLogEntry(id, r => {
            if (r.isSuccessful()) {
                setViewingLogEntry(r.result);
            } else if (r.isUnsuccessful()) {
                handleUnsuccessfulApiCall(r);
            }
        });
    }

    const tableHead = React.useMemo(() => (
        <thead>
            <tr>
                <th className={bulmaShared.iconColumn}></th>
                <th>{i18n.t('admin.logs.fields.username')}</th>
                <th>{i18n.t('admin.logs.fields.date')}</th>
                <th>{i18n.t('admin.logs.fields.userFullName')}</th>
            </tr>
        </thead>
    ), [i18n.language]);

    const tableBody = React.useMemo(() => logEntries.slice(0, displaying).map(logEntry => {
        return (
            <tr key={logEntry.ID}>
                <td className={bulmaShared.buttonCell}>
                    <a onClick={e => { e.preventDefault(); loadViewingLogEntry(logEntry.ID); }} href='#'>
                        <FontAwesome style='fas' icon='faEye' fixedWidth />
                    </a>
                </td>
                <td>{logEntry.Username}</td>
                <td>{logEntry.Date}</td>
                <td>{logEntry.UserFullName}</td>
            </tr>
        );
    }), [logEntries, displaying]);

    return (
        <div className={css.container}>
            <Table
                tableProps={{ className: css.table }}
                head={tableHead}
                fullWidth
                body={<tbody>{tableBody}</tbody>}
            />
            {logEntries.length === 0 && <PageLoader message={i18n.t('general.loading')} />}
            {logEntriesLoaded !== undefined &&
                <a
                    onClick={e => { e.preventDefault(); setLogEntriesLoaded(undefined); }}
                    href='#'
                >
                    {i18n.t('general.loadMore')}
                </a>
            }
            {logEntriesLoaded === undefined && displaying < logEntries.length &&
                <a
                    onClick={e => { e.preventDefault(); setDisplaying(displaying + 10); }}
                    href='#'
                >
                    {i18n.t('general.loadMore')}
                </a>
            }
            <Modal
                actions={[DefaultActions.getCloseAction()]}
                open={viewingLogEntry !== undefined}
                requestClose={() => setViewingLogEntry(undefined)}
                title={i18n.t('admin.logs.details')}
            >
                <Table
                    fullWidth
                    body={<tbody>
                        <tr>
                            <th>{i18n.t('admin.logs.fields.id')}</th>
                            <td>{viewingLogEntry && viewingLogEntry.ID}</td>
                        </tr>
                        <tr>
                            <th>{i18n.t('admin.logs.fields.userId')}</th>
                            <td>{viewingLogEntry && viewingLogEntry.UserID}</td>
                        </tr>
                        <tr>
                            <th>{i18n.t('admin.logs.fields.username')}</th>
                            <td>{viewingLogEntry && viewingLogEntry.Username}</td>
                        </tr>
                        <tr>
                            <th>{i18n.t('admin.logs.fields.date')}</th>
                            <td>{viewingLogEntry && viewingLogEntry.Date}</td>
                        </tr>
                        <tr>
                            <th>{i18n.t('admin.logs.fields.userRole')}</th>
                            <td>{viewingLogEntry && viewingLogEntry.UserRole}</td>
                        </tr>
                        <tr>
                            <th>{i18n.t('admin.logs.fields.userFullName')}</th>
                            <td>{viewingLogEntry && viewingLogEntry.UserFullName}</td>
                        </tr>
                        <tr>
                            <th>{i18n.t('admin.logs.fields.numberOfActivities')}</th>
                            <td>{viewingLogEntry && viewingLogEntry.Activities.length}</td>
                        </tr>
                    </tbody>}
                />
                <Table
                    fullWidth
                    head={<thead>
                        <tr>
                            <th>{i18n.t('admin.logs.fields.date')}</th>
                            <th>{i18n.t('admin.logs.fields.activity')}</th>
                            <th>{i18n.t('admin.logs.fields.data')}</th>
                        </tr>
                    </thead>}
                    body={<tbody>
                        {viewingLogEntry && viewingLogEntry.Activities.map(l =>
                            <tr key={l.Activity + l.DateTime}>
                                <td>{l.DateTime}</td>
                                <td>{l.Activity}</td>
                                <td>{l.Data}</td>
                            </tr>,
                        )}
                    </tbody>}
                />
            </Modal>
        </div >
    );
}
