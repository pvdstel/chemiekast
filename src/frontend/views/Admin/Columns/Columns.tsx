import clsx from 'clsx';
import React from 'react';
import { ApiResult } from '@/api/apiBase';
import { addColumn, ColumnMoveDirection, editColumn, emptyAdminColumn, getColumns, IAdminWorksheetColumn, moveColumn } from '@/api/column';
import Button from '@/components/Button';
import FontAwesome from '@/components/FA';
import Checkbox from '@/components/Form/Checkbox';
import { Control, Field, Input } from '@/components/Form/Form';
import { PageLoader } from '@/components/Loader/Loader';
import Modal, { DefaultActions } from '@/components/Modal/Modal';
import Table from '@/components/Table/Table';
import i18n from '@/i18n/i18n';
import { handleUnsuccessfulApiCall } from '@/logic/api';
import SessionContext from '@/logic/sessionContext';
import { PseudorandomString } from '@/logic/utilities';
import bulmaShared from '@/style/bulmaShared';
import css from './Columns.scss';

type EditingColumn = IAdminWorksheetColumn & { new?: true };

const columnOptionTrue = <FontAwesome style='fas' icon='faCheck' />;
const columnOptionFalse = <FontAwesome
    elementProps={{ className: css.columnOptionFalse }}
    style='fas'
    icon='faTimes'
/>;

export default function Columns() {
    const [columns, setColumns] = React.useState<IAdminWorksheetColumn[]>([]);
    const [editingColumn, setEditingColumn] = React.useState<EditingColumn>();
    const [updateCounter, setUpdateCounter] = React.useState(0);
    const [isSaving, setIsSaving] = React.useState(false);
    const ids = React.useMemo(() => Array(2).fill(undefined).map(() => PseudorandomString(10, 'Aa')), []);
    const sessionContext = React.useContext(SessionContext);

    React.useEffect(() => {
        getColumns(r => {
            if (r.isSuccessful()) {
                setColumns(r.result);
            } else if (r.isUnsuccessful()) {
                handleUnsuccessfulApiCall(r);
            }
        });
    }, [updateCounter]);

    function registerChange<TField extends keyof IAdminWorksheetColumn>(field: TField, value: IAdminWorksheetColumn[TField]) {
        setEditingColumn({ ...editingColumn!, [field]: value });
    }

    function saveColumn(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault();
        if (editingColumn) {
            setIsSaving(true);
            const apiResultHandler = (r: ApiResult<never>) => {
                if (r.isSuccessful()) {
                    setEditingColumn(undefined);
                    setUpdateCounter(updateCounter + 1);
                    sessionContext.refresh();
                } else if (r.isUnsuccessful()) {
                    handleUnsuccessfulApiCall(r);
                }
                setIsSaving(false);
            };
            if (editingColumn.new) {
                addColumn(editingColumn, apiResultHandler);
            } else {
                editColumn(editingColumn, apiResultHandler);
            }
        }
        return false;
    }

    function moveColumnFunction(columnName: string, direction: ColumnMoveDirection) {
        return (e: React.SyntheticEvent<HTMLElement>) => {
            e.preventDefault();
            setIsSaving(true);
            moveColumn(columnName, direction, r => {
                if (r.isSuccessful()) {
                    setUpdateCounter(updateCounter + 1);
                    sessionContext.refresh();
                } else if (r.isUnsuccessful()) {
                    handleUnsuccessfulApiCall(r);
                }
                setIsSaving(false);
            });
        };
    }

    const tableHead = React.useMemo(() => (
        <thead>
            <tr>
                <th className={bulmaShared.iconColumn} colSpan={3}></th>
                <th>{i18n.t('admin.columns.field.uiName')}</th>
                <th>{i18n.t('admin.columns.field.visible')}</th>
                <th>{i18n.t('admin.columns.field.searchable')}</th>
                <th>{i18n.t('admin.columns.field.urls')}</th>
                <th>{i18n.t('admin.columns.field.markdown')}</th>
                <th>{i18n.t('admin.columns.field.formula')}</th>
            </tr>
        </thead>
    ), [i18n.language]);

    const tableBody = React.useMemo(() => columns.map(column => {
        return (
            <tr key={column.Name}>
                <td className={bulmaShared.buttonCell}>
                    {!column.IsSystemColumn && <a href='#' onClick={e => { e.preventDefault(); setEditingColumn(column); }}>
                        <FontAwesome style='fas' icon='faPen' fixedWidth />
                    </a>}
                </td>
                <td className={bulmaShared.buttonCell}>
                    {!column.IsSystemColumn && <a href='#' onClick={moveColumnFunction(column.Name, ColumnMoveDirection.Up)}>
                        <FontAwesome style='fas' icon='faChevronUp' fixedWidth />
                    </a>}
                </td>
                <td className={bulmaShared.buttonCell}>
                    {!column.IsSystemColumn && <a target='_blank' onClick={moveColumnFunction(column.Name, ColumnMoveDirection.Down)}>
                        <FontAwesome style='fas' icon='faChevronDown' fixedWidth />
                    </a>}
                </td>
                <td>{column.UIName}</td>
                <td>{column.IsVisible ? columnOptionTrue : columnOptionFalse}</td>
                <td>{column.IsSearchable ? columnOptionTrue : columnOptionFalse}</td>
                <td>{column.IsUrl ? columnOptionTrue : columnOptionFalse}</td>
                <td>{column.IsMarkdown ? columnOptionTrue : columnOptionFalse}</td>
                <td>{column.IsFormula ? columnOptionTrue : columnOptionFalse}</td>
            </tr>
        );
    }), [columns]);

    return (
        <div>
            <Table
                tableProps={{ className: css.table }}
                head={tableHead}
                fullWidth
                body={<tbody>{tableBody}</tbody>}
            />
            {columns.length === 0 && <PageLoader message={i18n.t('general.loading')} />}
            <Button color='primary' onClick={() => setEditingColumn({ ...emptyAdminColumn(), new: true })}>
                {i18n.t('admin.columns.addColumn')}
            </Button>
            <Modal
                open={editingColumn !== undefined}
                requestClose={() => setEditingColumn(undefined)}
                title={i18n.t('admin.worksheets.editWorksheet')}
                actions={[
                    DefaultActions.getSaveAction(undefined, { type: 'submit', disabled: isSaving }),
                    DefaultActions.getCancelAction({ disabled: isSaving }),
                ]}
                onSubmit={saveColumn}
            >
                <Field>
                    <label className={clsx(bulmaShared.label, bulmaShared.requiredField)} id={ids[0]}>{i18n.t('admin.columns.field.name')}</label>
                    <Control>
                        <Input id={ids[0]} value={editingColumn !== undefined ? editingColumn.Name : ''} disabled={!editingColumn || !editingColumn.new || isSaving}
                            onChange={e => registerChange('Name', e.currentTarget.value)} />
                    </Control>
                </Field>
                <Field>
                    <label className={clsx(bulmaShared.label, bulmaShared.requiredField)} id={ids[1]}>{i18n.t('admin.columns.field.uiName')}</label>
                    <Control>
                        <Input id={ids[1]} value={editingColumn !== undefined ? editingColumn.UIName : ''} disabled={isSaving}
                            onChange={e => registerChange('UIName', e.currentTarget.value)} />
                    </Control>
                </Field>
                <hr />
                <Field>
                    <Checkbox checked={editingColumn !== undefined && editingColumn.IsVisible} onChange={e => registerChange('IsVisible', e.currentTarget.checked)} disabled={isSaving}>
                        {i18n.t('admin.columns.field.visible')}
                    </Checkbox>
                </Field>
                <Field>
                    <Checkbox checked={editingColumn !== undefined && editingColumn.IsSearchable} onChange={e => registerChange('IsSearchable', e.currentTarget.checked)} disabled={isSaving}>
                        {i18n.t('admin.columns.field.searchable')}
                    </Checkbox>
                </Field>
                <Field>
                    <Checkbox checked={editingColumn !== undefined && editingColumn.IsUrl} onChange={e => registerChange('IsUrl', e.currentTarget.checked)} disabled={isSaving}>
                        {i18n.t('admin.columns.field.urls')}
                    </Checkbox>
                </Field>
                <Field>
                    <Checkbox checked={editingColumn !== undefined && editingColumn.IsMarkdown} onChange={e => registerChange('IsMarkdown', e.currentTarget.checked)} disabled={isSaving}>
                        {i18n.t('admin.columns.field.markdown')}
                    </Checkbox>
                </Field>
                <Field>
                    <Checkbox checked={editingColumn !== undefined && editingColumn.IsFormula} onChange={e => registerChange('IsFormula', e.currentTarget.checked)} disabled={isSaving}>
                        {i18n.t('admin.columns.field.formula')}
                    </Checkbox>
                </Field>
            </Modal>
        </div >
    );
}
