import clsx from 'clsx';
import React from 'react';
import i18n from '@/i18n/i18n';
import bulmaShared from '@/style/bulmaShared';

export default function About() {
    return (
        <>
            <h2 className={clsx(bulmaShared.title)}>
                {CHEMIEKAST_GLOBAL_APP_NAME}{' '}
                {CHEMIEKAST_GLOBAL_APP_VERSION}
                {!CHEMIEKAST_GLOBAL_PRODUCTION_BUILD && <small> dev</small>}
            </h2>
            <p className={clsx(bulmaShared.subtitle)}>
                {i18n.t('admin.about.by')}
            </p>
            <p></p>
            <p>{i18n.t('admin.about.source')} <a href={CHEMIEKAST_GLOBAL_SOURCE_REPO} target='_blank' rel='noreferrer noopener'>{i18n.t('admin.about.repo')}</a></p>
            <p>{i18n.t('admin.about.version', { v: CHEMIEKAST_GLOBAL_APP_VERSION })}</p>
            <p>{i18n.t('admin.about.buildDate', { date: CHEMIEKAST_GLOBAL_APP_BUILD_DATE, interpolation: { escapeValue: false } })}</p>
            <p>{i18n.t(`admin.about.prod${CHEMIEKAST_GLOBAL_PRODUCTION_BUILD ? 'Y' : 'N'}`)}</p>
            {CHEMIEKAST_GLOBAL_HOST &&
                <p>{i18n.t('admin.about.apiHost', { host: CHEMIEKAST_GLOBAL_HOST, interpolation: { escapeValue: false } })}</p>
            }
        </>
    );
}
