import clsx from 'clsx';
import React from 'react';
import { ApiResult } from '@/api/apiBase';
import { addDomain, deleteDomain, editDomain, emptyDomain, getDomains, IDomain } from '@/api/domain';
import Button from '@/components/Button';
import FontAwesome from '@/components/FA';
import { Control, Field, Input } from '@/components/Form/Form';
import { PageLoader } from '@/components/Loader/Loader';
import Modal, { DefaultActions } from '@/components/Modal/Modal';
import Table from '@/components/Table/Table';
import i18n from '@/i18n/i18n';
import { handleUnsuccessfulApiCall } from '@/logic/api';
import { handleSwitchDomain } from '@/logic/domains';
import SessionContext from '@/logic/sessionContext';
import { PseudorandomString } from '@/logic/utilities';
import bulmaShared from '@/style/bulmaShared';
import css from './Domains.scss';

export default function Domains() {
    const [domains, setDomains] = React.useState<IDomain[]>([]);
    const [editingDomain, setEditingDomain] = React.useState<IDomain>();
    const [deletingDomain, setDeletingDomain] = React.useState<IDomain>();
    const [deleteNameConfirm, setDeleteNameConfirm] = React.useState<string>('');
    const [updateCounter, setUpdateCounter] = React.useState(0);
    const ids = React.useMemo(() => Array(1).fill(undefined).map(() => PseudorandomString(10, 'Aa')), []);
    const { refresh: refreshSession } = React.useContext(SessionContext);

    React.useEffect(() => {
        getDomains(r => {
            if (r.isSuccessful()) {
                setDomains(r.result);
            } else if (r.isUnsuccessful()) {
                handleUnsuccessfulApiCall(r);
            }
        });
    }, [updateCounter]);

    function setDomainStateFunction(domain: IDomain | undefined, stateSetter: React.Dispatch<IDomain | undefined>) {
        return (e?: React.MouseEvent<HTMLElement>) => {
            if (e) { e.preventDefault(); }
            stateSetter(domain);
            setDeleteNameConfirm('');
        };
    }

    function saveDomain(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault();
        if (editingDomain !== undefined) {
            const apiResultHandler = (r: ApiResult<never>) => {
                if (r.isSuccessful()) {
                    setDomainStateFunction(undefined, setEditingDomain)();
                    setUpdateCounter(updateCounter + 1);
                    refreshSession();
                } else if (r.isUnsuccessful()) {
                    handleUnsuccessfulApiCall(r);
                }
            };
            if (editingDomain.ID) {
                editDomain(editingDomain, apiResultHandler);
            } else {
                addDomain(editingDomain, apiResultHandler);
            }
        }
        return false;
    }

    function domainDeletionConfirmed(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault();
        if (deletingDomain) {
            if (deletingDomain.Name === deleteNameConfirm) {
                deleteDomain(deletingDomain.ID, r => {
                    if (r.isSuccessful()) {
                        setUpdateCounter(updateCounter + 1);
                        refreshSession();
                    } else if (r.isUnsuccessful()) {
                        handleUnsuccessfulApiCall(r);
                    }
                });
                setDeletingDomain(undefined);
            }
        }
    }

    const tableHead = React.useMemo(() => (
        <thead>
            <tr>
                <th className={bulmaShared.iconColumn} colSpan={2}></th>
                <th>{i18n.t('admin.domains.name')}</th>
                <th className={bulmaShared.iconColumn}></th>
            </tr>
        </thead>
    ), [i18n.language]);

    const tableBody = React.useMemo(() => domains.map(domain => {
        return (
            <tr key={domain.ID}>
                <td className={bulmaShared.buttonCell}>
                    <a onClick={setDomainStateFunction(domain, setEditingDomain)} href='#'>
                        <FontAwesome style='fas' icon='faPen' fixedWidth />
                    </a>
                </td>
                <td className={bulmaShared.buttonCell}>
                    <a onClick={setDomainStateFunction(domain, setDeletingDomain)} href='#'>
                        <FontAwesome style='fas' icon='faTrash' fixedWidth />
                    </a>
                </td>
                <td>{domain.Name}</td>
                <td className={bulmaShared.buttonCell}>
                    <a onClick={e => { e.preventDefault(); handleSwitchDomain(domain.ID, refreshSession); }} href='#'>
                        <FontAwesome style='fas' icon='faExternalLinkAlt' fixedWidth />
                    </a>
                </td>
            </tr>
        );
    }), [domains]);

    return (
        <div className={css.container}>
            <Table
                tableProps={{ className: css.table }}
                head={tableHead}
                fullWidth
                body={<tbody>{tableBody}</tbody>}
            />
            {domains.length === 0 && <PageLoader message={i18n.t('general.loading')} />}
            <Button color='primary' onClick={() => setEditingDomain(emptyDomain())}>{i18n.t('admin.domains.addDomain')}</Button>
            <Modal
                open={editingDomain !== undefined}
                requestClose={setDomainStateFunction(undefined, setEditingDomain)}
                title={i18n.t('admin.domains.editDomain')}
                actions={[DefaultActions.getSaveAction(undefined, { type: 'submit' }), DefaultActions.getCancelAction()]}
                onSubmit={saveDomain}
            >
                <Field>
                    <label className={clsx(bulmaShared.label, bulmaShared.requiredField)} id={ids[0]}>{i18n.t('admin.domains.name')}</label>
                    <Control>
                        <Input id={ids[0]} value={editingDomain !== undefined ? editingDomain.Name : ''}
                            onChange={e => setEditingDomain({ ...editingDomain!, Name: e.currentTarget.value })} />
                    </Control>
                </Field>
            </Modal>
            <Modal
                open={deletingDomain !== undefined}
                requestClose={setDomainStateFunction(undefined, setDeletingDomain)}
                title={i18n.t('admin.domains.deleteDomain')}
                actions={[
                    DefaultActions.getDeleteAction(undefined, { disabled: deletingDomain && deletingDomain.Name !== deleteNameConfirm, type: 'submit' }),
                    DefaultActions.getCancelAction(),
                ]}
                onSubmit={domainDeletionConfirmed}
            >
                <p>{i18n.t('admin.domains.deletionConfirmation', { name: deletingDomain ? deletingDomain.Name : '' })}</p>
                <div className={clsx(bulmaShared.message, bulmaShared.isDanger, css.warningMessage)}>
                    <div className={bulmaShared.messageHeader}>
                        <p>{i18n.t('general.warning')}</p>
                    </div>
                    <div className={clsx(bulmaShared.messageBody, css.warningBody)}>
                        <p>{i18n.t('admin.domains.deletionWarning')}</p>
                        <p>{i18n.t('admin.domains.nameConfirm')}</p>
                        <Input value={deleteNameConfirm}
                            onChange={e => setDeleteNameConfirm(e.currentTarget.value)} />
                    </div>
                </div>
            </Modal>
        </div >
    );
}
