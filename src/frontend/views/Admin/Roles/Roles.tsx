import clsx from 'clsx';
import React from 'react';
import { getRoles, IRole } from '@/api/role';
import { isSessionAuthorized } from '@/api/session';
import { PageLoader } from '@/components/Loader/Loader';
import i18n from '@/i18n/i18n';
import { handleUnsuccessfulApiCall } from '@/logic/api';
import SessionContext from '@/logic/sessionContext';
import bulmaShared from '@/style/bulmaShared';
import { AdminError } from '../AdminError';
import Role from './Role';

export default function Roles() {
    const [roles, setRoles] = React.useState<IRole[]>();
    const [updateCounter, setUpdateCounter] = React.useState(0);

    const sessionContext = React.useContext(SessionContext);
    const session = sessionContext.current;
    if (session === null || !isSessionAuthorized(session)) {
        return <AdminError />;
    }

    React.useEffect(() => {
        getRoles(r => {
            if (r.isSuccessful()) {
                setRoles(r.result);
                sessionContext.refresh();
            } else if (r.isUnsuccessful()) {
                handleUnsuccessfulApiCall(r);
            }
        });
    }, [updateCounter, session.DomainName]);

    if (roles === undefined) {
        return <PageLoader message={i18n.t('general.loading')} />;
    }

    return (
        <div className={clsx(bulmaShared.columns, bulmaShared.isMultiline)}>
            {roles.length === 0 &&
                <p>{i18n.t('admin.roles.noRoles')}</p>
            }

            {roles.map(r => <div className={clsx(bulmaShared.column, bulmaShared.isHalf, bulmaShared.isFullMobile)}>
                <Role key={r.ID} role={r} onUpdated={() => setUpdateCounter(updateCounter + 1)} />
            </div>)}
        </div>
    );
}
