import clsx from 'clsx';
import React from 'react';
import { deleteRole, IRole, removeChemicalFromRole, renameRole } from '@/api/role';
import Button, { Buttons } from '@/components/Button';
import FontAwesome from '@/components/FA';
import { Control, Field, Input } from '@/components/Form/Form';
import Modal, { DefaultActions } from '@/components/Modal/Modal';
import Table from '@/components/Table/Table';
import i18n from '@/i18n/i18n';
import { handleUnsuccessfulApiCall } from '@/logic/api';
import { inventoryChemicalRoute } from '@/logic/routes';
import SessionContext from '@/logic/sessionContext';
import { PseudorandomString } from '@/logic/utilities';
import bulmaShared from '@/style/bulmaShared';

export interface IRoleProps {
    role: IRole;
    onUpdated: () => void;
}

export default function Role(props: IRoleProps) {
    const [editingName, setEditingName] = React.useState<string>();
    const [isDeleting, setIsDeleting] = React.useState(false);
    const [isPerformingApiOperation, setIsPerformingApiOperation] = React.useState(false);
    const sessionContext = React.useContext(SessionContext);
    const renameFieldId = React.useMemo(() => PseudorandomString(10, 'Aa'), []);

    function removeHandler(chemicalId: number) {
        return (e: React.MouseEvent<HTMLElement>) => {
            e.preventDefault();
            removeChemicalFromRole(chemicalId, props.role.ID, r => {
                if (r.isSuccessful()) {
                    props.onUpdated();
                } else if (r.isUnsuccessful()) {
                    handleUnsuccessfulApiCall(r);
                }
            });
        };
    }

    function submitRename(e: React.FormEvent<HTMLFormElement>) {
        if (!editingName || isPerformingApiOperation) {
            return;
        }
        e.preventDefault();

        setIsPerformingApiOperation(true);
        renameRole({ ID: props.role.ID, Name: editingName }, r => {
            if (r.isSuccessful()) {
                setEditingName(undefined);
                props.onUpdated();
                sessionContext.refresh();
            } else if (r.isUnsuccessful()) {
                handleUnsuccessfulApiCall(r);
            }
            setIsPerformingApiOperation(false);
        });

        return false;
    }

    function deleteConfirm() {
        setIsDeleting(false);
        setIsPerformingApiOperation(true);

        deleteRole(props.role.ID, r => {
            setIsPerformingApiOperation(false);
            if (r.isSuccessful()) {
                props.onUpdated();
                sessionContext.refresh();
            } else if (r.isUnsuccessful()) {
                handleUnsuccessfulApiCall(r);
            }
        });
    }

    return <>
        <div className={clsx(bulmaShared.panel)}>
            <div className={bulmaShared.panelHeading}>
                {props.role.Name}
            </div>
            <div className={bulmaShared.panelBlock}>
                {props.role.Chemicals.length > 0
                    ? <Table
                        fullWidth
                        head={<thead>
                            <tr>
                                <th className={bulmaShared.iconColumn}></th>
                                <th>{i18n.t('inventory.properties.name')}</th>
                                <th className={bulmaShared.iconColumn}></th>
                            </tr>
                        </thead>}
                        body={<tbody>
                            {props.role.Chemicals.map(c => (
                                <tr key={c.ID}>
                                    <td className={bulmaShared.buttonCell}>
                                        <a onClick={removeHandler(c.ID)}>
                                            <FontAwesome style='fas' icon='faTimes' fixedWidth />
                                        </a>
                                    </td>
                                    <td>{c.Name}</td>
                                    <td className={bulmaShared.buttonCell}>
                                        <a href={inventoryChemicalRoute.fill(c.ID.toString())} target='_blank'>
                                            <FontAwesome style='fas' icon='faExternalLinkAlt' fixedWidth />
                                        </a>
                                    </td>
                                </tr>
                            ))}
                        </tbody>}
                    />
                    : <p>{i18n.t('admin.roles.noChemicals')}</p>
                }
            </div>
            <div className={bulmaShared.panelBlock}>
                <Buttons>
                    <Button color='link' outlined onClick={() => setEditingName(props.role.Name)}>{i18n.t('general.rename')}</Button>
                    <Button color='danger' outlined onClick={() => setIsDeleting(true)}>{i18n.t('general.delete')}</Button>
                </Buttons>
            </div>
        </div>
        <Modal
            open={editingName !== undefined}
            requestClose={() => setEditingName(undefined)}
            title={i18n.t('admin.roles.renameRole')}
            actions={[
                DefaultActions.getSaveAction(undefined, { type: 'submit', disabled: isPerformingApiOperation }),
                DefaultActions.getCancelAction({ disabled: isPerformingApiOperation }),
            ]}
            onSubmit={submitRename}
        >
            <Field>
                <Control>
                    <label className={clsx(bulmaShared.label, bulmaShared.requiredField)} htmlFor={renameFieldId}>{i18n.t('admin.roles.newName')}</label>
                    <Input
                        value={editingName || ''}
                        onChange={e => setEditingName(e.currentTarget.value)}
                        id={renameFieldId}
                        disabled={isPerformingApiOperation}
                        required
                    />
                </Control>
            </Field>
        </Modal>
        <Modal
            open={isDeleting}
            requestClose={() => setIsDeleting(false)}
            title={i18n.t('admin.roles.deleteRole')}
            actions={[
                DefaultActions.getDeleteAction(deleteConfirm),
                DefaultActions.getCancelAction(),
            ]}
        >
            <p>{i18n.t('admin.roles.deletionConfirmation', { roleName: props.role.Name })}</p>
        </Modal>
    </>;
}
