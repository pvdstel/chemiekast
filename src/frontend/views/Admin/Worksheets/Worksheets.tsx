import clsx from 'clsx';
import React from 'react';
import { ApiResult } from '@/api/apiBase';
import { isSessionAuthorized } from '@/api/session';
import { addWorksheet, deleteWorksheet, editWorksheet, emptyWorksheet, getWorksheets, IWorksheet } from '@/api/worksheet';
import Button from '@/components/Button';
import FontAwesome from '@/components/FA';
import { Control, Field, Input } from '@/components/Form/Form';
import { PageLoader } from '@/components/Loader/Loader';
import Modal, { DefaultActions } from '@/components/Modal/Modal';
import Table from '@/components/Table/Table';
import i18n from '@/i18n/i18n';
import { handleUnsuccessfulApiCall } from '@/logic/api';
import { inventoryWorksheetRoute } from '@/logic/routes';
import SessionContext from '@/logic/sessionContext';
import { PseudorandomString } from '@/logic/utilities';
import bulmaShared from '@/style/bulmaShared';
import { AdminError } from '../AdminError';
import css from './Worksheets.scss';

export default function Worksheets() {
    const [worksheets, setWorksheets] = React.useState<IWorksheet[]>();
    const [editingWorksheet, setEditingWorksheet] = React.useState<IWorksheet>();
    const [deletingWorksheet, setDeletingWorksheet] = React.useState<IWorksheet>();
    const [deleteNameConfirm, setDeleteNameConfirm] = React.useState<string>('');
    const [updateCounter, setUpdateCounter] = React.useState(0);
    const ids = React.useMemo(() => Array(2).fill(undefined).map(() => PseudorandomString(10, 'Aa')), []);

    const sessionContext = React.useContext(SessionContext);
    const session = sessionContext.current;
    if (session === null || !isSessionAuthorized(session)) {
        return <AdminError />;
    }

    React.useEffect(() => {
        getWorksheets(r => {
            if (r.isSuccessful()) {
                setWorksheets(r.result);
            } else if (r.isUnsuccessful()) {
                handleUnsuccessfulApiCall(r);
            }
        });
    }, [updateCounter, session.DomainName]);

    function setWorksheetStateFunction(worksheet: IWorksheet | undefined, stateSetter: React.Dispatch<IWorksheet | undefined>) {
        return (e?: React.MouseEvent<HTMLElement>) => {
            if (e) { e.preventDefault(); }
            stateSetter(worksheet);
            setDeleteNameConfirm('');
        };
    }

    function saveWorksheet(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault();
        if (editingWorksheet !== undefined) {
            const apiResultHandler = (r: ApiResult<never>) => {
                if (r.isSuccessful()) {
                    setWorksheetStateFunction(undefined, setEditingWorksheet)();
                    setUpdateCounter(updateCounter + 1);
                    sessionContext.refresh();
                } else if (r.isUnsuccessful()) {
                    handleUnsuccessfulApiCall(r);
                }
            };
            if (editingWorksheet.ID) {
                editWorksheet(editingWorksheet, apiResultHandler);
            } else {
                addWorksheet(editingWorksheet, apiResultHandler);
            }
        }
        return false;
    }

    function worksheetDeletionConfirmed(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault();
        if (deletingWorksheet) {
            if (deletingWorksheet.Name === deleteNameConfirm) {
                deleteWorksheet(deletingWorksheet.ID, r => {
                    if (r.isSuccessful()) {
                        setUpdateCounter(updateCounter + 1);
                    } else if (r.isUnsuccessful()) {
                        handleUnsuccessfulApiCall(r);
                    }
                });
                setDeletingWorksheet(undefined);
            }
        }
    }

    const tableHead = React.useMemo(() => (
        <thead>
            <tr>
                <th className={bulmaShared.iconColumn} colSpan={2}></th>
                <th>{i18n.t('admin.worksheets.code')}</th>
                <th>{i18n.t('admin.worksheets.name')}</th>
                <th className={bulmaShared.iconColumn}></th>
            </tr>
        </thead>
    ), [i18n.language]);

    const tableBody = React.useMemo(() => worksheets && worksheets.map(worksheet => {
        return (
            <tr key={worksheet.ID}>
                <td className={bulmaShared.buttonCell}>
                    <a onClick={setWorksheetStateFunction(worksheet, setEditingWorksheet)} href='#'>
                        <FontAwesome style='fas' icon='faPen' fixedWidth />
                    </a>
                </td>
                <td className={bulmaShared.buttonCell}>
                    <a onClick={setWorksheetStateFunction(worksheet, setDeletingWorksheet)} href='#'>
                        <FontAwesome style='fas' icon='faTrash' fixedWidth />
                    </a>
                </td>
                <td>{worksheet.Code}</td>
                <td>{worksheet.Name}</td>
                <td className={bulmaShared.buttonCell}>
                    <a href={inventoryWorksheetRoute.fill(worksheet.Code)(undefined)} target='_blank'>
                        <FontAwesome style='fas' icon='faExternalLinkAlt' fixedWidth />
                    </a>
                </td>
            </tr>
        );
    }), [worksheets]);

    if (worksheets === undefined) {
        return <PageLoader message={i18n.t('general.loading')} />;
    }

    return (
        <div className={css.container}>
            <Table
                tableProps={{ className: css.table }}
                head={tableHead}
                fullWidth
                body={<tbody>{tableBody}</tbody>}
            />
            {worksheets.length === 0 &&
                <p>{i18n.t('admin.worksheets.noWorksheets')}</p>
            }
            <Button color='primary' onClick={() => setEditingWorksheet(emptyWorksheet())}>{i18n.t('admin.worksheets.addWorksheet')}</Button>
            <Modal
                open={editingWorksheet !== undefined}
                requestClose={setWorksheetStateFunction(undefined, setEditingWorksheet)}
                title={i18n.t('admin.worksheets.editWorksheet')}
                actions={[DefaultActions.getSaveAction(undefined, { type: 'submit' }), DefaultActions.getCancelAction()]}
                onSubmit={saveWorksheet}
            >
                <Field>
                    <label className={clsx(bulmaShared.label, bulmaShared.requiredField)} id={ids[0]}>{i18n.t('admin.worksheets.code')}</label>
                    <Control>
                        <Input id={ids[0]} value={editingWorksheet !== undefined ? editingWorksheet.Code : ''} pattern='[A-Za-z0-9]+'
                            onChange={e => setEditingWorksheet({ ...editingWorksheet!, Code: e.currentTarget.value })} />
                    </Control>
                </Field>
                <Field>
                    <label className={clsx(bulmaShared.label, bulmaShared.requiredField)} id={ids[1]}>{i18n.t('admin.worksheets.name')}</label>
                    <Control>
                        <Input id={ids[1]} value={editingWorksheet !== undefined ? editingWorksheet.Name : ''}
                            onChange={e => setEditingWorksheet({ ...editingWorksheet!, Name: e.currentTarget.value })} />
                    </Control>
                </Field>
            </Modal>
            <Modal
                open={deletingWorksheet !== undefined}
                requestClose={setWorksheetStateFunction(undefined, setDeletingWorksheet)}
                title={i18n.t('admin.worksheets.deleteWorksheet')}
                actions={[
                    DefaultActions.getDeleteAction(undefined, { disabled: deletingWorksheet && deletingWorksheet.Name !== deleteNameConfirm, type: 'submit' }),
                    DefaultActions.getCancelAction(),
                ]}
                onSubmit={worksheetDeletionConfirmed}
            >
                <p>{i18n.t('admin.worksheets.deletionConfirmation', { name: deletingWorksheet ? deletingWorksheet.Name : '' })}</p>
                <div className={clsx(bulmaShared.message, bulmaShared.isDanger, css.warningMessage)}>
                    <div className={bulmaShared.messageHeader}>
                        <p>{i18n.t('general.warning')}</p>
                    </div>
                    <div className={clsx(bulmaShared.messageBody, css.warningBody)}>
                        <p>{i18n.t('admin.worksheets.deletionWarning')}</p>
                        <p>{i18n.t('admin.worksheets.nameConfirm')}</p>
                        <Input value={deleteNameConfirm}
                            onChange={e => setDeleteNameConfirm(e.currentTarget.value)} />
                    </div>
                </div>
            </Modal>
        </div >
    );
}
