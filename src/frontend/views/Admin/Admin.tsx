import clsx from 'clsx';
import React from 'react';
import { isAdminSession, isManagerSession } from '@/api/session';
import NotFound from '@/components/NotFound/NotFound';
import RoutingTabs, { ITab } from '@/components/RoutingTabs/RoutingTabs';
import i18n from '@/i18n/i18n';
import { adminRoute, getAdminPageRoute } from '@/logic/routes';
import { useTitle } from '@/logic/routing';
import SessionContext from '@/logic/sessionContext';
import bulmaShared from '@/style/bulmaShared';
import SystemConfig from '../SystemConfig/SystemConfig';
import About from './About/About';
import css from './Admin.scss';
import Columns from './Columns/Columns';
import Domains from './Domains/Domains';
import Logs from './Log/Logs';
import Notices from './Notices/Notices';
import Roles from './Roles/Roles';
import Users from './Users/Users';
import Worksheets from './Worksheets/Worksheets';

const defaultRoute = getAdminPageRoute('users');

export default function Admin() {
    const sessionContext = React.useContext(SessionContext);
    useTitle(i18n.t('admin.title'));

    const managerTabs: ITab[] = React.useMemo(() => [
        { component: Users, label: i18n.t('admin.users.title'), route: getAdminPageRoute('users') },
        { component: Roles, label: i18n.t('admin.roles.title'), route: getAdminPageRoute('roles') },
        { component: Notices, label: i18n.t('admin.notices.title'), route: getAdminPageRoute('notices') },
        { component: Worksheets, label: i18n.t('admin.worksheets.title'), route: getAdminPageRoute('worksheets') },
    ], [i18n.language]);

    const adminTabs: ITab[] = React.useMemo(() => [
        ...managerTabs,
        { component: Columns, label: i18n.t('admin.columns.title'), route: getAdminPageRoute('columns') },
        { component: Logs, label: i18n.t('admin.logs.title'), route: getAdminPageRoute('logs') },
        { component: Domains, label: i18n.t('admin.domains.title'), route: getAdminPageRoute('domains') },
        { component: SystemConfig, label: i18n.t('admin.system.title'), route: getAdminPageRoute('system') },
    ], [i18n.language]);

    const finalTabs: ITab[] = React.useMemo(() => [
        ...(sessionContext.current && isAdminSession(sessionContext.current) ? adminTabs : managerTabs),
        { component: About, label: i18n.t('admin.about.title'), route: getAdminPageRoute('about') },
    ], [i18n.language, sessionContext.current]);

    // The block below uses hooks incorrectly
    if (!sessionContext.current || !isManagerSession(sessionContext.current)) {
        return NotFound();
    } else {
        useTitle(i18n.t('admin.title'));
    }

    return (
        <div className={clsx(bulmaShared.container, css.adminContainer)}>
            <h1 className={clsx(bulmaShared.title, bulmaShared.is1)}>{i18n.t('admin.title')}</h1>
            <RoutingTabs
                baseRoute={adminRoute}
                tabs={finalTabs}
                defaultRoute={defaultRoute}
                notFound={NotFound}
                fullwidth
            />
        </div>
    );
}
