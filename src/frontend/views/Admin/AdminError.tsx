import clsx from 'clsx';
import React from 'react';
import i18n from '@/i18n/i18n';
import bulmaShared from '@/style/bulmaShared';

export function AdminError() {
    return (
        <div className={clsx(bulmaShared.notification, bulmaShared.isWarning)}>
            {i18n.t('general.error')}
        </div>
    );
}
