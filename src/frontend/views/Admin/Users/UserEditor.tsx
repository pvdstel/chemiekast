import clsx from 'clsx';
import React from 'react';
import { isAdminSession, isManagerSession } from '@/api/session';
import { addUser, CHEMIEKAST_HIGHEST_DEFAULT_ROLE, editUser, IUser } from '@/api/user';
import Button from '@/components/Button';
import FontAwesome from '@/components/FA';
import Checkbox from '@/components/Form/Checkbox';
import { Control, Field, Input, Select } from '@/components/Form/Form';
import Modal, { DefaultActions, IModalProps, modalAnimationDuration } from '@/components/Modal/Modal';
import i18n from '@/i18n/i18n';
import { handleUnsuccessfulApiCall } from '@/logic/api';
import { broadcastNotification, createNotification, NotificationType } from '@/logic/notifications';
import SessionContext from '@/logic/sessionContext';
import { PseudorandomString } from '@/logic/utilities';
import bulmaShared from '@/style/bulmaShared';
import { AdminError } from '../AdminError';
import UserAccessEditor from './UserAccessEditor';
import css from './Users.scss';

const defaultPasswordValue = ':default:';
export interface IUserEditorProps extends Pick<IModalProps, 'open' | 'requestClose'> {
    editingUser: IUser;
}

export default function UserEditor(props: IUserEditorProps) {
    const { open, requestClose, editingUser } = props;
    const [editDelta, setEditDelta] = React.useState<Partial<IUser>>({});
    const [passwordRepeat, setPasswordRepeat] = React.useState(defaultPasswordValue);
    const [userAccessEditorOpen, setUserAccessEditorOpen] = React.useState(false);
    const [isSaving, setIsSaving] = React.useState(false);

    const clearingTimeout = React.useRef<number | undefined>();
    const ids = React.useMemo(() => Array(9).fill(undefined).map(() => PseudorandomString(10, 'Aa')), []);

    const session = React.useContext(SessionContext);
    if (session.current === null || (!isManagerSession(session.current) && !isAdminSession(session.current))) {
        return <AdminError />;
    }
    const currentSession = session.current;

    React.useEffect(() => {
        if (!open) {
            clearingTimeout.current = window.setTimeout(() => {
                setEditDelta({});
                setPasswordRepeat(defaultPasswordValue);
            }, modalAnimationDuration);
        } else {
            clearTimeout(clearingTimeout.current);
            setEditDelta({});
            setPasswordRepeat(!editingUser.ID ? '' : defaultPasswordValue);
        }
    }, [open]);

    function registerChange<TField extends keyof IUser>(field: TField, value: IUser[TField]) {
        setEditDelta({ ...editDelta, [field]: value });
    }

    function onRandomPasswordClick() {
        const randomPassword = PseudorandomString(16);
        registerChange('Password', randomPassword);
        setPasswordRepeat(randomPassword);
    }

    const userValues = { ...editingUser, ...editDelta };

    function submit(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault();

        if (isSaving) {
            return false;
        }

        if (passwordRepeat !== userValues.Password && !(editingUser.ID && userValues.Password === undefined && passwordRepeat === defaultPasswordValue)) {
            broadcastNotification(createNotification(i18n.t('admin.users.unequalPasswords'), NotificationType.danger, 5000));
            return false;
        }

        setIsSaving(true);

        const apiCall = !editingUser.ID ? addUser : editUser;
        apiCall(userValues, result => {
            if (result.isSuccessful()) {
                requestClose();
            } else if (result.isUnsuccessful()) {
                handleUnsuccessfulApiCall(result);
            }
            setIsSaving(false);
        });
        return false;
    }

    const canEditUserRights = userValues.Role === 3 || userValues.Role === 4 || userValues.Role === 6;

    const fieldClassName = clsx(bulmaShared.column, bulmaShared.isFullMobile, bulmaShared.isHalfTablet, css.editField);
    return (
        <>
            <Modal
                open={open}
                requestClose={requestClose}
                title={i18n.t('admin.users.editor')}
                actions={[
                    DefaultActions.getSaveAction(undefined, { type: 'submit', disabled: isSaving }),
                    DefaultActions.getCancelAction({ disabled: isSaving }),
                ]}
                onSubmit={submit}
            >
                <div className={clsx(bulmaShared.columns, bulmaShared.isMultiline)}>
                    <Field className={fieldClassName}>
                        <label htmlFor={ids[0]} className={bulmaShared.label}>{i18n.t('admin.users.fields.id')}</label>
                        <Control>
                            <Input value={userValues.ID} id={ids[0]} type='number' min='1' max='10000' disabled />
                        </Control>
                    </Field>
                    <Field className={fieldClassName}>
                        <label htmlFor={ids[1]} className={bulmaShared.requiredField}>{i18n.t('admin.users.fields.username')}</label>
                        <Control>
                            <Input value={userValues.Username} onChange={e => registerChange('Username', e.currentTarget.value)} id={ids[1]} required disabled={isSaving} />
                        </Control>
                    </Field>
                    <Field className={fieldClassName}>
                        <label htmlFor={ids[2]} className={bulmaShared.requiredField}>{i18n.t('admin.users.fields.password')}</label>
                        <Field hasAddons>
                            <Control>
                                <Button type='button' title={i18n.t('admin.users.generatePassword')} onClick={onRandomPasswordClick} disabled={isSaving}>
                                    <FontAwesome style='fas' icon='faKey' />
                                </Button>
                            </Control>
                            <Control expanded>
                                <Input id={ids[2]} type='password' value={userValues.Password !== undefined ? userValues.Password : defaultPasswordValue}
                                    onChange={e => registerChange('Password', e.target.value)} required disabled={isSaving} />
                            </Control>
                        </Field>
                    </Field>
                    <Field className={fieldClassName}>
                        <label htmlFor={ids[3]} className={bulmaShared.requiredField}>{i18n.t('admin.users.fields.passwordRepeat')}</label>
                        <Control>
                            <Input id={ids[3]} type='password' value={passwordRepeat} onChange={e => setPasswordRepeat(e.currentTarget.value)} required disabled={isSaving} />
                        </Control>
                    </Field>
                    <Field className={fieldClassName}>
                        <label htmlFor={ids[4]} className={bulmaShared.requiredField}>{i18n.t('admin.users.fields.firstName')}</label>
                        <Control>
                            <Input value={userValues.FirstName} onChange={e => registerChange('FirstName', e.currentTarget.value)} id={ids[4]} required disabled={isSaving} />
                        </Control>
                    </Field>
                    <Field className={fieldClassName}>
                        <label htmlFor={ids[5]} className={bulmaShared.requiredField}>{i18n.t('admin.users.fields.lastName')}</label>
                        <Control>
                            <Input value={userValues.LastName} onChange={e => registerChange('LastName', e.currentTarget.value)} id={ids[5]} required disabled={isSaving} />
                        </Control>
                    </Field>
                    <Field className={fieldClassName}>
                        <label htmlFor={ids[6]} className={bulmaShared.requiredField}>{i18n.t('admin.users.fields.email')}</label>
                        <Control>
                            <Input value={userValues.Email} onChange={e => registerChange('Email', e.currentTarget.value)}
                                id={ids[6]} required type='email' disabled={isSaving} />
                        </Control>
                    </Field>
                    <Field className={fieldClassName}>
                        <label htmlFor={ids[7]} className={bulmaShared.requiredField}>{i18n.t('admin.users.fields.role')}</label>
                        <Field hasAddons>
                            <Control expanded>
                                <Select value={userValues.Role} onChange={e => registerChange('Role', Number(e.currentTarget.value))}
                                    id={ids[7]} containerClassName={bulmaShared.isFullwidth} disabled={isSaving}>
                                    <optgroup label={i18n.t('admin.users.systemRoles')}>
                                        {currentSession.Roles.filter(r => r.ID <= CHEMIEKAST_HIGHEST_DEFAULT_ROLE).map(r => (
                                            <option value={r.ID} key={r.ID}>{r.Name}</option>
                                        ))}
                                    </optgroup>
                                    <optgroup label={i18n.t('admin.users.userRoles')}>
                                        {currentSession.Roles.filter(r => r.ID > CHEMIEKAST_HIGHEST_DEFAULT_ROLE).map(r => (
                                            <option value={r.ID} key={r.ID}>{r.Name}</option>
                                        ))}
                                    </optgroup>
                                </Select>
                            </Control>
                            <Control>
                                <Button onClick={() => setUserAccessEditorOpen(true)} disabled={!canEditUserRights || isSaving} title={i18n.t('admin.users.fields.userAccess')}>
                                    <FontAwesome style='fas' icon='faUserCheck' />
                                </Button>
                            </Control>
                        </Field>
                    </Field>
                    <Field className={fieldClassName}>
                        <label className={bulmaShared.label}>Aanmelden</label>
                        <Checkbox checked={userValues.State} onChange={e => registerChange('State', e.currentTarget.checked)} disabled={isSaving}>
                            {i18n.t('admin.users.fields.stateCheckbox')}
                        </Checkbox>
                    </Field>
                    {!!editingUser.ID && isAdminSession(currentSession) &&
                        <Field className={fieldClassName}>
                            <label htmlFor={ids[8]} className={bulmaShared.requiredField}>{i18n.t('admin.users.fields.domain')}</label>
                            <Control>
                                <Select value={userValues.Domain} onChange={e => registerChange('Domain', Number(e.currentTarget.value))}
                                    id={ids[8]} containerClassName={bulmaShared.isFullwidth} disabled={isSaving}>
                                    {currentSession.Domains.map(d => (
                                        <option value={d.ID} key={d.ID}>{d.Name}</option>
                                    ))}
                                </Select>
                            </Control>
                        </Field>
                    }
                </div>
            </Modal>
            <UserAccessEditor
                open={userAccessEditorOpen}
                requestClose={() => setUserAccessEditorOpen(false)}
                userAccess={userValues.UserAccess || []}
                onUserAccessChange={value => registerChange('UserAccess', value)}
            />
        </>
    );
}
