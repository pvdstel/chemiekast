import React from 'react';
import { isDefaultRoleSession, ISessionWorksheet } from '@/api/session';
import { IUserAccess } from '@/api/user';
import Checkbox from '@/components/Form/Checkbox';
import Modal, { DefaultActions, IModalProps, modalAnimationDuration } from '@/components/Modal/Modal';
import Table from '@/components/Table/Table';
import i18n from '@/i18n/i18n';
import SessionContext from '@/logic/sessionContext';
import { AdminError } from '../AdminError';

export interface IUserAccessEditorProps extends Pick<IModalProps, 'open' | 'requestClose'> {
    userAccess: IUserAccess[];
    onUserAccessChange: React.Dispatch<IUserAccess[]>;
}

export default function UserAccessEditor(props: IUserAccessEditorProps) {
    const { userAccess, onUserAccessChange, open, requestClose } = props;
    const clearingTimeout = React.useRef<number | undefined>();
    const [editingUserAccess, setEditingUserAccess] = React.useState<IUserAccess[]>([]);
    const sessionContext = React.useContext(SessionContext);
    if (!sessionContext.current || !isDefaultRoleSession(sessionContext.current)) {
        return <AdminError />;
    }

    React.useEffect(() => {
        if (!open) {
            clearingTimeout.current = window.setTimeout(() => {
                setEditingUserAccess([]);
            }, modalAnimationDuration);
        } else {
            clearTimeout(clearingTimeout.current);
            setEditingUserAccess(userAccess);
        }
    }, [open]);

    function submit(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault();
        onUserAccessChange(editingUserAccess);
    }

    const renderWorksheetAccess = (w: ISessionWorksheet) => {
        const currentUserAccessIndex = editingUserAccess.findIndex(ua => ua.Worksheet === w.Code);
        const currentUserAccess = currentUserAccessIndex >= 0 ? editingUserAccess[currentUserAccessIndex] : undefined;
        const canView = currentUserAccess !== undefined;
        const canEdit = currentUserAccess !== undefined && currentUserAccess.Editing;

        const onViewCheckChange = () => {
            const newUserAccesses = [...editingUserAccess];
            if (currentUserAccess) {
                newUserAccesses.splice(currentUserAccessIndex, 1);
            } else {
                newUserAccesses.push({ Worksheet: w.Code, Editing: false });
            }
            setEditingUserAccess(newUserAccesses);
        };

        const onEditCheckChange = (e: React.ChangeEvent<HTMLInputElement>) => {
            const newUserAccesses = [...editingUserAccess];
            if (currentUserAccess) {
                newUserAccesses[currentUserAccessIndex].Editing = e.currentTarget.checked;
            } else {
                newUserAccesses.push({ Worksheet: w.Code, Editing: true });
            }
            setEditingUserAccess(newUserAccesses);
        };

        return (
            <tr key={w.Code}>
                <td>{w.Name}</td>
                <td><Checkbox checked={canView} onChange={onViewCheckChange} /></td>
                <td><Checkbox checked={canEdit} onChange={onEditCheckChange} /></td>
            </tr>
        );
    };

    return (
        <Modal
            open={open}
            requestClose={requestClose}
            title={i18n.t('admin.users.fields.userAccess')}
            actions={[DefaultActions.getOkAction(undefined, { type: 'submit' }), DefaultActions.getCancelAction()]}
            onSubmit={submit}
        >
            <Table
                head={
                    <thead>
                        <tr>
                            <th>{i18n.t('general.worksheet')}</th>
                            <th>{i18n.t('admin.users.rights.view')}</th>
                            <th>{i18n.t('admin.users.rights.edit')}</th>
                        </tr>
                    </thead>
                }
                body={
                    <tbody>
                        {sessionContext.current.Worksheets.map(renderWorksheetAccess)}
                    </tbody>
                }
                fullWidth
            />
        </Modal>
    );
}
