import React from 'react';
import { isManagerSession } from '@/api/session';
import { deleteUser, emptyUser, getUser, getUsers, IUser } from '@/api/user';
import Button from '@/components/Button';
import FontAwesome from '@/components/FA';
import { PageLoader } from '@/components/Loader/Loader';
import Modal, { DefaultActions } from '@/components/Modal/Modal';
import Table from '@/components/Table/Table';
import i18n from '@/i18n/i18n';
import { handleUnsuccessfulApiCall } from '@/logic/api';
import SessionContext from '@/logic/sessionContext';
import bulmaShared from '@/style/bulmaShared';
import { AdminError } from '../AdminError';
import UserEditor from './UserEditor';
import css from './Users.scss';

const noEditingUser: IUser = emptyUser(); // use const object, so the prop does not change

export default function Users() {
    const [users, setUsers] = React.useState<IUser[]>([]);
    const [editingUser, setEditingUser] = React.useState<IUser>(noEditingUser);
    const [deletingUser, setDeletingUser] = React.useState<IUser>();
    const [updateCounter, setUpdateCounter] = React.useState(0);

    const session = React.useContext(SessionContext).current;
    if (session === null || !isManagerSession(session)) {
        return <AdminError />;
    }

    React.useEffect(() => {
        getUsers(r => {
            if (r.isSuccessful()) {
                setUsers(r.result);
            } else if (r.isUnsuccessful()) {
                handleUnsuccessfulApiCall(r);
            }
        });
    }, [updateCounter, session.DomainName]);

    const tableHead = React.useMemo(() => (
        <thead>
            <tr>
                <th className={bulmaShared.iconColumn} colSpan={2}></th>
                <th>{i18n.t('admin.users.fields.username')}</th>
                <th>{i18n.t('admin.users.fields.role')}</th>
                <th>{i18n.t('admin.users.fields.email')}</th>
                <th>{i18n.t('admin.users.fields.firstName')}</th>
                <th>{i18n.t('admin.users.fields.lastName')}</th>
                <th>{i18n.t('admin.users.fields.state')}</th>
            </tr>
        </thead>
    ), [i18n.language]);

    function editUserFunction(user: IUser) {
        return (e: React.MouseEvent<HTMLElement>) => {
            e.preventDefault();
            getUser(user.ID, r => {
                if (r.isSuccessful()) {
                    setEditingUser(r.result);
                } else if (r.isUnsuccessful()) {
                    handleUnsuccessfulApiCall(r);
                }
            });
        };
    }

    function showDeletionDialog(user: IUser) {
        return (e: React.MouseEvent<HTMLElement>) => {
            e.preventDefault();
            setDeletingUser(user);
        };
    }

    function userDeletionConfirmed() {
        if (deletingUser) {
            deleteUser(deletingUser.ID, result => {
                if (result.isSuccessful()) {
                    setUpdateCounter(updateCounter + 1);
                } else if (result.isUnsuccessful()) {
                    handleUnsuccessfulApiCall(result);
                }
            });
            setDeletingUser(undefined);
        }
    }

    const tableBody = React.useMemo(() => users.map(user => {
        const role = session.Roles.find(r => r.ID === user.Role);
        return (
            <tr key={user.ID}>
                <td className={bulmaShared.buttonCell}>
                    <a onClick={editUserFunction(user)} href='#'><FontAwesome style='fas' icon='faPen' fixedWidth /></a>
                </td>
                <td className={bulmaShared.buttonCell}>
                    <a onClick={showDeletionDialog(user)} href='#'><FontAwesome style='fas' icon='faTrash' fixedWidth /></a>
                </td>
                <td>{user.Username}</td>
                {role !== undefined
                    ? <td>{role.Name}</td>
                    : <td className={bulmaShared.hasTextDanger}>
                        <FontAwesome style='fas' icon='faExclamationTriangle' /> {i18n.t('general.unknown')}
                    </td>
                }
                <td>{user.Email}</td>
                <td>{user.FirstName}</td>
                <td>{user.LastName}</td>
                <td>
                    {user.State &&
                        <FontAwesome style='fas' icon='faCheckCircle' />
                    }
                </td>
            </tr>
        );
    }), [users]);

    return (
        <div>
            <Table
                tableProps={{ className: css.table }}
                fullWidth
                head={tableHead}
                body={<tbody>{tableBody}</tbody>}
            />
            {users.length === 0 && <PageLoader message={i18n.t('general.loading')} />}
            <Button color='primary' onClick={() => setEditingUser(emptyUser())}>{i18n.t('admin.users.addUser')}</Button>
            <UserEditor
                editingUser={editingUser}
                open={editingUser !== noEditingUser}
                requestClose={() => { setEditingUser(noEditingUser); setUpdateCounter(updateCounter + 1); }}
            />
            <Modal
                open={deletingUser !== undefined}
                requestClose={() => setDeletingUser(undefined)}
                title={i18n.t('admin.users.deletion')}
                actions={[DefaultActions.getDeleteAction(userDeletionConfirmed), DefaultActions.getCancelAction()]}
            >
                <p>{i18n.t('admin.users.deletionConfirmation', { username: deletingUser ? deletingUser.Username : '' })}</p>
            </Modal>
        </div>
    );
}
