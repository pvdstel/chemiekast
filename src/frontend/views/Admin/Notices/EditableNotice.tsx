import clsx from 'clsx';
import { sanitize } from 'dompurify';
import marked from 'marked';
import React from 'react';
import { deleteNotice, editNotice, INotice } from '@/api/notice';
import { isSessionAuthorized } from '@/api/session';
import Checkbox from '@/components/Form/Checkbox';
import { Control, Field } from '@/components/Form/Form';
import Modal, { DefaultActions } from '@/components/Modal/Modal';
import i18n from '@/i18n/i18n';
import { handleUnsuccessfulApiCall } from '@/logic/api';
import SessionContext from '@/logic/sessionContext';
import bulmaShared from '@/style/bulmaShared';
import { AdminError } from '../AdminError';
import css from './Notices.scss';

export interface IEditableNoticeProps {
    notice: INotice;
    onUpdated: () => void;
}

export default function EditableNotice(props: IEditableNoticeProps) {
    const { notice, onUpdated } = props;
    const session = React.useContext(SessionContext).current;
    const [editingMessage, setEditingMessage] = React.useState<string>();
    const [editingSystem, setEditingSystem] = React.useState(false);
    const [isDeleting, setIsDeleting] = React.useState(false);
    const [isMakingApiCall, setIsMakingApiCall] = React.useState(false);

    if (!session || !isSessionAuthorized(session)) {
        return <AdminError />;
    }

    function onEditClicked(e: React.MouseEvent<HTMLAnchorElement>) {
        e.preventDefault();
        setEditingMessage(notice.Message);
        setEditingSystem(notice.System);
    }

    function onDeleteClicked(e: React.MouseEvent<HTMLAnchorElement>) {
        e.preventDefault();
        setIsDeleting(true);
    }

    function onCancelClicked(e: React.MouseEvent<HTMLAnchorElement>) {
        e.preventDefault();
        setEditingMessage(undefined);
        setEditingSystem(false);
    }

    function onSaveClicked(e: React.MouseEvent<HTMLAnchorElement>) {
        if (isMakingApiCall) { return; }
        e.preventDefault();
        setIsMakingApiCall(true);

        editNotice({ ...notice, Message: editingMessage!, System: editingSystem }, r => {
            if (r.isSuccessful()) {
                onUpdated();
                setEditingMessage(undefined);
                setEditingSystem(false);
            } else if (r.isUnsuccessful()) {
                handleUnsuccessfulApiCall(r);
            }
            setIsMakingApiCall(false);
        });
    }

    function deleteConfirm() {
        setIsDeleting(false);
        setIsMakingApiCall(true);

        deleteNotice(notice.ID, r => {
            setIsMakingApiCall(false);
            if (r.isSuccessful()) {
                props.onUpdated();
            } else if (r.isUnsuccessful()) {
                handleUnsuccessfulApiCall(r);
            }
        });
    }

    if (editingMessage !== undefined) {
        return (
            <div className={bulmaShared.card}>
                <div className={bulmaShared.cardHeader}>
                    <p className={bulmaShared.cardHeaderTitle}>{i18n.t('admin.notices.editing')}</p>
                </div>
                <div className={bulmaShared.cardContent}>
                    <Field>
                        <Control>
                            <textarea
                                className={bulmaShared.textarea}
                                value={editingMessage}
                                onChange={e => setEditingMessage(e.currentTarget.value)}
                                rows={10}
                                disabled={isMakingApiCall}
                            />
                        </Control>
                    </Field>
                    {session.CanAccessGlobalSettings &&
                        <Field>
                            <Control>
                                <Checkbox checked={editingSystem} onChange={e => setEditingSystem(e.currentTarget.checked)} color='primary'>
                                    {i18n.t('admin.notices.systemNotice')}
                                </Checkbox>
                            </Control>
                        </Field>
                    }
                    <p className={css.info}>
                        <span>{session.User.FirstName} {session.User.LastName}</span>
                        <span>{i18n.t('general.now')}</span>
                        {editingSystem && <span>{i18n.t('admin.notices.systemNotice')}</span>}
                    </p>
                </div>
                <footer className={bulmaShared.cardFooter}>
                    <a href='#' className={bulmaShared.cardFooterItem} onClick={onCancelClicked}>{i18n.t('general.cancel')}</a>
                    <a href='#' className={bulmaShared.cardFooterItem} onClick={onSaveClicked}>{i18n.t('general.save')}</a>
                </footer>
            </div>
        );
    } else {
        return <>
            <div className={bulmaShared.card}>
                <div className={bulmaShared.cardContent}>
                    <div className={bulmaShared.content} dangerouslySetInnerHTML={{ __html: sanitize(marked(notice.Message)) }} />
                    <p className={css.info}>
                        <span>{notice.Poster}</span>
                        <span>{new Date(notice.Date).toLocaleString()}</span>
                        {notice.System && <span>{i18n.t('admin.notices.systemNotice')}</span>}
                    </p>
                </div>
                {(!notice.System || session.CanAccessGlobalSettings) &&
                    <footer className={bulmaShared.cardFooter}>
                        <a href='#' className={bulmaShared.cardFooterItem} onClick={onEditClicked}>{i18n.t('general.edit')}</a>
                        <a href='#' className={clsx(bulmaShared.cardFooterItem, bulmaShared.hasTextDanger)} onClick={onDeleteClicked}>{i18n.t('general.delete')}</a>
                    </footer>
                }
            </div>
            <Modal
                open={isDeleting}
                requestClose={() => setIsDeleting(false)}
                title={i18n.t('admin.notices.deleteNotice')}
                actions={[
                    DefaultActions.getDeleteAction(deleteConfirm),
                    DefaultActions.getCancelAction(),
                ]}
            >
                <p>{i18n.t('admin.notices.deletionConfirmation')}</p>
            </Modal>
        </>;
    }
}
