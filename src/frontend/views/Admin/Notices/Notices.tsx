import clsx from 'clsx';
import React from 'react';
import { addNotice, getNotices, INotice } from '@/api/notice';
import { isSessionAuthorized } from '@/api/session';
import Button from '@/components/Button';
import Checkbox from '@/components/Form/Checkbox';
import { Control, Field } from '@/components/Form/Form';
import { PageLoader } from '@/components/Loader/Loader';
import Modal, { DefaultActions } from '@/components/Modal/Modal';
import i18n from '@/i18n/i18n';
import { handleUnsuccessfulApiCall } from '@/logic/api';
import SessionContext from '@/logic/sessionContext';
import bulmaShared from '@/style/bulmaShared';
import { AdminError } from '../AdminError';
import EditableNotice from './EditableNotice';

export default function Notices() {
    const [notices, setNotices] = React.useState<INotice[]>();
    const session = React.useContext(SessionContext).current;
    const [updateCounter, setUpdateCounter] = React.useState(0);
    const [addingMessage, setAddingMessage] = React.useState<string>();
    const [addingSystem, setAddingSystem] = React.useState(false);
    const [isMakingApiCall, setIsMakingApiCall] = React.useState(false);

    if (!session || !isSessionAuthorized(session)) {
        return <AdminError />;
    }

    React.useEffect(() => {
        getNotices(r => {
            if (r.isSuccessful()) {
                setNotices(r.result.Notices);
            } else if (r.isUnsuccessful()) {
                handleUnsuccessfulApiCall(r);
            }
        });
    }, [updateCounter, session.DomainName]);

    if (!session || !isSessionAuthorized(session)) {
        return <AdminError />;
    }

    if (!notices) {
        return <PageLoader message={i18n.t('general.loading')} />;
    }

    function addNoticeHandler(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault();
        if (isMakingApiCall) { return; }

        setIsMakingApiCall(true);
        addNotice({ Message: addingMessage!, System: addingSystem }, r => {
            if (r.isSuccessful()) {
                setAddingMessage(undefined);
                setAddingSystem(false);
                setUpdateCounter(updateCounter + 1);
            } else if (r.isUnsuccessful()) {
                handleUnsuccessfulApiCall(r);
            }
            setIsMakingApiCall(false);
        });
        return false;
    }

    return <>
        <Field>
            <Button color='primary' onClick={() => { setAddingMessage(''); setAddingSystem(false); }}>{i18n.t('admin.notices.addNotice')}</Button>
        </Field>

        {notices.length === 0 &&
            <p>{i18n.t('admin.notices.noNotices')}</p>
        }

        <div className={clsx(bulmaShared.columns, bulmaShared.isMultiline)}>
            {notices.map(n => (
                <div key={n.ID} className={clsx(bulmaShared.column, bulmaShared.is12Mobile, bulmaShared.isHalfTablet)}>
                    <EditableNotice notice={n} onUpdated={() => setUpdateCounter(updateCounter + 1)} />
                </div>
            ))}
        </div>

        <Modal
            open={addingMessage !== undefined}
            requestClose={() => { setAddingMessage(undefined); setAddingSystem(false); }}
            title={i18n.t('admin.notices.addNotice')}
            actions={[
                DefaultActions.getSaveAction(undefined, { type: 'submit', disabled: isMakingApiCall }),
                DefaultActions.getCancelAction(),
            ]}
            onSubmit={addNoticeHandler}
        >
            <Field>
                <Control>
                    <textarea
                        className={bulmaShared.textarea}
                        value={addingMessage}
                        onChange={e => setAddingMessage(e.currentTarget.value)}
                        rows={10}
                        disabled={isMakingApiCall}
                    />
                </Control>
            </Field>
            {session.CanAccessGlobalSettings &&
                <Field>
                    <Control>
                        <Checkbox checked={addingSystem} onChange={e => setAddingSystem(e.currentTarget.checked)} color='primary'>
                            {i18n.t('admin.notices.systemNotice')}
                        </Checkbox>
                    </Control>
                </Field>
            }
        </Modal>
    </>;
}
