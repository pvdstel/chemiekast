import React from 'react';
import { Route, RouteComponentProps, Switch } from 'react-router';
import { emtpyChemical, IChemical } from '@/api/chemical';
import { isDefaultRoleSession } from '@/api/session';
import NotFound from '@/components/NotFound/NotFound';
import Content from '@/components/Sidebar/Content';
import Sidebar from '@/components/Sidebar/Sidebar';
import { notifyTableUpdate } from '@/components/Table/Table';
import i18n from '@/i18n/i18n';
import InventoryContext, { IInventory, IInventoryContext } from '@/logic/inventoryContext';
import { inventoryAllRoute, inventoryChemicalRoute, inventoryRestrictedRoute, inventorySearchRoute, inventoryWorksheetRoute } from '@/logic/routes';
import SessionContext from '@/logic/sessionContext';
import { getPersistentStorageItem, setPersistentStorageItem } from '@/logic/storage';
import ChemicalEditor, { EditingChemicalType } from './Components/ChemicalEditor/ChemicalEditor';
import ChemicalInventory from './Components/ChemicalInventory/ChemicalInventory';
import RestrictedInventory from './Components/RestrictedInventory';
import SearchInventory from './Components/SearchInventory';
import WorksheetInventory from './Components/WorksheetInventory';

type InventoryPropsType = RouteComponentProps;

interface IInventoryState extends IInventory {
    editingChemical: EditingChemicalType;
    preferredWorksheetCode: string;
}

export default class Inventory extends React.Component<InventoryPropsType, IInventoryState> {
    static contextType = SessionContext;
    context!: React.ContextType<typeof SessionContext>;

    constructor(props: InventoryPropsType) {
        super(props);

        this.state = {
            sidebarOpen: false,
            sidebarPinned: getPersistentStorageItem('inventory-sidebar-pinned', true),
            spaceTable: getPersistentStorageItem('space-inventory-tables', false),
            hiddenColumns: [],
            refreshHandler: () => { /* noop */ },
            activeChemical: undefined,

            editingChemical: emtpyChemical(),
            preferredWorksheetCode: '',
        };
    }

    private updateInventoryContext = <K extends keyof IInventory>(next: Pick<IInventory, K>) => {
        this.setState(next as Pick<IInventoryState, K>);
    }

    private editChemical = (chemical: IChemical) => {
        this.setState({ editingChemical: chemical });
        this.updateInventoryContext({ sidebarOpen: true });
    }
    private setWorksheetCode = (worksheet: string) =>
        this.setState({ preferredWorksheetCode: worksheet, editingChemical: emtpyChemical() })

    render() {
        const session = this.context.current;
        const showSidebar = session !== null && isDefaultRoleSession(session) && session.Worksheets.some(w => w.CanEdit);
        const inventoryContextValue: IInventoryContext = {
            current: {
                activeChemical: this.state.activeChemical,
                refreshHandler: this.state.refreshHandler,
                sidebarOpen: this.state.sidebarOpen,
                sidebarPinned: this.state.sidebarPinned,
                spaceTable: this.state.spaceTable,
                hiddenColumns: this.state.hiddenColumns,
            },
            update: this.updateInventoryContext,
            editChemical: this.editChemical,
            setWorksheetCode: this.setWorksheetCode,
        };

        return <InventoryContext.Provider value={inventoryContextValue}>
            {showSidebar &&
                <Sidebar
                    title={i18n.t('inventory.sidebarTitle')}
                    printable={false}
                    pinned={this.state.sidebarPinned}
                    open={this.state.sidebarOpen}
                    requestOpen={o => this.updateInventoryContext({ sidebarOpen: o })}
                    requestPinned={p => {
                        setPersistentStorageItem('inventory-sidebar-pinned', p);
                        this.updateInventoryContext({ sidebarPinned: p });
                        notifyTableUpdate();
                    }}
                    pullButtonIconProps={{ style: 'fas', icon: 'faEdit' }}
                >
                    <ChemicalEditor
                        editingChemical={this.state.editingChemical}
                        preferredWorksheetCode={this.state.preferredWorksheetCode}
                        requestClear={() => this.setState({ editingChemical: emtpyChemical() })}
                        onModification={this.state.refreshHandler}
                    />
                </Sidebar>
            }
            <Content sidebar={showSidebar && this.state.sidebarPinned}>
                <Switch>
                    <Route path={inventoryWorksheetRoute.path} component={WorksheetInventory} />
                    <Route path={inventoryChemicalRoute.path} component={ChemicalInventory} />
                    <Route path={inventorySearchRoute.path} component={SearchInventory} />
                    <Route path={inventoryAllRoute.path} component={SearchInventory} />
                    <Route path={inventoryRestrictedRoute.path} component={RestrictedInventory} />
                    <NotFound />
                </Switch>
            </Content>
        </InventoryContext.Provider>;
    }
}
