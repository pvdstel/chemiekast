import React from 'react';
import { RouteComponentProps } from 'react-router';
import { getWorksheetInventory, IInventoryWorksheet } from '@/api/inventory';
import { isDefaultRoleSession } from '@/api/session';
import i18n from '@/i18n/i18n';
import { handleUnsuccessfulApiCall } from '@/logic/api';
import InventoryContext from '@/logic/inventoryContext';
import { inventoryWorksheetRoute } from '@/logic/routes';
import { useTitle } from '@/logic/routing';
import SessionContext from '@/logic/sessionContext';
import InventoryPane from './InventoryPane/InventoryPane';

type WorksheetInventoryPropType = RouteComponentProps<typeof inventoryWorksheetRoute.params>;

export default function WorksheetInventory(props: WorksheetInventoryPropType) {
    const [worksheets, setWorksheets] = React.useState<IInventoryWorksheet[] | undefined>(undefined);
    const [date, setDate] = React.useState(new Date());
    const inventoryContext = React.useContext(InventoryContext);
    const sessionContext = React.useContext(SessionContext);

    const session = sessionContext.current;
    if (session === null || !isDefaultRoleSession(session)) {
        return null;
    }

    React.useEffect(() => {
        refreshWorksheets();
        inventoryContext.setWorksheetCode(props.match.params.ws);
        inventoryContext.update({ refreshHandler: refreshWorksheets });
    }, [props.match.params.ws, session.DomainName]);

    React.useEffect(() => {
        inventoryContext.update({ activeChemical: Number(props.match.params.chemical) });
        return () => inventoryContext.update({ activeChemical: undefined });
    }, [props.match.params.chemical]);

    function refreshWorksheets() {
        const worksheetCode = props.match.params.ws;
        if (worksheetCode === undefined) { return; }
        getWorksheetInventory(worksheetCode, r => {
            if (r.isSuccessful()) {
                setWorksheets(r.result);
                setDate(new Date());
            } else if (r.isUnsuccessful()) {
                setWorksheets(undefined);
                handleUnsuccessfulApiCall(r);
            }
        });
    }

    const worksheet = session.Worksheets.filter(w => w.Code === props.match.params.ws)[0];
    const title = worksheet !== undefined ? worksheet.Name : i18n.t('general.unknown');
    useTitle(title);

    return <>
        <InventoryPane
            title={title}
            superTitle={i18n.t('general.worksheet')}
            worksheets={worksheets}
            date={date}
        />
    </>;
}

