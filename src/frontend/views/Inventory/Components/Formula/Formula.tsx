import React from 'react';
import { ChemicalFormulaTokenTypes, tokenizeFormula } from '@/logic/chemicalFormulas';
import css from './Formula.scss';

export interface IFormulaViewerProps {
    formula: string;
    classNames?: {
        [P in ChemicalFormulaTokenTypes]?: string;
    };
}

export default function Formula(props: IFormulaViewerProps) {
    const {
        formula,
        classNames,
    } = props;

    const elements = React.useMemo(() => {
        const formulaTokens = tokenizeFormula(formula);

        return formulaTokens.map((t, i) => {
            const tokenElementClassName = classNames && classNames[t.type];

            switch (t.type) {
                case 'atom':
                case 'number':
                case 'text':
                case 'unknown':
                    return <span className={tokenElementClassName} key={i}>{t.data}</span>;
                case 'atomNumber':
                case 'abstractAtomNumber':
                    return <sub className={tokenElementClassName} key={i}>{t.data}</sub>;
                case 'groupStart':
                    return <span className={tokenElementClassName} key={i}>(</span>;
                case 'groupEnd':
                    return <span className={tokenElementClassName} key={i}>)</span>;
                case 'hydrate':
                    return <span className={tokenElementClassName} key={i}>·</span>;
            }
        });
    }, [formula, classNames]);

    return <>{elements}</>;
}

(Formula as React.FunctionComponent<IFormulaViewerProps>).defaultProps = {
    classNames: {
        hydrate: css.hydrate,
        unknown: css.unknown,
    },
} as Required<Pick<IFormulaViewerProps, 'classNames'>>;
