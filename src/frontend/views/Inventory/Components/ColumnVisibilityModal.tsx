import React from 'react';
import { getNonSystemColumns, isSessionAuthorized } from '@/api/session';
import Checkbox from '@/components/Form/Checkbox';
import { Control, Field } from '@/components/Form/Form';
import Modal, { DefaultActions, IModalProps } from '@/components/Modal/Modal';
import { notifyTableUpdate } from '@/components/Table/Table';
import i18n from '@/i18n/i18n';
import InventoryContext from '@/logic/inventoryContext';
import SessionContext from '@/logic/sessionContext';
import { PseudorandomString } from '@/logic/utilities';

const styleElementTag = PseudorandomString(10, 'Aa');
type PropsType = Pick<IModalProps, 'open' | 'requestClose'>;

export default function ColumnVisibilityModal(props: PropsType) {
    const inventoryContext = React.useContext(InventoryContext);
    const [hiddenColumns, setHiddenColumns] = React.useState(inventoryContext.current.hiddenColumns);
    const sessionContext = React.useContext(SessionContext);

    React.useEffect(() => {
        commit();
    }, []);
    React.useEffect(() => {
        if (props.open) {
            setHiddenColumns(inventoryContext.current.hiddenColumns);
        }
    }, [props.open]);

    const columns = sessionContext !== null
        ?
        isSessionAuthorized(sessionContext.current!)
            ? getNonSystemColumns(sessionContext.current)
            : []
        : [];

    function createOnCheckChange(column: string) {
        return () => {
            const index = hiddenColumns.indexOf(column);
            const newHiddenColumns = [...hiddenColumns];
            if (index >= 0) {
                newHiddenColumns.splice(index, 1);
            } else {
                newHiddenColumns.push(column);
            }
            setHiddenColumns(newHiddenColumns);
        };
    }

    function commit() {
        inventoryContext.update({ hiddenColumns: hiddenColumns });

        let style: HTMLStyleElement | null = document.head.querySelector(`#${styleElementTag}`);
        if (style === null) {
            style = document.createElement('style');
            style.id = styleElementTag;
            document.head.appendChild(style);
        }
        style.childNodes.forEach(v => v.remove());
        const css = `${hiddenColumns.map(h => `[data-column="${h}"]`).join(', ')} {
    display: none;
}`;
        style.appendChild(document.createTextNode(css));
        notifyTableUpdate();
    }

    return (
        <Modal
            open={props.open}
            requestClose={props.requestClose}
            title={i18n.t('inventory.visibleColumns')}
            actions={[DefaultActions.getOkAction(commit), DefaultActions.getCancelAction()]}
        >
            {columns.map(c => (
                <Field key={c.Name}>
                    <Control>
                        <Checkbox
                            checked={!hiddenColumns.includes(c.Name)}
                            onChange={createOnCheckChange(c.Name)}
                        >
                            {c.UIName}
                        </Checkbox>
                    </Control>
                </Field>
            ))}
        </Modal>
    );
}
