import clsx from 'clsx';
import React from 'react';
import { addChemicalsToRole } from '@/api/role';
import { isAdminSession, isManagerSession } from '@/api/session';
import Button from '@/components/Button';
import { Control, Field, Input, Select } from '@/components/Form/Form';
import RadioButton from '@/components/Form/RadioButton';
import i18n from '@/i18n/i18n';
import { handleUnsuccessfulApiCall } from '@/logic/api';
import { broadcastNotification, createNotification, NotificationType } from '@/logic/notifications';
import SessionContext from '@/logic/sessionContext';
import bulmaShared from '@/style/bulmaShared';
import { inventoryWorksheetCheckboxClassName } from '../InventoryWorksheet/InventoryWorksheet';
import css from './AddToRole.scss';

const tr = (key: string) => i18n.t(`inventory.addToCustomRole.${key}`);

export default function AddToRole() {
    const sessionContext = React.useContext(SessionContext);
    const [addingToNew, setAddingToNew] = React.useState<boolean | null>(null);
    const [newRoleName, setNewRoleName] = React.useState('');
    const [existingRoleId, setExistingRoleId] = React.useState<number | ''>('');
    const [isAdding, setIsAdding] = React.useState(false);

    if (!sessionContext.current || !(isAdminSession(sessionContext.current) || isManagerSession(sessionContext.current))) {
        return null;
    }
    const customRoles = sessionContext.current.CustomRoles;

    function validate() {
        if (addingToNew === true) {
            return !!newRoleName;
        } else if (addingToNew === false) {
            return existingRoleId !== '';
        }
        return false;
    }

    function onSubmit(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault();
        if (isAdding) { return; }
        if (!validate()) {
            broadcastNotification(createNotification(tr('invalidForm'), NotificationType.danger, 5000));
            return;
        }
        const selectedChemicalIds: number[] = [];
        document.querySelectorAll(`.${inventoryWorksheetCheckboxClassName}`).forEach(el => {
            if (el instanceof HTMLInputElement && el.checked) {
                selectedChemicalIds.push(Number(el.value));
            }
        });
        if (selectedChemicalIds.length === 0) {
            broadcastNotification(createNotification(tr('noneSelected'), NotificationType.warning, 5000));
            return;
        }

        setIsAdding(true);
        addChemicalsToRole(selectedChemicalIds, addingToNew ? newRoleName : existingRoleId, result => {
            if (result.isSuccessful()) {
                document.querySelectorAll(`.${inventoryWorksheetCheckboxClassName}`).forEach(el => {
                    if (el instanceof HTMLInputElement) {
                        el.checked = false;
                    }
                });
                setAddingToNew(null);
                setNewRoleName('');
                setExistingRoleId('');
                if (addingToNew) {
                    sessionContext.refresh();
                }
            } else if (result.isUnsuccessful()) {
                handleUnsuccessfulApiCall(result);
            }
            setIsAdding(false);
        });
    }

    function onNewRoleNameChange(e: React.ChangeEvent<HTMLInputElement>) {
        setAddingToNew(true);
        setNewRoleName(e.currentTarget.value);
    }

    function onExistingRoleChange(e: React.ChangeEvent<HTMLSelectElement>) {
        setAddingToNew(false);
        setExistingRoleId(Number(e.currentTarget.value));
    }

    return <form className={clsx(css.addToRole, bulmaShared.columns)} onSubmit={onSubmit}>
        <div className={clsx(bulmaShared.column, bulmaShared.is6Tablet, bulmaShared.is4Desktop)}>
            <h4 className={clsx(bulmaShared.title, bulmaShared.is4)}>{tr('title')}</h4>
            <Field>
                <Control>
                    <RadioButton
                        checked={addingToNew === false}
                        onChange={e => { if (e.currentTarget.checked) { setAddingToNew(false); } }}
                        disabled={isAdding || customRoles.length === 0}
                    >
                        {tr('existing')}
                    </RadioButton>
                </Control>
            </Field>
            <Field>
                <Control>
                    <Select
                        value={existingRoleId}
                        onChange={onExistingRoleChange}
                        disabled={isAdding || customRoles.length === 0}
                        containerClassName={bulmaShared.isFullwidth}
                    >
                        {existingRoleId === '' && <option value=''>​</option> /* zero-width space */}
                        {customRoles.map(r => <option key={r.ID} value={r.ID}>{r.Name}</option>)}
                    </Select>
                </Control>
            </Field>
            <Field>
                <Control>
                    <RadioButton
                        checked={addingToNew === true}
                        onChange={e => { if (e.currentTarget.checked) { setAddingToNew(true); } }}
                        disabled={isAdding}
                    >
                        {tr('new')}
                    </RadioButton>
                </Control>
            </Field>
            <Field>
                <Control>
                    <Input
                        placeholder={tr('name')}
                        value={newRoleName}
                        onChange={onNewRoleNameChange}
                        disabled={isAdding}
                    />
                </Control>
            </Field>
            <Field>
                <Control>
                    <Button type='submit' color='primary' disabled={isAdding}>{tr('add')}</Button>
                </Control>
            </Field>
        </div>
    </form>;
}
