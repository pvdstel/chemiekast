import clsx from 'clsx';
import { sanitize } from 'dompurify';
import marked from 'marked';
import React from 'react';
import { IChemical } from '@/api/chemical';
import { CHEMICALS_GHS, CHEMICALS_ID_COLUMN, CHEMICALS_NAME_COLUMN } from '@/api/column';
import { IInventoryWorksheet } from '@/api/inventory';
import { getNonSystemColumns, isSessionAuthorized } from '@/api/session';
import { IWorksheetColumn } from '@/api/worksheet';
import FontAwesome from '@/components/FA';
import Checkbox from '@/components/Form/Checkbox';
import { navigationHeight } from '@/components/Navigation/Navigation';
import Table from '@/components/Table/Table';
import i18n from '@/i18n/i18n';
import { ghsLinkStringToArray } from '@/logic/ghs';
import InventoryContext from '@/logic/inventoryContext';
import SessionContext from '@/logic/sessionContext';
import { extractOrigin, PseudorandomString } from '@/logic/utilities';
import bulmaShared from '@/style/bulmaShared';
import EditButton from '../EditButton';
import Formula from '../Formula/Formula';
import GhsIndicator from '../GHS/GhsIndicator';
import { renderEmptyInventory, worksheetTocId } from '../InventoryPane/InventoryPane';
import css from './InventoryWorksheet.scss';

export const inventoryWorksheetCheckboxClassName = PseudorandomString(16, 'Aa');

export interface IInventoryWorksheetProps {
    worksheet: IInventoryWorksheet;
    renderTitle?: boolean;
}

export function renderCellValue(chemical: IChemical, column: IWorksheetColumn) {
    const columnValue = chemical[column.Name];
    if (typeof columnValue === 'string') {
        if (column.IsMarkdown) {
            const html = marked(columnValue);
            const cleanHtml = sanitize(html);
            return <div dangerouslySetInnerHTML={{ __html: cleanHtml }} />;
        } else if (column.IsUrl) {
            const urls = ghsLinkStringToArray(columnValue);
            return urls.map(url =>
                <a
                    key={url}
                    className={css.link}
                    href={url}
                    target='_blank'
                    rel='noopener nofollow'
                    title={extractOrigin(url)}>
                    <FontAwesome style='fas' icon='faLink' />
                </a>,
            );
        } else if (column.IsFormula) {
            return <Formula formula={chemical[column.Name].toString()} />;
        }
    }
    return chemical[column.Name];
}

export default function InventoryWorksheet(props: IInventoryWorksheetProps) {
    const sessionContext = React.useContext(SessionContext);
    const inventoryContext = React.useContext(InventoryContext);
    const { worksheet, renderTitle } = props;

    function renderTableHead(columns: IWorksheetColumn[]) {
        return <thead>
            <tr>
                {worksheet.Selectable && <th className={clsx(css.unprintable, bulmaShared.iconColumn)} />}
                {worksheet.Editable && <th className={clsx(css.unprintable, bulmaShared.iconColumn)} data-column={CHEMICALS_ID_COLUMN}>
                    {i18n.t('inventory.properties.id')}
                </th>}
                <th data-column={CHEMICALS_NAME_COLUMN}>{i18n.t('inventory.properties.name')}</th>
                <th className={bulmaShared.iconColumn} data-column='GHS'>{i18n.t('inventory.ghsColumn')}</th>
                {columns.map(c => <th key={c.Name} data-column={c.Name}>{c.UIName}</th>)}
            </tr>
        </thead>;
    }

    function renderChemicalRow(chemical: IChemical, columns: IWorksheetColumn[]) {
        const rowClassName = clsx({
            [bulmaShared.isSelected]: chemical.ID === inventoryContext.current.activeChemical,
        });
        return <tr className={rowClassName} key={chemical.ID}>
            {worksheet.Selectable && <td className={clsx(bulmaShared.buttonCell, css.unprintable)}>
                <Checkbox value={chemical.ID} className={inventoryWorksheetCheckboxClassName} />
            </td>}
            {worksheet.Editable && <td className={clsx(bulmaShared.buttonCell, css.unprintable)} data-column={CHEMICALS_ID_COLUMN}>
                <EditButton chemical={chemical} />
            </td>}
            <td data-column={CHEMICALS_NAME_COLUMN}>{chemical.Name}</td>
            <GhsIndicator
                container={<td className={clsx(bulmaShared.buttonCell, css.ghsCell)} data-column={CHEMICALS_GHS} />}
                ghsSymbolsValue={chemical.GhsSymbols}
                ghsLinks={chemical.GhsLinks} />
            {columns.map(c => <td key={c.Name} data-column={c.Name}>
                {renderCellValue(chemical, c)}
            </td>)}
        </tr>;
    }

    function renderTable(columns: IWorksheetColumn[]) {
        return <Table
            tableProps={{ className: css.worksheetTable }}
            head={renderTableHead(columns)}
            body={<tbody>{worksheet.Chemicals.map(c => renderChemicalRow(c, columns))}</tbody>}
            hasFixedThead
            fixedBounds={{ top: navigationHeight - 1, bottom: window.innerHeight }} // -1 so the navbar shadow acts as the table border
            fullWidth
            striped={!inventoryContext.current.spaceTable}
            bordered={!inventoryContext.current.spaceTable}
            narrow={!inventoryContext.current.spaceTable}
        />;
    }

    const session = sessionContext.current;
    if (!session || !isSessionAuthorized(session)) { return null; }

    const nonSystemColumns = React.useMemo(() => getNonSystemColumns(session), [session]);

    return React.useMemo(() => {
        return (<>
            {renderTitle && !!worksheet.Name &&
                <h3 className={clsx(bulmaShared.title, bulmaShared.is3)} id={worksheetTocId(worksheet.Code)}>
                    {worksheet.Name}{' '}
                    <span className={clsx(bulmaShared.tag, bulmaShared.isRounded, bulmaShared.isMedium)}>{worksheet.Chemicals.length}</span>
                </h3>
            }

            {worksheet.Chemicals.length > 0
                ? renderTable(nonSystemColumns)
                : renderEmptyInventory()
            }
        </>);
    }, [worksheet, renderTitle, inventoryContext.current.activeChemical, inventoryContext.current.spaceTable, session, i18n.language]);
}
