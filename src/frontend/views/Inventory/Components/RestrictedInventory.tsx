import React from 'react';
import { getRestrictedInventory, IInventoryWorksheet } from '@/api/inventory';
import { isSessionAuthorized } from '@/api/session';
import i18n from '@/i18n/i18n';
import { handleUnsuccessfulApiCall } from '@/logic/api';
import InventoryContext from '@/logic/inventoryContext';
import { useTitle } from '@/logic/routing';
import SessionContext from '@/logic/sessionContext';
import InventoryPane from './InventoryPane/InventoryPane';

export default function RestrictedInventory() {
    const [inventory, setInventory] = React.useState<IInventoryWorksheet[]>();
    const [date, setDate] = React.useState(new Date());
    const inventoryContext = React.useContext(InventoryContext);
    const sessionContext = React.useContext(SessionContext);

    const session = sessionContext.current;
    if (session === null || !isSessionAuthorized(session)) {
        return null;
    }

    React.useEffect(() => {
        refreshSearchResults();
        inventoryContext.setWorksheetCode('');
        inventoryContext.update({ refreshHandler: refreshSearchResults });
    }, [session.DomainName]);

    function refreshSearchResults() {
        getRestrictedInventory(r => {
            if (r.isSuccessful()) {
                setInventory(r.result);
                setDate(new Date());
            } else if (r.isUnsuccessful()) {
                setInventory(undefined);
                handleUnsuccessfulApiCall(r);
            }
        });
    }

    useTitle(i18n.t('inventory.inventory'));

    return <>
        <InventoryPane
            title={i18n.t('inventory.inventory')}
            superTitle=''
            worksheets={inventory}
            renderWorksheetTitles
            date={date}
        />
    </>;
}

