import React from 'react';
import FontAwesome from '@/components/FA';
import { Input } from '@/components/Form/Form';
import Modal, { DefaultActions, IModalProps } from '@/components/Modal/Modal';
import Table from '@/components/Table/Table';
import i18n from '@/i18n/i18n';
import { tokenizeFormula } from '@/logic/chemicalFormulas';
import Formula from '../Formula/Formula';

export interface IFormulaExampleProps {
    open: IModalProps['open'];
    requestClose: IModalProps['requestClose'];
}

const examples: [string, boolean][] = [
    ['C6H12O6', true],
    ['C6 H12 O6', true],
    ['Cn H2n On', false],
    ['C_n H2n O_n', true],
    ['Ag2nSen', true],
    ['Ag2nO_n', true],
    ['(C6H10O5)n', true],
    ['(NaOH)4n', true],
    ['CaSO4 * 2H20', true],
    ['CaSO4 . 2H20', false],
    ['(NH4)2Fe(SO4)2 * 6H20', true],
    ['\'Mix of chemicals', true],
    ['Bad formula', false],
];

export default function FormulaExamples(props: IFormulaExampleProps) {
    const [userExample, setUserExample] = React.useState('');
    const isUserExampleCorrect = React.useMemo(
        () => !tokenizeFormula(userExample).some(t => t.type === 'unknown'),
        [userExample],
    );

    return <Modal
        open={props.open}
        requestClose={props.requestClose}
        actions={[DefaultActions.getCloseAction()]}
        title={i18n.t('inventory.formulaExample.title')}
    >
        <Table
            fullWidth
            head={
                <thead>
                    <tr>
                        <th>{i18n.t('inventory.formulaExample.data')}</th>
                        <th>{i18n.t('inventory.formulaExample.result')}</th>
                        <th>{i18n.t('inventory.formulaExample.correct')}</th>
                    </tr>
                </thead>
            }
            body={
                <tbody>
                    <tr>
                        <td>
                            <Input
                                value={userExample}
                                onChange={e => setUserExample(e.currentTarget.value)}
                                placeholder={i18n.t('inventory.formulaExample.try')}
                            />
                        </td>
                        <td><Formula formula={userExample} /></td>
                        <td>
                            {isUserExampleCorrect
                                ? <FontAwesome style='fas' icon='faCheck' />
                                : <FontAwesome style='fas' icon='faTimes' />
                            }
                        </td>
                    </tr>
                    {examples.map(e => <tr key={`${e[0]}:${e[1]}`}>
                        <td><code>{e}</code></td>
                        <td><Formula formula={e[0]} /></td>
                        <td>
                            {e[1]
                                ? <FontAwesome style='fas' icon='faCheck' />
                                : <FontAwesome style='fas' icon='faTimes' />
                            }
                        </td>
                    </tr>)}
                </tbody>
            }
        />
    </Modal>;
}
