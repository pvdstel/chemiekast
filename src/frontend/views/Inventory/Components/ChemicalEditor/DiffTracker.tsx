import clsx from 'clsx';
import React from 'react';
import { IChemical } from '@/api/chemical';
import { getNonSystemColumns, isDefaultRoleSession } from '@/api/session';
import Modal, { DefaultActions, IModalProps, modalAnimationDuration } from '@/components/Modal/Modal';
import Table from '@/components/Table/Table';
import i18n from '@/i18n/i18n';
import { determineStandardGhsSymbols, ghsLinkStringToArray } from '@/logic/ghs';
import SessionContext from '@/logic/sessionContext';
import { extractOrigin } from '@/logic/utilities';
import bulmaShared from '@/style/bulmaShared';
import { renderCellValue } from '../InventoryWorksheet/InventoryWorksheet';
import { EditingChemicalType } from './ChemicalEditor';

export interface IDiffTrackerProps extends Pick<IModalProps, 'requestClose'> {
    editingChemical: EditingChemicalType;
    delta: Partial<IChemical> | undefined;
}

type DiffItem = [string, React.ReactNode, React.ReactNode];

export default function DiffTracker(props: IDiffTrackerProps) {
    const { editingChemical } = props;
    const [internalDelta, setInternalDelta] = React.useState<Partial<IChemical> | undefined>(undefined);
    const clearingTimeout = React.useRef<number | undefined>(undefined);
    React.useEffect(() => {
        if (props.delta !== undefined) {
            setInternalDelta(props.delta);
            window.clearTimeout(clearingTimeout.current);
        } else {
            clearingTimeout.current = window.setTimeout(() => setInternalDelta(undefined), modalAnimationDuration);
        }
    }, [props.delta]);

    const sessionContext = React.useContext(SessionContext);
    const session = sessionContext.current;
    if (session === null || !isDefaultRoleSession(session)) {
        return null;
    }
    const nonSystemColumns = React.useMemo(() => getNonSystemColumns(session), [session]);

    const delta = internalDelta;

    const computeDiff = (): DiffItem[] => { // const because this depends on a session check (no hoisting)
        const diff: DiffItem[] = [];

        if (delta) {
            if (delta.ID !== undefined && delta.ID !== editingChemical.ID) {
                diff.push([i18n.t('inventory.properties.id'), editingChemical.ID, delta.ID]);
            }

            if (delta.Name !== undefined) {
                diff.push([i18n.t('inventory.properties.name'), editingChemical.Name, delta.Name]);
            }

            if (delta.Worksheet !== undefined) {
                const currentWorksheet = editingChemical.Worksheet !== undefined ? session.Worksheets.find(w => w.Code === editingChemical.Worksheet) : undefined;
                const newWorksheet = session.Worksheets.find(w => w.Code === delta.Worksheet);
                diff.push([
                    i18n.t('inventory.properties.worksheet'),
                    currentWorksheet !== undefined ? currentWorksheet.Name : '',
                    newWorksheet !== undefined ? newWorksheet.Name : '',
                ]);
            }

            if (delta.GhsSymbols !== undefined) {
                diff.push([
                    i18n.t('inventory.properties.ghsSymbols'),
                    determineStandardGhsSymbols(editingChemical.GhsSymbols).map(g => g.Name).join(', '),
                    determineStandardGhsSymbols(delta.GhsSymbols).map(g => g.Name).join(', '),
                ]);
            }

            if (delta.GhsLinks !== undefined) {
                diff.push([
                    i18n.t('inventory.properties.ghsLinks'),
                    ghsLinkStringToArray(editingChemical.GhsLinks).map(extractOrigin).join(', '),
                    ghsLinkStringToArray(delta.GhsLinks).map(extractOrigin).join(', '),
                ]);
            }

            for (const column of nonSystemColumns) {
                if (delta[column.Name] !== undefined) {
                    diff.push([
                        column.UIName,
                        renderCellValue(editingChemical as IChemical, column),
                        renderCellValue(delta as IChemical, column),
                    ]);
                }
            }
        }

        return diff;
    };

    function renderTable() {
        const diff = computeDiff();

        return <Table
            fullWidth
            head={
                <thead>
                    <tr>
                        <th>{i18n.t('inventory.diff.property')}</th>
                        <th>{i18n.t('inventory.diff.current')}</th>
                        <th>{i18n.t('inventory.diff.new')}</th>
                    </tr>
                </thead>
            }
            body={
                <tbody>
                    {diff.map((d, i) => (
                        <tr key={d[0] + i}>
                            <th>{d[0]}</th>
                            <td>{d[1]}</td>
                            <td>{d[2]}</td>
                        </tr>
                    ))}
                </tbody>
            }
        />;
    }

    const isNewChemical = editingChemical.ID === undefined || (editingChemical.ID !== undefined && (delta !== undefined && delta.ID === undefined));
    const isModifyingOther = !isNewChemical && (delta !== undefined && editingChemical.ID !== delta.ID);

    return React.useMemo(() => <Modal
        actions={[DefaultActions.getCloseAction()]}
        open={props.delta !== undefined}
        requestClose={props.requestClose}
        title={i18n.t('inventory.diff.title')}
    >
        {isNewChemical && (
            <div className={clsx(bulmaShared.notification, bulmaShared.isInfo)}>
                <p>{i18n.t('inventory.diff.addingNew')}</p>
            </div>
        )}
        {isModifyingOther && (
            <div className={clsx(bulmaShared.notification, bulmaShared.isInfo)}>
                <p>{i18n.t('inventory.diff.modifyingOther', { id: delta!.ID })}</p>
            </div>
        )}
        {renderTable()}
    </Modal>, [props.delta, internalDelta, session, i18n.language]);
}
