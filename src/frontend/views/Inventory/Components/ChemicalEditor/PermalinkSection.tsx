import React from 'react';
import Button from '@/components/Button';
import FontAwesome from '@/components/FA';
import { Control, Field, Input } from '@/components/Form/Form';
import { SidebarTitle } from '@/components/Sidebar/Sidebar';
import i18n from '@/i18n/i18n';
import { broadcastNotification, createNotification, NotificationType } from '@/logic/notifications';
import bulmaShared from '@/style/bulmaShared';

export interface IPermalinkSectionProps {
    permalink: string;
}

export default function PermalinkSection(props: IPermalinkSectionProps) {
    const permalinkField = React.useRef<HTMLInputElement>(null);
    const { permalink } = props;

    function copyPermalinkField() {
        const linkBox = permalinkField.current!;
        const activeElement = document.activeElement;
        linkBox.focus();
        linkBox.select();
        document.execCommand('copy');
        if (activeElement instanceof HTMLElement) { activeElement.focus(); }
        broadcastNotification(createNotification(i18n.t('inventory.notifications.permalinkCopied'), NotificationType.info, 3000));
    }

    return <div>
        <SidebarTitle>{i18n.t('inventory.properties.permalink')}</SidebarTitle>
        <Field hasAddons>
            <Control>
                <a href={permalink}
                    target='_blank'
                    className={bulmaShared.button}>
                    <FontAwesome style='fas' icon='faLink' />
                </a>
            </Control>
            <Control expanded>
                <Input value={permalink} readOnly innerRef={permalinkField} />
            </Control>
            <Control>
                <Button color='primary' onClick={copyPermalinkField} title={i18n.t('general.copy')}>
                    <FontAwesome style='far' icon='faCopy' />
                </Button>
            </Control>
        </Field>
    </div>;
}
