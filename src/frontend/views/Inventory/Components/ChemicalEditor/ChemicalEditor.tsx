import React from 'react';
import { addChemical, deleteChemical, editChemical, IChemical } from '@/api/chemical';
import i18n from '@/i18n/i18n';
import { handleUnsuccessfulApiCall } from '@/logic/api';
import { broadcastNotification, createNotification, NotificationType } from '@/logic/notifications';
import { inventoryChemicalRoute } from '@/logic/routes';
import { DeleteSection } from './DeleteSection';
import EditSection from './EditSection';
import PermalinkSection from './PermalinkSection';

export type EditingChemicalType = Partial<IChemical>;

export interface IChemicalEditorProps {
    editingChemical: EditingChemicalType;
    preferredWorksheetCode: string;
    requestClear: () => void;
    onModification: (change: number | EditingChemicalType) => void;
}

export interface IChemicalEditorState {
    isSubmitting: boolean;
}

export default class ChemicalEditor extends React.Component<IChemicalEditorProps, IChemicalEditorState> {
    constructor(props: IChemicalEditorProps) {
        super(props);

        this.state = {
            isSubmitting: false,
        };
    }

    private onSubmitEditSection = (delta: EditingChemicalType, clear: boolean) => {
        if (this.state.isSubmitting) { return; }
        this.setState({ isSubmitting: true });
        const action = delta.ID === undefined ? addChemical : editChemical;
        action(delta, r => {
            this.setState({ isSubmitting: false });
            if (r.isSuccessful()) {
                this.props.onModification(delta);
                if (clear) {
                    this.props.requestClear();
                } else {
                    Object.keys(delta).forEach(k => this.props.editingChemical[k] = delta[k]);
                }
                broadcastNotification(createNotification(i18n.t('inventory.notifications.saved'), NotificationType.success, 3000));
            } else if (r.isUnsuccessful()) {
                handleUnsuccessfulApiCall(r);
            }
        });
    }

    private onSubmitDeleteSection = (id: number) => {
        if (this.state.isSubmitting) { return; }
        this.setState({ isSubmitting: true });
        deleteChemical(id, r => {
            this.setState({ isSubmitting: false });
            if (r.isSuccessful()) {
                this.props.onModification(id);
                this.props.requestClear();
                broadcastNotification(createNotification(i18n.t('inventory.notifications.deleted'), NotificationType.success, 3000));
            } else if (r.isUnsuccessful()) {
                handleUnsuccessfulApiCall(r);
            }
        });
    }

    render() {
        const { editingChemical, requestClear, preferredWorksheetCode } = this.props;

        const showPermalink = editingChemical.ID !== undefined;
        const permalink = editingChemical.ID !== undefined
            ? `${location.origin}${inventoryChemicalRoute.fill(editingChemical.ID.toString())}`
            : location.origin;

        return <>
            <EditSection
                editingChemical={editingChemical}
                preferredWorksheetCode={preferredWorksheetCode}
                onSubmit={this.onSubmitEditSection}
                requestClear={requestClear}
                disabled={this.state.isSubmitting}
            />
            <hr />
            <DeleteSection
                editingChemical={editingChemical}
                onSubmit={this.onSubmitDeleteSection}
                disabled={this.state.isSubmitting}
            />
            {showPermalink && <hr />}
            {showPermalink && <PermalinkSection permalink={permalink} />}
        </>;
    }
}
