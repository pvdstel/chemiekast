import React from 'react';
import Button from '@/components/Button';
import FontAwesome from '@/components/FA';
import BlurInput, { Control, Field } from '@/components/Form/Form';
import Modal, { DefaultActions, IModalProps, modalAnimationDuration } from '@/components/Modal/Modal';
import i18n from '@/i18n/i18n';
import { ghsLinkArrayToString, ghsLinkStringToArray } from '@/logic/ghs';
import { EditingChemicalType } from './ChemicalEditor';

export interface IGhsLinkEditorProps extends Pick<IModalProps, 'open' | 'requestClose'> {
    editingChemical: EditingChemicalType;
    ghsLinks: string | undefined;
    allowSave: boolean;
    onCommit: (links: string, save: boolean) => void;
}

export default function GhsLinkEditor(props: IGhsLinkEditorProps) {
    const [ghsLinks, setGhsLinks] = React.useState<string[]>([]);
    const clearingTimeout = React.useRef<number | undefined>();

    const { open, editingChemical, requestClose, allowSave, onCommit } = props;

    React.useEffect(() => {
        if (open) {
            window.clearTimeout(clearingTimeout.current);
            setGhsLinks(ghsLinkStringToArray(props.ghsLinks));
        } else {
            clearingTimeout.current = window.setTimeout(() => setGhsLinks([]), modalAnimationDuration);
        }
    }, [open, editingChemical]);

    function createChangeHandler(index: number) {
        return (e: React.ChangeEvent<HTMLInputElement>) => {
            const newLinks = ghsLinks.map((l, li) => li === index ? e.currentTarget.value : l);
            setGhsLinks(newLinks);
        };
    }

    function createRemoveHandler(index: number) {
        return () => {
            const newLinks = ghsLinks.filter((_l, li) => li !== index);
            setGhsLinks(newLinks);
        };
    }

    function addHandler() {
        setGhsLinks(ghsLinks.concat(''));
    }

    function renderEditBox(ghsLink: string, index: number) {
        return (
            <Field hasAddons key={index + ghsLink}>
                <Control expanded>
                    <BlurInput
                        onChange={createChangeHandler(index)}
                        value={ghsLink}
                    />
                </Control>
                <Control>
                    <Button onClick={createRemoveHandler(index)}>
                        <FontAwesome icon='faTimes' style='fas' />
                    </Button>
                </Control>
            </Field>
        );
    }

    const actions = [
        DefaultActions.getOkAction(() => onCommit(ghsLinkArrayToString(ghsLinks), false)),
        DefaultActions.getCancelAction(),
    ];

    if (allowSave) {
        actions.push({
            label: i18n.t('general.save'),
            callback: () => onCommit(ghsLinkArrayToString(ghsLinks), true),
        });
    }

    return (
        <Modal
            open={open}
            requestClose={requestClose}
            title={i18n.t('inventory.ghsEditor.title')}
            actions={actions}
        >
            {ghsLinks.map(renderEditBox)}
            <Field>
                <Control>
                    <Button color='primary' onClick={addHandler}>{i18n.t('inventory.ghsEditor.addLink')}</Button>
                </Control>
            </Field>
        </Modal>
    );
}
