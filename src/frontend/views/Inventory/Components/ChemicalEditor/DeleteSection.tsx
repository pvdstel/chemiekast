import React from 'react';
import Button, { Buttons } from '@/components/Button';
import FontAwesome from '@/components/FA';
import { Control, Field, Input } from '@/components/Form/Form';
import Icon from '@/components/Icon';
import Modal, { DefaultActions } from '@/components/Modal/Modal';
import { SidebarTitle } from '@/components/Sidebar/Sidebar';
import i18n from '@/i18n/i18n';
import bulmaShared from '@/style/bulmaShared';
import { EditingChemicalType } from './ChemicalEditor';

export interface IDeleteSectionProps {
    editingChemical: EditingChemicalType;
    onSubmit: (id: number) => void;
    disabled?: boolean;
}

interface IDeleteSectionState {
    id: number | undefined;
    confirmationModalOpen: boolean;
}

const DeletionIdFieldId = 'chemical-editor-deletion-id';

export class DeleteSection extends React.Component<IDeleteSectionProps, IDeleteSectionState> {
    constructor(props: IDeleteSectionProps) {
        super(props);

        this.state = {
            id: undefined,
            confirmationModalOpen: false,
        };
    }

    private getFinalId = () => this.state.id || this.props.editingChemical.ID;

    private onSubmit = (e: React.FormEvent) => {
        e.preventDefault();
        this.setState({ confirmationModalOpen: true });
    }

    private onConfirm = () => {
        const finalId = this.getFinalId();
        this.props.onSubmit(finalId || 0);
    }

    private confirmModal = () => {
        const finalId = this.getFinalId();
        return (
            <Modal
                open={this.state.confirmationModalOpen}
                requestClose={() => this.setState({ confirmationModalOpen: false })}
                actions={[DefaultActions.getDeleteAction(this.onConfirm), DefaultActions.getCancelAction()]}
                title={i18n.t('inventory.confirmDelete.title')}
            >
                <div className={bulmaShared.content}>
                    <p>{i18n.t('inventory.confirmDelete.action')}</p>
                    <ul>
                        <li>
                            {finalId === this.props.editingChemical.ID
                                ? i18n.t('inventory.confirmDelete.byName', { name: this.props.editingChemical.Name, id: finalId })
                                : i18n.t('inventory.confirmDelete.withId', { id: finalId })
                            }
                        </li>
                    </ul>
                    <p>{i18n.t('inventory.confirmDelete.consequence')}</p>
                </div>
            </Modal>
        );
    }

    componentDidUpdate(prevProps: IDeleteSectionProps) {
        if (prevProps.editingChemical !== this.props.editingChemical) {
            this.setState({ id: this.props.editingChemical.ID });
        }
    }

    render() {
        return <>
            <form onSubmit={this.onSubmit}>
                <SidebarTitle>{i18n.t('inventory.properties.deleteTitle')}</SidebarTitle>
                <Field>
                    <label htmlFor={DeletionIdFieldId} className={bulmaShared.requiredField}>{i18n.t('inventory.properties.deletionId')}</label>
                    <Control>
                        <Input
                            id={DeletionIdFieldId}
                            value={this.state.id || ''}
                            onChange={e => this.setState({ id: Number(e.currentTarget.value) || undefined })}
                            disabled={this.props.disabled}
                            type='number'
                            min={1}
                            max={2147483647}
                            required />
                    </Control>
                </Field>
                <Buttons hasAddons>
                    <Button color='warning' type='submit' disabled={this.props.disabled}>
                        <Icon><FontAwesome style='fas' icon='faTrash' /></Icon>
                        <span>{i18n.t('general.delete')}</span>
                    </Button>
                    <Button type='reset'
                        onClick={() => this.setState({ id: this.props.editingChemical.ID })}
                        title={i18n.t('general.undo')}
                        disabled={this.props.disabled}>
                        <FontAwesome style='fas' icon='faUndo' />
                    </Button>
                </Buttons>
            </form>
            {this.confirmModal()}
        </>;
    }
}
