import clsx from 'clsx';
import React from 'react';
import { IChemical } from '@/api/chemical';
import { CHEMICALS_ID_COLUMN, CHEMICALS_NAME_COLUMN, CHEMICALS_WORKSHEET_COLUMN, isNumericColumn } from '@/api/column';
import { getNonSystemColumns, isDefaultRoleSession, ISessionWorksheet } from '@/api/session';
import { IWorksheetColumn } from '@/api/worksheet';
import Button, { Buttons } from '@/components/Button';
import FontAwesome from '@/components/FA';
import { Control, Field, Input, Select } from '@/components/Form/Form';
import Icon from '@/components/Icon';
import i18n from '@/i18n/i18n';
import SessionContext from '@/logic/sessionContext';
import bulmaShared from '@/style/bulmaShared';
import { EditingChemicalType } from './ChemicalEditor';
import css from './ChemicalEditor.scss';
import DiffTracker from './DiffTracker';
import FormulaExamples from './FormulaExamples';
import GhsLinkEditor from './GhsLinkEditor';
import GhsSymbolEditor from './GhsSymbolEditor';

export interface IEditSectionProps {
    editingChemical: EditingChemicalType;
    preferredWorksheetCode: string;
    onSubmit: (delta: EditingChemicalType, clear: boolean) => void;
    requestClear: () => void;
    disabled?: boolean;
}

interface IEditSectionState {
    delta: Partial<IChemical>;
    ghsSymbolEditorOpen: boolean;
    ghsLinkEditorOpen: boolean;
    diffTrackerDelta: Partial<IChemical> | undefined;
    formulaExamplesOpen: boolean;
}

const IdFieldId = 'chemical-editor-field-id';
const NameFieldId = 'chemical-editor-field-name';
const WorksheetSelectId = 'chemical-editor-select-worksheet';
const CustomFieldId = (name: string) => `chemical-editor-custom-field-${name}`; // name field is alphanumeric characters only

export default class EditSection extends React.Component<IEditSectionProps, IEditSectionState> {
    static contextType = SessionContext;
    context!: React.ContextType<typeof SessionContext>;

    constructor(props: IEditSectionProps) {
        super(props);

        this.state = {
            delta: { ID: props.editingChemical.ID },
            ghsSymbolEditorOpen: false,
            ghsLinkEditorOpen: false,
            diffTrackerDelta: undefined,
            formulaExamplesOpen: false,
        };
    }

    private getValues = () => ({ ...this.props.editingChemical, ...this.state.delta });

    private undoChanges = () => this.setState({ delta: { ID: this.props.editingChemical.ID } });

    private updateValues = (values: Partial<IChemical>) => this.setState({ delta: { ...this.state.delta, ...values } });

    private modifyValue = (column: string, value: string | number) => {
        if (isNumericColumn(column)) {
            if (column === CHEMICALS_ID_COLUMN) {
                const id = Number(value);
                this.updateValues({ ID: (id === 0 || isNaN(id)) ? undefined : id });
            } else {
                this.updateValues({ [column]: Number(value) });
            }
        } else {
            this.updateValues({ [column]: value });
        }
    }

    private getFinalDelta = (): EditingChemicalType => {
        const base = this.props.editingChemical;
        const delta = this.state.delta;
        if (base.ID === undefined || (base.ID !== undefined && delta.ID === undefined) || (delta.ID !== undefined && delta.ID !== base.ID)) {
            // The chemical ID has changed, or we're adding a new chemical.
            // Therefore, we need to construct a full delta.
            return { Worksheet: this.props.preferredWorksheetCode, ...base, ...delta };
        } else {
            // The chemical ID is unchanged, and we're not adding a new chemical.
            // Therefore, we can send just the modified fields.
            const finalDelta: EditingChemicalType = { ID: base.ID };
            Object.keys(delta).forEach(key => {
                if (delta[key] !== base[key]) {
                    finalDelta[key] = delta[key];
                }
            });

            return finalDelta;
        }
    }

    private onCommitGhsSymbols = (symbols: number | undefined, save: boolean) => {
        if (save) {
            if (!this.props.editingChemical.ID !== undefined) {
                this.props.onSubmit({
                    ID: this.props.editingChemical.ID,
                    GhsSymbols: symbols,
                }, false);
                const { GhsSymbols, ...deltaRest } = this.state.delta;
                this.setState({ delta: deltaRest });
            }
        } else {
            this.updateValues({ GhsSymbols: symbols });
        }
    }

    private onCommitGhsLinks = (links: string, save: boolean) => {
        if (save) {
            if (!this.props.editingChemical.ID !== undefined) {
                this.props.onSubmit({
                    ID: this.props.editingChemical.ID,
                    GhsLinks: links,
                }, false);
                const { GhsLinks, ...deltaRest } = this.state.delta;
                this.setState({ delta: deltaRest });
            }
        } else {
            this.updateValues({ GhsLinks: links });
        }
    }

    private renderIdField = (values: EditingChemicalType) => <Field>
        <label htmlFor={IdFieldId} className={bulmaShared.label}>{i18n.t('inventory.properties.id')}</label>
        <Control>
            <Input
                id={IdFieldId}
                value={values.ID || ''}
                onChange={e => this.modifyValue(CHEMICALS_ID_COLUMN, Number(e.currentTarget.value))}
                disabled={this.props.disabled}
                type='number'
                min={1}
                max={2147483647}
            />
        </Control>
    </Field>

    private renderNameField = (values: EditingChemicalType) => <Field>
        <label htmlFor={NameFieldId} className={bulmaShared.requiredField}>{i18n.t('inventory.properties.name')}</label>
        <Control>
            <Input
                id={NameFieldId}
                value={values.Name || ''}
                onChange={e => this.modifyValue(CHEMICALS_NAME_COLUMN, e.currentTarget.value)}
                disabled={this.props.disabled}
                type='text'
                maxLength={255}
                required />
        </Control>
    </Field>

    private renderGhsButtons = () => <Field>
        <label className={bulmaShared.label}>{i18n.t('inventory.properties.ghsData')}</label>
        <Field hasAddons>
            <Control>
                <Button disabled={this.props.disabled} color='link' outlined onClick={() => this.setState({ ghsSymbolEditorOpen: true })}>
                    {i18n.t('inventory.properties.ghsSymbols')}
                </Button>
            </Control>
            <Control>
                <Button disabled={this.props.disabled} color='link' outlined onClick={() => this.setState({ ghsLinkEditorOpen: true })}>
                    {i18n.t('inventory.properties.ghsLinks')}
                </Button>
            </Control>
        </Field>
    </Field>

    private renderNonSystemField = (c: IWorksheetColumn, values: EditingChemicalType) => {
        const id = CustomFieldId(c.Name);
        const hasAddons = c.IsFormula;
        return <Field
            key={c.Name}
            className={clsx(bulmaShared.column, bulmaShared.isFullMobile, bulmaShared.isHalfTablet)}
        >
            <label htmlFor={id} className={bulmaShared.label}>{c.UIName}</label>
            <Field hasAddons={hasAddons}>
                <Control expanded>
                    <Input
                        id={id}
                        value={values[c.Name] || ''}
                        onChange={e => this.modifyValue(c.Name, e.currentTarget.value)}
                        disabled={this.props.disabled}
                        type='text'
                        maxLength={255} />
                </Control>
                {c.IsFormula && <Control>
                    <Button onClick={() => this.setState({ formulaExamplesOpen: true })}>
                        <FontAwesome style='fas' icon='faInfoCircle' />
                    </Button>
                </Control>}
            </Field>
        </Field>;
    }

    private renderWorksheetSelect = (worksheets: ISessionWorksheet[], values: EditingChemicalType) => <Field>
        <label htmlFor={WorksheetSelectId} className={bulmaShared.requiredField}>{i18n.t('inventory.properties.worksheet')}</label>
        <Control>
            <Select id={WorksheetSelectId}
                value={values.Worksheet || this.props.preferredWorksheetCode || ''}
                onChange={e => this.modifyValue(CHEMICALS_WORKSHEET_COLUMN, e.currentTarget.value)}
                disabled={this.props.disabled}
                required>
                {worksheets
                    .filter(w => w.CanEdit)
                    .map(w => <option key={w.Code} value={w.Code}>{w.Name}</option>)}
            </Select>
        </Control>
    </Field>

    private renderEditFormButtons = () => <Buttons hasAddons>
        <Button disabled={this.props.disabled} color='primary' type='submit'>
            <Icon><FontAwesome style='fas' icon='faSave' /></Icon>
            <span>{i18n.t('general.save')}</span>
        </Button>
        <Button disabled={this.props.disabled} onClick={() => this.setState({ diffTrackerDelta: this.getFinalDelta() })} title={i18n.t('general.changes')}>
            <FontAwesome style='fas' icon='faColumns' />
        </Button>
        <Button disabled={this.props.disabled} onClick={this.undoChanges} title={i18n.t('general.undo')}>
            <FontAwesome style='fas' icon='faUndo' />
        </Button>
        <Button disabled={this.props.disabled} onClick={this.props.requestClear} title={i18n.t('general.clear')}>
            <FontAwesome style='fas' icon='faEraser' />
        </Button>
    </Buttons>

    private onSubmit = (e: React.FormEvent) => {
        e.preventDefault();
        const finalDelta = this.getFinalDelta();
        this.props.onSubmit(finalDelta, true);
    }

    componentDidUpdate(prevProps: IEditSectionProps) {
        if (prevProps.editingChemical !== this.props.editingChemical) {
            this.setState({ delta: { ID: this.props.editingChemical.ID } });
        }
    }

    render() {
        const session = this.context.current;
        if (session === null || !isDefaultRoleSession(session)) { return null; }

        const nonSystemColumns = getNonSystemColumns(session);
        const editingValues = this.getValues();

        return <>
            <form onSubmit={this.onSubmit}>
                {this.renderIdField(editingValues)}
                {this.renderNameField(editingValues)}
                {this.renderGhsButtons()}

                <div className={clsx(bulmaShared.columns, bulmaShared.isMultiline, css.nonSystem)}>
                    {nonSystemColumns.map(c => this.renderNonSystemField(c, editingValues))}
                </div>

                {this.renderWorksheetSelect(session.Worksheets, editingValues)}
                {this.renderEditFormButtons()}
            </form>
            <GhsSymbolEditor
                open={this.state.ghsSymbolEditorOpen}
                requestClose={() => this.setState({ ghsSymbolEditorOpen: false })}
                editingChemical={this.props.editingChemical}
                ghsSymbols={this.state.delta.GhsSymbols || this.props.editingChemical.GhsSymbols}
                allowSave={this.props.editingChemical.ID !== undefined}
                onCommit={this.onCommitGhsSymbols}
            />
            <GhsLinkEditor
                open={this.state.ghsLinkEditorOpen}
                requestClose={() => this.setState({ ghsLinkEditorOpen: false })}
                editingChemical={this.props.editingChemical}
                ghsLinks={this.state.delta.GhsLinks || this.props.editingChemical.GhsLinks}
                allowSave={this.props.editingChemical.ID !== undefined}
                onCommit={this.onCommitGhsLinks}
            />
            <DiffTracker
                requestClose={() => this.setState({ diffTrackerDelta: undefined })}
                editingChemical={this.props.editingChemical}
                delta={this.state.diffTrackerDelta}
            />
            <FormulaExamples
                open={this.state.formulaExamplesOpen}
                requestClose={() => this.setState({ formulaExamplesOpen: false })}
            />
        </>;
    }
}
