import React from 'react';
import Checkbox from '@/components/Form/Checkbox';
import Modal, { DefaultActions, IModalProps, modalAnimationDuration } from '@/components/Modal/Modal';
import i18n from '@/i18n/i18n';
import { determineStandardGhsSymbols, getGhsSymbolsNumber, ghsData, IGhsSymbol } from '@/logic/ghs';
import GhsSymbol from '../GHS/GhsSymbol';
import { EditingChemicalType } from './ChemicalEditor';
import css from './ChemicalEditor.scss';

export interface IGhsSymbolEditorProps extends Pick<IModalProps, 'open' | 'requestClose'> {
    editingChemical: EditingChemicalType;
    ghsSymbols?: number;
    allowSave: boolean;
    onCommit: (symbols: number | undefined, save: boolean) => void;
}

export default function GhsSymbolEditor(props: IGhsSymbolEditorProps) {
    const [enabledGhsSymbols, setEnabledGhsSymbols] = React.useState<IGhsSymbol[]>([]);
    const clearingTimeout = React.useRef<number | undefined>();

    const { allowSave, editingChemical, ghsSymbols, onCommit, open, requestClose } = props;

    React.useEffect(() => {
        if (open) {
            window.clearTimeout(clearingTimeout.current);
            setEnabledGhsSymbols(determineStandardGhsSymbols(ghsSymbols));
        } else {
            clearingTimeout.current = window.setTimeout(() => setEnabledGhsSymbols([]), modalAnimationDuration);
        }
    }, [open, editingChemical]);

    function toggleHandler(symbol: IGhsSymbol) {
        return () => {
            const symbols = enabledGhsSymbols.slice(0);
            const index = symbols.reduce((fi, s, i) => s.Symbol === symbol.Symbol ? i : fi, 0);
            if (symbols.some(s => s.Symbol === symbol.Symbol)) {
                symbols.splice(index, 1);
            } else {
                symbols.push(symbol);
            }
            setEnabledGhsSymbols(symbols);
        };
    }

    function renderGhsSymbol(symbol: IGhsSymbol) {
        const checked = enabledGhsSymbols && enabledGhsSymbols.some(s => s.Symbol === symbol.Symbol);
        const handler = toggleHandler(symbol);
        return <div key={symbol.Code}>
            <Checkbox checked={checked} onChange={handler} />
            <GhsSymbol symbol={symbol} className={css.ghsSymbol} imgProps={{ onClick: handler }} />
            <span onClick={handler}>{symbol.Name}</span>
        </div>;
    }

    const actions = [
        DefaultActions.getOkAction(() => onCommit(getGhsSymbolsNumber(enabledGhsSymbols), false)),
        DefaultActions.getCancelAction(),
    ];

    if (allowSave) {
        actions.push({
            label: i18n.t('general.save'),
            callback: () => onCommit(getGhsSymbolsNumber(enabledGhsSymbols), true),
        });
    }

    return <Modal
        open={open}
        requestClose={requestClose}
        title={i18n.t('inventory.ghsEditor.title')}
        actions={actions}
    >
        <div className={css.ghsCheckboxContainer}>
            {ghsData().map(renderGhsSymbol)}
        </div>
    </Modal>;
}
