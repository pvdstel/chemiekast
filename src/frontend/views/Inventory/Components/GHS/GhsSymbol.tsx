import clsx from 'clsx';
import React from 'react';
import { GhsSymbolClasses, IGhsSymbol } from '@/logic/ghs';
import css from './GhsSymbol.scss';

const symbolClassToImage = (className: GhsSymbolClasses): string => require(`@/resources/images/ghs/${className}.svg`).default;

export interface IGhsSymbolProps {
    symbol: IGhsSymbol;
    className?: string;
    showTitle?: boolean;
    imgProps?: Omit<React.DetailedHTMLProps<React.ImgHTMLAttributes<HTMLImageElement>, HTMLImageElement>, 'src' | 'alt' | 'className'>;
}

const GhsSymbol = React.memo(function (props: IGhsSymbolProps) {
    const image = symbolClassToImage(props.symbol.ClassName);

    return (
        <div
            className={clsx(props.className, css.symbol)}
            title={props.showTitle ? props.symbol.Description : undefined}
        >
            <img
                {...props.imgProps}
                className={css.symbol}
                alt={props.symbol.Description}
                src={image}
            />
        </div>
    );
});
export default GhsSymbol;
