import clsx from 'clsx';
import React from 'react';
import { sendLogActivity } from '@/api/track';
import Dropdown from '@/components/Dropdown/Dropdown';
import FontAwesome from '@/components/FA';
import i18n from '@/i18n/i18n';
import * as GHS from '@/logic/ghs';
import { extractOrigin } from '@/logic/utilities';
import css from './GhsIndicator.scss';
import GhsSymbol from './GhsSymbol';

export interface IGhsIndicatorProps {
    ghsSymbolsValue: number | undefined;
    ghsLinks: string | undefined;
    container?: React.ReactElement<React.HTMLProps<HTMLElement>>;
}

export default function GhsIndicator(props: IGhsIndicatorProps) {
    const status = GHS.determineGhsStatus(props.ghsSymbolsValue);
    const symbols = GHS.determineGhsSymbols(props.ghsSymbolsValue);
    const standardSymbols = GHS.determineStandardGhsSymbols(props.ghsSymbolsValue);
    const ghsLinks = GHS.ghsLinkStringToArray(props.ghsLinks);

    function renderMenu() {
        return (
            <div className={css.menu}>
                <div className={css.content}>
                    {symbols.map(s => <GhsSymbol className={css.symbol} showTitle key={s.Code} symbol={s} />)}
                    {ghsLinks.length > 0 && <>
                        <div className={css.divider} />
                        {ghsLinks.map(l => (
                            <a
                                key={l}
                                className={css.link}
                                href={l}
                                target='_blank'
                                rel='noopener nofollow'
                                title={extractOrigin(l)}
                                onClick={() => sendLogActivity('external-ghs-link', l)}
                            >
                                <FontAwesome style='fas' icon='faLink' />
                            </a>
                        ))}
                    </>}
                </div>
            </div>
        );
    }

    const symbolsText = standardSymbols.map(s => s.Name).join(', ');

    let tooltip;
    const ghsLinksTooltipText = i18n.t('inventory.ghs.nLinks', { count: ghsLinks.length });
    if (standardSymbols.length > 0) {
        tooltip = `${status.Description}\n${symbolsText}.\n${ghsLinksTooltipText}`;
    } else {
        tooltip = `${status.Description}\n${ghsLinksTooltipText}`;
    }

    const container = props.container;
    const containerNoPrint = container
        ? React.cloneElement(container, { ...container.props, className: clsx(container.props.className, css.noPrint) })
        : <div className={css.noPrint} />;

    const printSymbols = standardSymbols.map(s => <GhsSymbol key={s.Code} symbol={s} />);
    const containerPrint = container
        ? React.cloneElement(
            container, {
                ...container.props,
                className: clsx(container.props.className, css.print),
            },
            <div className={css.sideBySide}>{printSymbols}</div>,
        )
        : <div className={clsx(css.print, css.sideBySide)}>{printSymbols}</div>;

    return (
        <>
            <Dropdown
                container={containerNoPrint}
                trigger={
                    <a title={tooltip} className={css.trigger} href='#'>
                        <GhsSymbol symbol={status} />
                    </a>
                }
                menu={renderMenu()}
                menuOpenClassName={css.opened}
                menuOpeningClassName={css.opening}
                menuClosingClassName={css.closing}
            />
            {containerPrint}
        </>
    );
}
