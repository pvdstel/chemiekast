import clsx from 'clsx';
import React from 'react';
import { IInventoryWorksheet } from '@/api/inventory';
import { isManagerSession, isSessionAuthorized } from '@/api/session';
import Button, { Buttons } from '@/components/Button';
import FontAwesome from '@/components/FA';
import { PageLoader } from '@/components/Loader/Loader';
import { navigationHeight } from '@/components/Navigation/Navigation';
import { notifyTableUpdate } from '@/components/Table/Table';
import i18n from '@/i18n/i18n';
import { ghsData } from '@/logic/ghs';
import InventoryContext from '@/logic/inventoryContext';
import SessionContext from '@/logic/sessionContext';
import { setPersistentStorageItem } from '@/logic/storage';
import bulmaShared from '@/style/bulmaShared';
import AddToRole from '../AddToRole/AddToRole';
import ColumnVisibilityModal from '../ColumnVisibilityModal';
import GhsSymbol from '../GHS/GhsSymbol';
import InventoryWorksheet from '../InventoryWorksheet/InventoryWorksheet';
import css from './InventoryPane.scss';

export const worksheetTocId = (worksheetCode: string) => `inventory-worksheet-${worksheetCode}`;
export function renderEmptyInventory() {
    return <div className={clsx(bulmaShared.notification, bulmaShared.isInfo, bulmaShared.isInlineBlockTablet)}>
        {i18n.t('inventory.empty')}
    </div>;
}

export interface IInventoryPaneProps {
    title: string;
    superTitle: string;
    worksheets?: IInventoryWorksheet[];
    renderWorksheetTitles?: boolean;
    date: Date;
}

export default function InventoryPane(props: IInventoryPaneProps) {
    const { title, superTitle, date, worksheets } = props;
    const session = React.useContext(SessionContext).current;
    if (!session || !isSessionAuthorized(session)) { return null; }

    const inventoryContext = React.useContext(InventoryContext);
    const [columnVisibilityModalOpen, setColumnVisibilityModalOpen] = React.useState(false);

    const renderDateTime = (forPrint: boolean) => {
        let dateRender;
        if (forPrint) {
            dateRender = i18n.t(
                'inventory.generatedAtPrint',
                {
                    replace: {
                        datetime: date.toLocaleString(),
                        iso: date.toISOString(),
                        name: `${session.User.FirstName} ${session.User.LastName}`,
                    }, interpolation: { escapeValue: false },
                },
            );
        } else {
            dateRender = i18n.t(
                'inventory.generatedAt',
                { replace: { datetime: date.toLocaleString() }, interpolation: { escapeValue: false } },
            );
        }

        // use innerHTML because Firefox tends to return HTML entities
        return <time
            className={css.date}
        >
            {dateRender}
        </time>;
    };

    function renderHeading(chemicals?: number) {
        return <>
            <h4 className={clsx(bulmaShared.subtitle, bulmaShared.is4)}>{superTitle}</h4>
            <h1 className={clsx(bulmaShared.title, bulmaShared.is1)}>
                {title}
                {chemicals !== undefined && ' '}
                {chemicals !== undefined &&
                    <span className={clsx(bulmaShared.tag, bulmaShared.isRounded, bulmaShared.isMedium)}>
                        {chemicals}
                    </span>
                }
            </h1>

            {renderDateTime(true)}
        </>;
    }

    function renderControls() {
        return <Buttons className={css.controls} hasAddons>
            <Button onClick={() => inventoryContext.current.refreshHandler()}>
                <FontAwesome fixedWidth style='fas' icon='faSync' />
            </Button>
            <Button
                className={inventoryContext.current.sidebarPinned ? css.activeButton : undefined}
                onClick={() => {
                    setPersistentStorageItem('inventory-sidebar-pinned', !inventoryContext.current.sidebarPinned);
                    inventoryContext.update({ sidebarPinned: !inventoryContext.current.sidebarPinned });
                    notifyTableUpdate();
                }}
            >
                <FontAwesome fixedWidth style='fas' icon='faThumbtack' />
            </Button>
            <Button
                className={inventoryContext.current.hiddenColumns.length > 0 ? css.activeButton : undefined}
                onClick={() => setColumnVisibilityModalOpen(true)}
            >
                <FontAwesome fixedWidth style='fas' icon='faEye' />
            </Button>
            <Button
                className={inventoryContext.current.spaceTable ? css.activeButton : undefined}
                onClick={() => {
                    setPersistentStorageItem('space-inventory-tables', !inventoryContext.current.spaceTable);
                    inventoryContext.update({ spaceTable: !inventoryContext.current.spaceTable });
                }}
            >
                <FontAwesome fixedWidth style='fas' icon='faExpand' />
            </Button>
            <Button onClick={() => window.print()}>
                <FontAwesome fixedWidth style='fas' icon='faPrint' />
            </Button>
        </Buttons>;
    }

    function renderGhsLegend() {
        return <div className={css.legend}>
            <h2 className={clsx(bulmaShared.title, bulmaShared.is2)}>{i18n.t('inventory.legend.legend')}</h2>
            <h4 className={clsx(bulmaShared.title, bulmaShared.is4)}>{i18n.t('inventory.legend.ghs')}</h4>
            <div className={css.ghsLegend}>
                {ghsData().map(ghs => (
                    <div key={ghs.Symbol}>
                        <GhsSymbol symbol={ghs} />
                        <GhsSymbol symbol={ghs} />
                        <div>
                            <p>{ghs.Name}</p>
                            <p><i>{ghs.Code}</i></p>
                        </div>
                    </div>
                ))}
            </div>
        </div>;
    }

    function renderToc() {
        return <ul className={css.toc}>
            {(worksheets || []).map(w => {
                const headerId = worksheetTocId(w.Code);
                const onClick = (e: React.MouseEvent<HTMLAnchorElement>) => {
                    e.preventDefault();
                    const header = document.getElementById(headerId);
                    if (header) {
                        window.scrollTo({ top: header.offsetTop - navigationHeight - 14 });
                    }
                };
                return <li key={w.Code}>
                    <a
                        onClick={onClick}
                        href={`#${headerId}`}
                        className={clsx(bulmaShared.button, bulmaShared.isInfo, bulmaShared.isOutlined)}
                    >
                        {w.Name}
                    </a>
                </li>;
            })}
        </ul>;
    }

    if (worksheets === undefined) {
        return <>
            {renderHeading(undefined)}
            <PageLoader message={i18n.t('general.loading')} />
        </>;
    }

    const moreThanOneWorksheet = worksheets.length > 1;
    const chemicalCount = worksheets
        .map(w => w.Chemicals.length)
        .reduce((p, c) => p + c, 0);

    return <>
        {renderHeading(chemicalCount)}
        {renderControls()}
        {moreThanOneWorksheet && renderToc()}
        {worksheets.length > 0
            ? worksheets.map(ws => <InventoryWorksheet
                key={ws.Code}
                worksheet={ws}
                renderTitle={props.renderWorksheetTitles || moreThanOneWorksheet}
            />)
            : renderEmptyInventory()
        }
        {session.CanAccessDomainSettings && isManagerSession(session) && session.CustomRolesEnabled &&
            <AddToRole />
        }
        <hr className={css.hr} />
        {renderDateTime(false)}
        {renderGhsLegend()}
        <ColumnVisibilityModal
            open={columnVisibilityModalOpen}
            requestClose={() => setColumnVisibilityModalOpen(false)}
        />
    </>;
}
