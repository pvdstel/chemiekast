import React from 'react';
import { RouteComponentProps } from 'react-router';
import { getSearchResults, IInventoryWorksheet } from '@/api/inventory';
import { isSessionAuthorized } from '@/api/session';
import i18n from '@/i18n/i18n';
import { handleUnsuccessfulApiCall } from '@/logic/api';
import InventoryContext from '@/logic/inventoryContext';
import { inventorySearchRoute } from '@/logic/routes';
import { useTitle } from '@/logic/routing';
import SessionContext from '@/logic/sessionContext';
import InventoryPane from './InventoryPane/InventoryPane';

type SearchInventoryPropType = RouteComponentProps<typeof inventorySearchRoute.params>;

export default function SearchInventory(props: SearchInventoryPropType) {
    const [searchResults, setSearchResults] = React.useState<IInventoryWorksheet[]>();
    const [date, setDate] = React.useState(new Date());
    const inventoryContext = React.useContext(InventoryContext);
    const sessionContext = React.useContext(SessionContext);

    const query = props.match.params.query;
    const isAll = query === undefined;

    const session = sessionContext.current;
    if (session === null || !isSessionAuthorized(session)) {
        return null;
    }

    React.useEffect(() => {
        refreshSearchResults();
        inventoryContext.setWorksheetCode('');
        inventoryContext.update({ refreshHandler: refreshSearchResults });
    }, [query, session.DomainName]);

    function refreshSearchResults() {
        const searchQuery = query;
        getSearchResults(searchQuery || '', r => {
            if (r.isSuccessful()) {
                setSearchResults(r.result);
                setDate(new Date());
            } else if (r.isUnsuccessful()) {
                setSearchResults(undefined);
                handleUnsuccessfulApiCall(r);
            }
        });
    }

    useTitle(isAll ? i18n.t('inventory.allInDomain') : i18n.t('inventory.searchResultsForQuery', { query }));

    return <>
        <InventoryPane
            title={isAll ? session.DomainName : query!}
            superTitle={isAll ? i18n.t('inventory.allInDomain') : i18n.t('inventory.searchResultsFor')}
            worksheets={searchResults}
            renderWorksheetTitles
            date={date}
        />
    </>;
}

