import React from 'react';
import { IChemical } from '@/api/chemical';
import FontAwesome from '@/components/FA';
import InventoryContext from '@/logic/inventoryContext';

export interface IEditButtonProps {
    chemical: IChemical;
}

export default function EditButton(props: IEditButtonProps) {
    const inventoryContext = React.useContext(InventoryContext);
    const { chemical } = props;

    function onClick(e: React.MouseEvent) {
        e.preventDefault();
        inventoryContext.editChemical(props.chemical);
    }

    return (
        <a title={String(chemical.ID)} href='#' onClick={onClick}>
            <FontAwesome style='fas' icon='faPen' fixedWidth />
        </a>
    );
}
