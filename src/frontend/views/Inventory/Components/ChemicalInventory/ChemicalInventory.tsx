import clsx from 'clsx';
import React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import { getChemical, GetChemicalResultType } from '@/api/chemical';
import { getNonSystemColumns, isSessionAuthorized } from '@/api/session';
import Button, { Buttons } from '@/components/Button';
import { PageLoader } from '@/components/Loader/Loader';
import i18n from '@/i18n/i18n';
import { handleUnsuccessfulApiCall } from '@/logic/api';
import InventoryContext from '@/logic/inventoryContext';
import { inventoryChemicalRoute, inventoryWorksheetRoute } from '@/logic/routes';
import SessionContext from '@/logic/sessionContext';
import bulmaShared from '@/style/bulmaShared';
import GhsIndicator from '../GHS/GhsIndicator';
import { renderCellValue } from '../InventoryWorksheet/InventoryWorksheet';
import css from './ChemicalInventory.scss';

type ChemicalInventoryPropsType = RouteComponentProps<typeof inventoryChemicalRoute.params>;

export default function ChemicalInventory(props: ChemicalInventoryPropsType) {
    const [value, setValue] = React.useState<GetChemicalResultType>();
    const chemicalId = Number(props.match.params.id);
    const inventoryContext = React.useContext(InventoryContext);
    const session = React.useContext(SessionContext).current;
    if (session === null || !isSessionAuthorized(session)) {
        return null;
    }
    const nonSystemColumns = React.useMemo(() => getNonSystemColumns(session), [session]);

    function fetchChemical() {
        if (chemicalId !== undefined) {
            getChemical(chemicalId, r => {
                if (r.isSuccessful()) {
                    setValue(r.result);
                    inventoryContext.setWorksheetCode(r.result.Chemical.Worksheet);
                } else if (r.isUnsuccessful()) {
                    setValue(undefined);
                    handleUnsuccessfulApiCall(r);
                }
            });
        }
    }

    React.useEffect(() => {
        fetchChemical();
        inventoryContext.update({ refreshHandler: fetchChemical });
    }, [chemicalId, session.DomainName]);

    const renderContent = () => {
        if (value && value.Chemical) {
            return <>
                <h1 className={clsx(bulmaShared.title, bulmaShared.is1)}>{value.Chemical.Name}</h1>
                <Buttons className={css.buttons} hasAddons>
                    {value.Editable &&
                        <Button onClick={() => inventoryContext.editChemical(value.Chemical)}>
                            {i18n.t('general.edit')}
                        </Button>
                    }
                    <Link
                        className={clsx(bulmaShared.button)}
                        to={inventoryWorksheetRoute.fill(value.Chemical.Worksheet)(value.Chemical.ID.toString())}
                    >
                        {i18n.t('inventory.openInWorksheet')}
                    </Link>
                </Buttons>
                <dl className={css.propertyList}>
                    <>
                        <dt>{i18n.t('inventory.properties.name')}</dt>
                        <dl>{value.Chemical.Name}</dl>
                        <dt>{i18n.t('inventory.ghsColumn')}</dt>
                        <dd>
                            <GhsIndicator
                                ghsSymbolsValue={value.Chemical.GhsSymbols}
                                ghsLinks={value.Chemical.GhsLinks}
                                container={<div className={css.ghsCell} />}
                            />
                        </dd>
                    </>
                    {nonSystemColumns.map(c => (
                        <React.Fragment key={c.Name}>
                            <dt>{c.UIName}</dt>
                            <dd>{renderCellValue(value.Chemical, c)}</dd>
                        </React.Fragment>
                    ))}
                </dl>
            </>;
        } else {
            if (chemicalId !== undefined) {
                return <PageLoader message={i18n.t('general.loading')} />;
            } else {
                return <p>The URL is invalid.</p>;
            }
        }
    };

    return <>
        <h4 className={clsx(bulmaShared.subtitle, bulmaShared.is4)}>{i18n.t('inventory.chemical')}</h4>
        {renderContent()}
    </>;
}

