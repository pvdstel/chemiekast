import clsx from 'clsx';
import React from 'react';
import { getConfiguration, IConfiguration, resetConfiguration, setConfiguration } from '@/api/config';
import Button, { Buttons } from '@/components/Button';
import Checkbox from '@/components/Form/Checkbox';
import { Control, Field, Input } from '@/components/Form/Form';
import { PageLoader } from '@/components/Loader/Loader';
import i18n from '@/i18n/i18n';
import { handleUnsuccessfulApiCall } from '@/logic/api';
import SessionContext from '@/logic/sessionContext';
import { getEphemeralStorageItem, setEphemeralStorageItem } from '@/logic/storage';
import { PseudorandomString } from '@/logic/utilities';
import bulmaShared from '@/style/bulmaShared';
import css from './SystemConfig.scss';

export default function SystemConfig() {
    const [showSettings, setShowSettings] = React.useState(getEphemeralStorageItem('admin-system-config-shown', false));
    const [config, setConfig] = React.useState<IConfiguration>();
    const [updateCounter, setUpdateCounter] = React.useState(0);
    const [editDelta, setEditDelta] = React.useState<Partial<IConfiguration>>({});
    const [isSaving, setIsSaving] = React.useState(false);
    const ids = React.useMemo(() => Array(7).fill(undefined).map(() => PseudorandomString(10, 'Aa')), []);
    const sessionContext = React.useContext(SessionContext);

    React.useEffect(() => {
        getConfiguration(r => {
            if (r.isSuccessful()) {
                setConfig(r.result);
            } else if (r.isUnsuccessful()) {
                handleUnsuccessfulApiCall(r);
            }
        });
    }, [updateCounter]);

    function acceptWarning() {
        setShowSettings(true);
        setEphemeralStorageItem('admin-system-config-shown', true);
    }

    function registerChange<TField extends keyof IConfiguration>(field: TField, value: IConfiguration[TField]) {
        setEditDelta({ ...editDelta, [field]: value });
    }

    function saveConfiguration() {
        setIsSaving(true);
        setConfiguration(editDelta, r => {
            if (r.isSuccessful()) {
                setEditDelta({});
                setUpdateCounter(updateCounter + 1);
                sessionContext.refresh();
            } else if (r.isUnsuccessful()) {
                handleUnsuccessfulApiCall(r);
            }
            setIsSaving(false);
        });
    }

    function handleResetConfiguration() {
        setIsSaving(true);
        resetConfiguration(r => {
            if (r.isSuccessful()) {
                setEditDelta({});
                setUpdateCounter(updateCounter + 1);
            } else if (r.isUnsuccessful()) {
                handleUnsuccessfulApiCall(r);
            }
            setIsSaving(false);
        });
    }

    if (!showSettings) {
        return (
            <div className={clsx(bulmaShared.message, bulmaShared.isDanger)}>
                <div className={bulmaShared.messageHeader}>
                    <p>{i18n.t('general.warning')}</p>
                </div>
                <div className={clsx(bulmaShared.messageBody, css.warningBody)}>
                    <p>{i18n.t('admin.system.warning')}</p>
                    <Button onClick={acceptWarning}>{i18n.t('admin.system.acceptRisk')}</Button>
                </div>
            </div>
        );
    }

    if (!config) {
        return <PageLoader message={i18n.t('general.loading')} />;
    }

    const configValues = { ...config, ...editDelta };

    return <div className={css.container}>
        <h4 className={clsx(bulmaShared.title, bulmaShared.is4, css.header)}>{i18n.t('admin.system.headers.system')}</h4>
        <Field>
            <Control>
                <Checkbox
                    checked={configValues['system-logging-enabled']} disabled={isSaving}
                    onChange={e => registerChange('system-logging-enabled', e.currentTarget.checked)}>
                    {i18n.t('admin.system.settings.system-logging-enabled')}
                </Checkbox>
            </Control>
        </Field>
        <Field>
            <Control>
                <Checkbox
                    checked={configValues['system-custom-roles-enabled']} disabled={isSaving}
                    onChange={e => registerChange('system-custom-roles-enabled', e.currentTarget.checked)}>
                    {i18n.t('admin.system.settings.system-custom-roles-enabled')}
                </Checkbox>
            </Control>
        </Field>

        <h4 className={clsx(bulmaShared.title, bulmaShared.is4, css.header)}>{i18n.t('admin.system.headers.security')}</h4>
        <Field>
            <Control>
                <Checkbox
                    checked={configValues['security-password-reset-enabled']} disabled={isSaving}
                    onChange={e => registerChange('security-password-reset-enabled', e.currentTarget.checked)}>
                    {i18n.t('admin.system.settings.security-password-reset-enabled')}
                </Checkbox>
            </Control>
        </Field>
        <Field>
            <label htmlFor={ids[0]} className={bulmaShared.label}>
                {i18n.t('admin.system.settings.security-user-session-timeout')} <small>{i18n.t('admin.system.timeoutDescription')}</small>
            </label>
            <Control>
                <Input value={configValues['security-user-session-timeout']} disabled={isSaving}
                    onChange={e => registerChange('security-user-session-timeout', Number(e.currentTarget.value))}
                    id={ids[0]} type='number' min={0} />
            </Control>
        </Field>
        <Field>
            <label htmlFor={ids[1]} className={bulmaShared.label}>
                {i18n.t('admin.system.settings.security-user-inactivity-timeout')} <small>{i18n.t('admin.system.timeoutDescription')}</small>
            </label>
            <Control>
                <Input value={configValues['security-user-inactivity-timeout']} disabled={isSaving}
                    onChange={e => registerChange('security-user-inactivity-timeout', Number(e.currentTarget.value))}
                    id={ids[1]} type='number' min={0} />
            </Control>
        </Field>
        <Field>
            <label htmlFor={ids[2]} className={bulmaShared.label}>
                {i18n.t('admin.system.settings.security-password-hash-cost')}
            </label>
            <Control>
                <Input value={configValues['security-password-hash-cost']} disabled={isSaving}
                    onChange={e => registerChange('security-password-hash-cost', Number(e.currentTarget.value))}
                    id={ids[2]} type='number' min={0} />
            </Control>
        </Field>

        <h4 className={clsx(bulmaShared.title, bulmaShared.is4, css.header)}>{i18n.t('admin.system.headers.email')}</h4>
        <Field>
            <label htmlFor={ids[3]} className={bulmaShared.label}>
                {i18n.t('admin.system.settings.mail-smtp-host')}
            </label>
            <Control>
                <Input value={configValues['mail-smtp-host']} disabled={isSaving}
                    onChange={e => registerChange('mail-smtp-host', e.currentTarget.value)}
                    id={ids[3]} />
            </Control>
        </Field>
        <Field>
            <label htmlFor={ids[4]} className={bulmaShared.label}>
                {i18n.t('admin.system.settings.mail-port')}
            </label>
            <Control>
                <Input value={configValues['mail-port']} disabled={isSaving}
                    onChange={e => registerChange('mail-port', Number(e.currentTarget.value))}
                    id={ids[4]} type='number' min={0} />
            </Control>
        </Field>
        <Field>
            <label htmlFor={ids[5]} className={bulmaShared.label}>
                {i18n.t('admin.system.settings.mail-username')}
            </label>
            <Control>
                <Input value={configValues['mail-username']} disabled={isSaving}
                    onChange={e => registerChange('mail-username', e.currentTarget.value)}
                    id={ids[5]} />
            </Control>
        </Field>
        <Field>
            <label htmlFor={ids[6]} className={bulmaShared.label}>
                {i18n.t('admin.system.settings.mail-password')}
            </label>
            <Control>
                <Input value={configValues['mail-password']} disabled={isSaving}
                    onChange={e => registerChange('mail-password', e.currentTarget.value)}
                    id={ids[7]} />
            </Control>
        </Field>

        <h4 className={clsx(bulmaShared.title, bulmaShared.is4, css.header)}>{i18n.t('admin.system.headers.ui')}</h4>
        <Field>
            <Control>
                <Checkbox
                    checked={configValues['ui-split-restricted-worksheet']} disabled={isSaving}
                    onChange={e => registerChange('ui-split-restricted-worksheet', e.currentTarget.checked)}>
                    {i18n.t('admin.system.settings.ui-split-restricted-worksheet')}
                </Checkbox>
            </Control>
        </Field>

        <Field>
            <Buttons>
                <Button color='primary' disabled={isSaving} onClick={saveConfiguration}>{i18n.t('general.save')}</Button>
                <Button color='warning' disabled={isSaving} onClick={handleResetConfiguration}>{i18n.t('general.reset')}</Button>
            </Buttons>
        </Field>
    </div>;
}
