import clsx from 'clsx';
import React from 'react';
import { isAdminSession, isSessionAuthorized } from '@/api/session';
import { sendTestMail } from '@/api/user';
import Button from '@/components/Button';
import FontAwesome from '@/components/FA';
import i18n from '@/i18n/i18n';
import { handleUnsuccessfulApiCall } from '@/logic/api';
import { broadcastNotification, createNotification, NotificationType } from '@/logic/notifications';
import SessionContext from '@/logic/sessionContext';
import { stringToColor } from '@/logic/utilities';
import bulmaShared from '@/style/bulmaShared';
import css from './Account.scss';
import ChangePassword from './Components/ChangePassword';

export default function Account() {
    const { current: currentSession } = React.useContext(SessionContext);
    if (!currentSession || !isSessionAuthorized(currentSession)) {
        return null;
    }

    function testMail() {
        sendTestMail(r => {
            if (r.isSuccessful()) {
                broadcastNotification(createNotification(i18n.t('account.testMailSent'), NotificationType.info, 5000));
            } else if (r.isUnsuccessful()) {
                handleUnsuccessfulApiCall(r);
            }
        });
    }

    const fullName = `${currentSession.User.FirstName} ${currentSession.User.LastName}`;
    return (
        <div className={clsx(bulmaShared.container, css.accountContainer)}>
            <div className={css.root}>
                <span className={css.avatar}>
                    <FontAwesome
                        style='fas'
                        icon='faUserCircle'
                        elementProps={{ style: { color: stringToColor(fullName) } }}
                    />
                </span>
                <div>
                    <h1>{fullName}</h1>
                    <table className={css.infoTable}>
                        <tbody>
                            <tr>
                                <td>{i18n.t('general.username')}</td>
                                <td>{currentSession.User.Username}</td>
                            </tr>
                            <tr>
                                <td>{i18n.t('general.email')}</td>
                                <td>{currentSession.User.Email}</td>
                            </tr>
                        </tbody>
                    </table>
                    <div className={clsx(bulmaShared.notification, css.incorrectness)}>
                        {i18n.t('account.incorrectness')}
                    </div>
                    <h3 className={clsx(bulmaShared.title, bulmaShared.is3)}>{i18n.t('account.password')}</h3>
                    <ChangePassword />
                    {isAdminSession(currentSession) && <>
                        <hr />
                        <Button onClick={testMail}>{i18n.t('account.testMail')}</Button>
                    </>}
                </div>
            </div>
        </div>
    );
}
