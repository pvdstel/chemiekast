import clsx from 'clsx';
import React from 'react';
import { changeUserPassword } from '@/api/user';
import Button from '@/components/Button';
import { Control, Field, Input } from '@/components/Form/Form';
import i18n from '@/i18n/i18n';
import { handleUnsuccessfulApiCall } from '@/logic/api';
import { broadcastNotification, createNotification, NotificationType } from '@/logic/notifications';
import { PseudorandomString } from '@/logic/utilities';
import bulmaShared from '@/style/bulmaShared';

export default function ChangePassword() {
    const [currentPassword, setCurrentPassword] = React.useState('');
    const [newPassword, setNewPassword] = React.useState('');
    const [passwordConfirm, setPasswordConfirm] = React.useState('');
    const [isSaving, setIsSaving] = React.useState(false);
    const ids = React.useMemo(() => Array(3).fill(undefined).map(() => PseudorandomString(10, 'Aa')), []);

    function savePassword(e: React.SyntheticEvent<HTMLElement>) {
        e.preventDefault();
        if (isSaving) { return; }
        if (newPassword !== passwordConfirm) {
            broadcastNotification(createNotification(i18n.t('account.passwordsNotEqual'), NotificationType.danger, 5000));
            return;
        }
        setIsSaving(true);
        changeUserPassword({ CurrentPassword: currentPassword, NewPassword: newPassword }, r => {
            if (r.isSuccessful()) {
                broadcastNotification(createNotification(i18n.t('account.passwordChangeDone'), NotificationType.success, 5000));
                setCurrentPassword('');
                setNewPassword('');
                setPasswordConfirm('');
            } else if (r.isUnsuccessful()) {
                handleUnsuccessfulApiCall(r);
            }
            setIsSaving(false);
        });
    }

    return (
        <form className={clsx(bulmaShared.columns, bulmaShared.isMultiline)} onSubmit={savePassword}>
            <div className={clsx(bulmaShared.column, bulmaShared.is12)}>
                <Field>
                    <label htmlFor={ids[0]} className={bulmaShared.requiredField}>{i18n.t('account.currentPassword')}</label>
                    <Control>
                        <Input id={ids[0]} type='password' disabled={isSaving}
                            value={currentPassword} onChange={e => setCurrentPassword(e.currentTarget.value)} />
                    </Control>
                </Field>
            </div>
            <div className={clsx(bulmaShared.column, bulmaShared.is6Desktop)}>
                <Field>
                    <label htmlFor={ids[1]} className={bulmaShared.requiredField}>{i18n.t('account.newPassword')}</label>
                    <Control>
                        <Input id={ids[1]} type='password' disabled={isSaving}
                            value={newPassword} onChange={e => setNewPassword(e.currentTarget.value)} />
                    </Control>
                </Field>
            </div>
            <div className={clsx(bulmaShared.column, bulmaShared.is6Desktop)}>
                <Field>
                    <label htmlFor={ids[2]} className={bulmaShared.requiredField}>{i18n.t('account.newPasswordConfirm')}</label>
                    <Control>
                        <Input id={ids[2]} type='password' disabled={isSaving}
                            value={passwordConfirm} onChange={e => setPasswordConfirm(e.currentTarget.value)} />
                    </Control>
                </Field>
            </div>
            <div className={clsx(bulmaShared.column, bulmaShared.is12)}>
                <Button color='primary' type='submit' loading={isSaving}>{i18n.t('account.setPassword')}</Button>
            </div>
        </form>
    );
}
