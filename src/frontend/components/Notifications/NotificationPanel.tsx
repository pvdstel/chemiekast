import React from 'react';
import { INotification, subscribeNotifications } from '@/logic/notifications';
import Notification from './Notification';
import css from './Notifications.scss';

type NotificationWrapper = {
    notification: INotification;
    id: string;
};

interface INotificationPanelState {
    notifications: NotificationWrapper[];
}

export default class NotificationPanel extends React.Component<{}, INotificationPanelState> {
    private unsubscribeNotifications: undefined | (() => void);

    constructor(props: {}) {
        super(props);

        this.state = {
            notifications: [],
        };
    }

    componentDidMount() {
        this.unsubscribeNotifications = subscribeNotifications(this.notificationListener);
    }

    componentWillUnmount() {
        if (this.unsubscribeNotifications !== undefined) {
            this.unsubscribeNotifications();
        }
    }

    private notificationListener = (notification: INotification) => {
        const original = this.state.notifications;
        const wrapped: NotificationWrapper = { notification, id: new Date().toISOString() };
        this.setState({ notifications: [wrapped, ...original] });
    }

    private removeNotification = (wrapper: NotificationWrapper) => {
        this.setState({ notifications: this.state.notifications.filter(w => w !== wrapper) });
    }

    render() {
        return (
            <div className={css.panel}>
                {this.state.notifications.map(w =>
                    <Notification
                        key={w.id}
                        notification={w.notification}
                        requestRemove={() => this.removeNotification(w)}
                    />,
                )}
            </div>
        );
    }
}
