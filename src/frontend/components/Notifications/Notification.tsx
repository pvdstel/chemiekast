import clsx from 'clsx';
import React from 'react';
import { INotification, NotificationType } from '@/logic/notifications';
import bulmaShared from '@/style/bulmaShared';
import css from './Notifications.scss';

const animationDurationShow = 200;
const animationDurationHide = 600;

interface INotificationProps {
    notification: INotification;
    requestRemove: () => void;
}

interface INotificationState {
    state: 'opened' | 'opening' | 'closing';
}

export default class Notification extends React.Component<INotificationProps, INotificationState> {
    private closeTimeout: number | undefined;
    private wrapperRef = React.createRef<HTMLDivElement>();

    constructor(props: INotificationProps) {
        super(props);

        this.state = {
            state: 'opening',
        };
    }

    public close = () => {
        if (this.state.state === 'closing') { return; }

        let above: Element | null = null;
        let below: Element | null = null;
        if (this.wrapperRef.current !== null) {
            this.wrapperRef.current.style.height = `${this.wrapperRef.current.offsetHeight}px`;

            above = this.wrapperRef.current.previousElementSibling;
            below = this.wrapperRef.current.nextElementSibling;
            if (above !== null) {
                above.classList.add(css.closingBelow);
            }
            if (below !== null) {
                below.classList.add(css.closingAbove);
            }
        }

        this.setState({ state: 'closing' });
        window.setTimeout(() => {
            if (above !== null) {
                above.classList.remove(css.closingBelow);
            }
            if (below !== null) {
                below.classList.remove(css.closingAbove);
            }

            this.props.requestRemove();
        }, animationDurationHide);
    }

    private onDismissClick = (e: React.MouseEvent) => {
        e.preventDefault();
        this.close();
    }

    componentDidMount() {
        window.setTimeout(() => {
            this.setState({ state: 'opened' });
            if (this.props.notification.timeout !== undefined) {
                this.closeTimeout = window.setTimeout(this.close, this.props.notification.timeout);
            }
        }, animationDurationShow);
    }

    componentWillUnmount() {
        if (this.closeTimeout !== undefined) {
            window.clearTimeout(this.closeTimeout);
        }
    }

    render() {
        const notification = this.props.notification;
        const wrapperClassName = clsx(css.wrapper, {
            [css.closing]: this.state.state === 'closing',
        });
        const notificationClassName = clsx(bulmaShared.notification, css.notification, {
            [bulmaShared.isPrimary]: notification.type === NotificationType.primary,
            [bulmaShared.isLink]: notification.type === NotificationType.link,
            [bulmaShared.isInfo]: notification.type === NotificationType.info,
            [bulmaShared.isSuccess]: notification.type === NotificationType.success,
            [bulmaShared.isWarning]: notification.type === NotificationType.warning,
            [bulmaShared.isDanger]: notification.type === NotificationType.danger,
            [css.opening]: this.state.state === 'opening',
            [css.closing]: this.state.state === 'closing',
        });

        return (
            <div className={wrapperClassName} ref={this.wrapperRef}>
                <div className={notificationClassName}>
                    <span>{notification.message}</span>
                    <a href='#' onClick={this.onDismissClick}><span className={css.dismiss} /></a>
                </div>
            </div>
        );
    }
}
