import clsx from 'clsx';
import FocusTrap from 'focus-trap-react';
import React from 'react';
import css from './Dropdown.scss';

export const DefaultAnimationDuration = 300;

export type DropdownState = 'closed' | 'opened' | 'opening' | 'closing';

export interface IDropdownProps {
    open?: boolean;
    stateChanged?: (state: DropdownState) => void;
    animationDuration?: number;

    container: React.ReactElement<React.HTMLProps<HTMLElement>>;
    containerOpenClassName?: string;
    containerProps?: Omit<React.HTMLAttributes<HTMLElement>, 'className' | 'children'>;

    trigger: React.ReactElement<React.HTMLProps<HTMLElement>>;
    triggerOpenClassName?: string;
    triggerOpeningClassName?: string;
    triggerClosingClassName?: string;

    menu: React.ReactElement<React.HTMLProps<HTMLElement>>;
    menuOpenClassName?: string;
    menuOpeningClassName?: string;
    menuClosingClassName?: string;

    renderBackdrop?: boolean;
    backdropClassName?: string;
}

interface IDropdownState {
    dropdownState: DropdownState;
}

export default class Dropdown extends React.Component<IDropdownProps, IDropdownState> {
    private animationTimer: number | undefined;

    static defaultProps: Required<Pick<IDropdownProps, 'animationDuration' | 'menuOpeningClassName' | 'menuClosingClassName' | 'renderBackdrop'>> = {
        animationDuration: DefaultAnimationDuration,
        menuOpeningClassName: css.menuOpeningDefault,
        menuClosingClassName: css.menuClosingDefault,
        renderBackdrop: true,
    };

    constructor(props: IDropdownProps) {
        super(props);

        this.state = {
            dropdownState: props.open ? 'opened' : 'closed',
        };
    }

    private createClickHandler = (invoke: () => void) => (e: React.MouseEvent) => {
        e.preventDefault();
        invoke();
    }

    private escapeHandler = (e: React.KeyboardEvent) => {
        if (e.keyCode === 27) { // escape
            e.preventDefault();
            this.close();
        }
    }

    private fireStateChange = (state: DropdownState) => this.props.stateChanged && this.props.stateChanged(state);

    public isVisible = () => this.state.dropdownState === 'opened'
        || this.state.dropdownState === 'opening'
        || this.state.dropdownState === 'closing'

    public toggle = () => (this.isVisible() ? this.close : this.open)();

    public open = () => {
        if (this.state.dropdownState === 'opened' || this.state.dropdownState === 'opening') { return; }
        if (this.animationTimer !== undefined) { clearTimeout(this.animationTimer); }

        this.setState({ dropdownState: 'opening' });
        this.animationTimer = window.setTimeout(() => {
            this.setState({ dropdownState: 'opened' });
            this.fireStateChange('opened');
        }, this.props.animationDuration);

        this.fireStateChange('opening');
    }

    public close = () => {
        if (this.state.dropdownState === 'closed' || this.state.dropdownState === 'closing') { return; }
        if (this.animationTimer !== undefined) { clearTimeout(this.animationTimer); }

        this.setState({ dropdownState: 'closing' });
        this.animationTimer = window.setTimeout(() => {
            this.setState({ dropdownState: 'closed' });
            this.fireStateChange('closed');
        }, this.props.animationDuration);
        this.fireStateChange('closing');
    }

    componentDidUpdate(prevProps: IDropdownProps) {
        if (this.props.open !== prevProps.open) {
            (this.props.open ? this.open : this.close)();
        }
    }

    render() {
        const p = this.props, s = this.state;
        const hasOpenPresence = this.isVisible();

        const className = clsx(
            this.props.container.props.className,
            hasOpenPresence ? p.containerOpenClassName : undefined,
        );
        const triggerClassName = clsx(
            p.trigger.props.className,
            hasOpenPresence ? p.triggerOpenClassName : undefined,
            s.dropdownState === 'opening' ? p.triggerOpeningClassName : undefined,
            s.dropdownState === 'closing' ? p.triggerClosingClassName : undefined,
        );
        const menuClassName = clsx(
            p.menu.props.className,
            hasOpenPresence ? p.menuOpenClassName : undefined,
            s.dropdownState === 'opening' ? p.menuOpeningClassName : undefined,
            s.dropdownState === 'closing' ? p.menuClosingClassName : undefined,
        );
        const backdropClassName = clsx(
            p.backdropClassName,
            css.backdrop,
            s.dropdownState === 'closing' ? css.hiding : undefined,
        );

        return <FocusTrap active={hasOpenPresence} focusTrapOptions={{ clickOutsideDeactivates: true, escapeDeactivates: false }}>
            {React.cloneElement(
                this.props.container,
                { ...this.props.containerProps, className: className, onKeyDown: this.escapeHandler },
                <>
                    {React.cloneElement(p.trigger, { className: triggerClassName, onClick: this.createClickHandler(this.toggle) })}
                    {React.cloneElement(p.menu, { className: menuClassName })}
                    {p.renderBackdrop && hasOpenPresence &&
                        <div className={backdropClassName} onClick={this.close} />
                    }
                </>,
            )}
        </FocusTrap>;
    }
}
