import clsx from 'clsx';
import React from 'react';
import i18n from '@/i18n/i18n';
import bulmaShared from '@/style/bulmaShared';
import Button from '../Button';
import css from './ErrorPage.scss';

interface IErrorPageProps {
    error: Error;
}

export default function ErrorPage(props: IErrorPageProps) {
    return (
        <div className={css.root}>
            <svg className={css.image} version='1.1' viewBox='0 0 48 48' xmlns='http://www.w3.org/2000/svg'>
                <g>
                    <g transform='translate(-5e-8 6.1052)'>
                        <path d='m24 7.5586c-3.1764 0.024108-10.227 0.3987-9.3242 1.9629 1.2864 2.2282 5.3984 2.9453 5.3984 2.9453v6.3789l-8.0488 13.744s-2.2578 3.298-2.2578 4.416c0 3.4089 2.9434 3.4355 2.9434 3.4355h22.578c1e-6 0 2.9434-0.04019 2.9434-3.4355 0-1.1554-2.2578-4.416-2.2578-4.416l-8.0488-13.744v-6.3789s4.0382-0.73587 5.3984-3.0918c0.93175-1.6138-6.1576-1.8404-9.3242-1.8164z' color='#000000' colorRendering='auto' fill='#fff' imageRendering='auto' shapeRendering='auto' stroke='#000' strokeDashoffset='13.75' strokeLinecap='round' strokeWidth='.5' />
                        <path d='m19.177 23.819-5.6217 9.3816s-1.4431 2.1463-1.8277 3.3756c-0.19158 0.72822 0.54027 1.9988 1.6057 1.8317h21.513c1.1801-0.01614 1.7554-1.4602 1.2795-2.3003-1.524-2.6095-1.5804-2.7101-1.5804-2.7101l-5.7224-9.5786z' fill='#9b0bed' fillRule='evenodd'/>
                    </g>
                </g>
                <g>
                    <g transform='translate(0 -1.4534)' fill='#9b0bed' fillOpacity='.5'>
                        <circle cx='10.076' cy='11.761' r='2.8284'/>
                        <circle cx='20.153' cy='14.412' r='3.8891'/>
                        <circle cx='27.489' cy='7.0762' r='4.4194'/>
                        <circle cx='40.128' cy='17.683' r='2.9168'/>
                        <circle cx='18.12' cy='10.347' r='4.4194'/>
                        <circle cx='22.627' cy='11.319' r='2.8284'/>
                    </g>
                </g>
            </svg>
            <div className={clsx(css.details, bulmaShared.content)}>
                <h1 className={clsx(bulmaShared.title, bulmaShared.is1)}>
                    {i18n.t('general.error')}
                </h1>
                <p>
                    <Button color='primary' size='medium' onClick={() => window.location.reload()}>{i18n.t('general.reload')}</Button>
                </p>
                <p>{props.error.name}</p>
                <p>{props.error.message}</p>
                <pre>{props.error.stack}</pre>
            </div>
        </div>
    );
}
