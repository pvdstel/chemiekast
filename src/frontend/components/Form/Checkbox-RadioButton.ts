import { BulmaColor } from '@/style/bulmaShared';

export function colorToClassName(style: BulmaColor) {
    switch (style) {
        case 'white': return checkboxRadioButtonCss.isWhite;
        case 'light': return checkboxRadioButtonCss.isLight;
        case 'dark': return checkboxRadioButtonCss.isDark;
        case 'black': return checkboxRadioButtonCss.isBlack;
        case 'text': return checkboxRadioButtonCss.isText;
        case 'primary': return checkboxRadioButtonCss.isPrimary;
        case 'link': return checkboxRadioButtonCss.isLink;
        case 'info': return checkboxRadioButtonCss.isInfo;
        case 'success': return checkboxRadioButtonCss.isSuccess;
        case 'warning': return checkboxRadioButtonCss.isWarning;
        case 'danger': return checkboxRadioButtonCss.isDanger;
        default: return undefined;
    }
}

export const checkboxRadioButtonCss = require('./Checkbox-RadioButton.scss').default;
