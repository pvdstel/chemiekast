import clsx from 'clsx';
import React from 'react';
import { PseudorandomString } from '@/logic/utilities';
import { BulmaColor } from '@/style/bulmaShared';
import { checkboxRadioButtonCss as css, colorToClassName } from './Checkbox-RadioButton';

export interface IRadioButtonProps {
    color?: BulmaColor;
}

type RadioButtonPropType = IRadioButtonProps
    & Omit<React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>, 'type'>;

const RadioButton = React.memo(function (props: RadioButtonPropType) {
    const internalId = React.useRef(PseudorandomString(10, 'Aa'));
    
    const { color, id, children, ...otherProps } = props;
    const finalId = `radiobutton-${id || internalId.current}`;
    const isEmpty = !children;

    const className = clsx(
        css.box,
        css.radiobutton,
        isEmpty ? css.isEmpty : undefined,
        colorToClassName(color),
    );
    const zeroWidthSpace = '\u200B';

    return (
        <div className={className}>
            <input {...otherProps} id={finalId} type='radio' />
            <label htmlFor={finalId}>{isEmpty ? zeroWidthSpace : children}</label>
        </div>
    );

});
export default RadioButton;
