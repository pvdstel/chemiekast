import clsx from 'clsx';
import React from 'react';
import { PseudorandomString } from '@/logic/utilities';
import { BulmaColor } from '@/style/bulmaShared';
import { checkboxRadioButtonCss as css, colorToClassName } from './Checkbox-RadioButton';

export interface ICheckboxProps {
    color?: BulmaColor;
    circle?: boolean;
}

type CheckboxPropType = ICheckboxProps
    & Omit<React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>, 'type'>;

export default class Checkbox extends React.PureComponent<CheckboxPropType> {
    private internalId: string;

    constructor(props: CheckboxPropType) {
        super(props);

        this.internalId = PseudorandomString(10, 'Aa');
    }

    render() {
        const { color, circle, id, children, ...otherProps } = this.props;
        const finalId = `checkbox-${id || this.internalId}`;
        const isEmpty = !children;

        const className = clsx(
            css.box,
            css.checkbox,
            isEmpty ? css.isEmpty : undefined,
            colorToClassName(color),
            circle ? css.isCircle : undefined,
        );
        const zeroWidthSpace = '\u200B';

        return (
            <div className={className}>
                <input {...otherProps} id={finalId} type='checkbox' />
                <label htmlFor={finalId}>{isEmpty ? zeroWidthSpace : children}</label>
            </div>
        );
    }
}
