import clsx from 'clsx';
import React from 'react';
import { useHistory } from 'react-router';
import Button from '@/components/Button';
import i18n from '@/i18n/i18n';
import { inventoryAllRoute, inventorySearchRoute } from '@/logic/routes';
import { navigate } from '@/logic/routing';
import bulmaShared, { BulmaColor } from '@/style/bulmaShared';
import FontAwesome from '../FA';

type InputSize = undefined | 'small' | 'medium' | 'large';

export interface IInputProps extends React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
    bulmaSize?: InputSize;
    innerRef?: React.Ref<HTMLInputElement>;
}

export interface ISelectProps extends React.SelectHTMLAttributes<HTMLSelectElement> {
    containerClassName?: string;
}

export interface IControlProps extends React.HTMLAttributes<HTMLElement> {
    expanded?: boolean;
}

export interface IFieldProps extends React.HTMLAttributes<HTMLElement> {
    hasAddons?: boolean;
    grouped?: boolean;
}

export interface ISearchBoxProps {
    formClassName?: string;
    inputId?: string;
    size?: InputSize;
    buttonColor?: BulmaColor;
}

function sizeToClassName(size: InputSize) {
    switch (size) {
        case 'small': return bulmaShared.isSmall;
        case 'medium': return bulmaShared.isMedium;
        case 'large': return bulmaShared.isLarge;
        default: return undefined;
    }
}

function inputClassName(className: string | undefined, bulmaSize: InputSize) {
    return clsx(
        className,
        bulmaShared.input,
        sizeToClassName(bulmaSize),
    );
}

export const Input = React.memo((props: IInputProps) => {
    const { className, bulmaSize, innerRef, ...otherProps } = props;
    const cn = inputClassName(className, bulmaSize);
    return (
        <input {...otherProps} className={cn} ref={innerRef} />
    );
});

export default function BlurInput(props: IInputProps) {
    const { className, bulmaSize, innerRef, value, ...otherProps } = props;
    const [valueInternal, setValueInternal] = React.useState<IInputProps['value']>(value);

    React.useEffect(() => {
        if (valueInternal !== value) {
            setValueInternal(props.value);
        }
    }, [value]);

    function onInput(e: React.ChangeEvent<HTMLInputElement>) {
        setValueInternal(e.currentTarget.value);
    }

    function onBlur(e: React.FocusEvent<HTMLInputElement>) {
        if (valueInternal !== value && props.onChange) {
            props.onChange(e);
        }
        if (props.onBlur) {
            props.onBlur(e);
        }
    }

    const cn = inputClassName(className, bulmaSize);

    return <input {...otherProps} className={cn} value={valueInternal} onChange={onInput} onBlur={onBlur} />;
}

export const Select = React.memo((props: ISelectProps) => {
    const { containerClassName, ...otherProps } = props;

    return (
        <div className={clsx(containerClassName, bulmaShared.select)}>
            <select {...otherProps}></select>
        </div>
    );
});

export const Control = React.memo((props: IControlProps) => {
    const { expanded, className, ...otherProps } = props;

    const controlClassName = clsx(
        className,
        bulmaShared.control,
        expanded ? bulmaShared.isExpanded : null,
    );

    return (
        <div {...otherProps} className={controlClassName}>{props.children}</div>
    );
});

export const Field = React.memo((props: IFieldProps) => {
    const { hasAddons, grouped, className, ...otherProps } = props;

    const fieldClassName = clsx(
        className,
        bulmaShared.field,
        hasAddons ? bulmaShared.hasAddons : undefined,
        grouped ? bulmaShared.isGrouped : undefined,
    );

    return (
        <div {...otherProps} className={fieldClassName}>{props.children}</div>
    );
});

export function SearchBox(props: ISearchBoxProps) {
    const history = useHistory();
    
    const [searchValue, setSearchValue] = React.useState('');

    function onSubmit(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault();
        if (searchValue.length > 0) {
            navigate(history, inventorySearchRoute.fill(encodeURIComponent(searchValue)));
        } else {
            navigate(history, inventoryAllRoute.fill);
        }
    }

    return (
        <form className={props.formClassName} role='search' onSubmit={onSubmit}>
            <Field hasAddons>
                <Control>
                    <Input
                        value={searchValue}
                        onChange={e => setSearchValue(e.currentTarget.value)}
                        id={props.inputId}
                        bulmaSize={props.size}
                        placeholder={i18n.t('general.search')}
                        type='search'
                    />
                </Control>
                <Control>
                    <Button
                        size={props.size}
                        color={props.buttonColor}
                        type='submit'>
                        <FontAwesome style='fas' icon='faSearch' />
                    </Button>
                </Control>
            </Field>
        </form>
    );
}
