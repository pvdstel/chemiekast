import clsx from 'clsx';
import FocusTrap from 'focus-trap-react';
import React from 'react';
import i18n from '@/i18n/i18n';
import { BulmaColor } from '@/style/bulmaShared';
import Button from '../Button';
import css from './Modal.scss';

export const modalAnimationDuration = 300;

// the four definitions below form a simple stack management system
const baseZIndex = 1000;
let modalStackCount = 0;
function openZIndex(): number {
    const zIndex = baseZIndex + modalStackCount * 10;
    ++modalStackCount;
    return zIndex;
}
function closeZIndex(): void { --modalStackCount; }

export type ModalState = 'closed' | 'opened' | 'opening' | 'closing';

export interface IModalAction {
    label: string;
    color?: BulmaColor;
    callback?: (e: React.SyntheticEvent<HTMLElement>) => void | boolean;
    props?: Omit<React.ButtonHTMLAttributes<HTMLButtonElement>, 'color'>;
}

export interface IModalProps {
    open: boolean;
    requestClose: () => void;
    title: string;
    actions: IModalAction[];
    onSubmit?: (e: React.FormEvent<HTMLFormElement>) => void | boolean;
}

export default function Modal(props: React.PropsWithChildren<IModalProps>) {
    const { title, children, requestClose, onSubmit, actions } = props;
    const animationTimer = React.useRef<number>();
    const [modalState, setModalState] = React.useState<ModalState>(props.open ? 'opened' : 'closed');
    const [zIndex, setZIndex] = React.useState(baseZIndex);

    React.useEffect(() => {
        (props.open ? open : close)();
    }, [props.open]);

    const isVisible = () => modalState === 'opened'
        || modalState === 'opening'
        || modalState === 'closing';

    const open = () => {
        if (modalState === 'opened' || modalState === 'opening') { return; }
        if (animationTimer.current !== undefined) { clearTimeout(animationTimer.current); }

        setModalState('opening');
        setZIndex(openZIndex());
        animationTimer.current = window.setTimeout(() => {
            setModalState('opened');
        }, modalAnimationDuration);
    };

    const close = () => {
        if (modalState === 'closed' || modalState === 'closing') { return; }
        if (animationTimer.current !== undefined) { clearTimeout(animationTimer.current); }

        setModalState('closing');
        animationTimer.current = window.setTimeout(() => {
            setModalState('closed');
            closeZIndex();
        }, modalAnimationDuration);
    };

    const onAction = (action: IModalAction) => (e: any) => {
        if (action.callback) {
            if (action.callback(e) === false) {
                return;
            }
        }
        if (!action.props || action.props.type !== 'submit') {
            requestClose();
        }
    };

    const escapeHandler = (e: React.KeyboardEvent) => {
        if (e.keyCode === 27) { // escape
            e.preventDefault();
            requestClose();
        }
    };

    const onSubmitHandler = (e: React.FormEvent<HTMLFormElement>) => {
        if (!e.currentTarget.checkValidity()) {
            e.preventDefault();
        } else if (onSubmit) {
            if (onSubmit(e) !== false) {
                requestClose();
            }
        }
    };


    const hasOpenPresence = isVisible();
    const className = clsx(
        css.modal,
        hasOpenPresence && css.isActive,
        css[modalState],
    );
    const modalProps: React.DetailedHTMLProps<React.HTMLAttributes<HTMLElement>, HTMLElement> =
        { className, onKeyDown: escapeHandler, tabIndex: 0, style: { zIndex } };
    if (onSubmit) {
        modalProps.onSubmit = onSubmitHandler;
    }

    return <FocusTrap active={hasOpenPresence} focusTrapOptions={{ clickOutsideDeactivates: true, escapeDeactivates: false }}>
        {React.createElement(onSubmit ? 'form' : 'div', modalProps,
            <>
                <div className={css.modalBackground} onClick={requestClose} />
                <div className={clsx(css.modalCard, css[modalState])}>
                    <header className={css.modalCardHead}>
                        <p className={css.modalCardTitle}>{title}</p>
                        <button
                            className={css.delete}
                            aria-label='close'
                            type='button'
                            onClick={requestClose} />
                    </header>
                    <section className={css.modalCardBody}>
                        {children}
                    </section>
                    <footer className={css.modalCardFoot}>
                        {actions.map((a, i) =>
                            <Button
                                key={i}
                                color={a.color}
                                className={css.button}
                                onClick={onAction(a)}
                                {...a.props}>
                                {a.label}
                            </Button>,
                        )}
                    </footer>
                </div>
            </>,
        )}
    </FocusTrap>;
}

export class DefaultActions {
    static getCloseAction(props?: IModalAction['props']): IModalAction {
        return {
            label: i18n.t('general.close'),
            color: 'primary',
            props,
        };
    }

    static getOkAction(callback?: () => void, props?: IModalAction['props']): IModalAction {
        return {
            label: i18n.t('general.ok'),
            color: 'primary',
            callback: callback,
            props,
        };
    }

    static getCancelAction(props?: IModalAction['props']): IModalAction {
        return {
            label: i18n.t('general.cancel'),
            props,
        };
    }

    static getSaveAction(callback?: () => void, props?: IModalAction['props']): IModalAction {
        return {
            label: i18n.t('general.save'),
            color: 'primary',
            callback: callback,
            props,
        };
    }

    static getDeleteAction(callback?: () => void, props?: IModalAction['props']): IModalAction {
        return {
            label: i18n.t('general.delete'),
            color: 'danger',
            callback: callback,
            props,
        };
    }
}
