import clsx from 'clsx';
import React from 'react';
import bulmaShared, { BulmaColor } from '@/style/bulmaShared';
import Loader from './Loader/Loader';

type ButtonSize = undefined | 'small' | 'normal' | 'medium' | 'large';

export interface IButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
    color?: BulmaColor;
    size?: ButtonSize;
    loading?: boolean;
    outlined?: boolean;
}

export interface IButtonsProps {
    hasAddons?: boolean;
    children: React.ReactNode;
    className?: string;
}

function colorToClassName(style: BulmaColor) {
    switch (style) {
        case 'white': return bulmaShared.isWhite;
        case 'light': return bulmaShared.isLight;
        case 'dark': return bulmaShared.isDark;
        case 'black': return bulmaShared.isBlack;
        case 'text': return bulmaShared.isText;
        case 'primary': return bulmaShared.isPrimary;
        case 'link': return bulmaShared.isLink;
        case 'info': return bulmaShared.isInfo;
        case 'success': return bulmaShared.isSuccess;
        case 'warning': return bulmaShared.isWarning;
        case 'danger': return bulmaShared.isDanger;
        default: return undefined;
    }
}

function sizeToClassName(size: ButtonSize) {
    switch (size) {
        case 'small': return bulmaShared.isSmall;
        case 'normal': return bulmaShared.isNormal;
        case 'medium': return bulmaShared.isMedium;
        case 'large': return bulmaShared.isLarge;
        default: return undefined;
    }
}

const Button = React.memo((props: IButtonProps) => {
    const { color, size, outlined, loading, className, ...otherProps } = props;
    const buttonClassName = clsx(
        className,
        bulmaShared.button,
        outlined ? bulmaShared.isOutlined : undefined,
        colorToClassName(color),
        sizeToClassName(size),
    );

    return (
        <button type='button' {...otherProps} className={buttonClassName}>
            {loading
                ? <Loader inline fat white={!(color === undefined || color === 'white' || color === 'light')} />
                : props.children}
        </button>
    );
});
export default Button;

export const Buttons = React.memo((props: IButtonsProps) => {
    const className = clsx(
        props.className,
        bulmaShared.buttons,
        props.hasAddons && bulmaShared.hasAddons,
    );

    return <div className={className}>
        {props.children}
    </div>;
});
