import React from 'react';
import i18n, { changeLanguage, geti18nLanguage, getLanguages } from '@/i18n/i18n';
import bulmaShared from '@/style/bulmaShared';
import { Control, Field, Select } from './Form/Form';
import Modal, { DefaultActions } from './Modal/Modal';

export interface ILanguageModalProps {
    open: boolean;
    requestClose: () => void;
}

const selectId = 'language-modal-select-id';

export default class LanguageModal extends React.Component<ILanguageModalProps> {
    render() {
        return <Modal
            open={this.props.open}
            title={i18n.t('language.language')}
            requestClose={this.props.requestClose}
            actions={[DefaultActions.getCloseAction()]}>
            <Field>
                <label htmlFor={selectId} className={bulmaShared.label}>{i18n.t('language.description')}</label>
                <Control>
                    <Select
                        id={selectId}
                        value={geti18nLanguage()}
                        onChange={e => changeLanguage(e.currentTarget.value)}>
                        {getLanguages().map(l =>
                            <option value={l} key={l}>{i18n.t(`language.${l}`)}</option>,
                        )}
                    </Select>
                </Control>
            </Field>
        </Modal>;
    }
}
