import clsx from 'clsx';
import React from 'react';
import bulmaShared from '@/style/bulmaShared';
import css from './Table.scss';

const animationTime = 150;

const tableSizeCallbacks = new Set<() => void>();
export function notifyTableUpdate() {
    setTimeout(() => tableSizeCallbacks.forEach(f => f()), 100);
}

type Bounds = {
    top: number;
    bottom: number;
};

type FixedTableState = 'hidden' | 'visible' | 'showing' | 'hiding';

export interface ITableProps {
    tableProps?: React.DetailedHTMLProps<React.TableHTMLAttributes<HTMLTableElement>, HTMLTableElement>;
    head?: React.ReactElement<HTMLTableSectionElement>;
    body?: React.ReactElement<HTMLTableSectionElement>;
    foot?: React.ReactElement<HTMLTableSectionElement>;

    hasFixedThead?: boolean;
    fixedBounds?: Bounds;
    visibilityCheckTarget?: HTMLElement;

    striped?: boolean;
    narrow?: boolean;
    bordered?: boolean;
    fullWidth?: boolean;
}

interface ITableState {
    fixedHeadTableState: FixedTableState;
}

export default class Table extends React.Component<ITableProps, ITableState> {
    private wrapper = React.createRef<HTMLDivElement>();
    private table = React.createRef<HTMLTableElement>();
    private fixedHeadTable = React.createRef<HTMLTableElement>();

    private animationTimer: number | undefined;

    constructor(props: ITableProps) {
        super(props);

        this.state = {
            fixedHeadTableState: 'hidden',
        };
    }

    private updateFixedHeadPosition = () => {
        if (!this.wrapper.current || !this.table.current || !this.fixedHeadTable.current) { return; }

        const wrapper = this.wrapper.current;
        const table = this.table.current;
        const fixedTable = this.fixedHeadTable.current;

        const bounds = this.props.fixedBounds || { top: 0, bottom: window.innerHeight };
        const left = table.getBoundingClientRect().left;

        fixedTable.style.top = `${bounds.top}px`;
        fixedTable.style.left = `${left}px`;

        const clipLeft = wrapper.scrollLeft;
        const clipRight = wrapper.scrollWidth - wrapper.clientWidth - wrapper.scrollLeft;

        fixedTable.style.clipPath = `inset(0 ${clipRight}px 0 ${clipLeft}px)`;
    }

    private updateFixedHeadSize = () => {
        if (!this.table.current || !this.fixedHeadTable.current) { return; }

        const table = this.table.current;
        const fixedTable = this.fixedHeadTable.current;

        fixedTable.style.width = `${table.offsetWidth}px`;

        const tableThs = table.querySelectorAll('th');
        const fixedTableThs = fixedTable.querySelectorAll('th');

        for (let i = 0; i < tableThs.length; ++i) {
            fixedTableThs.item(i).style.width = `${tableThs.item(i).offsetWidth}px`;
        }
    }

    private updateFixedHeadVisibility = () => {
        if (!this.table.current || !this.fixedHeadTable.current) { return; }

        const table = this.table.current;
        const fixedTable = this.fixedHeadTable.current;

        const bounds = this.props.fixedBounds || { top: 0, bottom: window.innerHeight };
        const tableBottomThreshold = bounds.top + fixedTable.offsetHeight;

        const tableRect = table.getBoundingClientRect();
        const visible = tableRect.top < bounds.top && tableRect.bottom > tableBottomThreshold;

        if (visible && (this.state.fixedHeadTableState === 'hidden' || this.state.fixedHeadTableState === 'hiding')) {
            clearTimeout(this.animationTimer);
            this.setState({fixedHeadTableState: 'showing'});
            this.animationTimer = window.setTimeout(() => this.setState({fixedHeadTableState: 'visible'}), animationTime);
        } else if (!visible && (this.state.fixedHeadTableState === 'showing' || this.state.fixedHeadTableState === 'visible')) {
            clearTimeout(this.animationTimer);
            this.setState({fixedHeadTableState: 'hiding'});
            this.animationTimer = window.setTimeout(() => this.setState({fixedHeadTableState: 'hidden'}), animationTime);
        }
    }

    public updatePosition = () => {
        if (this.props.hasFixedThead) {
            this.updateFixedHeadPosition();
        }
        this.updateVisibility();
    }

    public updateSize = () => {
        if (this.props.hasFixedThead) {
            this.updateFixedHeadSize();
        }
        this.updatePosition();
    }

    public updateVisibility = () => {
        if (this.props.hasFixedThead) {
            this.updateFixedHeadVisibility();
        }
    }

    private getVisibilityCheckTarget = (props: ITableProps) => props.visibilityCheckTarget || window;

    componentDidMount() {
        window.addEventListener('resize', this.updateSize);
        this.getVisibilityCheckTarget(this.props).addEventListener('scroll', this.updateVisibility);
        
        // Have a timeout, to let the elements render
        window.setTimeout(this.updateSize, 10);
        tableSizeCallbacks.add(this.updateSize);
    }
    
    componentWillUnmount() {
        window.removeEventListener('resize', this.updateSize);
        this.getVisibilityCheckTarget(this.props).addEventListener('scroll', this.updateVisibility);
        tableSizeCallbacks.delete(this.updateSize);
    }
    
    componentDidUpdate(prevProps: ITableProps) {
        if (prevProps.visibilityCheckTarget !== this.props.visibilityCheckTarget) {
            this.getVisibilityCheckTarget(prevProps).removeEventListener('scroll', this.updateVisibility);
            this.getVisibilityCheckTarget(this.props).addEventListener('scroll', this.updateVisibility);
        }
    }

    render() {
        const { tableProps } = this.props;
        const tableClassName = clsx(
            tableProps && tableProps.className,
            bulmaShared.table,
            this.props.striped && bulmaShared.isStriped,
            this.props.narrow && bulmaShared.isNarrow,
            this.props.bordered && bulmaShared.isBordered,
            this.props.fullWidth && bulmaShared.isFullwidth,
        );
        const fixedHeadTableClassName = clsx(
            tableClassName,
            css.fixed,
            css[this.state.fixedHeadTableState],
        );

        return (
            <div className={css.wrapper} onScroll={this.updatePosition} ref={this.wrapper}>
                {this.props.hasFixedThead &&
                    <table {...tableProps} className={fixedHeadTableClassName} ref={this.fixedHeadTable}>
                        {this.props.head}
                    </table>
                }
                <table {...tableProps} className={tableClassName} ref={this.table}>
                    {this.props.head}
                    {this.props.body}
                    {this.props.foot}
                </table>
            </div>
        );
    }
}
