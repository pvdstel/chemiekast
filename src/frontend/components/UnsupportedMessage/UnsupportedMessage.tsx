import clsx from 'clsx';
import { detectOS } from 'detect-browser';
import React from 'react';
import * as i18n from '@/i18n/i18n';
import bulmaShared from '@/style/bulmaShared';
import FontAwesome from '../FA';
import css from './UnsupportedMessage.scss';

const logo = require('@/resources/images/logo.svg').default;

interface IBrowser {
    name: string;
    className: string;
    url: string;
    condition: () => boolean;
}

const browsers: IBrowser[] = [
    {
        name: 'Microsoft Edge',
        className: 'browserEdge',
        url: 'https://www.microsoft.com/edge',
        condition: () => detectOS(navigator.userAgent) === 'Windows 10',
    },
    {
        name: 'Mozilla Firefox',
        className: 'browserFirefox',
        url: 'https://www.mozilla.org/firefox/',
        condition: () => true,
    },
    {
        name: 'Google Chrome',
        className: 'browserChrome',
        url: 'https://www.google.com/chrome/',
        condition: () => true,
    },
];

function renderLanguageSwitcher() {
    return <div className={css.languageSelect}>
        <FontAwesome style='fas' icon='faGlobe' />
        <select
            value={i18n.default.language}
            onChange={e => i18n.changeLanguage(e.currentTarget.value)}>
            {i18n.getLanguages().map(l =>
                <option value={l} key={l}>{i18n.default.t(`language.${l}`)}</option>,
            )}
        </select>
    </div>;
}

function Browser(browser: IBrowser) {
    if (!browser.condition()) {
        return null;
    }

    return (
        <a
            key={browser.name}
            className={css[browser.className]}
            href={browser.url}
            target='_blank'
            rel='noreferrer noopener'>
            <span>{browser.name}</span>
        </a>
    );
}

export default function UnsupportedMessage() {
    return (
        <div className={css.root}>
            <div className={css.table}>
                <div className={css.cell}>
                    <div className={css.content}>
                        <h1 className={clsx(bulmaShared.title, bulmaShared.is1)}>
                            {i18n.default.t('unsupportedNotification.title')}
                        </h1>
                        <p>{i18n.default.t('unsupportedNotification.description1')}</p>
                        <p>{i18n.default.t('unsupportedNotification.description2')}</p>
                        <div className={css.browserList}>
                            {browsers.map(Browser)}
                        </div>
                        <hr />
                        <img className={css.logo} src={logo} />
                        <h4 className={clsx(bulmaShared.subtitle, bulmaShared.is4)}>{CHEMIEKAST_GLOBAL_APP_NAME}</h4>
                        {renderLanguageSwitcher()}
                    </div>
                </div>
            </div>
        </div>
    );
}
