import clsx from 'clsx';
import React from 'react';
import bulmaShared from '@/style/bulmaShared';
import css from './Loader.scss';

export interface ILoaderProps {
    inline?: boolean;
    large?: boolean;
    xlarge?: boolean;
    fat?: boolean;
    multi?: boolean;
    className?: string;
    primary?: boolean;
    white?: boolean;
}

export default class Loader extends React.PureComponent<ILoaderProps> {
    render() {
        const className = clsx(css.loader, this.props.className, {
            [css.isInline]: this.props.inline,
            [css.isLarge]: this.props.large,
            [css.isXlarge]: this.props.xlarge,
        });

        const ringClassName = {
            [css.isWhite]: this.props.white,
            [css.isPrimary]: this.props.primary,
            [css.isFat]: this.props.fat,
        };

        const multiRingClassName = {
            ...ringClassName,
            [css.visible]: this.props.multi,
        };

        // Use three separate SVG elements to work around render bugs in
        // Firefox (incorrect position) and Edge (incorrect position + 
        // unstable rotation)
        return (
            <div className={className}>
                <svg
                    className={clsx(css.ringOuter, ringClassName)}
                    viewBox='0 0 12.7 12.7'
                    xmlns='http://www.w3.org/2000/svg'
                    preserveAspectRatio='xMinYMin'>
                    <path
                        d='m 10.296694,2.4033113 c -2.1796994,-2.17970017 -5.7136858,-2.17970017 -7.8933883,0 m 7.6e-6,7.8933697 c 2.1796907,2.179709 5.7136766,2.179709 7.8933737,9e-6' />
                </svg>
                <svg
                    className={clsx(css.ringMiddle, multiRingClassName)}
                    viewBox='0 0 12.7 12.7'
                    xmlns='http://www.w3.org/2000/svg'
                    preserveAspectRatio='xMinYMin'>
                    <path
                        d='m 6.3499995,2.2825319 c -2.2464023,0 -4.0674699,1.8210727 -4.0674722,4.067473 m 4.067473,4.0674641 c 2.2464052,9e-6 4.0674737,-1.8210543 4.0674727,-4.0674546' />
                </svg>
                <svg
                    className={clsx(css.ringInner, multiRingClassName)}
                    viewBox='0 0 12.7 12.7'
                    xmlns='http://www.w3.org/2000/svg'
                    preserveAspectRatio='xMinYMin'>
                    <path
                        d='m 8.9066941,6.3499951 c 0,-1.4120178 -1.1446716,-2.5566893 -2.556699,-2.5566893 M 3.7933058,6.3500047 c -9.5e-6,1.4120177 1.144662,2.5566893 2.5566798,2.5566893' />
                </svg>
            </div>
        );
    }
}

// export const StaticLoader = () => <div className={bulmaShared.notification}>
//     <p>{i18n.t('general.loading')}</p>
// </div>;

interface IPageLoaderProps {
    message: string;
    large?: boolean;
}

function pageLoader(props: IPageLoaderProps) {
    const pageLoaderClassName = clsx(css.pageLoader, {
        [css.large]: props.large,
    });
    
    const messageClassName = clsx({
        [bulmaShared.isSize2]: props.large,
        [bulmaShared.hasTextCentered]: props.large,
    });

    return (
        <div className={pageLoaderClassName}>
            <Loader className={css.loader} large={props.large} multi={props.large} primary={props.large} />
            <p className={messageClassName}>{props.message}</p>
        </div>
    );
}

export const PageLoader = React.memo(pageLoader);
