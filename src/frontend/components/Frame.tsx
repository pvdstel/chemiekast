import React from 'react';
import { isSessionAuthorized } from '@/api/session';
import FullscreenLoader from '@/components/FullscreenLoader/FullscreenLoader';
import Main from '@/components/Main';
import i18n from '@/i18n/i18n';
import { registerForegroundLoaderListener } from '@/logic/loading';
import { broadcastNotification, createNotification, NotificationType } from '@/logic/notifications';
import SessionContext from '@/logic/sessionContext';
import Gatekeeper from '@/views/Gatekeeper/Gatekeeper';
import NotificationPanel from './Notifications/NotificationPanel';

const secureProtocols = ['https:'];

export default function Frame() {
    const context = React.useContext(SessionContext);
    const [isFullscreenLoaderVisible, setIsFullscreenLoaderVisible] = React.useState(false);

    const fullscreenLoaderChange = (c: number) => setIsFullscreenLoaderVisible(c > 0);

    React.useEffect(() => {
        context.refresh();
        const { unregister, value } = registerForegroundLoaderListener(fullscreenLoaderChange);
        fullscreenLoaderChange(value);
        if (!secureProtocols.includes(window.location.protocol) && CHEMIEKAST_GLOBAL_PRODUCTION_BUILD) {
            setTimeout(() => {
                broadcastNotification(createNotification(i18n.t('noHttps'), NotificationType.info));
            }, 1000);
        }
        return unregister;
    }, []);

    React.useEffect(() => {
        const onFocusCallback = () => context.refresh();
        window.addEventListener('focus', onFocusCallback);
        return () => window.removeEventListener('focus', onFocusCallback);
    }, [context.refresh]);

    return <>
        <FullscreenLoader message={i18n.t('general.appLoading')} visible={context.current === null} primary />
        <FullscreenLoader message={i18n.t('general.fullscreenLoader')} visible={isFullscreenLoaderVisible} />
        <NotificationPanel />
        {context.current !== null && (
            !isSessionAuthorized(context.current)
                ? (<Gatekeeper />)
                : (<Main />)
        )}
    </>;
}
