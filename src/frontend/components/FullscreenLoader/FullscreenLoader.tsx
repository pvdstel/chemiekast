import clsx from 'clsx';
import React from 'react';
import Loader from '@/components/Loader/Loader';
import bulmaShared from '@/style/bulmaShared';
import css from './FullscreenLoader.scss';

export const animationDuration = 500;

export interface IFullscreenLoaderProps {
    visible?: boolean;
    message: string;
    primary?: boolean;
}

interface IFullscreenLoaderState {
    isHiding: boolean;
    isShowing: boolean;
}

export default class FullscreenLoader extends React.Component<IFullscreenLoaderProps, IFullscreenLoaderState> {
    private transitionTimer: number | undefined;

    constructor(props: IFullscreenLoaderProps) {
        super(props);

        this.state = {
            isHiding: false,
            isShowing: false,
        };
    }

    componentDidUpdate(prevProps: Readonly<IFullscreenLoaderProps>, _prevState: Readonly<IFullscreenLoaderState>) {
        if (prevProps.visible !== this.props.visible) {
            if (this.transitionTimer !== undefined) { clearTimeout(this.transitionTimer); }
            if (this.props.visible) {
                this.transitionTimer = window.setTimeout(() => {
                    this.setState({ isShowing: false, isHiding: false });
                }, animationDuration);
                this.setState({ isShowing: true, isHiding: false });
            }
            else {
                this.transitionTimer = window.setTimeout(() => {
                    this.setState({ isHiding: false, isShowing: false });
                }, animationDuration);
                this.setState({ isHiding: true, isShowing: false });
            }
        }
    }

    render() {
        const { primary, message, visible } = this.props;
        const { isHiding, isShowing } = this.state;

        const className = clsx(css.fullscreenLoader, {
            [bulmaShared.hasBackgroundWhite]: !primary,
            [bulmaShared.hasBackgroundPrimary]: primary,
            [css.visible]: visible,
            [css.hiding]: isHiding,
            [css.showing]: isShowing,
        });
        const messageClassName = clsx(
            bulmaShared.isSize2,
            bulmaShared.hasTextCentered,
            css.message,
            primary ? bulmaShared.hasTextWhite : undefined,
        );

        return <div className={className}>
            <Loader className={css.loader} large multi white={this.props.primary} />
            <h1 className={messageClassName}>{message}</h1>
        </div>;
    }
}
