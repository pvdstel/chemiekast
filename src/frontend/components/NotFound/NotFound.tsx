import clsx from 'clsx';
import React from 'react';
import i18n from '@/i18n/i18n';
import { useTitle } from '@/logic/routing';
import bulmaShared from '@/style/bulmaShared';
import css from './NotFound.scss';

export default function NotFound() {
    useTitle('404');

    return <div className={clsx(bulmaShared.container, bulmaShared.isWidescreen, css.notFound)}>
        <h1 className={css.title}>404</h1>
        <p>{i18n.t('404')}</p>
    </div>;
}
