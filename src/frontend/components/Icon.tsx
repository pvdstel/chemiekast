import React from 'react';
import bulmaShared from '@/style/bulmaShared';

export default function Icon(props: React.PropsWithChildren<{}>) {
    return <span className={bulmaShared.icon}>{props.children}</span>;
}
