import clsx from 'clsx';
import React from 'react';
import fontAwesome from '@/style/fontAwesome';

type FAClasses = keyof typeof fontAwesome;
type FAStyles = 'fas' | 'far' | 'fal' | 'fab';
type FASizes = 'faLg' | 'faSm' | 'faXs' | 'fa1X' | 'fa2X' | 'fa3X'
    | 'fa4X' | 'fa5X' | 'fa6X' | 'fa7X' | 'fa8X' | 'fa9X' | 'fa10X';
type FARotate = 'faRotate90' | 'faRotate180' | 'faRotate270';
type FAFlip = 'faFlipHorizontal' | 'faFlipVertical';
type FAAnimation = 'faSpin' | 'faPulse';
type FAUnused = 'fa' | 'faFw' | 'faUl' | 'faLi' | 'faBorder' | 'faPullLeft' | 'faPullRight'
    | 'faStack' | 'faStack1x' | 'faStack2x' | 'faInverse' | 'srOnly' | 'srOnlyFocusable';

type FAExcluded = FAStyles | FASizes | FARotate | FAFlip | FAAnimation | FAUnused;
type FAIcons = Exclude<FAClasses, FAExcluded>;

// Allows for checking spelling. Check error message for misspelled classes.
// const properSubset = (key: FAExcluded) => fontAwesome[key];

export interface IFontAwesomeProps {
    style: FAStyles;
    size?: FASizes;
    rotation?: FARotate;
    flip?: FAFlip;
    animation?: FAAnimation;
    fixedWidth?: boolean;
    srOnly?: boolean;
    icon: FAIcons;
    elementProps?: React.DetailedHTMLProps<React.HTMLAttributes<HTMLSpanElement>, HTMLSpanElement>;
}

const FontAwesome = React.memo(function (props: IFontAwesomeProps) {
    const { style, size, rotation, flip, animation, icon, srOnly, fixedWidth } = props;

    const className = clsx(
        fontAwesome[style],
        fontAwesome[icon],
        size !== undefined && fontAwesome[size],
        rotation !== undefined && fontAwesome[rotation],
        flip !== undefined && fontAwesome[flip],
        animation !== undefined && fontAwesome[animation],
        fixedWidth && fontAwesome.faFw,
        srOnly && fontAwesome.srOnly,
        props.elementProps && props.elementProps.className,
    );

    return <span {...props.elementProps} className={className} />;
});
export default FontAwesome;
