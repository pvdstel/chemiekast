import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Navigation from '@/components/Navigation/Navigation';
import i18n from '@/i18n/i18n';
import { accountRoute, adminRoute, homeRoute, inventoryRoute } from '@/logic/routes';
import Account from '@/views/Account/Account';
import { PageLoader } from './Loader/Loader';
import NotFound from './NotFound/NotFound';

const Home = React.lazy(() => import(/* webpackChunkName: 'home' */ '@/views/Home/Home'));
const Inventory = React.lazy(() => import(/* webpackChunkName: 'inventory' */ '@/views/Inventory/Inventory'));
const Admin = React.lazy(() => import(/* webpackChunkName: 'admin' */ '@/views/Admin/Admin'));

export default class Main extends React.Component {
    render() {
        return (
            <BrowserRouter>
                <>
                    <Navigation />
                    <React.Suspense fallback={<PageLoader large message={i18n.t('general.loading')} />}>
                        <Switch>
                            <Route exact path={homeRoute.path} component={Home} />
                            <Route path={inventoryRoute.path} component={Inventory} />
                            <Route path={adminRoute.path} component={Admin} />
                            <Route path={accountRoute.path} component={Account} />
                            <NotFound />
                        </Switch>
                    </React.Suspense>
                </>
            </BrowserRouter>
        );
    }
}
