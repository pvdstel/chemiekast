import clsx from 'clsx';
import React from 'react';
import { Redirect, Route, RouteComponentProps, Switch, withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import { ITypedRoute } from 'typed-route-builder';
import css from './RoutingTabs.scss';

/** A single tab in the {@see RoutingTabs} component. */
export interface ITab {
    /** The label to display on the tab. */
    label: string;
    /** The route for which this tab route is considered active. */
    route: ITypedRoute<any, any, any>;
    /** The component to render when the route is active. */
    component: React.ComponentType<RouteComponentProps<any>> | React.ComponentType<any>; // from react-router typings
}

export interface IRoutingTabsProps {
    /** The tab routes to render. */
    tabs: ITab[];
    /** The base route to use for determining the active tab. */
    baseRoute: ITypedRoute<any, any, string>;
    /** The location to redirect to if the default is selected. */
    defaultRoute?: ITypedRoute<any, any, string>;
    /** The component to be rendered when no tab route is considered active. */
    notFound?: React.ComponentType<any>;
    fullwidth?: boolean;
}

type PropsType = IRoutingTabsProps & RouteComponentProps<void>;

function RoutingTabs(props: PropsType) {
    const { tabs, baseRoute, defaultRoute, location, notFound, fullwidth } = props;

    if (typeof baseRoute.fill !== 'string') {
        console.warn('The base route should not contain parameters.');
    }

    const activeRoute = tabs.find(t => location.pathname.startsWith(t.route.path));
    const tabBarClassName = clsx(css.tabs, fullwidth && css.isFullwidth);

    return (
        <>
            <div className={tabBarClassName}>
                <ul>
                    {tabs.map(t => (
                        <li key={t.route.path} className={t === activeRoute ? css.isActive : undefined}>
                            <Link to={t.route.fill}>{t.label}</Link>
                        </li>
                    ))}
                </ul>
            </div>

            <Switch>
                {defaultRoute &&
                    <Redirect exact from={baseRoute.fill} to={defaultRoute.fill} />
                }
                {tabs.map(r =>
                    <Route
                        key={r.route.path}
                        component={r.component}
                        path={r.route.path}
                    />,
                )}
                {notFound && React.createElement(notFound)}
            </Switch>
        </>
    );
}

export default withRouter(RoutingTabs);
