import clsx from 'clsx';
import React from 'react';
import css from './Sidebar.scss';

export interface IContentProps {
    sidebar?: boolean;
}

export default class Content extends React.PureComponent<IContentProps> {
    render() {
        const className = clsx(
            css.main,
            this.props.sidebar ? css.withSidebar : undefined,
        );

        return (
            <main className={className}>
                {this.props.children}
            </main>
        );
    }
}
