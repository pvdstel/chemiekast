import clsx from 'clsx';
import React from 'react';
import bulmaShared from '@/style/bulmaShared';
import FontAwesome, { IFontAwesomeProps } from '../FA';
import css from './Sidebar.scss';

export interface ISidebarProps {
    title: string;
    pinned?: boolean;
    open?: boolean;
    requestOpen?: (open: boolean) => void;
    requestPinned?: (pinned: boolean) => void;
    printable?: boolean;
    pullButtonIconProps: IFontAwesomeProps;
}

export class SidebarTitle extends React.PureComponent {
    render() {
        return <h4 className={clsx(bulmaShared.title, bulmaShared.is4)}>{this.props.children}</h4>;
    }
}

export default function Sidebar(props: React.PropsWithChildren<ISidebarProps>) {
    function renderBackdrop() {
        const className = clsx(
            css.backdrop,
            (!props.pinned && props.open) ? css.open : undefined,
        );
        const { requestOpen } = props;
        return <button className={className} onClick={() => requestOpen && requestOpen(false)}>
            <FontAwesome style='fas' icon='faTimes' />
        </button>;
    }

    function renderPullButton() {
        const { requestOpen } = props;
        const className = clsx(bulmaShared.button, css.pullOpenButton, {
            [css.visible]: !props.pinned,
        });
        return <button
            className={className}
            onClick={() => requestOpen && requestOpen(true)}>
            <FontAwesome {...props.pullButtonIconProps} />
        </button>;
    }

    function renderCloseButton() {
        const { requestOpen } = props;
        return <a className={css.close} onClick={() => requestOpen && requestOpen(false)} />;
    }

    function renderPinnedButton() {
        const { requestPinned } = props;
        return <a className={css.pin} onClick={() => requestPinned && requestPinned(true)}>
            <FontAwesome style='fas' icon='faThumbtack' fixedWidth />
        </a>;
    }

    const rootClassName = clsx(
        css.sidebar,
        props.printable ? css.printable : undefined,
        props.pinned ? null : css.unpinned,
        props.open ? css.open : null,
    );

    return <>
        <aside className={rootClassName}>
            {renderCloseButton()}
            {!props.pinned && renderPinnedButton()}
            <SidebarTitle>{props.title}</SidebarTitle>
            {props.children}
        </aside>
        {renderBackdrop()}
        {renderPullButton()}
    </>;
}

const defaultSidebarProps: Required<Pick<ISidebarProps, 'pinned' | 'open' | 'printable'>> = {
    pinned: true,
    open: false,
    printable: true,
};
(Sidebar as React.FunctionComponent).defaultProps = defaultSidebarProps;
