import clsx from 'clsx';
import React from 'react';
import { Link, useHistory } from 'react-router-dom';
import { defaultSession, isAdminSession, isDefaultRoleSession, ISessionUser, ISessionWorksheet, isManagerSession, isSessionAuthorized, signOut } from '@/api/session';
import Dropdown from '@/components/Dropdown/Dropdown';
import { SearchBox } from '@/components/Form/Form';
import { animationDuration } from '@/components/FullscreenLoader/FullscreenLoader';
import Loader from '@/components/Loader/Loader';
import i18n from '@/i18n/i18n';
import delegateEventListener from '@/logic/delegateEventHandler';
import { handleSwitchDomain } from '@/logic/domains';
import { hideForegroundLoader, registerBackgroundLoaderListener, showForegroundLoader } from '@/logic/loading';
import { accountRoute, adminRoute, getAdminPageRoute, homeRoute, inventoryAllRoute, inventoryRestrictedRoute, inventoryWorksheetRoute } from '@/logic/routes';
import { navigate } from '@/logic/routing';
import SessionContext from '@/logic/sessionContext';
import { clearEphemeralStorageItem as clearEphemeralStorage } from '@/logic/storage';
import { stringToColor } from '@/logic/utilities';
import bulmaShared from '@/style/bulmaShared';
import FontAwesome from '../FA';
import LanguageModal from '../LanguageModal';
import css from './Navigation.scss';

const logo = require('@/resources/images/logo.svg').default;

export const navigationHeight = 3.25 * 14; // nav height in em times font size

export default function Navigation() {
    const { update: updateSession, current: currentSession, refresh: refreshSession } = React.useContext(SessionContext);
    const history = useHistory();
    
    const [active, setActive] = React.useState(false);
    const [backgroundTasks, setBackgroundTasks] = React.useState(0);
    const [languageModalOpen, setLanguageModalOpen] = React.useState(false);

    const worksheetsDropdownRef = React.useRef<Dropdown | null>(null);
    const domainsDropdownRef = React.useRef<Dropdown | null>(null);
    const userDropdownRef = React.useRef<Dropdown | null>(null);

    function handleSignOut(e: React.MouseEvent) {
        e.preventDefault();
        window.setTimeout(() => {
            signOut(d => {
                if (d.isSuccessful()) {
                    navigate(history, '/#signed-out');
                    updateSession(defaultSession());
                    clearEphemeralStorage()
                    hideForegroundLoader();
                } else {
                    alert(i18n.t(`api.${d.error}`));
                }
            });
        }, animationDuration);
        showForegroundLoader();
    }

    function itemClicked() {
        if (worksheetsDropdownRef.current) {
            worksheetsDropdownRef.current.close();
        }
        if (domainsDropdownRef.current) {
            domainsDropdownRef.current.close();
        }
        if (userDropdownRef.current) {
            userDropdownRef.current.close();
        }
        setActive(false);
    }

    const createDropdown = (label: string, menuContent: React.ReactElement<React.HTMLProps<HTMLElement>>,
        ref: React.MutableRefObject<Dropdown | null>, isRight: boolean = false) => (
            <Dropdown
                ref={ref}

                container={<div className={clsx(css.navbarItem, css.hasDropdown)} />}
                containerOpenClassName={css.isActive}
                containerProps={{ onClick: delegateEventListener(itemClicked, `.${css.navbarItem}:not(.${css.hasDropdown})`) }}

                trigger={<a className={css.navbarLink} href='#'><span>{label}</span></a>}
                triggerOpenClassName={css.dropdownTriggerOpen}

                menu={<div className={clsx(css.navbarDropdown, css.dropdown, isRight && css.isRight)}>{menuContent}</div>}
                menuOpenClassName={css.opened}
                menuOpeningClassName={css.opening}
                menuClosingClassName={css.closing}

                backdropClassName={css.dropdownBackdrop}
            />
        );

    const createWorksheetsDropdown = (worksheets: ISessionWorksheet[]) => createDropdown(
        i18n.t('general.worksheet', { count: 0 }),
        <>
            {worksheets.map(w =>
                <Link
                    key={w.Code}
                    to={inventoryWorksheetRoute.fill(w.Code)(undefined)}
                    className={css.navbarItem}>
                    {w.Name}
                </Link>,
            )}
            <hr className={css.navbarDivider} />
            <Link
                className={css.navbarItem}
                to={inventoryAllRoute.fill}
            >
                {i18n.t('navigation.allWorksheets')}
            </Link>
        </>,
        worksheetsDropdownRef,
    );

    const createDomainsDropdown = () => {
        if (!currentSession || !isAdminSession(currentSession)) {
            return undefined;
        }

        return createDropdown(
            currentSession.DomainName || i18n.t('admin.domains.noExist'),
            <>
                {currentSession.Domains.map(d =>
                    <a
                        key={d.ID}
                        onClick={e => { e.preventDefault(); handleSwitchDomain(d.ID, refreshSession); }}
                        className={css.navbarItem}>
                        {d.Name}
                    </a>,
                )}
                <hr className={css.navbarDivider} />
                <Link
                    className={css.navbarItem}
                    to={getAdminPageRoute('domains').fill}
                >
                    {i18n.t('navigation.allDomains')}
                </Link>
            </>,
            domainsDropdownRef,
        );
    };

    const createUserDropdown = (user: ISessionUser) => {
        const fullName = `${user.FirstName} ${user.LastName}`;
        return createDropdown(
            user.FirstName,
            <>
                <Link to={accountRoute.fill} className={css.userCard}>
                    <FontAwesome style='fas' icon='faUserCircle' elementProps={{ style: { color: stringToColor(fullName) } }} />
                    <div>
                        <h4>{fullName}</h4>
                        <small>{user.Username} · {user.Email}</small>
                    </div>
                </Link>
                <hr className={css.navbarDivider} />
                <a className={css.navbarItem} onClick={e => { e.preventDefault(); setLanguageModalOpen(true); }} href='#'>
                    {i18n.t('language.language')}
                </a>
                <a className={css.navbarItem} onClick={handleSignOut} href='#'>{i18n.t('navigation.signOut')}</a>
            </>,
            userDropdownRef,
            true,
        );
    };

    React.useEffect(() => {
        document.body.classList.add(css.navbarFixedTop);

        const { unregister, value } = registerBackgroundLoaderListener(setBackgroundTasks);
        setBackgroundTasks(value);
        return unregister;
    }, []);


    const navClassName = clsx(css.navbar, css.isFixedTop, css.hasShadow, css.isWhite);
    const navbarMenuClassName = clsx(css.navbarMenu, { [css.isActive]: active });

    return <>
        <nav className={navClassName}>
            <div className={css.navbarBrand}>
                <Link className={css.navbarItem} to={homeRoute.fill}>
                    <img src={logo} />
                    <h1>{CHEMIEKAST_GLOBAL_APP_NAME}</h1>
                </Link>
                <div className={css.navbarBurger} onClick={() => setActive(!active)}>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
            <div className={navbarMenuClassName}>
                <div className={css.navbarStart}>
                    {currentSession !== null && isDefaultRoleSession(currentSession) &&
                        createWorksheetsDropdown(currentSession.Worksheets)
                    }
                    {currentSession !== null && isSessionAuthorized(currentSession) && !currentSession.HasDefaultRole &&
                        <Link className={css.navbarItem} to={inventoryRestrictedRoute.fill} onClick={itemClicked}>
                            {i18n.t('navigation.inventory')}
                        </Link>
                    }
                    {currentSession !== null && isManagerSession(currentSession) &&
                        <Link className={css.navbarItem} to={adminRoute.fill} onClick={itemClicked}>
                            {i18n.t('admin.title')}
                        </Link>
                    }
                </div>
                <div className={css.navbarEnd}>
                    {backgroundTasks > 0 &&
                        <div className={css.backgroundTasksIndicator}>
                            <Loader fat inline primary />
                            <span className={bulmaShared.hasTextPrimary}>&nbsp;{backgroundTasks}</span>
                        </div>
                    }
                    {createDomainsDropdown()}
                    <SearchBox formClassName={css.navbarItem} inputId='navbar-search-box' />
                    {currentSession !== null && isSessionAuthorized(currentSession) &&
                        createUserDropdown(currentSession.User)
                    }
                </div>
            </div>
        </nav >
        <LanguageModal open={languageModalOpen} requestClose={() => setLanguageModalOpen(false)} />
        {active && <div className={css.backdrop} onClick={() => setActive(false)} />}
    </>;
}

