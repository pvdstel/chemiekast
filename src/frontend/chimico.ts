import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

function requiresPolyfills() {
    return typeof Set === 'undefined'
        || typeof Map === 'undefined';
}

if (requiresPolyfills()) {
    Promise.all([
        import(/* webpackChunkName: 'required-polyfills' */ 'core-js/es/set'),
        import(/* webpackChunkName: 'required-polyfills' */'core-js/es/map'),
    ]).then(mount);
} else {
    mount();
}

function mount() {
    const app = React.createElement(App);
    const mountPoint = document.getElementById('app')!;

    ReactDOM.render(app, mountPoint);
}
