<?php

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
\Chemiekast\Api\run_authenticator('admin');

require_once $relative_root . 'data/domains.php';

// Get the JSON data
$domain_data = Chemiekast\Api\get_api_json_data();

if (\Chemiekast\Domains\edit_domain($domain_data)) {
    \Chemiekast\Api\api_success();
} else {
    \Chemiekast\Api\api_failure();
}
