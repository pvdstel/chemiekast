<?php

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
\Chemiekast\Api\run_authenticator('admin');

require_once $relative_root . 'chemiekast/chemical-management.php';

function process_database_domain($db_domain) {
    $return_domain = [
        'ID' => $db_domain['ID'],
        'Name' => $db_domain['Name'],
    ];
    return $return_domain;
}

$dbh = \Chemiekast\Config::get_PDO();
$stmt_api_get_domains = $dbh->prepare('Select `ID`, `Name` From `domains` Order By `Name`');
$stmt_api_get_domains->execute();

$return_object = [];

while ($db_domain= $stmt_api_get_domains->fetch()) {
    $return_object[] = process_database_domain($db_domain);
}

\Chemiekast\Api\api_success($return_object);
