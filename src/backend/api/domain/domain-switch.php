<?php

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
\Chemiekast\Api\run_authenticator('admin');

require_once $relative_root . 'data/logging.php';

if (\Chemiekast\Authenticator\Authenticator::is_user_admin()) { // Double check 'cause why not
    $new_domain_id = array_shift($endpoint_arguments);

    if (is_numeric($new_domain_id)) {
        
        $active_user = Chemiekast\Session\session_get()->User;
        $previous_domain = $active_user->Domain;

        $dbh = \Chemiekast\Config::get_PDO();
        $stmt_switch_domain = $dbh->prepare('Update `users` Set `Domain` = :Domain Where `ID` = :ID And `Role` = 1');
        $stmt_switch_domain->bindValue(':Domain', $new_domain_id, PDO::PARAM_INT);
        $stmt_switch_domain->bindValue(':ID', $active_user->ID, PDO::PARAM_INT);
        $stmt_switch_domain->execute();
        
        \Chemiekast\Data\Logging::add_log_activity('domain-switch', $new_domain_id);
        $active_user->Domain = (int) $new_domain_id;
        \Chemiekast\Data\Logging::add_log_activity('switched-domain', $previous_domain);

        \Chemiekast\Api\api_success((int) $new_domain_id);
    }
}

\Chemiekast\Api\api_failure();
