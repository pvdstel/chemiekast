<?php

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
\Chemiekast\Api\run_authenticator('admin');

require_once $relative_root . 'data/domains.php';

$delete_domain_id = array_shift($endpoint_arguments);
$domain_deletion_result = \Chemiekast\Domains\delete_domain($delete_domain_id);

if ($domain_deletion_result) {
    Chemiekast\Api\api_success();
} else {
    Chemiekast\Api\api_failure();
}

