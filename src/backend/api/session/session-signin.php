<?php

if (!isset($relative_root)) {
    $relative_root = '../../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
require_once $relative_root . 'data/logging.php';

$data = \Chemiekast\Api\get_api_json_data();

if (!isset($data['username']) || !isset($data['password'])) {
    \Chemiekast\Api\api_failure(CHEMIEKAST_API_ERROR_MALFORMED_REQUEST);
}

function fail_login() {
    \Chemiekast\Api\api_failure('signIn.incorrectCredentials');
}

$username = \strtolower($data['username']);
$password = $data['password'];

$dbh = \Chemiekast\Config::get_PDO();

$login_get_user = $dbh->prepare('Select `ID`, `Username`, `Password`, `Domain`, `Role`, `Email`, `State`, `FirstName`, `LastName` From `users` Where `Username` = :Username');
$login_get_user->bindValue(':Username', $username, \PDO::PARAM_STR);
$login_get_user->execute();
$user_logging_in = $login_get_user->fetch();

if ($login_get_user->rowCount() == 0
        || !isset($user_logging_in['Username'])
        || $user_logging_in['Username'] === '') {
    fail_login();
}

// Check if the user is enabled
$checkState = \Chemiekast\Utility\Values::bit_to_bool($user_logging_in['State']);
// Check if the username matches (redundant check)
$checkUsername = $username == $user_logging_in['Username'];
// Check if the password matches
$checkPassword = \password_verify($password, $user_logging_in['Password']);
// Check whether the user has a default role, or if custom roles are currently enabled
$checkCustomRole = $user_logging_in['Role'] <= CHEMIEKAST_AUTHENTICATOR_HIGHEST_DEFAULT_ROLE 
        || (\Chemiekast\Config::get_config(\Chemiekast\Config::SETTING_SYSTEM_CUSTOM_ROLES_ENABLED));

if (!$checkUsername || !$checkPassword) {
    fail_login();
}

if (!$checkState) {
    \Chemiekast\Api\api_failure('signIn.accountDisabled');
}

if (!$checkCustomRole) {    
    \Chemiekast\Api\api_failure('signIn.disallowedByPolicy');
}

// Get the worksheets this user is able to view/edit
$stmt_login_get_user_access = $dbh->prepare('Select `Worksheet`, `Editing` From `useraccess` Where `User` = :User');
$stmt_login_get_user_access->bindParam(':User', $user_logging_in['ID']);
$stmt_login_get_user_access->execute();

$user_logging_in_access = $stmt_login_get_user_access->fetchAll();
$user_logging_in_access_count = count($user_logging_in_access);
for ($iUA = 0; $iUA < $user_logging_in_access_count; $iUA++) {
    $user_logging_in_access[$iUA]['Editing'] = \Chemiekast\Utility\Values::bit_to_bool($user_logging_in_access[$iUA]['Editing']);
}

$user = new \Chemiekast\Session\User(
        (int) $user_logging_in['ID'],
        $username,
        (int) $user_logging_in['Domain'],
        (int) $user_logging_in['Role'],
        $user_logging_in['Email'],
        $user_logging_in['FirstName'],
        $user_logging_in['LastName']
);

$new_session = new \Chemiekast\Session\Session($user, $user_logging_in_access);
Chemiekast\Session\session_set($new_session);

// Update user's status to indicate login
$stmt_set_state = $dbh->prepare('Update `users` Set `State` = 1 Where `ID` = :ID');
$stmt_set_state->bindValue('ID', $user_logging_in['ID'], \PDO::PARAM_INT);
$stmt_set_state->execute();

\Chemiekast\Data\Logging::add_log_activity('login', null);

require_once $relative_root . 'chemiekast/chemical-management.php';
require_once $relative_root . 'chemiekast/authenticator-chemicals.php';
\Chemiekast\Api\api_success(Chemiekast\Session\create_user_session_object($new_session));
