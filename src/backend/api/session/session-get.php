<?php

if (!isset($relative_root)) {
    $relative_root = '../../';
}

require_once $relative_root . 'chemiekast/authenticator.php';

Chemiekast\Authenticator\Authenticator::suppress_inactivity_update();
// This call will also void $session if Authenticator so decides
$authenticator_check_fails = Chemiekast\Authenticator\Authenticator::get_check_fails();

$session = \Chemiekast\Session\session_get();

if (empty($session)) {
    $user_session = [
        'Authenticated' => false
    ];
} else {
    require_once $relative_root . 'chemiekast/chemical-management.php';
    require_once $relative_root . 'chemiekast/authenticator-chemicals.php';
    $user_session = Chemiekast\Session\create_user_session_object($session);
}

\Chemiekast\Api\api_success($user_session);
