<?php

if (!isset($relative_root)) {
    $relative_root = '../../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
require_once $relative_root . 'data/logging.php';

if (\Chemiekast\Session\session_get() !== null) {
    \Chemiekast\Data\Logging::add_log_activity('logoff', null);
    \Chemiekast\Session\session_finalize();
    \Chemiekast\Api\api_success();
} else {
    \Chemiekast\Api\api_failure('signOut.notSignedIn');
}
?>
