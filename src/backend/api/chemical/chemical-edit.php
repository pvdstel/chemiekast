<?php

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
\Chemiekast\Api\run_authenticator('restrictededitor');

require_once $relative_root . 'chemiekast/authenticator-chemicals.php';
require_once $relative_root . 'chemiekast/chemical-management.php';
require_once $relative_root . 'data/chemicals.php';

// Get the JSON data
$data = Chemiekast\Api\get_api_json_data();

$edit_result = Chemiekast\Chemicals\edit_chemical($data);

if ($edit_result === true) {
    Chemiekast\Api\api_success();
} elseif (is_string($edit_result)) {
    Chemiekast\Api\api_failure($edit_result);
} else {
    Chemiekast\Api\api_failure('inventory.modificationFailure');
}
