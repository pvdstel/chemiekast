<?php

require_once $relative_root . 'chemiekast/authenticator.php';
require_once $relative_root . 'chemiekast/authenticator-chemicals.php';
require_once $relative_root . 'chemiekast/chemical-management.php';
require_once $relative_root . 'api/chemical/process-database-chemical.php';

Chemiekast\Api\run_authenticator();

$chemical_query = array_shift($endpoint_arguments);

if (!is_numeric($chemical_query)) {
    Chemiekast\Api\api_failure('malformedRequest');
}
// Assume it's a chemical ID

if (!Chemiekast\Authenticator\Chemicals\can_view_chemical($chemical_query)) {
    Chemiekast\Api\api_failure('inventory.chemicalNotFound');
}

$column_names = array_keys(Chemiekast\Chemicals\ChemicalManagement::get_visible_columns());

$dbh = \Chemiekast\Config::get_PDO();
$stmt_get_chemical = $dbh->prepare('Select `worksheets`.`Code` As `' . CHEMICALS_WORKSHEET_COLUMN . '`, '
        . '`chemicals`.`' . implode('`, `chemicals`.`', $column_names) . '` '
        . 'From `chemicals` '
        . 'Join `worksheets` on `worksheets`.`ID` = `chemicals`.`' . CHEMICALS_WORKSHEET_COLUMN . '` '
        . 'Where `chemicals`.`' . CHEMICALS_ID_COLUMN . '` = :id');
$stmt_get_chemical->bindValue(':id', $chemical_query, PDO::PARAM_INT);
$stmt_get_chemical->execute();

if ($stmt_get_chemical->rowCount() === 0) {
    Chemiekast\Api\api_failure('inventory.chemicalNotFound');
}

$fetched_chemical = $stmt_get_chemical->fetch();

$chemical = \Chemiekast\Api\process_database_chemical($fetched_chemical, $fetched_chemical[CHEMICALS_WORKSHEET_COLUMN]);

$return_object = [
    'Chemical' => $chemical,
    'Editable' => Chemiekast\Authenticator\Chemicals\can_edit_chemical($chemical_query),
];

Chemiekast\Api\api_success($return_object);
