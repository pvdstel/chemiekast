<?php

namespace Chemiekast\Api;

require_once $relative_root . 'chemiekast/chemical-management.php';

/**
 * Processes a database object to an API-facing object.
 * @param type $db_chemical The database chemical to process.
 * @param type $worksheet_code The worksheet code to assign.
 * @return type A chemical data type ready for sending out.
 */
function process_database_chemical($db_chemical, $worksheet_code) {
    $column_names = array_keys(\Chemiekast\Chemicals\ChemicalManagement::get_visible_columns());
    $chemical = [];
    $chemical[CHEMICALS_WORKSHEET_COLUMN] = $worksheet_code;
    foreach ($column_names as $column_name) {
        if ($column_name === CHEMICALS_ID_COLUMN || $column_name === CHEMICALS_GHS_SYMBOLS_COLUMN) {
            if ($db_chemical[$column_name] !== null) {
                $chemical[$column_name] = (int) $db_chemical[$column_name];
            }
        } else {
            $chemical[$column_name] = $db_chemical[$column_name];
        }
    }
    return $chemical;
}
