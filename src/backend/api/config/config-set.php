<?php

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
\Chemiekast\Api\run_authenticator('admin');

$config_query = array_shift($endpoint_arguments);

if (empty($config_query)) {
    \Chemiekast\Api\api_failure();
}

if ($config_query === '*') {
    $config = Chemiekast\Api\get_api_json_data();
    foreach ($config as $setting => $value) {
        \Chemiekast\Config::set_config($setting, $value);
    }

    \Chemiekast\Api\api_success();
} else {
    $setting_data = Chemiekast\Api\get_api_json_data();
    \Chemiekast\Config::set_config($config_query, $setting_data);

    \Chemiekast\Api\api_success();
}
