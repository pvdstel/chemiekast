<?php

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
\Chemiekast\Api\run_authenticator('admin');

$config_query = array_shift($endpoint_arguments);

if (empty($config_query)) {
    \Chemiekast\Api\api_failure();
}

if ($config_query === '*') {

    $settings = \Chemiekast\Config::get_settings();
    $config = [];
    foreach ($settings as $setting) {
        $config[$setting] = \Chemiekast\Config::get_config($setting);
    }

    \Chemiekast\Api\api_success($config);
} else {
    $setting = $config_query;
    $settingValue = \Chemiekast\Config::get_config($setting);

    if ($settingValue !== null) {
        \Chemiekast\Api\api_success(json_encode($settingValue));
    }
}

\Chemiekast\Api\api_failure();
