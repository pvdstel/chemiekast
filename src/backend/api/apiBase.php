<?php

namespace Chemiekast\Api;

define('CHEMIEKAST_API_ERROR_MALFORMED_REQUEST', 'malformedRequest');

/**
 * Describes an API result.
 */
class ApiResult {

    /**
     * Whether the operation completed successfully.
     * @var boolean
     */
    public $ok;

    /**
     * The request data.
     * @var string
     */
    public $data;

    /**
     * The error key.
     * @var string
     */
    public $error;

    /**
     * 
     * @param boolean $ok Whether the operation was successful.
     * @param mixed $data The data.
     * @param string $error The error key.
     */
    function __construct($ok, $data, $error = null) {
        $this->ok = $ok;
        $this->data = $data;
        $this->error = $error;
    }

}

/**
 * Indicates to the client that JSON data will be sent.
 */
function send_json_header() {
    header('Content-Type: application/json; charset=utf-8');
}

/**
 * Outputs the given data as the result of the API request. Exits
 * the script as the data is output.
 * @param mixed $data The data to send to the client.
 */
function api_success($data = null) {
    send_json_header();
    $result = new ApiResult(true, $data);
    exit(\json_encode($result));
}

/**
 * Outputs the given error as the result of the API request. Exits
 * the script as the error is output.
 * @param string $error The error key of to be sent.
 */
function api_failure($error = null) {
    send_json_header();
    $result = new ApiResult(false, null, $error);
    exit(\json_encode($result));
}

/**
 * Runs Authenticator for the current request and stops if an
 * authentication failure is encountered.
 * @param string $required_privileges The requested privileges.
 */
function run_authenticator($required_privileges = null) {
    $failed_check = \Chemiekast\Authenticator\Authenticator::get_check_fails($required_privileges);

    if (!empty($failed_check)) {
        api_failure($failed_check);
    }
}

/**
 * Gets the JSON data from the request body.
 * @return mixed The JSON object data associative array.
 */
function get_api_json_data() {
    $json = \file_get_contents('php://input');

    if (!empty($json)) {
        $decoded_json = \json_decode($json, true);

        if (\json_last_error() === JSON_ERROR_NONE) {
            return $decoded_json;
        }
    }

    return [];
}
