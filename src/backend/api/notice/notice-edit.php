<?php

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
\Chemiekast\Api\run_authenticator('manager');

require_once $relative_root . 'data/notices.php';

// Get the JSON data
$notice_data = Chemiekast\Api\get_api_json_data();

$edit_result = Chemiekast\Notices\edit_notice($notice_data);

if ($edit_result) {
    \Chemiekast\Api\api_success();
} else {
    \Chemiekast\Api\api_failure();
}
