<?php

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
require_once $relative_root . 'chemiekast/authenticator-admin.php';
\Chemiekast\Api\run_authenticator(null);

function process_database_notice($db_notice) {
    $return_notice = [];
    $return_notice['ID'] = (int) $db_notice['ID'];
    $return_notice['Poster'] = $db_notice['Poster'];
    $notice_date = DateTime::createFromFormat(DATABASE_DATETIME_FORMAT, $db_notice['Date']);
    $return_notice['Date'] = $notice_date->format(DateTime::ISO8601);
    $return_notice['Message'] = $db_notice['Message'];
    $return_notice['System'] = \Chemiekast\Utility\Values::bit_to_bool($db_notice['System']);

    return $return_notice;
}

$notice_query = array_shift($endpoint_arguments);

if ($notice_query === '*') {
    $dbh = \Chemiekast\Config::get_PDO();
    $stmt_get_notices = $dbh->prepare('Select `ID`, `Poster`, `Date`, `Message`, `System` From `notices` Where `Domain` = :Domain Or `System` = 1 Order By `System` Desc, `Date` Desc');
    $stmt_get_notices->bindValue(':Domain', \Chemiekast\Session\session_get()->User->Domain, \PDO::PARAM_INT);
    $stmt_get_notices->execute();

    $return_object = [];
    $return_object['CanEditSystem'] = \Chemiekast\Authenticator\Admin\can_edit_all_notices();
    $return_object['Notices'] = [];

    while ($db_notice = $stmt_get_notices->fetch()) {
        $return_object['Notices'][] = process_database_notice($db_notice);
    }

    \Chemiekast\Api\api_success($return_object);
} elseif (is_numeric($notice_query)) {

    $dbh = \Chemiekast\Config::get_PDO();
    $stmt_get_notice = $dbh->prepare('Select `ID`, `Poster`, `Date`, `Message`, `System` From `notices` Where `ID` = :ID And (`Domain` = :Domain Or `System` = 1) Order By `System` Desc, `Date` Desc');
    $stmt_get_notice->bindValue(':ID', $notice_query, \PDO::PARAM_INT);
    $stmt_get_notice->bindValue(':Domain', \Chemiekast\Session\session_get()->User->Domain, \PDO::PARAM_INT);
    $stmt_get_notice->execute();

    if ($stmt_get_notice->rowCount()) {
        \Chemiekast\Api\api_success(process_database_notice($stmt_get_notice->fetch()));
    }
}

Chemiekast\Api\api_failure();
