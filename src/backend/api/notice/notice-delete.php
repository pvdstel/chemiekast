<?php

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
\Chemiekast\Api\run_authenticator('manager');

require_once $relative_root . 'data/notices.php';

$delete_notice_id = array_shift($endpoint_arguments);
$notice_deletion_result = Chemiekast\Notices\delete_notice($delete_notice_id);

if ($notice_deletion_result) {
    Chemiekast\Api\api_success();
} else {
    Chemiekast\Api\api_failure();
}
