<?php

if (!isset($relative_root)) {
    $relative_root = '../../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
\Chemiekast\Api\run_authenticator('admin');

require_once $relative_root . 'chemiekast/mailing.php';

$current_user = \Chemiekast\Session\session_get()->User;
$mail = new Mail(
        $current_user->FullName(),
        $current_user->Email,
        'Test mail',
        Mailing::createBody(
                'Chemiekast test email',
                [
                    'This message was requested by you to test your e-mail delivery system. Since this message has arrived, everything seems to work properly.',
                    'This email was generated on ' . time() . ' seconds since epoch time.',
                ]
        ),
        'Chemiekast Test Email');

if (Mailing::sendMail($mail)) {
    \Chemiekast\Api\api_success();
} else {
    \Chemiekast\Api\api_failure();
}
