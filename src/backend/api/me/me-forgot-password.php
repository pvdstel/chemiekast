<?php

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/config.php';
require_once $relative_root . 'chemiekast/mailing.php';

$forgot_password_data = \Chemiekast\Api\get_api_json_data();

if (isset($forgot_password_data['Username'])) {
    $dbh = \Chemiekast\Config::get_PDO();

    $password_forgot_user_exists = $dbh->prepare('Select `ID`, `Email`, `FirstName`, `LastName` From `users` Where `Username` = :Username');
    $password_forgot_user_exists->bindParam(':Username', $forgot_password_data['Username'], PDO::PARAM_STR);
    $password_forgot_user_exists->execute();

    if ($password_forgot_user_exists->rowCount() === 1) {
        $password_forgot_user_data = $password_forgot_user_exists->fetch();
        $password_reset_token = \Chemiekast\Utility\Random::random_string(192);
        $password_reset_token_hash = hash('sha256', $password_reset_token);

        $password_forgot_set_key = $dbh->prepare('Update `users` Set `PasswordToken` = :PasswordToken Where `ID` = :ID');
        $password_forgot_set_key->bindParam(':PasswordToken', $password_reset_token_hash);
        $password_forgot_set_key->bindParam(':ID', $password_forgot_user_data['ID'], PDO::PARAM_INT);
        $password_forgot_set_key->execute();

        $password_forgot_origin = filter_input(INPUT_SERVER, 'REQUEST_SCHEME') . '://' . filter_input(INPUT_SERVER, 'HTTP_HOST');
        $password_forgot_url = $password_forgot_origin . '/reset-password#' . $forgot_password_data['Username'] . '&' . $password_reset_token;

        $password_forgot_mail = new Mail(
                $password_forgot_user_data['FirstName'] . ' ' . $password_forgot_user_data['LastName'],
                $password_forgot_user_data['Email'],
                'Password reset',
                Mailing::createBody(
                        'Password reset',
                        [
                            "Hi {$password_forgot_user_data['FirstName']},",
                            "You have requested a password reset at Chemiekast. <a href=\"$password_forgot_url\">Reset it here.</a>",
                            "If you did not request a password reset, you can safely disregard this e-mail. It might be a good idea to change your password to a more secure one, since it appears that someone is trying to gain access to your account.",
                        ]
                ),
                'Reset your Chemiekast password at the following URL: ' . $password_forgot_url . "\r\n" . 'If you did not request a password reset, feel free to ignore this e-mail.'
        );

        if (Mailing::sendMail($password_forgot_mail)) {
            \Chemiekast\Api\api_success();
        } else {
            Chemiekast\Api\api_failure();
        }
    }
}

\Chemiekast\Api\api_success();
