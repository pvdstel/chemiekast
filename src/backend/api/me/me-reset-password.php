<?php

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
require_once $relative_root . 'chemiekast/mailing.php';

$reset_password_data = \Chemiekast\Api\get_api_json_data();

if (!isset($reset_password_data['Username']) || !isset($reset_password_data['Token']) || !isset($reset_password_data['NewPassword'])) {
    \Chemiekast\Api\api_failure('api.malformedRequest');
}
if (empty($reset_password_data['Username']) || empty($reset_password_data['Token']) || empty($reset_password_data['NewPassword'])) {
    \Chemiekast\Api\api_failure('api.malformedRequest');
}

$dbh = \Chemiekast\Config::get_PDO();

$password_reset_candidate = $dbh->prepare('Select `ID`, `PasswordToken` From `users` Where `Username` = :Username');
$password_reset_candidate->bindParam(':Username', $reset_password_data['Username'], PDO::PARAM_STR);
$password_reset_candidate->execute();

if ($password_reset_candidate->rowCount() === 1) {
    $password_reset_candidate_data = $password_reset_candidate->fetch();

    $password_reset_known_token = $password_reset_candidate_data['PasswordToken'];
    $password_reset_given_token = hash('sha256', $reset_password_data['Token']);
    
    if ($password_reset_given_token === $password_reset_known_token) {
        $new_password_hash = \Chemiekast\Authenticator\Authenticator::hash_password($reset_password_data['NewPassword']);
        
        $password_reset_set_password = $dbh->prepare('Update `users` Set `Password` = :NewPassword, `PasswordToken` = NULL Where `ID` = :ID');
        $password_reset_set_password->bindValue(':NewPassword', $new_password_hash, \PDO::PARAM_STR);
        $password_reset_set_password->bindValue(':ID', $password_reset_candidate_data['ID'], \PDO::PARAM_INT);
        $password_reset_set_password->execute();
        
        Chemiekast\Api\api_success();
    }
}

\Chemiekast\Api\api_failure();