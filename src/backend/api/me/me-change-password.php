<?php

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
Chemiekast\Api\run_authenticator();
require_once $relative_root . 'data/logging.php';

$password_change_data = \Chemiekast\Api\get_api_json_data();

if (isset($password_change_data['CurrentPassword']) && isset($password_change_data['NewPassword'])) {
    $current_password = $password_change_data['CurrentPassword'];
    $new_password = $password_change_data['NewPassword'];

    if ($current_password !== null && $new_password !== null) {

        $dbh = \Chemiekast\Config::get_PDO();

        $check_current_password_stmt = $dbh->prepare('Select `Password` From `users` Where `ID` = :ID');
        $check_current_password_stmt->bindParam(':ID', \Chemiekast\Session\session_get()->User->ID, PDO::PARAM_INT);
        $check_current_password_stmt->execute();
        $db_password = $check_current_password_stmt->fetch();

        $checkPassword = password_verify($current_password, $db_password['Password']);

        if ($checkPassword) {

            \Chemiekast\Data\Logging::add_log_activity('password-change', 'correct-data-provided');

            $password_hash = \Chemiekast\Authenticator\Authenticator::hash_password($new_password);

            $password_change_stmt = $dbh->prepare('Update users Set `Password` = :Password Where `ID` = :ID');
            $password_change_stmt->bindValue(':Password', $password_hash, \PDO::PARAM_STR);
            $password_change_stmt->bindValue(':ID', \Chemiekast\Session\session_get()->User->ID, \PDO::PARAM_INT);
            $password_change_stmt->execute();

            \Chemiekast\Data\Logging::add_log_activity('password-changed', null);
            
            \Chemiekast\Api\api_success();
        } else {

            \Chemiekast\Data\Logging::add_log_activity('password-change-fail', 'current-password-wrong');
            \Chemiekast\Api\api_failure('passwordChange.currentIncorrect');
        }
    } else {

        \Chemiekast\Data\Logging::add_log_activity('password-change-fail', 'invalid-input');
        \Chemiekast\Api\api_failure('malformedRequest');
    }
}
