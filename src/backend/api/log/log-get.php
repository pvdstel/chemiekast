<?php

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
\Chemiekast\Api\run_authenticator('admin');

function process_database_log($db_log, $db_log_activities = []) {
    $return_log = [];
    $return_log['ID'] = (int) $db_log['ID'];
    $return_log['UserID'] = (int) $db_log['UserID'];
    $return_log['Username'] = $db_log['Username'];
    $log_date = DateTime::createFromFormat(DATABASE_DATETIME_FORMAT, $db_log['Date']);
    $return_log['Date'] = $log_date->format(DateTime::ISO8601);
    $return_log['UserRole'] = (int) $db_log['UserRole'];
    $return_log['UserFullName'] = $db_log['UserFullName'];
    
    $return_log['Activities'] = [];
    foreach ($db_log_activities as $db_log_activity) {
        $activity_date = DateTime::createFromFormat(DATABASE_DATETIME_FORMAT, $db_log_activity['DateTime']);
        $return_log['Activities'][] = [
            'DateTime' => $activity_date->format(DateTime::ISO8601),
            'Activity' => $db_log_activity['Activity'],
            'Data' => $db_log_activity['Data'],
        ];
    }

    return $return_log;
}

require_once $relative_root . 'chemiekast/chemical-management.php';

$log_query = array_shift($endpoint_arguments);
if ($log_query === 'limit') {

    $log_limit = array_shift($endpoint_arguments);
}

if (isset($log_limit) && is_numeric($log_limit)) {

    $dbh = \Chemiekast\Config::get_PDO();
    $stmt_api_get_logs_limit = $dbh->prepare('Select `ID`, `UserID`, `Username`, `Date`, `UserRole`, `UserFullName` From `logs` Where `Domain` = :Domain Order By `Date` Desc Limit :Limit');
    $stmt_api_get_logs_limit->bindValue(':Domain', Chemiekast\Session\session_get()->User->Domain, \PDO::PARAM_INT);
    $stmt_api_get_logs_limit->bindValue(':Limit', (int) $log_limit, \PDO::PARAM_INT);
    $stmt_api_get_logs_limit->execute();

    $return_object = [];
    while ($db_log = $stmt_api_get_logs_limit->fetch()) {
        $return_object[] = process_database_log($db_log);
    }

    Chemiekast\Api\api_success($return_object);
} elseif (is_numeric($log_query)) {

    $dbh = \Chemiekast\Config::get_PDO();
    $stmt_api_get_log = $dbh->prepare('Select `ID`, `UserID`, `Username`, `Date`, `UserRole`, `UserFullName` From `logs` Where `ID` = :ID Order By `Date` Desc');
    $stmt_api_get_log->bindValue(':ID', $log_query, \PDO::PARAM_INT);
    $stmt_api_get_log->execute();

    $stmt_api_get_log_activities = $dbh->prepare('Select `DateTime`, `Activity`, `Data` From `logactivities` Where `LogID` = :ID Order By `DateTime` Asc');
    $stmt_api_get_log_activities->bindValue(':ID', $log_query, \PDO::PARAM_INT);
    $stmt_api_get_log_activities->execute();

    $return_object = process_database_log($stmt_api_get_log->fetch(), $stmt_api_get_log_activities->fetchAll());
    
    Chemiekast\Api\api_success($return_object);
} elseif ($log_query === '*') {

    $dbh = \Chemiekast\Config::get_PDO();
    $stmt_api_get_logs_all = $dbh->prepare('Select `ID`, `UserID`, `Username`, `Date`, `UserRole`, `UserFullName` From `logs` Where `Domain` = :Domain Order By `Date` Desc');
    $stmt_api_get_logs_all->bindValue(':Domain', Chemiekast\Session\session_get()->User->Domain, \PDO::PARAM_INT);
    $stmt_api_get_logs_all->execute();

    $return_object = [];
    while ($db_log = $stmt_api_get_logs_all->fetch()) {
        $return_object[] = process_database_log($db_log);
    }

    Chemiekast\Api\api_success($return_object);
}
