<?php

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
\Chemiekast\Api\run_authenticator('manager');

require_once $relative_root . 'chemiekast/chemical-management.php';

function process_database_worksheet($db_worksheet) {
    $return_worksheet = [];
    $return_worksheet['ID'] = $db_worksheet['ID'];
    $return_worksheet['Code'] = $db_worksheet['Code'];
    $return_worksheet['Name'] = $db_worksheet['Name'];

    return $return_worksheet;
}

header('Content-Type: application/json; charset=utf-8');

$return_object = [];

$dbh = \Chemiekast\Config::get_PDO();
$stmt_api_get_worksheets = $dbh->prepare('Select `ID`, `Code`, `Name` From `worksheets` Where `Domain` = :Domain Order By `Name`');
$stmt_api_get_worksheets->bindValue(':Domain', Chemiekast\Session\session_get()->User->Domain, \PDO::PARAM_INT);
$stmt_api_get_worksheets->execute();

while ($db_worksheet = $stmt_api_get_worksheets->fetch()) {
    $return_object[] = process_database_worksheet($db_worksheet);
}

Chemiekast\Api\api_success($return_object);
