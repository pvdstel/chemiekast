<?php

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
\Chemiekast\Api\run_authenticator('manager');

require_once $relative_root . 'data/worksheets.php';

$delete_worksheet_id = array_shift($endpoint_arguments);
$worksheet_deletion_result = \Chemiekast\Worksheets\delete_worksheet($delete_worksheet_id);

if ($worksheet_deletion_result) {
    Chemiekast\Api\api_success();
} else {
    Chemiekast\Api\api_failure();
}
