<?php

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
\Chemiekast\Api\run_authenticator('admin');

require_once $relative_root . 'data/columns.php';

// Get the JSON data
$column_data = Chemiekast\Api\get_api_json_data();

$column_name = false;
if (isset($column_data['Name'])) {
    $column_name = $column_data['Name'];
}

if (Chemiekast\Columns\column_exists($column_name)) {
    if (\Chemiekast\Columns\edit_column($column_data)) {
        Chemiekast\Api\api_success();
    }
}
Chemiekast\Api\api_failure();
