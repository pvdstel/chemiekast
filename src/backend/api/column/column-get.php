<?php

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
\Chemiekast\Api\run_authenticator('admin');

require_once $relative_root . 'chemiekast/chemical-management.php';

Chemiekast\Api\api_success(
        Chemiekast\Chemicals\ChemicalManagement::serialize_columns(Chemiekast\Chemicals\ChemicalManagement::get_columns())
);
