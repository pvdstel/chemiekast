<?php

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
\Chemiekast\Api\run_authenticator('admin');

require_once $relative_root . 'data/columns.php';

// Get the JSON data
$move_data = Chemiekast\Api\get_api_json_data();

if (!isset($move_data['columnName']) || !isset($move_data['direction'])) {
    Chemiekast\Api\api_failure();
}

$column_name = $move_data['columnName'];
$move_direction = strtolower($move_data['direction']);

if (Chemiekast\Columns\column_exists($column_name)) {
    switch (strtolower($move_direction)) {
        case 'up':
            Chemiekast\Columns\move_column_up($column_name);
            Chemiekast\Api\api_success();
        case 'down':
            Chemiekast\Columns\move_column_down($column_name);
            Chemiekast\Api\api_success();
    }
}

Chemiekast\Api\api_failure();
