<?php

define('API_BASEPATH', '/api');
define('DEFAULT_SUB_ENDPOINT_NAME', '');
define('ENDPOINT_NAME_CASE_SENSITIVE', false);

require_once './apiBase.php';

// Defines all the admin endpoints
$api_endpoints = [
    'info' => 'info.php',
    'session' => [
        'get' => 'session/session-get.php',
        'signin' => 'session/session-signin.php',
        'signout' => 'session/session-signout.php'
    ],
    'inventory' => [
        'worksheet' => 'inventory/worksheet-get.php',
        'search' => 'inventory/search.php',
        'restricted' => 'inventory/chemical-restricted.php'
    ],
    'chemical' => [
        'get' => 'chemical/chemical-get.php',
        'add' => 'chemical/chemical-add.php',
        'edit' => 'chemical/chemical-edit.php',
        'delete' => 'chemical/chemical-delete.php',
    ],
    'user' => [
        'get' => 'user/user-get.php',
        'add' => 'user/user-add.php',
        'edit' => 'user/user-edit.php',
        'delete' => 'user/user-delete.php'
    ],
    'role' => [
        'add-chemicals' => 'role/role-add-chemicals.php',
        'get' => 'role/role-get.php',
        'rename' => 'role/role-rename.php',
        'remove-chemical' => 'role/role-remove-chemical.php',
        'delete' => 'role/role-delete.php'
    ],
    'notice' => [
        'get' => 'notice/notice-get.php',
        'add' => 'notice/notice-add.php',
        'edit' => 'notice/notice-edit.php',
        'delete' => 'notice/notice-delete.php'
    ],
    'worksheet' => [
        'get' => 'worksheet/worksheet-get.php',
        'add' => 'worksheet/worksheet-add.php',
        'edit' => 'worksheet/worksheet-edit.php',
        'delete' => 'worksheet/worksheet-delete.php'
    ],
    'column' => [
        'get' => 'column/column-get.php',
        'add' => 'column/column-add.php',
        'edit' => 'column/column-edit.php',
        'move' => 'column/column-move.php'
    ],
    'domain' => [
        'switch' => 'domain/domain-switch.php',
        'get' => 'domain/domain-get.php',
        'add' => 'domain/domain-add.php',
        'edit' => 'domain/domain-edit.php',
        'delete' => 'domain/domain-delete.php'
    ],
    'log' => [
        'get' => 'log/log-get.php',
    ],
    'config' => [
        'get' => 'config/config-get.php',
        'set' => 'config/config-set.php',
        'reset' => 'config/config-reset.php'
    ],
    'me' => [
        'change-password' => 'me/me-change-password.php',
        'test-email' => 'me/me-test-email.php',
        'forgot-password' => 'me/me-forgot-password.php',
        'reset-password' => 'me/me-reset-password.php',
    ],
    'track' => [
        'log-beacon' => 'track/log-beacon.php',
    ],
];
$default_endpoint = null;

// The not found page
function four_o_four() {
    header('HTTP/1.0 404 Not Found');
    require '../status/404.php';
    exit();
}

function load_endpoint($found_endpoint, $endpoint_arguments) {
    $relative_root = '../';
    require $found_endpoint;
    exit();
}

function api_endpoint_path() {
    $raw_api_arguments = filter_input(INPUT_SERVER, 'REQUEST_URI'); // Get the request path
    if (!ENDPOINT_NAME_CASE_SENSITIVE) {
        $raw_api_arguments = strtolower($raw_api_arguments);
    }
    $api_basepath_length = strlen(API_BASEPATH);
    $question_mark_position = strrpos($raw_api_arguments, '?'); // check if get parameters

    if ($question_mark_position !== false) {
        return trim(substr($raw_api_arguments, $api_basepath_length, $question_mark_position - $api_basepath_length), '/');
    } else {
        return trim(substr($raw_api_arguments, $api_basepath_length), '/');
    }
}

$endpoint_arguments = explode('/', api_endpoint_path());
$endpoint = array_shift($endpoint_arguments);

if (empty($endpoint)) {
    if (!isset($default_endpoint) || empty($default_endpoint)) {
        load_endpoint('i/ee.html', []);
    } else {
        load_endpoint($api_endpoints[$default_endpoint], []);
    }
}

// Checks the endpoint
if (array_key_exists($endpoint, $api_endpoints)) {
    $found_endpoint = $api_endpoints[$endpoint];

    while (is_array($found_endpoint)) {
        $sub_endpoint = array_shift($endpoint_arguments);
        if (empty($sub_endpoint)) {
            $sub_endpoint = DEFAULT_SUB_ENDPOINT_NAME;
        }

        if (array_key_exists($sub_endpoint, $found_endpoint)) {
            $found_endpoint = $found_endpoint[$sub_endpoint];
        } elseif (empty($sub_endpoint) && count($found_endpoint)) {
            require 'i/ee.html';
            exit();
        } else {
            four_o_four();
        }
    }

    if (file_exists($found_endpoint)) {
        load_endpoint($found_endpoint, $endpoint_arguments);
    }
}

four_o_four();
