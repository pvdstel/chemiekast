<?php

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
\Chemiekast\Api\run_authenticator('manager');

require_once $relative_root . 'data/users.php';

// Get the JSON data
$data = Chemiekast\Api\get_api_json_data();

$edit_result = Chemiekast\Users\update_user($data);

if ($edit_result) {
    $update_ua_result = Chemiekast\Users\update_user_access($data['ID'], $data['UserAccess']);

    if ($update_ua_result) {
        Chemiekast\Api\api_success();
    }
}

Chemiekast\Api\api_failure();
