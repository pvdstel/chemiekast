<?php

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
\Chemiekast\Api\run_authenticator('manager');

require_once $relative_root . 'data/users.php';

$delete_user_id = array_shift($endpoint_arguments);
$user_deletion_result = Chemiekast\Users\delete_user($delete_user_id);

if ($user_deletion_result) {
    Chemiekast\Api\api_success();
} else {
    Chemiekast\Api\api_failure();
}
