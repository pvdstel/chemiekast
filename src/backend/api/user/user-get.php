<?php

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
\Chemiekast\Api\run_authenticator('manager');

require_once $relative_root . 'chemiekast/authenticator-admin.php';
require_once $relative_root . 'chemiekast/chemical-management.php';

function process_database_user($db_user, $db_user_user_access) {
    $include_domain = Chemiekast\Authenticator\Admin\can_edit_all_users();

    $return_user = [];
    $return_user['ID'] = (int) $db_user['ID'];
    $return_user['Username'] = $db_user['Username'];
    if ($include_domain) {
        $return_user['Domain'] = (int) $db_user['Domain'];
    }
    $return_user['Role'] = (int) $db_user['Role'];
    $return_user['Email'] = $db_user['Email'];
    $return_user['FirstName'] = $db_user['FirstName'];
    $return_user['LastName'] = $db_user['LastName'];
    $return_user['State'] = \Chemiekast\Utility\Values::bit_to_bool($db_user['State']);
    $return_user['UserAccess'] = [];

    $db_user_user_access_count = count($db_user_user_access);
    $worksheets = \Chemiekast\Chemicals\ChemicalManagement::get_worksheets();
    for ($i = 0; $i < $db_user_user_access_count; ++$i) {
        $return_user['UserAccess'][$i]['Worksheet'] = $db_user_user_access[$i][0] = $worksheets[$db_user_user_access[$i]['Worksheet']]->Code;
        $return_user['UserAccess'][$i]['Editing'] = $db_user_user_access[$i][1] = \Chemiekast\Utility\Values::bit_to_bool($db_user_user_access[$i]['Editing']);
    }

    return $return_user;
}

$user_query = strtolower(array_shift($endpoint_arguments));

if (is_numeric($user_query) && Chemiekast\Authenticator\Admin\can_edit_user($user_query)) {
    $dbh = \Chemiekast\Config::get_PDO();

    $q_api_get_user_by_id = 'Select `ID`, `Username`, `Domain`, `Role`, `Email`, `FirstName`, `LastName`, `State` '
            . 'From `users` '
            . 'Where `ID` = :ID;'
            . 'Select `Worksheet`, `Editing` From `useraccess` Where `User` = :ID;';

    $api_get_user_by_id = $dbh->prepare($q_api_get_user_by_id);
    $api_get_user_by_id->bindValue(':ID', $user_query, \PDO::PARAM_INT);
    $api_get_user_by_id->execute();

    $db_user = $api_get_user_by_id->fetch();
    $api_get_user_by_id->nextRowset();
    $db_user_user_access = $api_get_user_by_id->fetchAll();
    
    $return_user = process_database_user($db_user, $db_user_user_access);

    \Chemiekast\Api\api_success($return_user);
} elseif ($user_query === '*') {

    $dbh = \Chemiekast\Config::get_PDO();

    $q_api_get_all_users = 'Select `ID`, `Username`, `Domain`, `Role`, `Email`, `FirstName`, `LastName`, `State` '
            . 'From `users` '
            . 'Where `Domain` = :Domain And `Role` >= :Role '
            . 'Order By `Role`, `Username`';

    $api_get_all_users = $dbh->prepare($q_api_get_all_users);
    $api_get_all_users->bindValue(':Domain', \Chemiekast\Session\session_get()->User->Domain, \PDO::PARAM_INT);
    $api_get_all_users->bindValue(':Role', \Chemiekast\Session\session_get()->User->Role, \PDO::PARAM_INT);
    $api_get_all_users->execute();

    $return_object = [];

    while ($db_user = $api_get_all_users->fetch()) {
        $return_object[] = process_database_user($db_user, []);
    }

    \Chemiekast\Api\api_success($return_object);
}

Chemiekast\Api\api_failure();
