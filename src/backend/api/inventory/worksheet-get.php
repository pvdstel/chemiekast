<?php

require_once $relative_root . 'chemiekast/authenticator.php';
require_once $relative_root . 'chemiekast/authenticator-chemicals.php';
require_once $relative_root . 'chemiekast/chemical-management.php';
require_once $relative_root . 'api/chemical/process-database-chemical.php';
require_once $relative_root . 'data/logging.php';

\Chemiekast\Api\run_authenticator();

if (!\Chemiekast\Authenticator\Authenticator::user_has_default_role()) {
    Chemiekast\Api\api_failure('inventory.noWorksheets');
}

$requested_worksheet_code = strtolower(array_shift($endpoint_arguments));

if (!ctype_alnum($requested_worksheet_code)) {
    Chemiekast\Api\api_failure('malformedRequest');
}

// Assume it's a worksheet code

$order_by = filter_input(INPUT_GET, 'order-by', FILTER_SANITIZE_STRING);

if (empty($order_by) || !array_key_exists($order_by, Chemiekast\Chemicals\ChemicalManagement::get_visible_columns())) {
    $order_by = CHEMICALS_NAME_COLUMN;
}

$worksheet_id = Chemiekast\Chemicals\ChemicalManagement::worksheet_id_by_code($requested_worksheet_code);
if ($worksheet_id === null || !\Chemiekast\Authenticator\Chemicals\can_view_worksheet_contents($worksheet_id)) {
    // do not let the client know if they cannot view a worksheet
    Chemiekast\Api\api_failure('inventory.worksheetNotFound');
}

\Chemiekast\Data\Logging::add_log_activity('worksheet-get', $requested_worksheet_code);

$column_names = array_keys(Chemiekast\Chemicals\ChemicalManagement::get_visible_columns());

$dbh = \Chemiekast\Config::get_PDO();

$q_get_worksheet_chemicals = 'Select `chemicals`.`' . implode('`, `chemicals`.`', $column_names) . '` '
        . 'From `chemicals` '
        . 'Join `worksheets` On `worksheets`.`ID` = `chemicals`.`Worksheet` '
        . 'Where `worksheets`.`Domain` = :Domain '
        . '    And `worksheets`.`Code` = :WorksheetCode '
        . 'Order By `chemicals`.`' . $order_by . '`, `chemicals`.`' . CHEMICALS_ID_COLUMN . '`;';

$domain = Chemiekast\Session\session_get()->User->Domain;

$stmt_get_worksheet_chemicals = $dbh->prepare($q_get_worksheet_chemicals);
$stmt_get_worksheet_chemicals->bindValue(':Domain', $domain, \PDO::PARAM_INT);
$stmt_get_worksheet_chemicals->bindValue(':WorksheetCode', $requested_worksheet_code, \PDO::PARAM_STR);
$stmt_get_worksheet_chemicals->execute();

$worksheet_chemicals = [];

while ($db_chemical = $stmt_get_worksheet_chemicals->fetch()) {
    array_push($worksheet_chemicals, \Chemiekast\Api\process_database_chemical($db_chemical, $requested_worksheet_code));
}

$return_worksheet = array(
    'Code' => Chemiekast\Chemicals\ChemicalManagement::get_worksheets()[$worksheet_id]->Code,
    'Name' => Chemiekast\Chemicals\ChemicalManagement::get_worksheets()[$worksheet_id]->Name,
    'Chemicals' => $worksheet_chemicals,
    'Editable' => \Chemiekast\Authenticator\Chemicals\can_edit_worksheet_contents($worksheet_id),
    'Selectable' => \Chemiekast\Authenticator\Authenticator::user_can_edit_roles() && \Chemiekast\Config::get_config(\Chemiekast\Config::SETTING_SYSTEM_CUSTOM_ROLES_ENABLED),
);

$return_object = [$return_worksheet];

Chemiekast\Api\api_success($return_object);
