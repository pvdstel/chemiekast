<?php

require_once $relative_root . 'chemiekast/authenticator.php';
require_once $relative_root . 'chemiekast/authenticator-chemicals.php';
require_once $relative_root . 'chemiekast/chemical-management.php';
require_once $relative_root . 'api/chemical/process-database-chemical.php';
require_once $relative_root . 'data/logging.php';

\Chemiekast\Api\run_authenticator();

$q = urldecode(array_shift($endpoint_arguments));
$q_set = !empty($q);

if ($q_set) {
    \Chemiekast\Data\Logging::add_log_activity('search-for', $q);
} else {
    \Chemiekast\Data\Logging::add_log_activity('get-all-chemicals', '');
}

$column_names = array_keys(Chemiekast\Chemicals\ChemicalManagement::get_visible_columns());
$searchable_columns = array();
foreach (Chemiekast\Chemicals\ChemicalManagement::get_visible_columns() as $column_name => $column_info) {
    if ($column_info->IsSearchable) {
        array_push($searchable_columns, $column_name);
    }
}
$searchable_columns_length = count($searchable_columns);
$sql_searchable_columns = array();
foreach ($searchable_columns as $searchable_column) {
    array_push($sql_searchable_columns, '`' . $searchable_column . '` Like :' . $searchable_column);
}

$return_object = [];

$order_by = filter_input(INPUT_GET, 'order-by', FILTER_SANITIZE_STRING);
if (empty($order_by) || !array_key_exists($order_by, Chemiekast\Chemicals\ChemicalManagement::get_visible_columns())) {
    $order_by = CHEMICALS_NAME_COLUMN;
}

$dbh = \Chemiekast\Config::get_PDO();

if (\Chemiekast\Authenticator\Authenticator::user_has_default_role()) {

    $search_sql_query = 'Select `' . implode('`, `', $column_names) . '` From `chemicals` c '
            . 'Where `' . CHEMICALS_WORKSHEET_COLUMN . '` = :' . CHEMICALS_WORKSHEET_COLUMN . ' '
            . ($q_set ? 'And (' . implode(' Or ', $sql_searchable_columns) . ') ' : '')
            . 'Order By `' . $order_by . '`, `' . CHEMICALS_ID_COLUMN . '`';
    $stmt_search = $dbh->prepare($search_sql_query);
    $stmt_search->bindParam(':' . CHEMICALS_WORKSHEET_COLUMN, $worksheet_id, PDO::PARAM_STR);
    if ($q_set) {
        foreach ($searchable_columns as $searchable_column) {
            $stmt_search->bindValue(':' . $searchable_column, '%' . $q . '%');
        }
    }

    foreach (Chemiekast\Chemicals\ChemicalManagement::get_worksheets() as $worksheet_id => $worksheet_data) {
        if (\Chemiekast\Authenticator\Chemicals\can_view_worksheet_contents($worksheet_id)) {
            $stmt_search->execute();

            if ($stmt_search->rowCount()) {
                $worksheet_chemicals = [];
                while ($db_chemical = $stmt_search->fetch()) {
                    array_push($worksheet_chemicals, \Chemiekast\Api\process_database_chemical($db_chemical, $worksheet_data->Code));
                }

                $return_worksheet = array(
                    'Code' => $worksheet_data->Code,
                    'Name' => $worksheet_data->Name,
                    'Chemicals' => $worksheet_chemicals,
                    'Editable' => \Chemiekast\Authenticator\Chemicals\can_edit_worksheet_contents($worksheet_id),
                    'Selectable' => \Chemiekast\Authenticator\Authenticator::user_can_edit_roles() && \Chemiekast\Config::get_config(\Chemiekast\Config::SETTING_SYSTEM_CUSTOM_ROLES_ENABLED),
                );

                array_push($return_object, $return_worksheet);
            }
        }
    }
} else {

    if (\Chemiekast\Config::get_config(\Chemiekast\Config::SETTING_UI_SPLIT_RESTRICTED_WORKSHEET)) {

        $restricted_chemical_search_sql_query = 'Select `' . implode('`, `', array_keys(Chemiekast\Chemicals\ChemicalManagement::get_visible_columns())) . '` From `chemicals` c '
                . 'Join `rolechemical` rc On c.`' . CHEMICALS_ID_COLUMN . '` = `rc`.`Chemical` '
                . 'Where rc.`Role` = :Role '
                . 'And `' . CHEMICALS_WORKSHEET_COLUMN . '` = :' . CHEMICALS_WORKSHEET_COLUMN . ' '
                . ($q_set ? 'And (' . implode(' Or ', $sql_searchable_columns) . ') ' : '')
                . 'Order By `' . $order_by . '`, `' . CHEMICALS_ID_COLUMN . '`';
        $restricted_chemical_split_search_stmt = $dbh->prepare($restricted_chemical_search_sql_query);
        $restricted_chemical_split_search_stmt->bindParam(':Role', Chemiekast\Session\session_get()->User->Role, PDO::PARAM_INT);
        $restricted_chemical_split_search_stmt->bindParam(':' . CHEMICALS_WORKSHEET_COLUMN, $worksheet_id, PDO::PARAM_STR);
        if ($q_set) {
            foreach ($searchable_columns as $searchable_column) {
                $restricted_chemical_split_search_stmt->bindValue(':' . $searchable_column, '%' . $q . '%');
            }
        }

        foreach (Chemiekast\Chemicals\ChemicalManagement::get_worksheets() as $worksheet_id => $worksheet_data) {
            $restricted_chemical_split_search_stmt->execute();
            if ($restricted_chemical_split_search_stmt->rowCount()) {
                $found_chemicals = [];
                while ($db_chemical = $restricted_chemical_split_search_stmt->fetch()) {
                    array_push($found_chemicals, \Chemiekast\Api\process_database_chemical($db_chemical, ''));
                }

                $return_worksheet = array(
                    'Code' => $worksheet_data->Code,
                    'Name' => $worksheet_data->Name,
                    'Chemicals' => $found_chemicals,
                    'Editable' => false,
                    'Selectable' => false,
                );

                array_push($return_object, $return_worksheet);
            }
        }
    } else {

        $restricted_chemical_search_sql_query = 'Select `' . implode('`, `', array_keys(Chemiekast\Chemicals\ChemicalManagement::get_visible_columns())) . '` From `chemicals` c '
                . 'Join `rolechemical` rc On c.`' . CHEMICALS_ID_COLUMN . '` = `rc`.`Chemical` '
                . 'Where rc.`Role` = :Role '
                . ($q_set ? 'And (' . implode(' Or ', $sql_searchable_columns) . ') ' : '')
                . 'Order By `' . $order_by . '`, `' . CHEMICALS_ID_COLUMN . '`';
        $restricted_chemical_search_stmt = $dbh->prepare($restricted_chemical_search_sql_query);
        $restricted_chemical_search_stmt->bindValue(':Role', Chemiekast\Session\session_get()->User->Role, PDO::PARAM_INT);
        if ($q_set) {
            foreach ($searchable_columns as $searchable_column) {
                $restricted_chemical_search_stmt->bindValue(':' . $searchable_column, '%' . $q . '%');
            }
        }
        $restricted_chemical_search_stmt->execute();

        if ($restricted_chemical_search_stmt->rowCount()) {
            $found_chemicals = [];
            while ($db_chemical = $restricted_chemical_search_stmt->fetch()) {
                array_push($found_chemicals, \Chemiekast\Api\process_database_chemical($db_chemical, ''));
            }

            $return_worksheet = array(
                'Code' => '',
                'Name' => '',
                'Chemicals' => $found_chemicals,
                'Editable' => false,
                'Selectable' => false,
            );

            array_push($return_object, $return_worksheet);
        }
    }
}

Chemiekast\Api\api_success($return_object);
