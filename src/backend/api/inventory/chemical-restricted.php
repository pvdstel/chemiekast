<?php

require_once $relative_root . 'chemiekast/authenticator.php';
require_once $relative_root . 'chemiekast/authenticator-chemicals.php';
require_once $relative_root . 'chemiekast/chemical-management.php';
require_once $relative_root . 'api/chemical/process-database-chemical.php';
require_once $relative_root . 'data/logging.php';

\Chemiekast\Api\run_authenticator();

$order_by = filter_input(INPUT_GET, 'order-by', FILTER_SANITIZE_STRING);
if (empty($order_by) || !array_key_exists($order_by, Chemiekast\Chemicals\ChemicalManagement::get_visible_columns())) {
    $order_by = CHEMICALS_NAME_COLUMN;
}

\Chemiekast\Data\Logging::add_log_activity('get-restricted-chemicals', '');

$dbh = \Chemiekast\Config::get_PDO();
if (\Chemiekast\Config::get_config(\Chemiekast\Config::SETTING_UI_SPLIT_RESTRICTED_WORKSHEET)) {

    $q = 'Select c.`' . implode('`, c.`', array_keys(Chemiekast\Chemicals\ChemicalManagement::get_visible_columns())) . '` From `chemicals` c '
            . 'Join `rolechemical` rc On c.`' . CHEMICALS_ID_COLUMN . '` = `rc`.`Chemical` '
            . 'Where c.`' . CHEMICALS_WORKSHEET_COLUMN . '` = :Worksheet And rc.`Role` = :Role '
            . 'Order By `' . $order_by . '`, `' . CHEMICALS_ID_COLUMN . '`';
    $resticted_chemical_split_sheet_stmt = $dbh->prepare($q);
    $resticted_chemical_split_sheet_stmt->bindParam(':' . CHEMICALS_WORKSHEET_COLUMN, $worksheet_id, \PDO::PARAM_STR);
    $resticted_chemical_split_sheet_stmt->bindValue(':Role', Chemiekast\Session\session_get()->User->Role, \PDO::PARAM_INT);

    $return_object = [];

    foreach (Chemiekast\Chemicals\ChemicalManagement::get_worksheets() as $worksheet_id => $worksheet_data) {
        $resticted_chemical_split_sheet_stmt->execute();
        if ($resticted_chemical_split_sheet_stmt->rowCount()) {
            $found_chemicals = [];
            while ($db_chemical = $resticted_chemical_split_sheet_stmt->fetch()) {
                array_push($found_chemicals, \Chemiekast\Api\process_database_chemical($db_chemical, ''));
            }

            $return_worksheet = array(
                'Code' => $worksheet_data->Code,
                'Name' => $worksheet_data->Name,
                'Chemicals' => $found_chemicals,
                'Editable' => false,
                'Selectable' => false,
            );

            $return_object[] = $return_worksheet;
        }
    }

    \Chemiekast\Api\api_success($return_object);
} else {

    $q = 'Select c.`' . implode('`, c.`', array_keys(Chemiekast\Chemicals\ChemicalManagement::get_visible_columns())) . '` From `chemicals` c '
            . 'Join `rolechemical` rc On c.`' . CHEMICALS_ID_COLUMN . '` = `rc`.`Chemical` '
            . 'Where rc.`Role` = :Role '
            . 'Order By `' . $order_by . '`, `' . CHEMICALS_ID_COLUMN . '`';
    $resticted_chemical_sheet_stmt = $dbh->prepare($q);
    $resticted_chemical_sheet_stmt->bindValue(':Role', Chemiekast\Session\session_get()->User->Role, \PDO::PARAM_INT);
    $resticted_chemical_sheet_stmt->execute();

    $found_chemicals = [];
    while ($db_chemical = $resticted_chemical_sheet_stmt->fetch()) {
        array_push($found_chemicals, \Chemiekast\Api\process_database_chemical($db_chemical, ''));
    }

    $return_worksheet = array(
        'Code' => '',
        'Name' => '',
        'Chemicals' => $found_chemicals,
        'Editable' => false,
        'Selectable' => false,
    );

    $return_object = [$return_worksheet];
    \Chemiekast\Api\api_success($return_object);
}
