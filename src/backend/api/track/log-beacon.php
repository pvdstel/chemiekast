<?php

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
require_once $relative_root . 'data/logging.php';
\Chemiekast\Api\run_authenticator(null);

$beacon_data = Chemiekast\Api\get_api_json_data();
if (!isset($beacon_data['Activity']) || !isset($beacon_data['Data'])) {
    \Chemiekast\Api\api_failure();
}

if (!is_string($beacon_data['Activity']) || !is_string($beacon_data['Data'])) {
    \Chemiekast\Api\api_failure();
}

if (strlen($beacon_data['Activity']) > 255 || strlen($beacon_data['Data']) > 4096) {
    \Chemiekast\Api\api_failure();
}

\Chemiekast\Data\Logging::add_log_activity($beacon_data['Activity'], $beacon_data['Data']);
Chemiekast\Api\api_success();
