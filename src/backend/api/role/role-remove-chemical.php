<?php

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
\Chemiekast\Api\run_authenticator('manager');

require_once $relative_root . 'data/roles.php';

// Get the JSON data
$data = Chemiekast\Api\get_api_json_data();

if (!isset($data['Chemical']) || !isset($data['Role']) || !is_numeric($data['Chemical']) || !is_numeric($data['Role'])) {
    Chemiekast\Api\api_failure();
}

if (Chemiekast\Roles\remove_chemical_from_role($data['Role'], $data['Chemical'])) {
    Chemiekast\Api\api_success();
}

Chemiekast\Api\api_failure();
