<?php

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
\Chemiekast\Api\run_authenticator('manager');

require_once $relative_root . 'data/roles.php';

$delete_role_id = array_shift($endpoint_arguments);
$role_deletion_result = Chemiekast\Roles\delete_role($delete_role_id);

if ($role_deletion_result) {
    Chemiekast\Api\api_success();
} else {
    Chemiekast\Api\api_failure();
}
