<?php

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
\Chemiekast\Api\run_authenticator('manager');

require_once $relative_root . 'chemiekast/chemical-management.php';

$role_query = array_shift($endpoint_arguments);

function get_role_chemicals($roleId) {
    $dbh = \Chemiekast\Config::get_PDO();
    $stmt_api_get_role_chemicals = $dbh->prepare('Select `' . CHEMICALS_ID_COLUMN . '`, `' . CHEMICALS_NAME_COLUMN . '` From `chemicals` c Join `rolechemical` rc On rc.`Chemical` = c.`ID` Where rc.`Role` = :Role Order By c.`' . CHEMICALS_NAME_COLUMN . '`, `' . CHEMICALS_ID_COLUMN . '` Asc');
    $stmt_api_get_role_chemicals->bindValue(':Role', $roleId, \PDO::PARAM_INT);
    $stmt_api_get_role_chemicals->execute();

    $return_chemicals = [];
    while ($db_chemical = $stmt_api_get_role_chemicals->fetch()) {
        $return_chemicals[] = array(
            'ID' => (int) $db_chemical['ID'],
            'Name' => $db_chemical['Name'],
        );
    }

    return $return_chemicals;
}

if ($role_query === '*') {

    $dbh = \Chemiekast\Config::get_PDO();
    $stmt_api_get_roles = $dbh->prepare('Select `ID`, `Name` From `roles` r Where `Domain` = :Domain Order By `ID`');
    $stmt_api_get_roles->bindValue(':Domain', Chemiekast\Session\session_get()->User->Domain, \PDO::PARAM_INT);
    $stmt_api_get_roles->execute();

    $return_object = array();

    while ($role = $stmt_api_get_roles->fetch()) {
        $return_role = array(
            'ID' => (int) $role['ID'],
            'Name' => $role['Name'],
        );
        $return_role['Chemicals'] = get_role_chemicals($role['ID']);
        $return_object[] = $return_role;
    }

    Chemiekast\Api\api_success($return_object);
} elseif (is_numeric($role_query)) {

    $dbh = \Chemiekast\Config::get_PDO();
    $stmt_api_get_role = $dbh->prepare('Select `ID`, `Name` From `roles` r Where `ID` = :ID And `Domain` = :Domain Order By `ID`');
    $stmt_api_get_role->bindValue(':ID', $role_query);
    $stmt_api_get_role->bindValue(':Domain', Chemiekast\Session\session_get()->User->Domain, \PDO::PARAM_INT);
    $stmt_api_get_role->execute();

    if ($stmt_api_get_role->rowCount()) {
        $role = $stmt_api_get_role->fetch();
        $return_role = array(
            'ID' => (int) $role['ID'],
            'Name' => $role['Name'],
        );
        $return_role['Chemicals'] = get_role_chemicals($role['ID']);
        Chemiekast\Api\api_success($return_role);
    }
}

Chemiekast\Api\api_failure();
