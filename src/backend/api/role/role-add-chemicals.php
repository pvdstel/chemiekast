<?php

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
\Chemiekast\Api\run_authenticator('manager');

require_once $relative_root . 'data/roles.php';

// Get the JSON data
$data = Chemiekast\Api\get_api_json_data();

if (!isset($data['Chemicals']) || !is_array($data['Chemicals']) || !isset($data['Role'])) {
    Chemiekast\Api\api_failure();
}

$role_id = null;
if (is_numeric($data['Role'])) {
    $role_id = (int) $data['Role'];
} elseif (!empty($data['Role'])) {
    $role_id = Chemiekast\Roles\create_role($data['Role']);
}

if ($role_id !== null) {
    if (\Chemiekast\Roles\add_chemicals_to_role($role_id, $data['Chemicals'])) {
        Chemiekast\Api\api_success();
    }
}

Chemiekast\Api\api_failure();
