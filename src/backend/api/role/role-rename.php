<?php

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
\Chemiekast\Api\run_authenticator('manager');

require_once $relative_root . 'data/roles.php';

// Get the JSON data
$data = Chemiekast\Api\get_api_json_data();

if (Chemiekast\Roles\change_role_name($data)) {
    Chemiekast\Api\api_success();
}

Chemiekast\Api\api_failure();
