<?php

namespace Chemiekast\Data;

if (!isset($relative_root)) {
    $relative_root = '';
}

require_once $relative_root . 'chemiekast/authenticator.php';

define('CHEMIEKAST_DATA_LOGGING_SESSION_KEY', 'LogEntries');

/**
 * A class for logging user activities.
 */
class Logging {

    /**
     * Creates a log entry for the given user or gets one from an existing session..
     * @return integer
     */
    private static function get_log_entry_id() {

        $session = \Chemiekast\Session\session_get();
        if (!$session) {
            return null;
        }

        if (\Chemiekast\Config::get_config(\Chemiekast\Config::SETTING_SYSTEM_LOGGING_ENABLED)) {

            $active_user = $session->User;
            if (isset($_SESSION[CHEMIEKAST_DATA_LOGGING_SESSION_KEY])
                    && is_array($_SESSION[CHEMIEKAST_DATA_LOGGING_SESSION_KEY])
                    && array_key_exists($active_user->Domain, $_SESSION[CHEMIEKAST_DATA_LOGGING_SESSION_KEY])) {

                return $_SESSION[CHEMIEKAST_DATA_LOGGING_SESSION_KEY][$active_user->Domain];
            }

            $dbh = \Chemiekast\Config::get_PDO();

            $stmt_create_log_entry = $dbh->prepare('Insert Into `logs` (`Domain`, `UserID`, `Username`, `Date`, `UserRole`, `UserFullName`) Values (:Domain, :UserID, :Username, Now(), :UserRole, :UserFullName);');
            $stmt_create_log_entry->bindValue(':Domain', $active_user->Domain, \PDO::PARAM_INT);
            $stmt_create_log_entry->bindValue(':UserID', $active_user->ID, \PDO::PARAM_INT);
            $stmt_create_log_entry->bindValue(':Username', $active_user->Username, \PDO::PARAM_STR);
            $stmt_create_log_entry->bindValue(':UserRole', $active_user->Role, \PDO::PARAM_INT);
            $stmt_create_log_entry->bindValue(':UserFullName', $active_user->FullName(), \PDO::PARAM_STR);

            $stmt_create_log_entry->execute();

            $log_entry_id = $dbh->lastInsertId();

            if (!isset($_SESSION[CHEMIEKAST_DATA_LOGGING_SESSION_KEY]) 
                    || !is_array($_SESSION[CHEMIEKAST_DATA_LOGGING_SESSION_KEY])) {
                $_SESSION[CHEMIEKAST_DATA_LOGGING_SESSION_KEY] = [];
            }
            $_SESSION[CHEMIEKAST_DATA_LOGGING_SESSION_KEY][$active_user->Domain] = $log_entry_id;

            return $log_entry_id;
        }
    }

    /**
     * 
     * @param string $activity The activity to log.
     * @param string The data to log.
     * @return boolean
     */
    public static function add_log_activity($activity, $data) {

        if (\Chemiekast\Config::get_config(\Chemiekast\Config::SETTING_SYSTEM_LOGGING_ENABLED)) {

            $log_entry_id = self::get_log_entry_id();

            $dbh = \Chemiekast\Config::get_PDO();

            $stmt_add_log_activity = $dbh->prepare('Insert Into `logactivities` (`LogID`, `DateTime`, `Activity`, `Data`) Values (:LogID, Now(), :Activity, :Data);');
            $stmt_add_log_activity->bindValue(':LogID', $log_entry_id, \PDO::PARAM_INT);
            $stmt_add_log_activity->bindValue(':Activity', $activity, \PDO::PARAM_STR);
            $stmt_add_log_activity->bindValue(':Data', $data, \PDO::PARAM_STR);

            $stmt_add_log_activity->execute();
            return true;
        }
    }
}
