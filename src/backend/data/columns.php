<?php

namespace Chemiekast\Columns;

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
$failed_authenticator_check = \Chemiekast\Authenticator\Authenticator::get_check_fails('admin');
if (!empty($failed_check)) {
    api_failure($failed_check);
}

require_once $relative_root . 'chemiekast/chemical-management.php';
require_once $relative_root . 'data/logging.php';

function validate_column_data($column_data) {

    if (!isset($column_data['Name']) ||
            !ctype_alnum($column_data['Name']) ||
            !preg_match('`^[a-z][a-z0-9]{0,31}$`i', $column_data['Name'])) { // Necessary for preventing SQL injection attacks on tables
        return false;
    }
    if (!isset($column_data['UIName'])) {
        return false;
    }
    if (!isset($column_data['IsVisible'])) {
        return false;
    }
    if (!isset($column_data['IsSearchable'])) {
        return false;
    }
    if (!isset($column_data['IsUrl'])) {
        return false;
    }
    if (!isset($column_data['IsMarkdown'])) {
        return false;
    }
    if (!isset($column_data['IsFormula'])) {
        return false;
    }
    if (!isset($column_data['IsNewColumn'])) {
        $column_data['IsNewColumn'] = false;
    }
    if (!isset($column_data['NameHash']) && !$column_data['IsNewColumn']) {
        return false;
    }

    return true;
}

function column_exists($column_name) {

    if (empty($column_name)) {
        return false;
    }

    if (\Chemiekast\Chemicals\ChemicalManagement::is_default_column($column_name)) {
        return true;
    }

    $dbh = \Chemiekast\Config::get_PDO();

    $stmt_get_column_exists = $dbh->prepare('Select Count(*) From `worksheetcolumns` Where `DatabaseName` = :DatabaseName;');
    $stmt_get_column_exists->bindValue(':DatabaseName', $column_name, \PDO::PARAM_STR);
    $stmt_get_column_exists->execute();

    $count = $stmt_get_column_exists->fetchColumn();
    return $count > 0;
}

function column_edit_allowed($column_name) {

    return !\Chemiekast\Chemicals\ChemicalManagement::is_default_column($column_name);
}

function add_column($column_data) {

    if (!validate_column_data($column_data)) {
        return false;
    }

    $column_name = $column_data['Name'];
    if (!ctype_alnum($column_name) || !column_edit_allowed($column_name) || column_exists($column_name)) {
        return false;
    }

    $add_column_query = 'Start Transaction;'
            . 'Alter Table `chemiekast`.`chemicals` '
            . 'Add Column ' . $column_name . ' VARCHAR(255) NULL; ' // SQL injection attack not possible because $column_data is checked
            . 'Select @HighestOrder := Max(`Order`) From `worksheetcolumns`; '
            . 'Insert Into `worksheetcolumns` (`DatabaseName`, `Order`, `IsVisible`, `UIName`, `IsSearchable`, `IsUrl`, `IsMarkdown`, `IsFormula`) '
            . '    Values (:Name, @HighestOrder + 1, :IsVisible, :UIName, :IsSearchable, :IsUrl, :IsMarkdown, :IsFormula); '
            . 'Commit;';

    $dbh = \Chemiekast\Config::get_PDO();

    $stmt_add_column_data = $dbh->prepare($add_column_query);
    $stmt_add_column_data->bindValue(':Name', $column_name, \PDO::PARAM_STR);
    $stmt_add_column_data->bindValue(':IsVisible', $column_data['IsVisible'], \PDO::PARAM_BOOL);
    $stmt_add_column_data->bindValue(':UIName', $column_data['UIName'], \PDO::PARAM_STR);
    $stmt_add_column_data->bindValue(':IsSearchable', $column_data['IsSearchable'], \PDO::PARAM_BOOL);
    $stmt_add_column_data->bindValue(':IsUrl', $column_data['IsUrl'], \PDO::PARAM_BOOL);
    $stmt_add_column_data->bindValue(':IsMarkdown', $column_data['IsMarkdown'], \PDO::PARAM_BOOL);
    $stmt_add_column_data->bindValue(':IsFormula', $column_data['IsFormula'], \PDO::PARAM_BOOL);
    $stmt_add_column_data->execute();

    return true;
}

function edit_column($column_data) {

    if (!validate_column_data($column_data)) {
        return false;
    }

    $column_name = $column_data['Name'];
    if (!column_edit_allowed($column_name) || !column_exists($column_name)) {
        return false;
    }

    $dbh = \Chemiekast\Config::get_PDO();

    $stmt_update_column_data = $dbh->prepare('Update `worksheetcolumns` Set `IsVisible` = :IsVisible, `UIName` = :UIName, `IsSearchable` = :IsSearchable, `IsUrl`= :IsUrl, `IsMarkdown` = :IsMarkdown, `IsFormula` = :IsFormula '
            . 'Where `DatabaseName` = :Name');
    $stmt_update_column_data->bindValue(':IsVisible', $column_data['IsVisible'], \PDO::PARAM_BOOL);
    $stmt_update_column_data->bindValue(':UIName', $column_data['UIName'], \PDO::PARAM_STR);
    $stmt_update_column_data->bindValue(':IsSearchable', $column_data['IsSearchable'], \PDO::PARAM_BOOL);
    $stmt_update_column_data->bindValue(':IsUrl', $column_data['IsUrl'], \PDO::PARAM_BOOL);
    $stmt_update_column_data->bindValue(':IsMarkdown', $column_data['IsMarkdown'], \PDO::PARAM_BOOL);
    $stmt_update_column_data->bindValue(':IsFormula', $column_data['IsFormula'], \PDO::PARAM_BOOL);
    $stmt_update_column_data->bindValue(':Name', $column_name, \PDO::PARAM_STR);
    $stmt_update_column_data->execute();

    return true;
}

function move_column_up($column_name) {

    if (!column_edit_allowed($column_name) || !column_exists($column_name)) {
        return false;
    }

    $dbh = \Chemiekast\Config::get_PDO();
    
    $move_column_up_query = 'Start Transaction;'
            . 'Select @MoveUpOrder := `Order` From `worksheetcolumns` Where `DatabaseName` = :Name; '
            . 'Update `worksheetcolumns` Set `Order` = -1 Where `Order` = @MoveUpOrder - 1; '
            . 'Update `worksheetcolumns` Set `Order` = Greatest(@MoveUpOrder - 1, 0) Where `Order` = @MoveUpOrder; '
            . 'Update `worksheetcolumns` Set `Order` = @MoveUpOrder Where `Order` = -1; '
            . 'Commit;';

    $stmt_update_column_data = $dbh->prepare($move_column_up_query);
    $stmt_update_column_data->bindValue(':Name', $column_name);
    $stmt_update_column_data->execute();

    return true;
}

function move_column_down($column_name) {

    if (!column_edit_allowed($column_name) || !column_exists($column_name)) {
        return false;
    }

    $dbh = \Chemiekast\Config::get_PDO();
    
    $move_column_up_query = 'Start Transaction; '
            . 'Select @HighestOrder := Max(`Order`) From `worksheetcolumns`; '
            . 'Select @MoveDownOrder := `Order` From `worksheetcolumns` Where `DatabaseName` = :Name; '
            . 'Update `worksheetcolumns` Set `Order` = -1 Where `Order` = @MoveDownOrder + 1; '
            . 'Update `worksheetcolumns` Set `Order` = Least(@MoveDownOrder + 1, @HighestOrder) Where `Order` = @MoveDownOrder; '
            . 'Update `worksheetcolumns` Set `Order` = @MoveDownOrder Where `Order` = -1; '
            . 'Commit;';

    $stmt_update_column_data = $dbh->prepare($move_column_up_query);
    $stmt_update_column_data->bindValue(':Name', $column_name);
    $stmt_update_column_data->execute();

    return true;
}
