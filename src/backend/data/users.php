<?php

namespace Chemiekast\Users;
define('CHEMIEKAST_USERS_DEFAULT_PASSWORD_VALUE', ':default:');

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
$failed_authenticator_check = \Chemiekast\Authenticator\Authenticator::get_check_fails('manager');
if (!empty($failed_check)) {
    api_failure($failed_check);
}

require_once $relative_root . 'chemiekast/chemical-management.php';
require_once $relative_root . 'chemiekast/authenticator-admin.php';
require_once $relative_root . 'data/logging.php';

function validate_user_data($user_data, $id_necessary = true, $password_necessary = true, $domain_necessary = true) {

    if (empty($user_data) || !is_array($user_data)) {
        return false;
    }

    if ($id_necessary && (!isset($user_data['ID']) || empty($user_data['ID']) || !is_numeric($user_data['ID']))) {
        return false;
    }

    if (!isset($user_data['Username']) || empty($user_data['Username'])) {
        return false;
    }

    if ($password_necessary && (!isset($user_data['Password']) || empty($user_data['Password']))) {
        return false;
    }

    if ($password_necessary && $user_data['Password'] === CHEMIEKAST_USERS_DEFAULT_PASSWORD_VALUE) {
        return false;
    }

    if ($domain_necessary && (!isset($user_data['Domain']) || empty($user_data['Domain']) || !is_numeric($user_data['Domain']))) {
        return false;
    }

    if (!isset($user_data['Role']) || empty($user_data['Role']) || !is_numeric($user_data['Role'])) {
        return false;
    }

    if (!isset($user_data['Email']) || empty($user_data['Email'])) {
        return false;
    }

    if (!isset($user_data['State']) || !is_bool($user_data['State'])) {
        return false;
    }

    if (!isset($user_data['FirstName']) || empty($user_data['FirstName'])) {
        return false;
    }

    if (!isset($user_data['LastName']) || empty($user_data['LastName'])) {
        return false;
    }

    if (!isset($user_data['LastName']) || empty($user_data['LastName'])) {
        return false;
    }

    return true;
}

function add_user($user_data) {

    if (!validate_user_data($user_data, false, true, false) || !\Chemiekast\Authenticator\Admin\can_use_role($user_data['Role'])) {
        return 0;
    }

    $active_user = \Chemiekast\Session\session_get()->User;

    \Chemiekast\Data\Logging::add_log_activity('add-user', json_encode(array('Username' => $user_data['Username'], 'Role' => $user_data['Role'], 'FirstName' => $user_data['FirstName'], 'LastName' => $user_data['LastName'])));

    $q = 'Insert Into `users` (`Username`, `Password`, `Domain`, `Role`, `Email`, `State`, `FirstName`, `LastName`) '
            . 'Values (:Username, :Password, :Domain, :Role, :Email, :State, :FirstName, :LastName);';

    $dbh = \Chemiekast\Config::get_PDO();
    $stmt_add_user = $dbh->prepare($q);
    $stmt_add_user->bindValue(':Username', strtolower($user_data['Username']), \PDO::PARAM_STR);
    $stmt_add_user->bindValue(':Password', \Chemiekast\Authenticator\Authenticator::hash_password($user_data['Password']), \PDO::PARAM_STR);
    $stmt_add_user->bindValue(':Domain', $active_user->Domain, \PDO::PARAM_INT);
    $stmt_add_user->bindValue(':Role', $user_data['Role'], \PDO::PARAM_INT);
    $stmt_add_user->bindValue(':Email', $user_data['Email'], \PDO::PARAM_STR);
    $stmt_add_user->bindValue(':State', $user_data['State'], \PDO::PARAM_BOOL);
    $stmt_add_user->bindValue(':FirstName', $user_data['FirstName'], \PDO::PARAM_STR);
    $stmt_add_user->bindValue(':LastName', $user_data['LastName'], \PDO::PARAM_STR);
    $stmt_add_user->execute();

    return $dbh->lastInsertId();
}

function update_user($user_data) {
    if (!validate_user_data($user_data, true, false, \Chemiekast\Authenticator\Admin\can_edit_all_users()) || !\Chemiekast\Authenticator\Admin\can_edit_user($user_data['ID']) || !\Chemiekast\Authenticator\Admin\can_use_role($user_data['Role'])) {
        return false;
    }

    $active_user = \Chemiekast\Session\session_get()->User;

    \Chemiekast\Data\Logging::add_log_activity('update-user', json_encode(array('Username' => $user_data['Username'], 'Role' => $user_data['Role'], 'FirstName' => $user_data['FirstName'], 'LastName' => $user_data['LastName'])));

    $password_set = isset($user_data['Password']) && !empty($user_data['Password']) && $user_data['Password'] != CHEMIEKAST_USERS_DEFAULT_PASSWORD_VALUE;
    $q = 'Update `users` Set `Username` = :Username, ' . ($password_set ? '`Password` = :Password, ' : '') . (\Chemiekast\Authenticator\Admin\can_edit_all_users() ? '`Domain` = :Domain, ' : '') . '`Role` = :Role, `Email` = :Email, `State` = :State, `FirstName` = :FirstName, `LastName` = :LastName '
            . 'Where `ID` = :ID;';

    $dbh = \Chemiekast\Config::get_PDO();
    $stmt_update_user = $dbh->prepare($q);
    $stmt_update_user->bindValue(':ID', $user_data['ID'], \PDO::PARAM_INT);
    $stmt_update_user->bindValue(':Username', strtolower($user_data['Username']), \PDO::PARAM_STR);
    if ($password_set) {

        $stmt_update_user->bindValue(':Password', \Chemiekast\Authenticator\Authenticator::hash_password($user_data['Password']), \PDO::PARAM_STR);
    }
    if (\Chemiekast\Authenticator\Admin\can_edit_all_users()) {

        $stmt_update_user->bindValue(':Domain', $user_data['Domain'], \PDO::PARAM_INT);
    }
    $stmt_update_user->bindValue(':Role', max($user_data['Role'], $active_user->Role), \PDO::PARAM_INT);
    $stmt_update_user->bindValue(':Email', $user_data['Email'], \PDO::PARAM_STR);
    $stmt_update_user->bindValue(':State', $user_data['State'], \PDO::PARAM_BOOL);
    $stmt_update_user->bindValue(':FirstName', $user_data['FirstName'], \PDO::PARAM_STR);
    $stmt_update_user->bindValue(':LastName', $user_data['LastName'], \PDO::PARAM_STR);

    $stmt_update_user->execute();

    return true;
}

function delete_user($user_id) {

    if (empty($user_id) || !is_numeric($user_id) || !\Chemiekast\Authenticator\Admin\can_edit_user($user_id)) {
        return false;
    }

    \Chemiekast\Data\Logging::add_log_activity('delete-user', $user_id);

    $dbh = \Chemiekast\Config::get_PDO();
    $stmt_delete_user = $dbh->prepare('Delete From `users` Where `ID` = :ID;');
    $stmt_delete_user->bindValue(':ID', $user_id, \PDO::PARAM_INT);
    $stmt_delete_user->execute();

    if ($user_id == \Chemiekast\Session\session_get()->User->ID) { // Log user off
        session_unset();
        session_destroy();
    }

    return true;
}

function update_user_access($user_id, $user_accesses) {

    if (!is_numeric($user_id) || !is_array($user_accesses) || !\Chemiekast\Authenticator\Admin\can_edit_user($user_id)) {
        return false;
    }

    $dbh = \Chemiekast\Config::get_PDO();
    $stmt_delete_user_accesses = $dbh->prepare('Delete From `useraccess` Where `User` = :UserID;');
    $stmt_delete_user_accesses->bindValue(':UserID', $user_id, \PDO::PARAM_INT);
    $stmt_delete_user_accesses->execute();

    $stmt_insert_user_access = $dbh->prepare('Insert Into `useraccess` (`User`, `Worksheet`, `Editing`) Values (:UserID, :Worksheet, :Editing);');

    foreach ($user_accesses as $user_access) {

        $insert_user_access_variables = array(
            ':UserID' => $user_id,
            ':Worksheet' => \Chemiekast\Chemicals\ChemicalManagement::worksheet_id_by_code($user_access['Worksheet']),
            ':Editing' => $user_access['Editing'],
        );
        $stmt_insert_user_access->execute($insert_user_access_variables);
    }

    return true;
}
