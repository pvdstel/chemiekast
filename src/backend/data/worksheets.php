<?php

namespace Chemiekast\Worksheets;

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
$failed_authenticator_check = \Chemiekast\Authenticator\Authenticator::get_check_fails('manager');
if (!empty($failed_check)) {
    api_failure($failed_check);
}

require_once $relative_root . 'chemiekast/authenticator-admin.php';
require_once $relative_root . 'data/logging.php';

function validate_worksheet_data($worksheet_data, $id_necessary = true) {

    if (empty($worksheet_data) || !is_array($worksheet_data)) {
        return false;
    }

    if ($id_necessary && (!isset($worksheet_data['ID']) || !is_numeric($worksheet_data['ID']))) {
        return false;
    }

    if (!isset($worksheet_data['Code']) || empty($worksheet_data['Code']) || !ctype_alnum($worksheet_data['Code'])) {
        return false;
    }

    if (!isset($worksheet_data['Name']) || empty($worksheet_data['Name'])) {
        return false;
    }

    return true;
}

function add_worksheet($worksheet_data) {

    $active_user = \Chemiekast\Session\session_get()->User;

    if (!validate_worksheet_data($worksheet_data, false)) {
        return 0;
    }

    $dbh = \Chemiekast\Config::get_PDO();

    $stmt_add_worksheet = $dbh->prepare('Insert Into `worksheets` (`Code`, `Domain`, `Name`) Values (:Code, :Domain, :Name);');
    $stmt_add_worksheet->bindValue(':Code', strtolower($worksheet_data['Code']), \PDO::PARAM_STR);
    $stmt_add_worksheet->bindValue(':Domain', $active_user->Domain, \PDO::PARAM_INT);
    $stmt_add_worksheet->bindValue(':Name', $worksheet_data['Name'], \PDO::PARAM_STR);
    $stmt_add_worksheet->execute();

    return $dbh->lastInsertId();
}

function edit_worksheet($worksheet_data) {

    if (!validate_worksheet_data($worksheet_data, true) || !\Chemiekast\Authenticator\Admin\can_edit_worksheet($worksheet_data['ID'])) {
        return 0;
    }

    $dbh = \Chemiekast\Config::get_PDO();

    $stmt_edit_worksheet = $dbh->prepare('Update `worksheets` Set `Code` = :Code, `Name` = :Name Where `ID` = :ID;');
    $stmt_edit_worksheet->bindValue(':Code', strtolower($worksheet_data['Code']), \PDO::PARAM_STR);
    $stmt_edit_worksheet->bindValue(':Name', $worksheet_data['Name'], \PDO::PARAM_STR);
    $stmt_edit_worksheet->bindValue(':ID', $worksheet_data['ID'], \PDO::PARAM_INT);
    $stmt_edit_worksheet->execute();

    return true;
}

function delete_worksheet($worksheet_id) {

    if (!is_numeric($worksheet_id) || !\Chemiekast\Authenticator\Admin\can_edit_worksheet($worksheet_id)) {
        return false;
    }

    $dbh = \Chemiekast\Config::get_PDO();

    $stmt_delete_worksheet = $dbh->prepare('Delete From `worksheets` Where `ID` = :ID;');
    $stmt_delete_worksheet->bindValue(':ID', $worksheet_id, \PDO::PARAM_INT);
    $stmt_delete_worksheet->execute();

    return true;
}
