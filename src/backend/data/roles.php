<?php

namespace Chemiekast\Roles;

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
$failed_authenticator_check = \Chemiekast\Authenticator\Authenticator::get_check_fails('manager');
if (!empty($failed_check)) {
    api_failure($failed_check);
}

require_once $relative_root . 'chemiekast/authenticator-admin.php';
require_once $relative_root . 'data/logging.php';

function validate_role_data($role_data, $id_necessary = true) {

    if (empty($role_data) || !is_array($role_data)) {
        return false;
    }

    if ($id_necessary && (!isset($role_data['ID']) || !is_numeric($role_data['ID']))) {
        return false;
    }

    if (!isset($role_data['Name']) || empty($role_data['Name'])) {
        return false;
    }

    return true;
}

function get_next_role_id() {
    $dbh = \Chemiekast\Config::get_PDO();
    $stmt_max_role_id = $dbh->prepare('Select Max(`ID`) FROM `chemiekast`.`roles`;');
    $stmt_max_role_id->execute();
    $max_id = $stmt_max_role_id->fetchColumn();
    if ($max_id === null) {
        $max_id = 100;
    }
    return $max_id + rand(0, 100);
}

function create_role($role_name) {
    $active_user = \Chemiekast\Session\session_get()->User;

    if (!empty($role_name) && \Chemiekast\Authenticator\Admin\can_edit_domain_roles()) {
        $dbh = \Chemiekast\Config::get_PDO();
        $stmt_check_existing_role = $dbh->prepare('Select `ID` From `roles` Where `Name` = :Name And `Domain` = :Domain;');
        $stmt_check_existing_role->bindValue(':Name', $role_name, \PDO::PARAM_STR);
        $stmt_check_existing_role->bindValue(':Domain', $active_user->Domain, \PDO::PARAM_INT);
        $stmt_check_existing_role->execute();

        if ($stmt_check_existing_role->rowCount()) {
            return $stmt_check_existing_role->fetchColumn();
        } else {
            \Chemiekast\Data\Logging::add_log_activity('create-role', json_encode($role_name));
            $stmt_create_new_role = $dbh->prepare('Insert Into `roles` (`ID`, `Name`, `Domain`) Values (:ID, :Name, :Domain);');
            $stmt_create_new_role->bindValue(':ID', get_next_role_id(), \PDO::PARAM_INT);
            $stmt_create_new_role->bindParam(':Name', $role_name, \PDO::PARAM_STR);
            $stmt_create_new_role->bindValue(':Domain', $active_user->Domain, \PDO::PARAM_INT);
            $stmt_create_new_role->execute();
            return $dbh->lastInsertId('ID');
        }
    }

    return null;
}

function add_chemicals_to_role($role_id, $chemicals) {
    if (!is_numeric($role_id) || !is_array($chemicals) || !\Chemiekast\Authenticator\Admin\can_edit_role($role_id)) {
        return false;
    }

    \Chemiekast\Data\Logging::add_log_activity('add-chemical-to-role', $role_id . '; ' . json_encode($chemicals));

    $adding_chemical = 0;

    $dbh = \Chemiekast\Config::get_PDO();
    $stmt_add_chemical_to_role = $dbh->prepare('Insert Into `rolechemical` Values (:Chemical, :Role);');
    $stmt_add_chemical_to_role->bindParam(':Chemical', $adding_chemical, \PDO::PARAM_INT);
    $stmt_add_chemical_to_role->bindValue(':Role', $role_id, \PDO::PARAM_INT);

    foreach ($chemicals as $adding_chemical) {
        $stmt_add_chemical_to_role->execute();
    }

    return true;
}

function remove_chemical_from_role($role_id, $chemical_id) {
    if (!is_numeric($role_id) || !is_numeric($chemical_id) || !\Chemiekast\Authenticator\Admin\can_edit_role($role_id)) {
        return false;
    }

    \Chemiekast\Data\Logging::add_log_activity('remove-chemical-from-role', $role_id . '; ' . $chemical_id);

    $dbh = \Chemiekast\Config::get_PDO();
    $stmt_remove_chemical_from_role = $dbh->prepare('Delete From `rolechemical` Where `Role` = :Role And `Chemical` = :Chemical;');
    $stmt_remove_chemical_from_role->bindValue(':Role', $role_id, \PDO::PARAM_INT);
    $stmt_remove_chemical_from_role->bindValue(':Chemical', $chemical_id, \PDO::PARAM_INT);
    $stmt_remove_chemical_from_role->execute();

    return true;
}

function change_role_name($role_data) {
    if (!validate_role_data($role_data, true) || !\Chemiekast\Authenticator\Admin\can_edit_role($role_data['ID'])) {
        return false;
    }

    \Chemiekast\Data\Logging::add_log_activity('change-role-name', $role_data['ID'] . '; ' . $role_data['Name']);

    $dbh = \Chemiekast\Config::get_PDO();
    $stmt = $dbh->prepare('Update `roles` Set `Name` = :Name Where `ID` = :ID;');
    $stmt->bindValue(':Name', $role_data['Name'], \PDO::PARAM_STR);
    $stmt->bindValue(':ID', $role_data['ID'], \PDO::PARAM_INT);
    $stmt->execute();

    return true;
}

function delete_role($role_id) {
    if (!is_numeric($role_id) || !\Chemiekast\Authenticator\Admin\can_edit_role($role_id)) {
        return false;
    }

    \Chemiekast\Data\Logging::add_log_activity('delete-role', $role_id);

    $dbh = \Chemiekast\Config::get_PDO();
    $stmt_delete_role = $dbh->prepare('Delete From `roles` Where `ID` = :ID;');
    $stmt_delete_role->bindValue(':ID', $role_id, \PDO::PARAM_INT);
    $stmt_delete_role->execute();

    return true;
}
