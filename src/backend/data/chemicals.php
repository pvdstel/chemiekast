<?php

namespace Chemiekast\Chemicals;

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/chemical-management.php';
$failed_authenticator_check = \Chemiekast\Authenticator\Authenticator::get_check_fails();
if (!empty($failed_check)) {
    api_failure($failed_check);
}

require_once $relative_root . 'data/logging.php';

/**
 * Determines whether a given chemical data structure is valid.
 * @param type $chemical_data The data to verify.
 * @param type $is_new Whether the ID field is necessary in the structure.
 * @return boolean Whether the data structure is valid.
 */
function validate_chemical_data($chemical_data, $is_new = true) {
    if (empty($chemical_data) || !is_array($chemical_data)) {
        return false;
    }

    // Check for the ID column.
    if (!$is_new && !isset($chemical_data[CHEMICALS_ID_COLUMN])) {
        return false;
    }
    if (isset($chemical_data[CHEMICALS_ID_COLUMN]) && !is_numeric($chemical_data[CHEMICALS_ID_COLUMN])) {
        return false;
    }

    // Check for the name column.
    if ($is_new && (!isset($chemical_data[CHEMICALS_NAME_COLUMN]) || empty($chemical_data[CHEMICALS_NAME_COLUMN]))) {
        return false;
    }

    // Checks for the worksheet column.
    if ($is_new && !isset($chemical_data[CHEMICALS_WORKSHEET_COLUMN])) {
        return false;
    }
    if (isset($chemical_data[CHEMICALS_WORKSHEET_COLUMN]) && !ChemicalManagement::worksheet_with_code_exists($chemical_data[CHEMICALS_WORKSHEET_COLUMN])) {
        return false;
    }

    return true;
}

/**
 * Adds a chemical to the data store.
 * @param type $chemical_data The chemical patch data to store.
 * @return boolean|int Whether the operation was successful.
 */
function add_chemical($chemical_data) {
    if (!validate_chemical_data($chemical_data, true)) {
        return false;
    }

    if (!\Chemiekast\Authenticator\Chemicals\can_edit_worksheet_contents(ChemicalManagement::worksheet_id_by_code($chemical_data[CHEMICALS_WORKSHEET_COLUMN]))) {
        return false;
    }

    \Chemiekast\Data\Logging::add_log_activity('add-chemical', json_encode($chemical_data));

    // Process only the columns that exist
    $record_values = array();
    $worksheet_columns_keys = array_keys(ChemicalManagement::get_columns());
    foreach ($worksheet_columns_keys as $worksheet_column) {
        if ($worksheet_column != CHEMICALS_ID_COLUMN && $worksheet_column !== CHEMICALS_WORKSHEET_COLUMN && array_key_exists($worksheet_column, $chemical_data)) {
            $record_values[$worksheet_column] = $chemical_data[$worksheet_column];
        }
    }

    $record_keys = array_keys($record_values);
    $worksheet_id = ChemicalManagement::worksheet_id_by_code($chemical_data[CHEMICALS_WORKSHEET_COLUMN]);

    $q = 'Insert Into `chemicals` '
            . '(`' . CHEMICALS_WORKSHEET_COLUMN . '`, `' . implode('`, `', $record_keys) . '`) '
            . 'Values (:' . CHEMICALS_WORKSHEET_COLUMN . ', :' . implode(', :', $record_keys) . ');';

    $dbh = \Chemiekast\Config::get_PDO();
    $stmt_add_chemical = $dbh->prepare($q);
    $stmt_add_chemical->bindValue(':' . CHEMICALS_WORKSHEET_COLUMN, $worksheet_id, \PDO::PARAM_INT);
    foreach ($record_values as $record_key => &$record_value) {
        $stmt_add_chemical->bindParam(':' . $record_key, $record_value);
    }

    $stmt_add_chemical->execute();

    $bla = $stmt_add_chemical->errorInfo();
    if (\Chemiekast\Utility\pdo_statement_has_error($stmt_add_chemical)) {
        return false;
    }

    return $dbh->lastInsertId();
}

/**
 * Edits a chemical in the data store.
 * @param mixed $chemical_data The patch data to use.
 * @return boolean|string Whether the edit operation was successful.
 */
function edit_chemical($chemical_data) {
    if (!validate_chemical_data($chemical_data, false)) {
        return false;
    }

    $worksheet_field_set = isset($chemical_data[CHEMICALS_WORKSHEET_COLUMN]);
    if ($worksheet_field_set) {
        if (!\Chemiekast\Authenticator\Chemicals\can_edit_chemical_transaction(
                        $chemical_data[CHEMICALS_ID_COLUMN],
                        ChemicalManagement::worksheet_id_by_code($chemical_data[CHEMICALS_WORKSHEET_COLUMN])
                )) {
            return false;
        }
    } else {
        if (!\Chemiekast\Authenticator\Chemicals\can_edit_chemical($chemical_data[CHEMICALS_ID_COLUMN])) {
            return false;
        }
    }

    // Process only the columns that exist
    $record_values = array();
    $worksheet_columns_keys = array_keys(ChemicalManagement::get_columns());
    foreach ($worksheet_columns_keys as $worksheet_column) {
        if ($worksheet_column != CHEMICALS_ID_COLUMN && array_key_exists($worksheet_column, $chemical_data)) {
            $record_values[$worksheet_column] = $chemical_data[$worksheet_column];
        }
    }

    if (count($record_values) === 0 && !$worksheet_field_set) {
        return 'inventory.unchanged';
    }

    \Chemiekast\Data\Logging::add_log_activity('update-chemical', json_encode($chemical_data));

    $q = 'Update `chemicals` Set `';
    $is_first = true;
    if ($worksheet_field_set) { // allow changing worksheet
        $q .= CHEMICALS_WORKSHEET_COLUMN . '` = :' . CHEMICALS_WORKSHEET_COLUMN;
        $is_first = false;
    }
    foreach ($record_values as $record_key => &$record_value) {
        if (!$is_first) {
            $q .= ', `';
        }
        $q .= $record_key . '` = :' . $record_key;
        $is_first = false;
    }
    $q .= ' Where `' . CHEMICALS_ID_COLUMN . '` = :' . CHEMICALS_ID_COLUMN . ';';

    $dbh = \Chemiekast\Config::get_PDO();
    $stmt_update_chemical = $dbh->prepare($q);
    if ($worksheet_field_set) { // bind worksheet value
        $stmt_update_chemical->bindValue(
                ':' . CHEMICALS_WORKSHEET_COLUMN,
                ChemicalManagement::worksheet_id_by_code($chemical_data[CHEMICALS_WORKSHEET_COLUMN]),
                \PDO::PARAM_INT
        );
    }
    foreach ($record_values as $record_key => &$record_value) {
        $stmt_update_chemical->bindValue(':' . $record_key, $record_value, \PDO::PARAM_STR);
    }
    $stmt_update_chemical->bindValue(':' . CHEMICALS_ID_COLUMN, $chemical_data[CHEMICALS_ID_COLUMN], \PDO::PARAM_INT);
    $stmt_update_chemical->execute();

    return !\Chemiekast\Utility\pdo_statement_has_error($stmt_update_chemical);
}

/**
 * Deletes a chemical from the data store.
 * @param type $chemical_id The ID of the chemical to delete.
 * @return boolean Whether the delete operation was successful.
 */
function delete_chemical($chemical_id) {
    if (!is_numeric($chemical_id)) {
        return false;
    }

    if (!\Chemiekast\Authenticator\Chemicals\can_edit_chemical($chemical_id)) {
        return false;
    }

    \Chemiekast\Data\Logging::add_log_activity('delete-chemical', $chemical_id);

    $q = 'Delete From `chemicals` Where `' . CHEMICALS_ID_COLUMN . '` = :' . CHEMICALS_ID_COLUMN . ';';

    $dbh = \Chemiekast\Config::get_PDO();
    $stmt_delete_chemical = $dbh->prepare($q);
    $stmt_delete_chemical->bindValue(':' . CHEMICALS_ID_COLUMN, $chemical_id, \PDO::PARAM_INT);
    $stmt_delete_chemical->execute();

    return !\Chemiekast\Utility\pdo_statement_has_error($stmt_delete_chemical);
}
