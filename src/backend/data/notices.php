<?php

namespace Chemiekast\Notices;

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
$failed_authenticator_check = \Chemiekast\Authenticator\Authenticator::get_check_fails('manager');
if (!empty($failed_check)) {
    api_failure($failed_check);
}

require_once $relative_root . 'chemiekast/authenticator-admin.php';
require_once $relative_root . 'data/logging.php';

function validate_notice_data($notice_data, $id_necessary = true) {

    if (empty($notice_data) || !is_array($notice_data)) {
        return false;
    }

    if ($id_necessary && (!isset($notice_data['ID']) || !is_numeric($notice_data['ID']))) {
        return false;
    }

    if (!isset($notice_data['Message']) || empty($notice_data['Message'])) {
        return false;
    }

    if (!isset($notice_data['System']) || !is_bool($notice_data['System'])) {
        return false;
    }

    return true;
}

function add_notice($notice_data) {

    if (!validate_notice_data($notice_data, false)) {
        return 0;
    }

    $user = \Chemiekast\Session\session_get()->User;
    $set_system = is_bool($notice_data['System']) && \Chemiekast\Authenticator\Admin\can_edit_all_notices();

    $dbh = \Chemiekast\Config::get_PDO();

    $stmt_add_notice = $dbh->prepare('Insert Into `notices` (`Domain`, `Poster`, `Date`, `Message`' . ($set_system ? ', `System`' : '') . ') Values (:Domain, :Poster, Now(), :Message' . ($set_system ? ', :System' : '') . ');');
    $stmt_add_notice->bindValue(':Domain', $user->Domain, \PDO::PARAM_INT);
    $stmt_add_notice->bindValue(':Poster', $user->FullName(), \PDO::PARAM_STR);
    $stmt_add_notice->bindValue(':Message', $notice_data['Message'], \PDO::PARAM_STR);
    if ($set_system) {
        $stmt_add_notice->bindValue(':System', $notice_data['System'], \PDO::PARAM_BOOL);
    }
    $stmt_add_notice->execute();

    return $dbh->lastInsertId();
}

function edit_notice($notice_data) {

    if (!validate_notice_data($notice_data) || !\Chemiekast\Authenticator\Admin\can_edit_notice($notice_data['ID'])) {
        return false;
    }

    $user = \Chemiekast\Session\session_get()->User;
    $set_system = is_bool($notice_data['System']) && \Chemiekast\Authenticator\Admin\can_edit_all_notices();

    $dbh = \Chemiekast\Config::get_PDO();

    $stmt_update_notice = $dbh->prepare('Update `notices` Set `Poster` = :Poster, `Date` = Now(), `Message` = :Message' . ($set_system ? ', `System` = :System' : '') . ' Where `ID` = :ID;');
    $stmt_update_notice->bindValue(':Poster', $user->FullName(), \PDO::PARAM_STR);
    $stmt_update_notice->bindValue(':Message', $notice_data['Message'], \PDO::PARAM_STR);
    if ($set_system) {
        $stmt_update_notice->bindValue(':System', $notice_data['System'], \PDO::PARAM_BOOL);
    }
    $stmt_update_notice->bindValue(':ID', $notice_data['ID'], \PDO::PARAM_STR);
    $stmt_update_notice->execute();

    return true;
}

function delete_notice($notice_id) {

    if (!is_numeric($notice_id) || !\Chemiekast\Authenticator\Admin\can_edit_notice($notice_id)) {
        return false;
    }

    $dbh = \Chemiekast\Config::get_PDO();

    $stmt_delete_notice = $dbh->prepare('Delete From `notices` Where `ID` = :ID;');
    $stmt_delete_notice->bindValue(':ID', $notice_id, \PDO::PARAM_INT);
    $stmt_delete_notice->execute();

    return true;
}
