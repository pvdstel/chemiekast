<?php

namespace Chemiekast\Domains;

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/authenticator.php';
$failed_authenticator_check = \Chemiekast\Authenticator\Authenticator::get_check_fails('admin');
if (!empty($failed_check)) {
    api_failure($failed_check);
}

require_once $relative_root . 'chemiekast/authenticator-admin.php';
require_once $relative_root . 'data/logging.php';

function validate_domain_data($domain, $id_necessary = true) {
    if (empty($domain) || !is_array($domain)) {
        return false;
    }

    if ($id_necessary && (!isset($domain['ID']) || !is_numeric($domain['ID']))) {
        return false;
    }

    if (!isset($domain['Name']) || empty($domain['Name'])) {
        return false;
    }

    return true;
}

function add_domain($domain) {
    if (!validate_domain_data($domain, false)) {
        return false;
    }

    $dbh = \Chemiekast\Config::get_PDO();

    $stmt_add_domain = $dbh->prepare('Insert Into `domains` (`Name`) Values (:Name);');
    $stmt_add_domain->bindValue(':Name', $domain['Name'], \PDO::PARAM_STR);
    $stmt_add_domain->execute();

    return $dbh->lastInsertId();
}

function edit_domain($domain) {
    if (!validate_domain_data($domain)) {
        return false;
    }

    $dbh = \Chemiekast\Config::get_PDO();

    $stmt_edit_domain = $dbh->prepare('Update `domains` Set `Name` = :Name Where `ID` = :ID;');
    $stmt_edit_domain->bindValue(':Name', $domain['Name'], \PDO::PARAM_STR);
    $stmt_edit_domain->bindValue(':ID', $domain['ID'], \PDO::PARAM_INT);
    $stmt_edit_domain->execute();

    return true;
}

function delete_domain($domain_id) {
    if (!is_numeric($domain_id)) {
        return false;
    }

    $dbh = \Chemiekast\Config::get_PDO();

    $stmt_delete_domain = $dbh->prepare('Delete From `domains` Where `ID` = :ID;');
    $stmt_delete_domain->bindValue(':ID', $domain_id, \PDO::PARAM_INT);
    $stmt_delete_domain->execute();

    $stmt_delete_domain_users = $dbh->prepare('Delete From `users` Where `Domain` = :Domain And `Role` > 1');
    $stmt_delete_domain_users->bindValue(':Domain', $domain_id, \PDO::PARAM_INT);
    $stmt_delete_domain_users->execute();

    return true;
}
