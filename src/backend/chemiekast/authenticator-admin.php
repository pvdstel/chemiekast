<?php

namespace Chemiekast\Authenticator\Admin;

if (!isset($relative_root)) {
    $relative_root = '';
}

use Chemiekast\Authenticator\Authenticator;

require_once $relative_root . 'chemiekast/authenticator.php';

/**
 * Determines whether the current user can edit domains.
 * @return boolean
 */
function can_edit_domains() {
    return Authenticator::is_user_admin();
}

/**
 * Determines whether the current user can edit all users.
 * @return boolean
 */
function can_edit_all_users() {
    return Authenticator::is_user_admin();
}

/**
 * Determines whether the current user can edit users in its own domain.
 * @return boolean
 */
function can_edit_domain_users() {
    return Authenticator::is_user_admin() || Authenticator::is_user_manager();
}

/**
 * Determines whether the current user can edit all notices.
 * @return boolean
 */
function can_edit_all_notices() {
    return Authenticator::is_user_admin();
}

/**
 * Determines whether the current user can edit notices in its own domain.
 * @return boolean
 */
function can_edit_domain_notices() {
    return Authenticator::is_user_admin() || Authenticator::is_user_manager();
}

/**
 * Determines whether the current user can edit all roles.
 * @return boolean
 */
function can_edit_all_roles() {
    return Authenticator::is_user_admin();
}

/**
 * Determines whether the current user can edit roles in its own domain.
 * @return type bool
 */
function can_edit_domain_roles() {
    return Authenticator::is_user_admin() || Authenticator::is_user_manager();
}

/**
 * Determines whether the current user can edit all worksheets.
 * @return boolean
 */
function can_edit_all_worksheets() {
    return Authenticator::is_user_admin();
}

/**
 * Determines whether the current user can edit worksheets in its own domain.
 * @return boolean
 */
function can_edit_domain_worksheets() {
    return Authenticator::is_user_admin() || Authenticator::is_user_manager();
}

/**
 * Determines whether the current user can edit a user.
 * @param int $user_id The ID of the user to check for.
 * @return boolean
 */
function can_edit_user($user_id) {
    if (can_edit_all_users()) {
        return true;
    }

    if (can_edit_domain_users()) {
        $active_user = \Chemiekast\Session\session_get()->User;

        $dbh = \Chemiekast\Config::get_PDO();

        $stmt_get_user_info = $dbh->prepare('Select `Role`, `Domain` From `users` Where `ID` = :ID');
        $stmt_get_user_info->bindValue(':ID', $user_id, \PDO::PARAM_INT);
        $stmt_get_user_info->execute();
        $user_info = $stmt_get_user_info->fetch();

        return $active_user->Domain == $user_info['Domain'] && $active_user->Role <= $user_info['Role'];
    }

    return false;
}

/**
 * Determines whether the current user can edit a notice.
 * @param int $notice_id The ID of the notice to check for.
 * @return boolean
 */
function can_edit_notice($notice_id) {
    if (can_edit_all_notices()) {
        return true;
    }

    if (can_edit_domain_notices()) {
        $active_user = \Chemiekast\Session\session_get()->User;

        $dbh = \Chemiekast\Config::get_PDO();

        $stmt_get_notice_info = $dbh->prepare('Select `System`, `Domain` From `notices` Where `ID` = :ID');
        $stmt_get_notice_info->bindValue(':ID', $notice_id, \PDO::PARAM_INT);
        $stmt_get_notice_info->execute();
        $notice_info = $stmt_get_notice_info->fetch();

        return $active_user->Domain == $notice_info['Domain'] && !\Chemiekast\Utility\Values::bit_to_bool($notice_info['System']);
        // no need to check for system being 1 and being an admin because it already returned if user is an admin
    }

    return false;
}

/**
 * Determines whether the current user can edit a role.
 * @param int $role_id The ID of the role to check for.
 * @return boolean
 */
function can_edit_role($role_id) {
    if (can_edit_all_roles()) {
        return true;
    }

    if (can_edit_domain_roles()) {
        $active_user = \Chemiekast\Session\session_get()->User;

        $dbh = \Chemiekast\Config::get_PDO();

        $stmt_get_role_info = $dbh->prepare('Select `Domain` From `roles` Where `ID` = :ID');
        $stmt_get_role_info->bindValue(':ID', $role_id, \PDO::PARAM_INT);
        $stmt_get_role_info->execute();
        $role_info = $stmt_get_role_info->fetch();

        return $active_user->Domain == $role_info['Domain'];
    }

    return false;
}

/**
 * Determines whether the current user may assign the given role to another user.
 * @param type $role_id The ID of the role to check for.
 * @return boolean
 */
function can_use_role($role_id) {

    $active_user = \Chemiekast\Session\session_get()->User;

    if ($role_id <= CHEMIEKAST_AUTHENTICATOR_HIGHEST_DEFAULT_ROLE && $active_user->Role <= $role_id) {

        return true;
    }

    $dbh = \Chemiekast\Config::get_PDO();

    $stmt_get_role_info = $dbh->prepare('Select `Domain` From `roles` Where `ID` = :ID');
    $stmt_get_role_info->bindValue(':ID', $role_id, \PDO::PARAM_INT);
    $stmt_get_role_info->execute();
    $role_info = $stmt_get_role_info->fetch();

    return $active_user->Domain == $role_info['Domain'];
}

function can_edit_worksheet($worksheet_id) {
    if (can_edit_all_worksheets()) {
        return true;
    }

    if (can_edit_domain_worksheets()) {
        $active_user = \Chemiekast\Session\session_get()->User;

        $dbh = \Chemiekast\Config::get_PDO();

        $stmt_get_worksheet_info = $dbh->prepare('Select `Domain` From `worksheets` Where `ID` = :ID');
        $stmt_get_worksheet_info->bindValue(':ID', $worksheet_id, \PDO::PARAM_INT);
        $stmt_get_worksheet_info->execute();
        $worksheet_info = $stmt_get_worksheet_info->fetch();

        return $active_user->Domain == $worksheet_info['Domain'];
    }

    return false;
}
