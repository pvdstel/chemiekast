<?php

namespace Chemiekast\Session;

use Chemiekast\Authenticator\Authenticator;
use Underscore\Types\Arrays;

define('CHEMIEKAST_SESSION_CURRENT_SESSION_KEY', 'ChemiekastSession');

/**
 * Describes the user of a session.
 */
class User {

    /**
     * The user's ID.
     * @var integer
     */
    public $ID;

    /**
     * The user's username.
     * @var string
     */
    public $Username;

    /**
     * The user's domain.
     * @var integer
     */
    public $Domain;

    /**
     * The user's role.
     * @var integer
     */
    public $Role;

    /**
     * The user's email.
     * @var string
     */
    public $Email;

    /**
     * The user's first name.
     * @var string
     */
    public $FirstName;

    /**
     * The user's last name.
     * @var string
     */
    public $LastName;

    /**
     * Initializes a new instance of the User class.
     * @param int $id The user's ID.
     * @param string $username The user's username.
     * @param int $domain The user's domain.
     * @param int $role The user's role.
     * @param string $email The user's email.
     * @param string $firstName The user's first name.
     * @param string $lastName The user's last name.
     */
    function __construct($id, $username, $domain, $role, $email, $firstName, $lastName) {
        $this->ID = $id;
        $this->Username = $username;
        $this->Domain = $domain;
        $this->Role = $role;
        $this->Email = $email;
        $this->FirstName = $firstName;
        $this->LastName = $lastName;
    }

    /**
     * Gets the user's full name.
     * @return string
     */
    public function FullName() {
        return $this->FirstName . ' ' . $this->LastName;
    }

}

/**
 * Describes a session.
 */
class Session {

    /**
     * The user belonging to this session.
     * @var \Chemiekast\Session\User
     */
    public $User;

    /**
     * The user's access allowances within their role.
     * @var array
     */
    public $UserAccess;

    /**
     * The last request time.
     * @var int
     */
    public $LastRequest;

    /**
     * The session start timestamp.
     * @var int
     */
    public $Started;

    /**
     * Initializes a new instance of the Session class.
     * @param \Chemiekast\Session\User $user The session user.
     * @param array $userAccess The user's access for this session.
     */
    public function __construct($user, $userAccess) {
        $this->User = $user;
        $this->UserAccess = $userAccess;
        $this->LastRequest = \time();
        $this->Started = \time();
    }

}

/**
 * Sets the current session.
 * @param \Chemiekast\Session\Session $session The new session.
 */
function session_set($session) {
    if (\session_status() !== \PHP_SESSION_ACTIVE) {
        \session_start();
    }

    $_SESSION[CHEMIEKAST_SESSION_CURRENT_SESSION_KEY] = $session;
}

/**
 * Gets the current session.
 * @return \Chemiekast\Session\Session The current session.
 */
function session_get() {
    if (\session_status() !== PHP_SESSION_ACTIVE) {
        \session_start();
    }

    if (isset($_SESSION[CHEMIEKAST_SESSION_CURRENT_SESSION_KEY])) {
        return $_SESSION[CHEMIEKAST_SESSION_CURRENT_SESSION_KEY];
    }

    return null;
}

/**
 * Destroys the PHP session.
 */
function session_finalize() {
    // These are calls to the PHP builtins.
    \session_unset();
    \session_destroy();
}

/**
 * Builds a user session object.
 * @param \Chemiekast\Session\Session $session
 */
function create_user_session_object($session) {
    if (!class_exists('\Chemiekast\Chemicals\ChemicalManagement')) {
        throw new \Exception('The Chemical Management class is not defined.');
    }
    if (!function_exists('\Chemiekast\Authenticator\Chemicals\can_view_worksheet_contents')) {
        throw new \Exception('The Chemical Authenticator functions are not defined.');
    }

    $canAccessGlobalSettings = Authenticator::is_user_admin();
    $canAccessDomainSettings = Authenticator::is_user_admin() || Authenticator::is_user_manager();

    $currentDomain = Arrays::find(Authenticator::get_domains(), function($value) use ($session) {
                return $value['ID'] === $session->User->Domain;
            });

    $user_session = [
        'Authenticated' => true,
        'User' => [
            'Username' => $session->User->Username,
            'FirstName' => $session->User->FirstName,
            'LastName' => $session->User->LastName,
            'Email' => $session->User->Email
        ],
        'HasDefaultRole' => Authenticator::user_has_default_role(),
        'CanAccessGlobalSettings' => $canAccessGlobalSettings,
        'CanAccessDomainSettings' => $canAccessDomainSettings,
        'Worksheets' => [],
        'Columns' => \Chemiekast\Chemicals\ChemicalManagement::serialize_columns(\Chemiekast\Chemicals\ChemicalManagement::get_visible_columns()),
        'DomainName' => $currentDomain['Name'],
    ];

    $worksheets = \Chemiekast\Chemicals\ChemicalManagement::get_worksheets();
    foreach ($worksheets as $worksheet) {
        if (\Chemiekast\Authenticator\Chemicals\can_view_worksheet_contents($worksheet->ID)) {
            $user_session['Worksheets'][] = [
                'Code' => $worksheet->Code,
                'Name' => $worksheet->Name,
                'CanEdit' => \Chemiekast\Authenticator\Chemicals\can_edit_worksheet_contents($worksheet->ID),
            ];
        }
    }

    if ($canAccessGlobalSettings) {
        $user_session['CurrentDomain'] = $session->User->Domain;
        $user_session['Domains'] = Authenticator::get_domains();
    }

    if ($canAccessDomainSettings) {
        $user_session['Roles'] = Authenticator::get_roles();
        $user_session['CustomRoles'] = Authenticator::get_custom_roles();
        $user_session['CustomRolesEnabled'] = \Chemiekast\Config::get_config(\Chemiekast\Config::SETTING_SYSTEM_CUSTOM_ROLES_ENABLED);
    }

    return $user_session;
}
