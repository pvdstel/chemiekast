<?php

namespace Chemiekast\Authenticator;

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/config.php';
require_once $relative_root . 'chemiekast/session.php';

define('CHEMIEKAST_AUTHENTICATOR_ROLE_DEV', 0);
define('CHEMIEKAST_AUTHENTICATOR_ROLE_ADMIN', 1);
define('CHEMIEKAST_AUTHENTICATOR_ROLE_MANAGER', 2);
define('CHEMIEKAST_AUTHENTICATOR_ROLE_EDITOR', 3);
define('CHEMIEKAST_AUTHENTICATOR_ROLE_RESTRICTED_EDITOR', 4);
define('CHEMIEKAST_AUTHENTICATOR_ROLE_VIEWER', 5);
define('CHEMIEKAST_AUTHENTICATOR_ROLE_RESTRICTED_VIEWER', 6);

define('CHEMIEKAST_AUTHENTICATOR_HIGHEST_DEFAULT_ROLE', 6);

/**
 * Provides several methods of authentication for the currently active user.
 */
class Authenticator {

    /**
     * The domains found for this user (if administrator).
     * @var Array
     */
    private static $domains = null;

    /**
     * The roles in this domain.
     * @var Array
     */
    private static $roles = null;

    /**
     * The custom roles in this domain.
     * @var Array
     */
    private static $custom_roles = null;

    /**
     * Determines whether the session inactivity update is allowed.
     * @var type boolean
     */
    private static $session_inactivity_update_allowed = true;

    /**
     * Ensures the domains member is non-empty.
     */
    private static function ensure_domains() {
        if (self::$domains === null) {
            self::$domains = [];

            $dbh = \Chemiekast\Config::get_PDO();
            $authenticator_get_all_domains = $dbh->prepare('Select `ID`, `Name` From `domains`;');
            $authenticator_get_all_domains->execute();

            while ($domain = $authenticator_get_all_domains->fetch()) {
                \array_push(self::$domains, ['ID' => (int) $domain['ID'], 'Name' => $domain['Name']]);
            }
        }
    }

    /**
     * Ensures the role members is non-empty.
     */
    private static function ensure_roles() {

        if (self::$roles === null
                || self::$custom_roles === null) {

            self::$roles = self::default_roles();

            self::$custom_roles = [];

            if (\Chemiekast\Config::get_config(\Chemiekast\Config::SETTING_SYSTEM_CUSTOM_ROLES_ENABLED)) {

                $dbh = \Chemiekast\Config::get_PDO();
                $session = \Chemiekast\Session\session_get();

                $custom_roles_stmt = $dbh->prepare('Select `ID`, `Name` From `roles` Where `Domain` = :Domain');
                $custom_roles_stmt->bindValue(':Domain', $session->User->Domain, \PDO::PARAM_INT);
                $custom_roles_stmt->execute();

                while ($custom_db_role = $custom_roles_stmt->fetch()) {
                    if (!array_key_exists($custom_db_role['ID'], self::$roles)) {
                        $custom_role = [
                            'ID' => (int) $custom_db_role['ID'],
                            'Name' => $custom_db_role['Name'],
                        ];
                        self::$roles[] = $custom_role;
                        self::$custom_roles[] = $custom_role;
                    }
                }
            }
        }
    }

    public static function default_roles() {
        return [
            ['ID' => 1, 'Name' => 'Administrator'],
            ['ID' => 2, 'Name' => 'Manager'],
            ['ID' => 3, 'Name' => 'Editor'],
            ['ID' => 4, 'Name' => 'Restricted Editor'],
            ['ID' => 5, 'Name' => 'Viewer'],
            ['ID' => 6, 'Name' => 'Restricted Viewer']
        ];
    }

    /**
     * Gets whether the user's domain exists.
     * @return boolean
     */
    public static function domain_exists() {
        $domain = \Chemiekast\Session\session_get()->User->Domain;
        return array_key_exists($domain, self::get_domains());
    }

    /**
     * Gets whether the user is a developer.
     * @return boolean
     */
    public static function is_user_dev() {
        return \Chemiekast\Session\session_get()->User->Role === CHEMIEKAST_AUTHENTICATOR_ROLE_DEV;
    }

    /**
     * Gets whether the user is an administrator.
     * @return boolean
     */
    public static function is_user_admin() {
        return \Chemiekast\Session\session_get()->User->Role === CHEMIEKAST_AUTHENTICATOR_ROLE_ADMIN
                || \Chemiekast\Session\session_get()->User->Role === CHEMIEKAST_AUTHENTICATOR_ROLE_DEV;
    }

    /**
     * Gets whether the user is a manager.
     * @return boolean
     */
    public static function is_user_manager() {
        return \Chemiekast\Session\session_get()->User->Role === CHEMIEKAST_AUTHENTICATOR_ROLE_MANAGER;
    }

    /**
     * Gets whether the user is an editor.
     * @return boolean
     */
    public static function is_user_editor() {
        return \Chemiekast\Session\session_get()->User->Role === CHEMIEKAST_AUTHENTICATOR_ROLE_EDITOR;
    }

    /**
     * Gets whether the user is a restricted editor.
     * @return boolean
     */
    public static function is_user_restricted_editor() {
        return \Chemiekast\Session\session_get()->User->Role === CHEMIEKAST_AUTHENTICATOR_ROLE_RESTRICTED_EDITOR;
    }

    /**
     * Gets whether the user is a viewer.
     * @return boolean
     */
    public static function is_user_viewer() {
        return \Chemiekast\Session\session_get()->User->Role === CHEMIEKAST_AUTHENTICATOR_ROLE_VIEWER;
    }

    /**
     * Gets whether the user is a restricted viewer.
     * @return boolean
     */
    public static function is_user_restricted_viewer() {
        return \Chemiekast\Session\session_get()->User->Role === CHEMIEKAST_AUTHENTICATOR_ROLE_RESTRICTED_VIEWER;
    }

    /**
     * Gets whether the user has a default role.
     * @return boolean
     */
    public static function user_has_default_role() {
        $role = \Chemiekast\Session\session_get()->User->Role;
        return $role >= 0
                && $role <= CHEMIEKAST_AUTHENTICATOR_HIGHEST_DEFAULT_ROLE;
    }

    /**
     * Gets whether the user can edit roles.
     * @return type boolean
     */
    public static function user_can_edit_roles() {
        $role = \Chemiekast\Session\session_get()->User->Role;
        return $role === CHEMIEKAST_AUTHENTICATOR_ROLE_ADMIN
                || $role === CHEMIEKAST_AUTHENTICATOR_ROLE_MANAGER;
    }

    /**
     * Determines whether a user is authenticated and if so, whether this user
     * has the requested privileges.
     * @param string $auth_requirement The minimum privilege the user should have.
     * @return boolean
     */
    public static function has_privilege($privileges = null) {

        $session = \Chemiekast\Session\session_get();
        if (empty($session)) {
            return false;
        }

        if ($privileges === null) {
            return true;
        }

        $privileges_normalized = strtolower($privileges);
        switch ($privileges_normalized) {
            case 'dev':
                return $session->User->Role <= CHEMIEKAST_AUTHENTICATOR_ROLE_DEV;
            case 'admin':
                return $session->User->Role <= CHEMIEKAST_AUTHENTICATOR_ROLE_ADMIN;
            case 'manager':
                return $session->User->Role <= CHEMIEKAST_AUTHENTICATOR_ROLE_MANAGER;
            case 'editor':
                return $session->User->Role <= CHEMIEKAST_AUTHENTICATOR_ROLE_EDITOR;
            case 'restrictededitor':
                return $session->User->Role <= CHEMIEKAST_AUTHENTICATOR_ROLE_RESTRICTED_EDITOR;
            case 'viewer':
                return $session->User->Role <= CHEMIEKAST_AUTHENTICATOR_ROLE_VIEWER;
            case 'restrictedview':
                return $session->User->Role <= CHEMIEKAST_AUTHENTICATOR_ROLE_RESTRICTED_VIEWER;
            default:
                return false;
        }
    }

    /**
     * Determines whether the current session has been inactive for too long.
     * If it has been, the session will be destroyed.
     * @return boolean
     */
    public static function is_session_inactive() {
        $session = \Chemiekast\Session\session_get();

        if (empty($session)) {
            return false;
        }

        $inactivity_timeout = \Chemiekast\Config::get_config(\Chemiekast\Config::SETTING_SECURITY_USER_INACTIVITY_TIMEOUT);
        $time_since_last = time()
                - $session->LastRequest;

        if ($inactivity_timeout
                > 0
                && $time_since_last
                > $inactivity_timeout) {
            \Chemiekast\Session\session_finalize();
            return true;
        }

        if (self::$session_inactivity_update_allowed) {
            $session->LastRequest = time();
        }
        return false;
    }
    
    /**
     * Suppresses the inactivity timer update.
     */
    public static function suppress_inactivity_update() {
        self::$session_inactivity_update_allowed = false;
    }

    /**
     * Determines whether the current session has expired.
     * If it has, the session will be destroyed.
     * @return boolean
     */
    public static function is_session_expired() {
        $session = \Chemiekast\Session\session_get();

        if (empty($session)) {
            return false;
        }

        $session_timeout = \Chemiekast\Config::get_config(\Chemiekast\Config::SETTING_SECURITY_USER_SESSION_TIMEOUT);
        $time_since_start = time()
                - $session->Started;

        if ($session_timeout
                > 0
                && $time_since_start
                > $session_timeout) {
            \Chemiekast\Session\session_finalize();
            return true;
        }

        return false;
    }

    /**
     * Gets all domains.
     * @return array
     */
    public static function get_domains() {
        self::ensure_domains();
        return self::$domains;
    }

    /**
     * Gets all roles in the domain of the current user.
     * @return array
     */
    public static function get_roles() {
        self::ensure_roles();
        return self::$roles;
    }

    /**
     * Gets all custom roles in the domain of the current user.
     * @return array
     */
    public static function get_custom_roles() {
        self::ensure_roles();
        return self::$custom_roles;
    }

    public static function get_check_fails($required_privileges = null) {
        if (self::is_session_expired()) {
            return 'authenticator.expired';
        }

        if (self::is_session_inactive()) {
            return 'authenticator.inactive';
        }

        if (!self::has_privilege($required_privileges)) {
            return 'authenticator.denied';
        }

        return null;
    }

    /**
     * Hashes the given password.
     * @param type $password The password to hash.
     * @return type The password hash.
     */
    public static function hash_password($password) {
        $hash_options = [
            'cost' => \Chemiekast\Config::get_config(\Chemiekast\Config::SETTING_SECURITY_PASSWORD_HASH_COST),
        ];
        return password_hash($password, PASSWORD_DEFAULT, $hash_options);
    }

    /**
     * Re-initializes Authenticator.
     */
    public static function reinitialize() {
        self::$custom_roles = null;
        self::$domains = null;
        self::$roles = null;
    }

}
