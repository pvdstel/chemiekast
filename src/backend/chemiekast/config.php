<?php

namespace Chemiekast {

    if (!isset($relative_root)) {
        $relative_root = '../';
    }

    require_once $relative_root . 'chemiekast/utility.php';

    define('CHEMIEKAST_NAME', 'Chemiekast');
    define('CHEMIEKAST_IS_CONNECTION_SECURE', !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off');
    define('DATABASE_DATETIME_FORMAT', 'Y-m-d H:i:s');

    class Config {

        private static $_DATABASE_OPTIONS = [\PDO::MYSQL_ATTR_INIT_COMMAND => 'Set Names \'utf8\''];

        const CONFIG_CACHE_TIMEOUT = 300;
        const SETTING_SYSTEM_LOGGING_ENABLED = 'system-logging-enabled';
        const SETTING_SYSTEM_CUSTOM_ROLES_ENABLED = 'system-custom-roles-enabled';
        const SETTING_SECURITY_USER_SESSION_TIMEOUT = 'security-user-session-timeout';
        const SETTING_SECURITY_USER_INACTIVITY_TIMEOUT = 'security-user-inactivity-timeout';
        const SETTING_SECURITY_PASSWORD_RESET_ENABLED = 'security-password-reset-enabled';
        const SETTING_SECURITY_PASSWORD_HASH_COST = 'security-password-hash-cost';
        const SETTING_UI_SPLIT_RESTRICTED_WORKSHEET = 'ui-split-restricted-worksheet';
        const SETTING_MAIL_SMTP = 'mail-smtp-host';
        const SETTING_MAIL_PORT = 'mail-port';
        const SETTING_MAIL_USERNAME = 'mail-username';
        const SETTING_MAIL_PASSWORD = 'mail-password';

        static private $_default_values = [
            self::SETTING_SYSTEM_LOGGING_ENABLED => true,
            self::SETTING_SYSTEM_CUSTOM_ROLES_ENABLED => true,
            self::SETTING_SECURITY_USER_SESSION_TIMEOUT => 0,
            self::SETTING_SECURITY_USER_INACTIVITY_TIMEOUT => 0,
            self::SETTING_SECURITY_PASSWORD_RESET_ENABLED => false,
            self::SETTING_SECURITY_PASSWORD_HASH_COST => 14,
            self::SETTING_UI_SPLIT_RESTRICTED_WORKSHEET => false,
            self::SETTING_MAIL_SMTP => '',
            self::SETTING_MAIL_PORT => 25,
            self::SETTING_MAIL_USERNAME => '',
            self::SETTING_MAIL_PASSWORD => '',
        ];
        static private $_settings = null;
        static private $env_config;

        /**
         * Initializes Config
         * @return boolean Whether the initialization succeeded.
         */
        public static function Initialize() {
            $env_config_file = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT') . '/../chemiekast.ini';
            $env_config_file = realpath($env_config_file);

            if (file_exists($env_config_file)) {
                self::$env_config = parse_ini_file($env_config_file);
            } else {
                $db_connection = getenv('DB_CONNECTION_STRING');
                $db_user = getenv('DB_USER');
                $db_password = getenv('DB_PASSWORD');
                self::$env_config = [
                    'DB_CONNECTION_STRING' => $db_connection,
                    'DB_USER' => $db_user,
                    'DB_PASSWORD' => $db_password
                ];
            }

            if (empty(self::$env_config['DB_CONNECTION_STRING'])
                    || empty(self::$env_config['DB_USER'])
                    || empty(self::$env_config['DB_PASSWORD'])) {
                return false;
            }

            if (!isset(self::$env_config['ALLOWED_ORIGINS'])) {
                self::$env_config['ALLOWED_ORIGINS'] = '';
            }

            return true;
        }

        /**
         * Gets a new PDO instance.
         * @return \PDO The PDO.
         */
        public static function get_PDO() {
            return new \PDO(
                    self::$env_config['DB_CONNECTION_STRING'], self::$env_config['DB_USER'], self::$env_config['DB_PASSWORD'], self::$_DATABASE_OPTIONS);
        }

        /**
         * Gets all the settings available.
         * @return type The available settings.
         */
        static public function get_settings() {
            if (self::$_settings === null) {
                self::$_settings = array_keys(self::$_default_values);
            }
            return self::$_settings;
        }

        /**
         * Gets a setting from the database.
         * @param type $setting The setting to fetch.
         * @returns
         */
        static private function get_database_setting($setting) {
            if (!isset($setting) || empty($setting)) {
                return null;
            }

            $dbh = self::get_PDO();
            $stmt_get_setting = $dbh->prepare('Select `Value` From `settings` Where `Setting` = :Setting;');
            $stmt_get_setting->bindValue(':Setting', $setting, \PDO::PARAM_STR);
            $stmt_get_setting->execute();

            if ($stmt_get_setting->rowCount()) {
                $db_value = $stmt_get_setting->fetchColumn();
                return Utility\Values::typify_value($db_value);
            }

            return null;
        }

        /**
         * Writes a setting to the database.
         * @param type $setting The setting to set.
         */
        static private function set_database_setting($setting, $value) {
            if (!isset($setting) || empty($setting)) {
                return;
            }

            $dbh = self::get_PDO();
            $stmt_get_setting = $dbh->prepare('Select Count(*) From `settings` Where `Setting` = :Setting;');
            $stmt_get_setting->bindValue(':Setting', $setting, \PDO::PARAM_STR);
            $stmt_get_setting->execute();

            if ($stmt_get_setting->fetchColumn() > 0) {
                $stmt_set_setting = $dbh->prepare('Update `settings` Set `Value` = :Value Where `Setting` = :Setting;');
                $stmt_set_setting->bindValue(':Setting', $setting, \PDO::PARAM_STR);
                $stmt_set_setting->bindValue(':Value', Utility\Values::untypify_value($value));
                $stmt_set_setting->execute();
            } else {
                $stmt_set_setting = $dbh->prepare('Insert Into `settings` Values (:Setting, :Value);');
                $stmt_set_setting->bindValue(':Setting', $setting, \PDO::PARAM_STR);
                $stmt_set_setting->bindValue(':Value', Utility\Values::untypify_value($value));
                $stmt_set_setting->execute();
            }

            return;
        }

        /**
         * Gets a setting.
         * @param type $setting The setting to get.
         * @return
         */
        static public function get_config($setting) {
            $result = null;

            if (session_status() != PHP_SESSION_ACTIVE) {
                session_start();
            }

            if (!isset($_SESSION['Config-Cache']) || !is_array($_SESSION['Config-Cache'])) {
                $_SESSION['Config-Cache'] = [];
            }

            if (array_key_exists($setting, $_SESSION['Config-Cache']) && time() - $_SESSION['Config-Cache'][$setting]['Time'] < self::CONFIG_CACHE_TIMEOUT) {

                $result = $_SESSION['Config-Cache'][$setting]['Value'];
                return $result;
            } else {

                $result = self::get_database_setting($setting);
            }

            if ($result === null && array_key_exists($setting, self::$_default_values)) {

                $result = self::$_default_values[$setting];
            }

            if ($result !== null) { // Cache the setting for about five minutes
                $_SESSION['Config-Cache'][$setting] = ['Value' => $result, 'Time' => time()];
            }

            return $result;
        }

        /**
         * Sets the value of a setting.
         * @param type $setting The setting to set.
         * @param type $value The value of the setting.
         */
        static public function set_config($setting, $value) {
            if (session_status() != PHP_SESSION_ACTIVE) {
                session_start();
            }

            if (!isset($_SESSION['Config-Cache']) || !is_array($_SESSION['Config-Cache'])) {
                $_SESSION['Config-Cache'] = [];
            }

            $_SESSION['Config-Cache'][$setting] = ['Value' => $value, 'Time' => time()];

            self::set_database_setting($setting, $value);
        }

        /**
         * Resets the configuration.
         */
        static public function reset_config() {
            if (session_status() != PHP_SESSION_ACTIVE) {
                session_start();
            }

            $_SESSION['Config-Cache'] = [];

            $dbh = self::get_PDO();
            $stmt_delete_settings = $dbh->prepare('Delete From `settings`;');
            $stmt_delete_settings->execute();
        }

        /**
         * Gets an environment config setting.
         * @param string $key The environment config key.
         * @return string The requested environment config value.
         */
        static public function get_env_config($key) {
            return self::$env_config[$key];
        }
    }

    if (!Config::Initialize()) {
        exit(CHEMIEKAST_NAME . ' could not be initialized.');
    }
    
    session_set_cookie_params([
        'samesite' => 'Strict',
    ]);

    // Handle CORS; check origins
    if (isset($_SERVER['HTTP_ORIGIN']) && strpos(Config::get_env_config('ALLOWED_ORIGINS'), $_SERVER['HTTP_ORIGIN']) !== false) {
        header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }

    // Handle CORS for OPTIONS
    if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
        header('Access-Control-Allow-Origin: ' . Config::get_env_config('ALLOWED_ORIGINS'));
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
        }
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        }
        exit(0);
    }
}
