<?php

namespace Chemiekast\Authenticator\Chemicals;

if (!isset($relative_root)) {
    $relative_root = '';
}

use Chemiekast\Authenticator\Authenticator;
use Underscore\Types\Arrays;

require_once $relative_root . 'chemiekast/authenticator.php';
require_once $relative_root . 'vendor/autoload.php';

/**
 * Determines whether the current user can see all chemicals.
 * @return boolean
 */
function can_see_all_chemicals() {
    return Authenticator::is_user_admin();
}

/**
 * Determines whether the current user can see all domain chemicals.
 * @return boolean
 */
function can_see_all_domain_chemicals() {
    return Authenticator::is_user_admin()
            || Authenticator::is_user_manager()
            || Authenticator::is_user_editor()
            || Authenticator::is_user_viewer();
}

/**
 * Determines whether the current user can edit all chemicals.
 * @return boolean
 */
function can_edit_all_chemicals() {
    return Authenticator::is_user_admin();
}

/**
 * Determines whether the current user can edit all domain chemicals.
 * @return boolean
 */
function can_edit_all_domain_chemicals() {
    return Authenticator::is_user_admin()
            || Authenticator::is_user_manager();
}

/**
 * Determines whether the current user can edit some chemicals.
 * @return boolean
 */
function can_edit_some_chemicals() {
    return Authenticator::is_user_admin()
            || Authenticator::is_user_manager()
            || Authenticator::is_user_editor()
            || Authenticator::is_user_restricted_editor();
}

/**
 * Determines whether the current user can modify roles.
 * @return boolean
 */
function can_edit_roles() {
    return Authenticator::is_user_admin()
            || Authenticator::is_user_manager();
}

/**
 * Determines whether a user can view the specified worksheet.
 * @param integer $worksheet_id The ID of the worksheet.
 * @return boolean
 */
function can_view_worksheet_contents($worksheet_id) {

    if (can_see_all_chemicals()) { // This user can see all
        return true;
    }

    $session = \Chemiekast\Session\session_get();

    if (can_see_all_domain_chemicals()) { // Determine if the worksheet is in the user's domain
        $dbh = \Chemiekast\Config::get_PDO();
        $stmt_get_worksheet_info = $dbh->prepare('Select `Domain` From `worksheets` Where `ID` = :ID;');
        $stmt_get_worksheet_info->bindValue(':ID', $worksheet_id, \PDO::PARAM_INT);
        $stmt_get_worksheet_info->execute();
        return $stmt_get_worksheet_info->fetchColumn() == $session->User->Domain;
    }

    // If there are items in the User Access list, check if any of those values
    // is for the current worksheet. If they can edit, they can see it as well.
    return count($session->UserAccess) > 0 && Arrays::matchesAny($session->UserAccess, function($value) use ($worksheet_id) {
                return $value['Worksheet'] == $worksheet_id;
            });
}

/**
 * Determines whether the current user can view the specified worksheet.
 * @param integer $worksheet_id The ID of the worksheet.
 * @return boolean
 */
function can_edit_worksheet_contents($worksheet_id) {
    if (can_edit_all_chemicals()) {
        return true;
    }

    $session = \Chemiekast\Session\session_get();

    if (can_edit_all_domain_chemicals()) {
        $dbh = \Chemiekast\Config::get_PDO();
        $stmt_get_worksheet_info = $dbh->prepare('Select `Domain` From `worksheets` Where `ID` = :ID;');
        $stmt_get_worksheet_info->bindValue(':ID', $worksheet_id, \PDO::PARAM_INT);
        $stmt_get_worksheet_info->execute();
        return $stmt_get_worksheet_info->fetchColumn() == $session->User->Domain;
    }

    if (can_edit_some_chemicals()) {
        // If there are items in the User Access list, check if any of those values
        // is for the current worksheet. Only allow for editing.
        return count($session->UserAccess) > 0 && Arrays::matchesAny($session->UserAccess, function($value) use ($worksheet_id) {
                    return $value['Worksheet'] == $worksheet_id && $value['Editing'];
                });
    }

    return false;
}

/**
 * Determines whether the current user can see the specified chemical.
 * @param integer $chemical_id The chemical ID.
 * @return boolean
 */
function can_view_chemical($chemical_id) {
    if (can_see_all_chemicals()) {
        return true;
    }

    $dbh = \Chemiekast\Config::get_PDO();
    if (Authenticator::user_has_default_role()) {
        $stmt_get_worksheet = $dbh->prepare('Select `' . CHEMICALS_WORKSHEET_COLUMN . '` From `chemicals` Where `' . CHEMICALS_ID_COLUMN . '` = :' . CHEMICALS_ID_COLUMN);
        $stmt_get_worksheet->bindValue(':' . CHEMICALS_ID_COLUMN, $chemical_id, \PDO::PARAM_INT);
        $stmt_get_worksheet->execute();
        $db_worksheet = $stmt_get_worksheet->fetchColumn();

        return can_view_worksheet_contents($db_worksheet);
    } else {
        $stmt_in_user_role = $dbh->prepare('Select Count(*) From `rolechemical` Where Chemical = :Chemical And Role = :Role;');
        $stmt_in_user_role->bindValue(':Chemical', $chemical_id, \PDO::PARAM_INT);
        $stmt_in_user_role->bindValue(':Role', \Chemiekast\Session\session_get()->User->Role, \PDO::PARAM_INT);
        $stmt_in_user_role->execute();
        $db_viewable = $stmt_in_user_role->fetchColumn() > 0;

        return $db_viewable;
    }
}

/**
 * Determines whether the current user can edit the specified chemical.
 * @param integer $chemical_id The chemical ID
 * @return boolean
 */
function can_edit_chemical($chemical_id) {
    if (can_edit_all_chemicals()) {
        return true;
    }

    if (!can_edit_some_chemicals()) {
        return false;
    }

    $dbh = \Chemiekast\Config::get_PDO();
    $stmt_get_worksheet = $dbh->prepare('Select `' . CHEMICALS_WORKSHEET_COLUMN . '` From `chemicals` Where `' . CHEMICALS_ID_COLUMN . '` = :' . CHEMICALS_ID_COLUMN);
    $stmt_get_worksheet->bindValue(':' . CHEMICALS_ID_COLUMN, $chemical_id, \PDO::PARAM_INT);
    $stmt_get_worksheet->execute();
    $db_worksheet = $stmt_get_worksheet->fetchColumn();

    return can_edit_worksheet_contents($db_worksheet);
}

/**
 * Determines whether a user can modify the chemical such that it is moved to
 * the specified worksheet.
 * @param type $chemical_id The chemical ID.
 * @param type $new_worksheet The new worksheet of the chemical.
 * @return boolean
 */
function can_edit_chemical_transaction($chemical_id, $new_worksheet) {
    if (can_edit_all_chemicals()) {
        return true;
    }

    if (!can_edit_some_chemicals()) {
        return false;
    }

    if (empty($new_worksheet)) {
        return false;
    }

    // The user must be able to edit the current chemical, as well
    // as the worksheet the chemical is moved to.
    return can_edit_chemical($chemical_id)
            && can_edit_worksheet_contents($new_worksheet);
}
