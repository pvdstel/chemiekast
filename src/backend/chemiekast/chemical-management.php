<?php

namespace Chemiekast\Chemicals;

if (!isset($relative_root)) {
    $relative_root = '';
}

require_once $relative_root . 'vendor/autoload.php';

use \Underscore\Types\Arrays;

define('CHEMICALS_ID_COLUMN', 'ID');
define('CHEMICALS_NAME_COLUMN', 'Name');
define('CHEMICALS_WORKSHEET_COLUMN', 'Worksheet');
define('CHEMICALS_GHS_SYMBOLS_COLUMN', 'GhsSymbols');
define('CHEMICALS_GHS_LINKS_COLUMN', 'GhsLinks');

/**
 * Describes a worksheet.
 */
class Worksheet {

    /**
     * The ID of the worksheet.
     * @var integer
     */
    public $ID;

    /**
     * The code of the worksheet.
     * @var string
     */
    public $Code;

    /**
     * The name of the worksheet.
     * @var string
     */
    public $Name;

    function __construct($id, $code, $name) {

        $this->ID = $id;
        $this->Code = $code;
        $this->Name = $name;
    }

}

/**
 * Describes a worksheet column.
 */
class WorksheetColumn {

    /**
     * The database name of the column.
     * @var string
     */
    public $DatabaseName;

    /**
     * The UI name of the column.
     * @var string
     */
    public $UIName;

    /**
     * Whether the column is visible.
     * @var boolean
     */
    public $IsVisible;

    /**
     * Whether the column is searchable.
     * @var boolean
     */
    public $IsSearchable;

    /**
     * Whether the column is a number representing GHS symbols.
     * @var type boolean
     */
    public $IsGhsSymbols;

    /**
     * Whether the column is a list of urls.
     * @var boolean
     */
    public $IsUrl;

    /**
     * Whether the column is markdown formatted.
     * @var boolean
     */
    public $IsMarkdown;

    /**
     * Whether the column is formatted as a formula.
     * @var boolean
     */
    public $IsFormula;

    /**
     * Initializes a new instance of the WorksheetColumn class.
     * @param string $databaseName The column name in the database.
     * @param string $uiName The column name in the UI.
     * @param boolean $isVisible Whether the column is visible.
     * @param boolean $isSearchable Whether the column is searchable.
     * @param boolean $isGhsSymbols Whether the column is a number representing GHS symbols.
     * @param boolean $isUrl Whether the column is a list of urls.
     * @param boolean $isMarkdown Whether the column is markdown formatted.
     * @param boolean $isFormula Whether the column is formatted as a formula.
     */
    function __construct($databaseName, $uiName, $isVisible, $isSearchable, $isGhsSymbols, $isUrl, $isMarkdown, $isFormula) {
        $this->DatabaseName = $databaseName;
        $this->UIName = $uiName;
        $this->IsVisible = $isVisible;
        $this->IsSearchable = $isSearchable;
        $this->IsUrl = $isUrl;
        $this->IsGhsSymbols = $isGhsSymbols;
        $this->IsMarkdown = $isMarkdown;
        $this->IsFormula = $isFormula;
    }

}

class ChemicalManagement {

    /**
     * All worksheets in the current domain.
     * @var Array
     */
    private static $worksheets = null;

    /**
     * All default columns in the system, except the Worksheet column.
     * @var Array
     */
    private static $default_columns = null;

    /**
     * All non-default columns in the system.
     * @var Array
     */
    private static $nondefault_columns = null;

    /**
     * All columns in the system, except the Worksheet column.
     * @var Array
     */
    private static $columns = null;

    /**
     * All visible columns in the system.
     * @var Array
     */
    private static $visible_columns = null;

    /**
     * Ensures that the worksheets member is initialized.
     */
    private static function ensure_worksheets() {
        if (self::$worksheets === null) {
            self::$worksheets = [];
            $dbh = \Chemiekast\Config::get_PDO();

            $stmt_get_worksheets = $dbh->prepare('Select `ID`, `Code`, `Name` From `worksheets` Where `Domain` = :Domain Order By `Name`');
            $stmt_get_worksheets->bindValue(':Domain', \Chemiekast\Session\session_get()->User->Domain, \PDO::PARAM_INT);
            $stmt_get_worksheets->execute();

            while ($db_worksheet = $stmt_get_worksheets->fetch()) {
                self::$worksheets[$db_worksheet['ID']] = new Worksheet($db_worksheet['ID'], $db_worksheet['Code'], $db_worksheet['Name']);
            }
        }
    }

    /**
     * Ensures that the columns member is initialized.
     */
    private static function ensure_columns() {
        if (self::$columns === null) {
            self::$default_columns = [//DATABASE COLUMN NAME, [UIName, Visible, Searchable, Symbols, Url, Markdown]
                CHEMICALS_ID_COLUMN => new WorksheetColumn(
                        CHEMICALS_ID_COLUMN,
                        'ID',
                        true,
                        true,
                        false,
                        false,
                        false,
                        false
                ),
                CHEMICALS_NAME_COLUMN => new WorksheetColumn(
                        CHEMICALS_NAME_COLUMN,
                        'Naam',
                        true,
                        true,
                        false,
                        false,
                        false,
                        false
                ),
                CHEMICALS_GHS_SYMBOLS_COLUMN => new WorksheetColumn(
                        CHEMICALS_GHS_SYMBOLS_COLUMN,
                        'GHS-symbolen',
                        true,
                        false,
                        true,
                        false,
                        false,
                        false
                ),
                CHEMICALS_GHS_LINKS_COLUMN => new WorksheetColumn(
                        CHEMICALS_GHS_LINKS_COLUMN,
                        'GHS-links',
                        true,
                        false,
                        false,
                        true,
                        false,
                        false
                ),
            ];

            $dbh = \Chemiekast\Config::get_PDO();

            $stmt_get_worksheet_columns = $dbh->prepare('Select `DatabaseName`, `UIName`, `IsVisible`, `IsSearchable`, `IsUrl`, `IsMarkdown`, `IsFormula` From `worksheetcolumns` Order By `Order`');
            $stmt_get_worksheet_columns->execute();
            while ($nondefault_column = $stmt_get_worksheet_columns->fetch()) {
                self::$nondefault_columns[$nondefault_column['DatabaseName']] = new WorksheetColumn(
                        $nondefault_column['DatabaseName'],
                        $nondefault_column['UIName'],
                        \Chemiekast\Utility\Values::bit_to_bool($nondefault_column['IsVisible']),
                        \Chemiekast\Utility\Values::bit_to_bool($nondefault_column['IsSearchable']),
                        false,
                        \Chemiekast\Utility\Values::bit_to_bool($nondefault_column['IsUrl']),
                        \Chemiekast\Utility\Values::bit_to_bool($nondefault_column['IsMarkdown']),
                        \Chemiekast\Utility\Values::bit_to_bool($nondefault_column['IsFormula'])
                );
            }

            self::$columns = array_merge(self::$default_columns, self::$nondefault_columns);
            self::$visible_columns = Arrays::filter(self::$columns, function($value) {
                        return $value->IsVisible;
                    });
        }
    }

    /**
     * Gets the worksheets of the current domain.
     * @return Array
     */
    public static function get_worksheets() {
        self::ensure_worksheets();
        return self::$worksheets;
    }

    /**
     * Finds the ID of the given worksheet.
     * @param string $worksheet_code The code of the worksheet.
     * @return Worksheet
     */
    public static function worksheet_id_by_code($worksheet_code) {
        self::ensure_worksheets();

        $worksheet_data = Arrays::find(self::$worksheets, function($value) use ($worksheet_code) {
                    return $value->Code == $worksheet_code;
                });

        return empty($worksheet_data) ? null : $worksheet_data->ID;
    }

    /**
     * Checks whether the given worksheet code exists.
     * @param string $worksheet_code The code of the worksheet.
     * @return boolean
     */
    public static function worksheet_with_code_exists($worksheet_code) {
        return self::worksheet_id_by_code($worksheet_code) !== null;
    }

    /**
     * Gets the default columns in the system.
     * @return Array
     */
    public static function get_default_columns() {
        self::ensure_columns();
        return self::$default_columns;
    }

    /**
     * Determines whether the column with the given key is a default column.
     * @param string $column_key The key of the column.
     * @return boolean
     */
    public static function is_default_column($column_key) {
        return $column_key === CHEMICALS_WORKSHEET_COLUMN || array_key_exists($column_key, self::get_default_columns());
    }

    /**
     * Gets the non-default columns in the system.
     * @return Array
     */
    public static function get_nondefault_columns() {
        self::ensure_columns();
        return self::$nondefault_columns;
    }

    /**
     * Gets the columns in the system.
     * @return Array
     */
    public static function get_columns() {
        self::ensure_columns();
        return self::$columns;
    }

    /**
     * Gets the visible columns in the system.
     * @return Array
     */
    public static function get_visible_columns() {
        self::ensure_columns();
        return self::$visible_columns;
    }

    /**
     * Serializes a column to an array.
     * @param string $column_key The key of the column to serialize.
     * @return Array
     */
    public static function serialize_column($column_key) {
        $is_system_column = self::is_default_column($column_key);

        return array(
            'Name' => $column_key,
            'NameHash' => md5($column_key),
            'UIName' => self::$columns[$column_key]->UIName,
            'IsVisible' => self::$columns[$column_key]->IsVisible ? true : false,
            'IsSearchable' => self::$columns[$column_key]->IsSearchable ? true : false,
            'IsUrl' => self::$columns[$column_key]->IsUrl ? true : false,
            'IsMarkdown' => self::$columns[$column_key]->IsMarkdown ? true : false,
            'IsFormula' => self::$columns[$column_key]->IsFormula ? true : false,
            'IsSystemColumn' => $is_system_column ? true : false
        );
    }

    /**
     * Serializes the given column keys to arrays.
     * @param Array $columns An array of column keys.
     * @return Array
     */
    public static function serialize_columns($columns) {
        $return_data = array();
        $column_keys = array_keys($columns);
        foreach ($column_keys as $column_key) {
            $return_data[] = self::serialize_column($column_key);
        }
        return $return_data;
    }

}
