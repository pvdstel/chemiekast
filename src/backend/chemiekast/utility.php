<?php

namespace Chemiekast\Utility {
    
    /**
     * Determines whether a PDOStatement object has had errors.
     * @param PDOStatement $stmt
     * @return boolean Wehther the PDOStatement object has had errors.
     */
    function pdo_statement_has_error($stmt) {
        return $stmt->errorInfo()[1] !== null;
    }

    class Values {

        /**
         * Casts the specified value to the asssumed type.
         * @param type $value The value to cast.
         */
        static public function typify_value($value) {

            if (is_numeric($value)) {

                return intval($value);
            }
            if (strtolower($value) === 'true') {

                return true;
            }
            if (strtolower($value) === 'false') {

                return false;
            }

            return $value;
        }

        /**
         * Casts the specified value to the corresponding string.
         * @param type $value The value to cast.
         */
        static public function untypify_value($value) {

            if ($value === true) {

                return 'true';
            }
            if ($value === false) {

                return 'false';
            }

            return $value;
        }

        /**
         * Propertly converts a value to an integer and masks it with a bit mask.
         * @param type $value The value.
         * @param type $mask The mask to apply to the integer.
         * @param type $cutoff The length of a cut off string.
         * @return type A masked integer.
         */
        static public function normalize_bitfield($value, $mask, $cutoff = null) {

            if (gettype($value) === 'string') {
                if (empty($cutoff)) {
                    $value = $value + 0;
                } else {
                    $value = substr($value, -$cutoff) + 0;
                }
            }

            return $value & $mask;
        }

        /**
         * Converts a bit value to a boolean value.
         * @param type $value The bit value to convert.
         * @return type A boolean.
         */
        static public function bit_to_bool($value) {
            if ($value === "\u{0000}") {
                return false;
            }
            if ($value === "\u{0001}") {
                return true;
            }
            return self::normalize_bitfield($value, 1, 1) === 1;
        }

    }

    class Random {

        /**
         * Generates a pool of characters to choose from.
         * @param string $type The type of characters to add to the pool.
         * @return string The pool of characters.
         */
        private static function get_pool($type) {
            $pool = '';
            if (strpos($type, '0') !== false) {
                $pool .= '0123456789';
            }
            if (strpos($type, 'a') !== false) {
                $pool .= 'abcdefghijklmnopqrstuvwxyz';
            }
            if (strpos($type, 'A') !== false) {
                $pool .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            }
            if (strpos($type, '#') !== false) {
                $pool .= '!@#$%^&*-_+=~`;\'"/\\,.';
            }
            if (strpos($type, '(') !== false || strpos($type, ')') !== false) {
                $pool += '()[]{}<>';
            }
            return $pool;
        }

        /**
         * Gets a cryptographically secure random number.
         * @param integer $min The minimum bound.
         * @param integer $max The maximum bound.
         * @return integer The random number.
         */
        private static function crypto_rand_secure($min, $max) {
            $range = $max - $min;
            if ($range < 0) {
                return $min; // not so random...
            }

            $log = log($range, 2);
            $bytes = (int) ( $log / 8 ) + 1; // length in bytes
            $bits = (int) $log + 1; // length in bits
            $filter = (int) ( 1 << $bits ) - 1; // set all lower bits to 1
            
            do {
                $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
                $rnd = $rnd & $filter; // discard irrelevant bits
            } while ($rnd >= $range);
            
            return $min + $rnd;
        }

        /**
         * Gets a random string.
         * @param integer $length The length of the string.
         * @param string $type The type of the string.
         * @return string The random token.
         */
        public static function random_string($length = 8, $type = '0aA') {
            $pool = self::get_pool($type);

            $token = "";
            $max = strlen($pool);
            for ($i = 0; $i < $length; $i++) {
                $token .= $pool[self::crypto_rand_secure(0, $max)];
            }
            return $token;
        }

    }

}
