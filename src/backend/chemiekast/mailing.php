<?php

if (!isset($relative_root)) {
    $relative_root = '../';
}

require_once $relative_root . 'chemiekast/config.php';
require_once $relative_root . 'vendor/autoload.php';

use PHPMailer\PHPMailer\PHPMailer;

class Mail {

    public $recipient_name;
    public $recipient_address;
    public $subject;
    public $body;
    public $alt_body;

    public function __construct($recipient_name, $recipient_address, $subject, $body, $alt_body) {
        $this->recipient_name = $recipient_name;
        $this->recipient_address = $recipient_address;
        $this->subject = $subject;
        $this->body = $body;
        $this->alt_body = $alt_body;
    }

}

class Mailing {

    /**
     * Initializes a new instance of the PHPMailer class using settings.
     * @return PHPMailer.
     */
    private static function setupMail() {
        $mail_smtp_host = \Chemiekast\Config::get_config(\Chemiekast\Config::SETTING_MAIL_SMTP);
        $mail_port = \Chemiekast\Config::get_config(\Chemiekast\Config::SETTING_MAIL_PORT);
        $mail_username = \Chemiekast\Config::get_config(\Chemiekast\Config::SETTING_MAIL_USERNAME);
        $mail_password = \Chemiekast\Config::get_config(\Chemiekast\Config::SETTING_MAIL_PASSWORD);
        if (empty($mail_smtp_host)
                || empty($mail_port)) {
            return null;
        }
        
        $mail_use_auth = false;
        if (!empty($mail_username) || !empty($mail_password)) {
            $mail_use_auth = true;
        }

        $host = filter_input(INPUT_SERVER, 'SERVER_NAME');
        if ($host === 'localhost') {
            $host = 'example.com';
        }

        // Create a new PHPMailer instance
        $mailer = new PHPMailer();
        // Tell PHPMailer to use SMTP
        $mailer->isSMTP();
        //Enable SMTP debugging
        // 0 = off (for production use)
        // 1 = client messages
        // 2 = client and server messages
        $mailer->SMTPDebug = 0;
        // Set authentication
        $mailer->SMTPAuth = $mail_use_auth;
        if ($mail_port === 465) {
            $mailer->SMTPSecure = 'ssl';
        } else if ($mail_port === 587) {
            $mailer->SMTPSecure = 'tls';
        }
        // Set the hostname of the mail server
        $mailer->Host = $mail_smtp_host;
        // Set the SMTP port number - likely to be 25, 465 or 587
        $mailer->Port = $mail_port;
        // Set the mailer username
        $mailer->Username = $mail_username;
        // Set the mailer password
        $mailer->Password = $mail_password;
        // Set who the message is to be sent from
        $mailer->setFrom('no-reply@' . $host, 'Chemiekast');

        return $mailer;
    }

    /**
     * Creates a PHPMailer object with the given data.
     * @param Mail $mail The mail data.
     * @return PHPMailer.
     */
    private static function createMail($mail) {
        $mailer = self::setupMail();
        if ($mailer === null) {
            return null;
        }

        $mailer->addAddress($mail->recipient_address, $mail->recipient_name);
        $mailer->Subject = $mail->subject;
        $mailer->msgHTML($mail->body);
        $mailer->AltBody = $mail->alt_body;

        return $mailer;
    }

    /**
     * Sends an email.
     * @param Mail $mail The mail data.
     * @return any The mail result.
     */
    public static function sendMail($mail) {
        $mailer = self::createMail($mail);
        if ($mailer === null) {
            return false;
        }

        if ($mailer->send()) {
            return true;
        } else {
            return $mailer->ErrorInfo;
        }
    }

    public static function createBody($header, $paragraphs) {
        $html = '';

        $htmlHeader = '<h1 style="font-family: \'Open Sans\', \'Segoe UI\', \'Helvetica Neue\', Ubuntu, sans-serif; font-weight: lighter; font-size: 2em;">'
                . $header
                . '</h1>';

        $html .= $htmlHeader;

        foreach ($paragraphs as $parText) {
            $htmlParagraph = '<p style="font-family: \'Open Sans\', \'Segoe UI\', \'Helvetica Neue\', Ubuntu, sans-serif">'
                    . $parText
                    . '</p>';
            $html .= $htmlParagraph;
        }

        return $html;
    }

}
