module.exports = function (api) {
    api.cache(true);

    const plugins = [
        '@babel/plugin-syntax-dynamic-import',
    ];

    const presets = [
        [
            '@babel/preset-env',
            {
                targets: {
                    browsers: [
                        'ie >= 11',
                        'safari >= 9'
                    ]
                },
                useBuiltIns: 'usage',
                corejs: 3,
                modules: false,
            },
        ]
    ];

    return {
        presets,
        plugins,
    };
}
