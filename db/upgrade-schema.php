<?php
$relative_root = '../';

require_once $relative_root . 'chemiekast/config.php';

function echo_html($str) {
    echo "<p>$str</p>";
}

$from = filter_input(INPUT_GET, 'from');
$to = filter_input(INPUT_GET, 'to');

if (empty($from) || !preg_match('/^\d+-\d+-\d+$/', $from)) {
    echo_html('Parameter <em>from</em> not provided.');
    exit();
}
if (empty($to) || !preg_match('/^\d+-\d+-\d+$/', $to)) {
    echo_html('Parameter <em>to</em> not provided.');
    exit();
}

echo_html("Upgrading from $from to $to...");

$upgrade_sql_path = $relative_root . "db/$from-to-$to.sql";
if (!file_exists($upgrade_sql_path)) {
    echo_html('Upgrade file does not exist.');
    exit();
}

echo_html('Loading SQL file...');
$sql = file_get_contents($upgrade_sql_path);

echo "<pre>$sql</pre>";

echo_html('Constructing PDO...');

$dbh = Chemiekast\Config::get_PDO();

echo_html('Executing SQL...');

$result = $dbh->exec($sql);

var_dump($dbh->errorInfo());

echo_html('Execution finished.');
