-- Chemiekast 7.5

-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 18, 2021 at 12:32 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chemiekast`
--
CREATE DATABASE IF NOT EXISTS `chemiekast` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `chemiekast`;

-- --------------------------------------------------------

--
-- Table structure for table `chemicals`
--

CREATE TABLE `chemicals` (
  `ID` int(11) NOT NULL,
  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Worksheet` int(11) NOT NULL,
  `GhsSymbols` int(11) DEFAULT NULL,
  `GhsLinks` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `domains`
--

CREATE TABLE `domains` (
  `ID` int(11) NOT NULL,
  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `logactivities`
--

CREATE TABLE `logactivities` (
  `LogID` int(11) NOT NULL,
  `DateTime` datetime NOT NULL,
  `Activity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Data` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `ID` int(11) NOT NULL,
  `Domain` int(11) NOT NULL,
  `UserID` int(11) DEFAULT NULL,
  `Username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Date` datetime NOT NULL,
  `UserRole` int(11) NOT NULL,
  `UserFullName` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notices`
--

CREATE TABLE `notices` (
  `ID` int(11) NOT NULL,
  `Domain` int(11) NOT NULL,
  `Poster` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Date` datetime NOT NULL,
  `Message` text COLLATE utf8_unicode_ci NOT NULL,
  `System` bit(1) NOT NULL DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rolechemical`
--

CREATE TABLE `rolechemical` (
  `Chemical` int(11) NOT NULL,
  `Role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `ID` int(11) NOT NULL,
  `Domain` int(11) NOT NULL,
  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `Setting` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `useraccess`
--

CREATE TABLE `useraccess` (
  `User` int(11) NOT NULL,
  `Worksheet` int(11) NOT NULL,
  `Editing` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `ID` int(11) NOT NULL,
  `Username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Domain` int(11) NOT NULL,
  `Role` int(11) NOT NULL DEFAULT 5,
  `Email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PasswordToken` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `State` tinyint(1) NOT NULL DEFAULT 1,
  `FirstName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `LastName` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `worksheetcolumns`
--

CREATE TABLE `worksheetcolumns` (
  `DatabaseName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Order` int(11) NOT NULL,
  `IsVisible` bit(1) NOT NULL DEFAULT b'1',
  `UIName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `IsSearchable` bit(1) NOT NULL,
  `IsUrl` bit(1) NOT NULL,
  `IsMarkdown` bit(1) NOT NULL,
  `IsFormula` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `worksheets`
--

CREATE TABLE `worksheets` (
  `ID` int(11) NOT NULL,
  `Code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Domain` int(11) NOT NULL,
  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chemicals`
--
ALTER TABLE `chemicals`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `CHEMICALS_WORKSHEET_WORKSHEETS_ID_idx` (`Worksheet`);

--
-- Indexes for table `domains`
--
ALTER TABLE `domains`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `Name_UNIQUE` (`Name`);

--
-- Indexes for table `logactivities`
--
ALTER TABLE `logactivities`
  ADD KEY `logactivities.LogID_logs.ID_FK_idx` (`LogID`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Logs.Domain_Domains.ID_idx` (`Domain`),
  ADD KEY `Logs.UserID_Users.ID_FK_idx` (`UserID`);

--
-- Indexes for table `notices`
--
ALTER TABLE `notices`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `NoticesDomain_DomainsID_idx` (`Domain`);

--
-- Indexes for table `rolechemical`
--
ALTER TABLE `rolechemical`
  ADD PRIMARY KEY (`Chemical`,`Role`),
  ADD KEY `RoleChemical_Role_FK` (`Role`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `RolesDomain_DomainsID_idx` (`Domain`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`Setting`);

--
-- Indexes for table `useraccess`
--
ALTER TABLE `useraccess`
  ADD PRIMARY KEY (`User`,`Worksheet`,`Editing`),
  ADD KEY `Useracces.Worksheet_Worksheets.Code_idx` (`Worksheet`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `Username_UNIQUE` (`Username`);

--
-- Indexes for table `worksheetcolumns`
--
ALTER TABLE `worksheetcolumns`
  ADD PRIMARY KEY (`DatabaseName`),
  ADD UNIQUE KEY `Order_UNIQUE` (`Order`);

--
-- Indexes for table `worksheets`
--
ALTER TABLE `worksheets`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `CodeDomain_UNIQUE` (`Code`,`Domain`),
  ADD KEY `WorksheetsDomain_DomainsID_idx` (`Domain`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chemicals`
--
ALTER TABLE `chemicals`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `domains`
--
ALTER TABLE `domains`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notices`
--
ALTER TABLE `notices`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `worksheets`
--
ALTER TABLE `worksheets`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `chemicals`
--
ALTER TABLE `chemicals`
  ADD CONSTRAINT `ChemicalsWorksheet_WorksheetsID` FOREIGN KEY (`Worksheet`) REFERENCES `worksheets` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `logactivities`
--
ALTER TABLE `logactivities`
  ADD CONSTRAINT `logactivities.LogID_logs.ID_FK` FOREIGN KEY (`LogID`) REFERENCES `logs` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `logs`
--
ALTER TABLE `logs`
  ADD CONSTRAINT `Logs.Domain_Domains.ID` FOREIGN KEY (`Domain`) REFERENCES `domains` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Logs.UserID_Users.ID_FK` FOREIGN KEY (`UserID`) REFERENCES `users` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `notices`
--
ALTER TABLE `notices`
  ADD CONSTRAINT `NoticesDomain_DomainsID` FOREIGN KEY (`Domain`) REFERENCES `domains` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rolechemical`
--
ALTER TABLE `rolechemical`
  ADD CONSTRAINT `RoleChemical_Chemical_FK` FOREIGN KEY (`Chemical`) REFERENCES `chemicals` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `RoleChemical_Role_FK` FOREIGN KEY (`Role`) REFERENCES `roles` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `roles`
--
ALTER TABLE `roles`
  ADD CONSTRAINT `RolesDomain_DomainsID` FOREIGN KEY (`Domain`) REFERENCES `domains` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `useraccess`
--
ALTER TABLE `useraccess`
  ADD CONSTRAINT `Useracces.Worksheet_Worksheets.Code` FOREIGN KEY (`Worksheet`) REFERENCES `worksheets` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Useraccess.User_Users.ID_FK` FOREIGN KEY (`User`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `worksheets`
--
ALTER TABLE `worksheets`
  ADD CONSTRAINT `WorksheetsDomain_DomainsID` FOREIGN KEY (`Domain`) REFERENCES `domains` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
