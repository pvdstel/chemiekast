/**
 * Chemiekast Webpack configuration
 * 
 * Run with parameter --CHEMIEKAST_HOST=HOST to set the base request host.
 */

// @ts-check

const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const appName = 'Chemiekast';
const appVersion = require('./package.json').version;
const buildDate = new Date().toISOString();

console.log(`Building ${appName} version ${appVersion}...`);

module.exports = (env, argv) => {
    const prod = argv['mode'] !== 'development';
    const chemiekastHost = env['CHEMIEKAST_HOST'] || '';

    if (prod) {
        console.log('This is a production build.');
    }
    if (chemiekastHost.length > 0) {
        console.log(`Building frontend for host ${chemiekastHost}.`);
    }

    const cssLoaderConfig = {
        sourceMap: !prod,
        esModule: true,
        modules: {
            mode: 'local',
            localIdentName: prod ? '[hash:base64:10]' : '[name]--[local]--[hash:base64:5]',
            exportLocalsConvention: 'camelCaseOnly',
        },
    };

    /** @type {HtmlWebpackPlugin.Options} */
    const htmlWebpackPluginConfig = {
        template: 'src/frontend/index.ejs',
        templateParameters: {
            appName,
            appVersion,
            buildDate,
            chemiekastHost,
            prod,
        },
    };
    const faviconsWebpackPluginConfig = {
        logo: path.resolve(__dirname, 'src/frontend/resources/images/logo-bg.svg'),
        favicons: {
            appName,
            appShortName: appName,
            developerName: 'pvdstel',
            developerUrl: 'https://gitlab.com/pvdstel/chemiekast',
        },
    }

    return {
        entry: './src/frontend/chimico.ts',
        output: {
            path: path.resolve(__dirname, 'dist'),
            filename: prod ? 'assets/chimico.[chunkhash].js' : 'assets/chimico.js',
            chunkFilename: prod ? 'assets/chimico.[name].[chunkhash].js' : 'assets/chimico.[name].js',
            publicPath: '/',
        },

        devtool: prod ? undefined : 'source-map',
        devServer: {
            historyApiFallback: true,
            port: 8005,
        },
        performance: prod ? {
            maxEntrypointSize: 10E6, // 1 MB
        } : false,

        resolve: {
            extensions: [
                '.ts',
                '.tsx',
                '.js',
                '.jsx',
            ],
            alias: {
                "@": path.resolve(__dirname, 'src/frontend'),
            },
        },

        module: {
            rules: [
                {
                    test: /\.tsx?/,
                    use: [
                        {
                            loader: 'babel-loader',
                            options: {
                                cacheDirectory: true,
                            },
                        },
                        'ts-loader',
                    ],
                },
                {
                    test: /\.css$/,
                    use: [
                        'style-loader',
                        {
                            loader: 'css-loader',
                            options: cssLoaderConfig,
                        },
                    ],
                },
                {
                    test: /\.(scss|sass)$/,
                    use: [
                        'style-loader',
                        {
                            loader: 'css-loader',
                            options: cssLoaderConfig,
                        },
                        'sass-loader',
                    ],
                },
                {
                    test: /\.svg$|\.png$|\.jpe?g$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                name: 'assets/images/[name]-[hash:base64:5].[ext]',
                            },
                        },
                    ],
                },
                {
                    test: /\.eot$|\.woff2?$|\.ttf$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                name: 'assets/fonts/[name]-[hash:base64:5].[ext]',
                            },
                        },
                    ],
                },
            ],
        },

        plugins: [
            new FaviconsWebpackPlugin(faviconsWebpackPluginConfig),
            new HtmlWebpackPlugin(htmlWebpackPluginConfig),
        ],
    };
}
